## Wie veröffentliche ich eine weitere Sprachversion des POIs?

Weitere Sprachversionen kannst du im Tab "Einstellungen" hinzufügen und im Tab "Inhalt" bearbeiten sowie veröffentlichen. Die Sprache wird aktiviert, wenn du ihren Status im Tab "Inhalt" auf "Veröffentlicht" stellst. 