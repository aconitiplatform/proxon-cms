## Warum wird meine erstellte Notification nicht richtig agezeigt?
Das kann verschiedene Ursachen haben. Das Problem lässt sich folgendermaßen beheben:
- Entferne den verknüpften Beacon im betroffenen POI
- Speichere den POI und gehe zurück in die POI Übersicht
- Gehe wieder in den POI und verknüpfe den Beacon erneut
- Speichere den POI

