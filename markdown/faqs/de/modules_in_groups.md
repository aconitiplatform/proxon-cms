## Warum sollte man Module in Gruppen anordnen?

Gruppen können unterschiedlich bearbeitet werden, z.B. kannst du ihnen verschiedene Styles geben oder einzelne Gruppen nur zu einer bestimmten Uhrzeit sichtbar machen. Dies wirkt sich dann auf alle Module dieser Gruppe aus.
