## Warum wird mein POI auf Geräten nicht angezeigt?

Ein POI ist nicht sichtbar, wenn er noch nicht veröffentlicht wurde oder die Sprache des POI noch nicht aktiv ist. 

Den POI kannst du auf zwei Arten veröffentlichen. Klicke dafür im Menüpunkt "POIs" auf den POI, den du veröffentlichen willst. 
 - Ändere den Status im Tab "Einstellungen" von "Entwurf" auf "Veröffentlicht"
 - Oder klicke auf den gelben Kreis oben mittig neben dem Button "POI speichern"  

Die Sprache des POI wird aktiviert, wenn du ihren Status im Tab "Inhalt" auf "Veröffentlicht" stellst.
