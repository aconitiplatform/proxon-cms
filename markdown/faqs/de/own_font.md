## Wie kann ich eine eigene Schriftart benutzen?

Substance besitzt eine Integration mit Google Webfonts. Unter Admin - Design können Schriftarten ausgewählt werden. Diese werden für jeden POI geladen. Die Schriftart, die als Standard festgelegt wurde, wird bei jedem POI benutzt. Andere Schriftarten können in den jeweiligen Designeinstellungen (z.B. Kopf- oder Fußzeile) für einzelne Elemente gesetzt werden.
