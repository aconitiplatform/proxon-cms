## Was sind die Notifications im POI Editor?

Die Notifications sind Informationen, die dem Nutzer in den Benachrichtigungen bzw. seiner Mitteilungszentrale angezeigt werden, sobald der POI veröffentlich wurde. Sie dienen als Anreiz, den angezeigten Link zu öffnen.
