## Wie veröffentliche ich einen POI?

Den POI kannst du auf zwei Arten veröffentlichen. Klicke dafür im Menüpunkt "POIs" auf den POI, den du veröffentlichen willst. 

 - Ändere den Status im Tab "Einstellungen" 
 - Oder klicke auf den gelben Kreis oben mittig neben dem Button "POI speichern" 
 
Zusätzlich lassen sich einzelne Sprachen veröffentlichen bzw. auf Entwurf schalten. Dies dient dazu, Sprachen einzeln erstellen und bearbeiten zu können, ohne die schon bestehenden Sprachen zu beeinflussen. 

Die Sprache des POI wird aktiviert, wenn du ihren Status im Tab "Inhalt" auf "Veröffentlicht" stellst.

Der Veröffentlichungszustand eines POI hat Vorrang über den einer einzelnen Sprache. 
