## Welches Datum wird in der POI Übersicht angezeigt?

Das Datum der letzten Aktualisierung, also der Tag, an dem du den POI zuletzt verändert hast. Die erweiterte Ansicht, die du über den Button oben rechts aktivieren kannst, zeigt dir zusätzlich das Datum an, an dem der POI erstellt wurde.