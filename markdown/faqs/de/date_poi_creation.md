## Wie kann ich sehen, wann ein POI erstellt wurde?

Die POI-Übersicht zeigt dir das Erstellungsdatum an, wenn du oben rechts die erweiterte Ansicht aktivierst.