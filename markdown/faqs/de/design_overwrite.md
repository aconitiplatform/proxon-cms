## Was bedeutet "Standard" im Design?

Du kannst individuelle Design Einstellungen für diesen POI vornehmen. Falls du nichts verändern möchtest, greifen die vom Administrator festgelegten Standard Einstellungen.