## Wie konfiguriere ich einen Beacon?

Dir ist eventuell aufgefallen, dass die physclwb.io Domain zu lange ist, um auf einen Eddystone Beacon konfiguriert zu werden. Dafür stellen wir die Domain edst.one bereit.

Beispiel: Wenn deine Touchpoint URL https://physclwb.io/ughewn8f ist, ist deine Beacon URL https://edst.one/ughewn8f.
