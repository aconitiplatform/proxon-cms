## Why is the notification I created not displayed properly?

There are different reasons why notifications are not displayed properly. But you can easily fix the problem:
- Remove the connected beacon from your POI
- Save POI und return to POI overview
- Go back to your POI and add the beacon once again
- Save POI

