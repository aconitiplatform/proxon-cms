## How can I use an own font?

Google Webfonts is integrated in Substance. In the Admin - Design section, fonts can be selected. These fonts are loaded on every POI. The default font is used for every POI. Other fonts can be selected in the design settings (e.g. header, footer) for most elements.
