## Why should modules be arranged in groups?

Groups can be styled individually, e.g. concerning colors or temporal management. The changes apply equally for all modules in the same group.