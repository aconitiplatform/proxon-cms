## What does "default" mean in design?

You can change the design settings of your POI individually. If you don’t want to do any changes, the default design set by the administrator will apply.