## Which date is shown in the POI overview?

The POI overview displays the date of the latest update, thus the day of your last POI changes.
The extended view which you can choose in the upper right-hand corner additionally shows the creation date of the POI.