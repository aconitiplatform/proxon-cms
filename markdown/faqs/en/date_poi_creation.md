## Where can I find the creation date of my POI?

The creation date is displayed in the POI overview if 
you choose the extended view in the upper right-hand corner.