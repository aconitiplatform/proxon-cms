## How do I configure a beacon?

You might have recognized that the physclwb.io domain is too long to be configured on an eddystone beacon. To use Proxon Connect with an eddystone beacon, please configure it to edst.one.

Example: If your touchpoint URL is https://physclwb.io/ughewn8f, your beacon URL is https://edst.one/ughewn8f.
