## How do I publish a POI?

There are two ways of publishing a POI. Therefore, you need to click on the POI you want to publish in the POI overview.
 - Change the POI state from "draft" to "public" in the settings tab of the POI menu
 - Or klick on the yellow circle next to the button "Update POI" at the top
 
Additionally, it is possible to publish a single language, or set it to draft, respectively. This is necessary to create and edit new languages without influencing existing ones. 

Activate the POI language by changing its state from "draft" to "public" in the content tab.
 
The general POI publishing state has the priority over a language's publishing state.
