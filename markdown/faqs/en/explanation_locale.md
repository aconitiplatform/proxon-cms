## What is the locale in my profile settings?

The locale has an impact on the date format of your
Proxon Connect interface, e.g. the date format in the POI overview.
