## What is the conversion rate in the analytics section?

The conversion rate displays the amount of people who 
both received and opened the beacon URL on their phones.