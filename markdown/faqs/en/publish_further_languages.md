## How can I publish further language versions of my POI?

You can add further language versions in the "settings" tab of the POI menu and both edit and publish them in the "content" tab. Activate the POI language by changing its state from "draft" to "public".