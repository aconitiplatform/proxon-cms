## Why is my POI not displayed on phones?

A POI remains invisible for users if it has not been published yet or if a POI language 
is still inactive.

There are two ways of publishing a POI. Therefore, you need to click on the POI you want to publish in the POI overview.
 - Change the POI state from "draft" to "published" in the settings tab of the POI menu
 - Or klick on the yellow circle next to the button "Save POI" at the top

Activate the POI language by changing its state from "draft" to "published" in the content tab.