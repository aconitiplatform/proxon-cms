## What are notifications in the POI editor?

Notifications are information displayed on the users' phones when they scan for Touchpoint pages as soon as a POI is published. They are meant to attract the users' attention and to encourage them to open the URL.

