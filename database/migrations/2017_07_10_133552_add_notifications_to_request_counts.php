<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsToRequestCounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_counts', function (Blueprint $table) {
            $table->boolean('notified_approach')->default(false);
            $table->boolean('notified_limit')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_counts', function (Blueprint $table) {
            $table->dropColumn('notified_approach');
            $table->dropColumn('notified_limit');
        });
    }
}
