<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePkOfNlcToUuid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newsletter_configuration_user', function (Blueprint $table) {
            $table->dropForeign('newsletter_configuration_user_nlc_id_foreign');
        });

        Schema::table('newsletter_configurations', function (Blueprint $table) {
            $table->string('id',36)->change();
        });

        Schema::table('newsletter_configuration_user', function (Blueprint $table) {
            $table->string('nlc_id',36)->change();
            $table->foreign('nlc_id')->references('id')->on('newsletter_configurations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter_configurations', function (Blueprint $table) {
            //
        });
    }
}
