<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicroPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('micro_payments', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->integer('amount')->default(0);
            $table->string('type')->default('single');
            $table->string('currency')->default('EUR');
            $table->longText('allowed_payment_types');
            $table->longText('fields');
            $table->boolean('emails_payee')->default(false);
            $table->boolean('emails_payer')->default(false);
            $table->string('email');
            $table->text('callback_url');
            $table->text('heartbeat_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('micro_payments');
    }
}
