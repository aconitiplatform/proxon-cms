<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDesignStringsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->string('header_bg_color')->nullable()->change();
            $table->string('header_font_color')->nullable()->change();
            $table->string('header_format')->nullable()->change();
            $table->string('header_height')->nullable()->change();
            $table->text('h1')->nullable()->change();
            $table->text('h2')->nullable()->change();
            $table->text('h3')->nullable()->change();
            $table->text('font')->nullable()->change();
            $table->string('footer_bg_color')->nullable()->change();
            $table->string('footer_font_color')->nullable()->change();
            $table->string('footer_height')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->string('header_bg_color')->change();
            $table->string('header_font_color')->change();
            $table->string('header_format')->change();
            $table->string('header_height')->change();
            $table->text('h1')->change();
            $table->text('h2')->change();
            $table->text('h3')->change();
            $table->text('font')->change();
            $table->string('footer_bg_color')->change();
            $table->string('footer_font_color')->change();
            $table->string('footer_height')->change();
        });
    }
}
