<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionsTable extends Migration
{

    public function up()
    {
        Schema::create('survey_questions', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('survey_id',36);
            $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
            $table->string('language',5);
            $table->text('question');
            $table->string('type')->default('single_choice');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('survey_questions');
    }
}
