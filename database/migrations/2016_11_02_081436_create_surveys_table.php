<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration {

    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('name',255);
            $table->string('fallback_language',5);
            $table->string('status')->default('draft');
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->string('user_id',36);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('surveys');
    }
}
