<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInvoiceIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign('invoices_billable_id_foreign');
            $table->dropColumn('billable_id');
            $table->string('billable_object_type')->default('tenant');
            $table->string('billable_object_id',36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('billable_id',36);
            $table->foreign('billable_id')->references('id')->on('billables')->onDelete('cascade');
            $table->dropColumn('billable_object_type');
            $table->dropColumn('billable_object_id');
        });
    }
}
