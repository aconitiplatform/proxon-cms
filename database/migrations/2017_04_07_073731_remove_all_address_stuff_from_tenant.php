<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAllAddressStuffFromTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->dropColumn('address_street');
            $table->dropColumn('address_city');
            $table->dropColumn('address_phone');
            $table->dropColumn('address_misc');
            $table->dropColumn('address_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->text('address_street')->nullable();
            $table->text('address_city')->nullable();
            $table->text('address_phone')->nullable();
            $table->text('address_misc')->nullable();
            $table->text('address_country')->nullable();
        });
    }
}
