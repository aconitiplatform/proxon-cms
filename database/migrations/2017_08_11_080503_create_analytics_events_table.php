<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics_events', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->string('poi_id',36);
            $table->foreign('poi_id')->references('id')->on('pois')->onDelete('cascade');
            $table->string('journey_id',36)->nullable();
            $table->string('accessed',32); //poi, channel
            $table->text('url');
            $table->string('user_agent')->nullable();
            $table->string('from',32)->nullable(); //beacon, web
            $table->string('event',64)->nullable(); //beacon_via_scanner, beacon_via_browser, poi_via_beacon, poi_via_browser, channel_via_beacon, channel_via_browser
            $table->string('os')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('analytics_events');
    }
}
