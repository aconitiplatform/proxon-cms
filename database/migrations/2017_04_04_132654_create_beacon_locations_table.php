<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaconLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beacon_locations', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('beacon_id',36);
            $table->foreign('beacon_id')->references('id')->on('beacons')->onDelete('cascade');
            $table->text('note')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->boolean('indoor')->default(false);
            $table->integer('level')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beacon_locations');
    }
}
