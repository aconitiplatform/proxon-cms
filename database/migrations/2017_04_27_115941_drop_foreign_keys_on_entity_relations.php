<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignKeysOnEntityRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newsletter_configurations', function (Blueprint $table) {
            $table->dropForeign('newsletter_configurations_user_id_foreign');
        });

        Schema::table('media', function (Blueprint $table) {
            $table->dropForeign('contents_user_id_foreign');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletter_configurations', function (Blueprint $table) {
            //
        });
    }
}
