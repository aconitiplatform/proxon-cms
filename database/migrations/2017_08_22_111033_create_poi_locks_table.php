<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiLocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi_locks', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('poi_id',36);
            $table->foreign('poi_id')->references('id')->on('pois')->onDelete('cascade');
            $table->string('user_id',36);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poi_locks');
    }
}
