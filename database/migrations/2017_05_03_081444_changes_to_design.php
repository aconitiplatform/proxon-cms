<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesToDesign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->text('header_logo_settings')->nullable();
            $table->text('footer_logo_settings')->nullable();
            $table->text('footer_format')->nullable();
            $table->string('footer_logo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->dropColumn('header_logo_settings');
            $table->dropColumn('footer_logo_settings');
            $table->dropColumn('footer_format');
            $table->dropColumn('footer_logo');
        });
    }
}
