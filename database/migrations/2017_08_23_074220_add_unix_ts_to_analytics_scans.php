<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnixTsToAnalyticsScans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytics_scans', function (Blueprint $table) {
            $table->bigInteger('fired_at');
            $table->index('journey_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytics_scans', function (Blueprint $table) {
            $table->dropColumn('fired_at');
            $table->dropIndex('analytics_scans_journey_id_index');
        });
    }
}
