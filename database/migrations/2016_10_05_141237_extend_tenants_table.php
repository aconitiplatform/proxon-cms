<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->text('url')->nullable();
            $table->text('css')->nullable();
            $table->text('logo')->nullable();
            $table->text('imprint')->nullable();
            $table->text('email')->nullable();
            $table->integer('allowed_pois')->default(0);
            $table->integer('used_pois')->default(0);
            $table->string('fallback_language',5)->default('de');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenants', function (Blueprint $table) {
            //
        });
    }
}
