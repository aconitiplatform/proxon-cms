<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewedChannelsChartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viewed_channels_charts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prefix');
            $table->string('user')->default('admin');
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->longText('meta');
            $table->longText('labels');
            $table->longText('series');
            $table->string('start');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('viewed_channels_charts');
    }
}
