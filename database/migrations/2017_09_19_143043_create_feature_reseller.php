<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureReseller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_reseller', function (Blueprint $table) {
            $table->string('feature_id',36);
            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
            $table->string('reseller_id',36);
            $table->foreign('reseller_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feature_reseller');
    }
}
