<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInteractionFlagsToPois extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pois', function (Blueprint $table) {
            $table->boolean('has_micro_payment')->default(false);
            $table->boolean('has_feedback')->default(false);
            $table->boolean('has_sweepstake')->default(false);
            $table->boolean('has_contact')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pois', function (Blueprint $table) {
            $table->dropColumn('has_micro_payment');
            $table->dropColumn('has_feedback');
            $table->dropColumn('has_sweepstake');
            $table->dropColumn('has_contact');
        });
    }
}
