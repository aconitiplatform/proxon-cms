<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameContentIdInMediaPoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_poi', function (Blueprint $table) {
            $table->renameColumn('content_id','media_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_poi', function (Blueprint $table) {
            $table->renameColumn('media_id','content_id');
        });
    }
}
