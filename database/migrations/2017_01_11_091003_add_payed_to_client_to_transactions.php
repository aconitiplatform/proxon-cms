<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayedToClientToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('micro_payment_transactions', function (Blueprint $table) {
            $table->boolean('received_by_client')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('micro_payment_transactions', function (Blueprint $table) {
            $table->dropColumn('received_by_client');
        });
    }
}
