<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_billing_infos', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->text('person')->nullable();
            $table->text('department')->nullable();
            $table->text('company')->nullable();
            $table->text('street')->nullable();
            $table->text('zip')->nullable();
            $table->text('city')->nullable();
            $table->text('country')->nullable();
            $table->text('vat')->nullable();
            $table->integer('price_per_beacon')->default(500);
            $table->integer('discount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tenant_billing_infos');
    }
}
