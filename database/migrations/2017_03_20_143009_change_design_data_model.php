<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDesignDataModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->dropColumn('header_visible');
            $table->dropColumn('header_bg_color');
            $table->dropColumn('header_font_color');
            $table->dropColumn('header_height');
            $table->dropColumn('footer_visible');
            $table->dropColumn('footer_bg_color');
            $table->dropColumn('footer_font_color');
            $table->dropColumn('footer_height');
            $table->longText('header')->nullable();
            $table->longText('footer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designs', function (Blueprint $table) {
            //
        });
    }
}
