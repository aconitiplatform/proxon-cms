<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNlcUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('newsletter_configurations', function (Blueprint $table) {
            $table->integer('id')->unsigned()->change();
        });

        Schema::create('newsletter_configuration_user', function (Blueprint $table) {
            $table->integer('nlc_id')->unsigned();
            $table->foreign('nlc_id')->references('id')->on('newsletter_configurations')->onDelete('cascade');
            $table->string('user_id',36);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('newsletter_configuration_user');
    }
}
