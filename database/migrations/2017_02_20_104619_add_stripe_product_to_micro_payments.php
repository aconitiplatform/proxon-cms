<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStripeProductToMicroPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('micro_payments', function (Blueprint $table) {
            $table->string('stripe_product_id',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('micro_payments', function (Blueprint $table) {
            $table->dropColumn('stripe_product_id');
        });
    }
}
