<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('billable_id',36);
            $table->foreign('billable_id')->references('id')->on('billables')->onDelete('cascade');
            $table->string('easybill_invoice_id',64)->nullable();
            $table->string('easybill_customer_id',64)->nullable();
            $table->string('currency')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->default('INVOICE');
            $table->string('use_shipping_address')->default(false);
            $table->longText('items')->nullable();
            $table->integer('times_generated')->default(0);
            $table->integer('month');
            $table->integer('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
