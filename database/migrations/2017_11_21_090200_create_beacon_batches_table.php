<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaconBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beacon_batches', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('kontakt_io_venue_id',36)->nullable();
            $table->string('kontakt_io_venue_name')->nullable();
            $table->boolean('synced')->default(false);
            $table->integer('number_of_beacons')->default(0);
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beacon_batches');
    }
}
