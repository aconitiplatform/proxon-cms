<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designs', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->boolean('header_visible')->default(true);
            $table->boolean('header_fixed')->default(false);
            $table->string('header_bg_color');
            $table->string('header_font_color');
            $table->boolean('header_logo')->default(true);
            $table->boolean('header_title')->default(true);
            $table->string('header_format');
            $table->string('header_height');
            $table->text('h1');
            $table->text('h2');
            $table->text('h3');
            $table->text('font');
            $table->boolean('footer_visible')->default(true);
            $table->string('footer_bg_color');
            $table->string('footer_font_color');
            $table->string('footer_height');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('designs');
    }
}
