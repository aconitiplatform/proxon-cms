<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegalInformationToPois extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('pois', function (Blueprint $table) {
            $table->boolean('overwrite_imprint')->default(false);
            $table->boolean('overwrite_privacy')->default(false);
            $table->boolean('overwrite_copyright')->default(false);
			$table->text('imprint')->nullable();
			$table->text('privacy')->nullable();
			$table->text('copyright')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pois', function (Blueprint $table) {
            $table->dropColumn('overwrite_imprint');
            $table->dropColumn('overwrite_privacy');
            $table->dropColumn('overwrite_copyright');
			$table->dropColumn('imprint');
			$table->dropColumn('privacy');
			$table->dropColumn('copyright');
        });
    }
}
