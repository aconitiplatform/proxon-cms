<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellerSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_settings', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('kontakt_io_api_key')->nullable();
            $table->text('substance_url')->nullable();
            $table->text('edward_stone_url')->nullable();
            $table->text('sidebar_product_logo')->nullable();
            $table->text('login_screen_company_logo')->nullable();
            $table->text('login_screen_product_logo')->nullable();
            $table->string('product_name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('easybill_customer_id');
            $table->string('reseller_id',36);
            $table->foreign('reseller_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reseller_settings');
    }
}
