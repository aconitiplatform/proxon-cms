<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutoFlagToRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_user', function (Blueprint $table) {
            $table->boolean('added_automatically')->default(true);
        });

        Schema::table('micro_payment_user', function (Blueprint $table) {
            $table->boolean('added_automatically')->default(true);
        });

        Schema::table('newsletter_configuration_user', function (Blueprint $table) {
            $table->boolean('added_automatically')->default(true);
        });

        Schema::table('survey_user', function (Blueprint $table) {
            $table->boolean('added_automatically')->default(true);
        });

        Schema::table('poi_user', function (Blueprint $table) {
            $table->boolean('added_automatically')->default(true);
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_user', function (Blueprint $table) {
            $table->dropColumn('added_automatically');
        });

        Schema::table('micro_payment_user', function (Blueprint $table) {
            $table->dropColumn('added_automatically');
        });

        Schema::table('newsletter_configuration_user', function (Blueprint $table) {
            $table->dropColumn('added_automatically');
        });

        Schema::table('survey_user', function (Blueprint $table) {
            $table->dropColumn('added_automatically');
        });

        Schema::table('poi_user', function (Blueprint $table) {
            $table->dropColumn('added_automatically');
        });
    }
}
