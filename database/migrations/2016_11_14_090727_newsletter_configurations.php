<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsletterConfigurations extends Migration {

    public function up() {
        Schema::create('newsletter_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->string('type');
            $table->longText('configuration');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('newsletter_configurations');
    }
}
