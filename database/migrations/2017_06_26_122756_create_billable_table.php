<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billables', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('billable_object_type')->default('tenant');
            $table->string('billable_object_id',36);
            $table->string('easybill_customer_id',64);
            $table->integer('price_per_beacon');
            $table->integer('discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billables');
    }
}
