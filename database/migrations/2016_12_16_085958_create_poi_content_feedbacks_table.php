<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiContentFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi_content_feedbacks', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->integer('positive')->default(0);
            $table->integer('negative')->default(0);
            $table->string('poi_id',36);
            $table->foreign('poi_id')->references('id')->on('pois')->onDelete('cascade');
            $table->string('poi_content_id',36);
            $table->foreign('poi_content_id')->references('id')->on('poi_contents')->onDelete('cascade');
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poi_content_feedbacks');
    }
}
