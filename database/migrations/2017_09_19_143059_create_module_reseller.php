<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleReseller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_reseller', function (Blueprint $table) {
            $table->string('module_id',36);
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
            $table->string('reseller_id',36);
            $table->foreign('reseller_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_reseller');
    }
}
