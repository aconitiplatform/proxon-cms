<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultForTenantStrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->longText('imprint')->default('')->change();
            $table->text('email')->default('')->change();
            $table->text('url')->default('')->change();
            $table->text('css')->default('')->change();
            $table->text('logo')->default('')->change();
            $table->text('ga_tracking_id')->default('')->change();
            $table->text('ga_utm_source')->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenants', function (Blueprint $table) {
            //
        });
    }
}
