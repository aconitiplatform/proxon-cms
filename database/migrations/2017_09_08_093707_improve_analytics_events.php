<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImproveAnalyticsEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytics_events', function (Blueprint $table) {
            $table->timestamp('fired_at_timestamp')->nullable();
            $table->index('tenant_id', 'tenant_id_analytics_events_index');
            $table->index('poi_id', 'poi_id_analytics_events_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytics_events', function (Blueprint $table) {
            $table->dropColumn('fired_at_timestamp');
            $table->dropIndex('tenant_id_analytics_events_index');
            $table->dropIndex('poi_id_analytics_events_index');
        });
    }
}
