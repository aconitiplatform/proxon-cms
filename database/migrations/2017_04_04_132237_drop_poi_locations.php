<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPoiLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('poi_locations');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('poi_locations', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('poi_id',36);
            $table->foreign('poi_id')->references('id')->on('pois')->onDelete('cascade');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->boolean('indoor')->default(false);
            $table->integer('level')->nullable();
            $table->timestamps();
        });
    }
}
