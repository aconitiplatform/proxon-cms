<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->longText('items')->nullable();
            $table->string('stripe_token_id');
            $table->string('address_name');
            $table->string('address_optional')->nullable();
            $table->string('address_street');
            $table->string('address_zip');
            $table->string('address_city');
            $table->string('address_country');
            $table->boolean('shipped')->default(false);
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carts');
    }
}
