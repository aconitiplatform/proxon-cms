<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentPoiLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_poi', function (Blueprint $table) {
            $table->string('content_id',36);
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
            $table->string('poi_id',36);
            $table->foreign('poi_id')->references('id')->on('pois')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_poi');
    }
}
