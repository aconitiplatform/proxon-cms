<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDesignDataModelToStrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->string('header_fixed',32)->default('fixed')->change();
            $table->string('header_visible',32)->default('visible')->change();
            $table->string('header_logo',32)->default('visible')->change();
            $table->string('header_title',32)->default('visible')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('designs', function (Blueprint $table) {
            $table->boolean('header_fixed')->default(true)->change();
            $table->boolean('header_visible')->default(true)->change();
            $table->boolean('header_logo')->default(true)->change();
            $table->boolean('header_title')->default(true)->change();
        });
    }
}
