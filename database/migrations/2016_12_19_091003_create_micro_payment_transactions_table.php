<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicroPaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('micro_payment_transactions', function (Blueprint $table) {
            $table->string('id',36)->primary();
            $table->string('tenant_id',36);
            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
            $table->string('micro_payment_id',36);
            $table->foreign('micro_payment_id')->references('id')->on('micro_payments')->onDelete('cascade');
            $table->integer('amount')->default(0);
            $table->string('currency')->default('EUR');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('micro_payment_transactions');
    }
}
