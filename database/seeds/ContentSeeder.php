<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Tenant;
use App\Models\Beacon;
use App\Http\Controllers\DesignController;
use App\Http\Services\BeaconService;
use App\Http\Services\DesignService;
use App\Http\Services\NewsletterConfigurationService;
use App\Models\NewsletterConfiguration;
use App\Models\MicroPayment;
use App\Models\Poi;
use App\Models\PoiContent;
use App\Models\BeaconLocation;
use App\Http\Services\PoiService;
use App\Jobs\DeliverTenantToDistributionJob;
use App\Jobs\SetPoiInteractionFlagsJob;
use App\Traits\AffectsEdwardStone;
use App\Http\Services\PoiContentModules\GroupService;
use App\Http\Services\MicroPaymentService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Traits\WorksWithQueue;

class ContentSeeder extends Seeder
{

    use AffectsEdwardStone, DispatchesJobs, WorksWithQueue;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker\Factory::create();

        $tenant = new Tenant();
        $cn = $faker->company;
        $tenant->name = $cn;
        $tenant->fallback_language = 'de';
        $tenant->locale = 'de_DE';
        $tenant->allowed_beacons = rand(10,50);
        $tenant->slug = str_slug($cn);
        $tenant->logo = $faker->imageUrl(200, 200, 'cats', true, $cn);
        $tenant->url = $faker->url;
        $tenant->imprint = $faker->text(200);
        $tenant->privacy = $faker->text(200);
        $tenant->email = $faker->safeEmail;
        $tenant->logo_settings = [
            'type' => 'url',
            'url' => $faker->imageUrl(200, 200, 'cats', true, $cn),
        ];
        $tenant->copyright_label = $faker->word;
        $tenant->save();

        echo "\n\nSeeding ".$tenant->name;

        $designController = new DesignController();
        $design = $designController->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',$tenant->id);

        $tenant->tenant_design_id = $design->id;
        $tenant->save();

        $amountOfUsers = rand(5,20);

        echo "\nSeeding ".$amountOfUsers." users";

        for($i=0;$i<$amountOfUsers;$i++) {

            $user = new User();
            $user->firstname = $faker->firstName;
            $user->lastname = $faker->lastName.' ('.$tenant->name.')';
            $user->email = time().$faker->safeEmail;
            $user->password = bcrypt('substanceFake!');
            $user->language = $faker->languageCode;
            $user->locale = $faker->locale;
            $user->role = $this->substanceRole($i);
            $user->tenant_id = $tenant->id;
            $user->active = true;
            $user->save();

            if($i==0) {
                $admin = $user;
            }

            echo "\nSeeding ".$user->firstname." ".$user->lastname;

        }

        $this->dispatch(new DeliverTenantToDistributionJob($tenant->id));

        for($i=0;$i<$tenant->allowed_beacons;$i++) {

            $beaconInfos[] = $this->beacon($tenant,$i);

        }

        $end = rand(2,20);
        echo "\nSeeding ".$end." POIs";

        for($i=0;$i<$end;$i++) {

            $this->poi($tenant);

        }

        $end = rand(1,5);
        echo "\nSeeding ".$end." Newsletter Configurations";

        for($i=0;$i<$end;$i++) {

            $this->nlc($tenant);

        }

        $end = rand(1,5);
        echo "\nSeeding ".$end." Micro Payments";

        for($i=0;$i<$end;$i++) {

            $this->mp($tenant);

        }

        $this->poiContents($tenant);

    }

    private function substanceRole($i) {
        $role = ($i==0) ? 1 : rand(1,3);
        switch ($role) {
            case 1:
                return 'admin';
                break;
            case 2:
                return 'user';
                break;
            default:
                return 'subuser';
                break;
        }
    }

    private function substanceLanguage($i) {
        switch ($i) {
            case 0:
                return 'de';
                break;
            case 1:
                return 'en';
                break;
            case 2:
                return 'es';
                break;
            case 3:
                return 'fr';
                break;
        }
    }

    private function nlc($tenant) {

        echo "\nSeeding Newsletter Configuration";

        $faker = Faker\Factory::create();
        $user = User::inRandomOrder()->where('tenant_id',$tenant->id)->first();

        $nlc = NewsletterConfiguration::create([
            'name'=>$faker->word.' ('.$tenant->name.')',
            'tenant_id'=>$tenant->id,
            'type'=>'mailchimp',
            'user_id'=>$user->id,
            'configuration'=> [
                'apikey'=>'04d7618eedec894ddfd1bc4ab5804d27-us14',
                'list'=>'6f2bc7d397'
            ]
        ]);

        NewsletterConfigurationService::addUsersToNewsletterConfiguration($nlc->id,$tenant->id,$user->id);

    }

    private function mp($tenant) {

        echo "\nSeeding Micro-Payment";

        $faker = Faker\Factory::create();
        $user = User::inRandomOrder()->where('tenant_id',$tenant->id)->first();

        $microPaymentData = [
            'name' => $faker->word.' ('.$tenant->name.')',
            'description' => $faker->realText($faker->numberBetween(100,1000)),
            'amount' => $faker->numberBetween(100,10000),
            'type' => 'single',
            'currency' => 'EUR',
            'allowed_payment_types' => [
                'credit_card'
            ],
            'emails_payee' => true,
            'emails_payer' => true,
            'email' => $faker->safeEmail,
            'tenant_id' => $tenant->id,
            'callback_url' => config('substance.edward_stone_url').config('substance.payment.standard_callback'),
            'heartbeat_url' =>config('substance.edward_stone_url').config('substance.payment.standard_heartbeat')
        ];

        $microPayment = MicroPayment::create($microPaymentData);

        $this->publishMicroPaymentToEdwardStone($microPayment->id,true);

        MicroPaymentService::addUsersToMicroPayment($microPayment->id,$tenant->id,$user->id);

    }

    private function beacon($tenant,$createdBeacons) {

        echo "\nSeeding Beacon";

        $faker = Faker\Factory::create();

        $serial = str_pad($createdBeacons+1, strlen($tenant->allowed_beacons), "0", STR_PAD_LEFT);

        $beacon = new Beacon();
        $string = BeaconService::generateRandomString(config('substance.url_length'));
        $beacon->name = "r7-".$tenant->slug."-".$serial." ".config('substance.edward_stone_url')."/".$string;
        $beacon->url = $string;
        $beacon->url_hash = md5($string);
        // $beacon->description = $faker->text(800);
        $beacon->tenant_id = $tenant->id;
        rand(1,2) == 1 ? $beacon->alias = $faker->word : $beacon->alias = null;
        $beacon->save();

        BeaconService::addTenantUsersAndAdminsToBeacon($beacon->id,$beacon->tenant_id);

        $beaconLocation = new BeaconLocation();
        $beaconLocation->beacon_id = $beacon->id;
        $beaconLocation->note = $faker->text(800);
        $beaconLocation->latitude = $faker->latitude;
        $beaconLocation->longitude = $faker->longitude;
        $beaconLocation->indoor = $faker->boolean;
        $beaconLocation->level = $faker->randomDigit;
        $beaconLocation->save();

        return array(
            $beacon->name,
            $beacon->url
        );

    }

    private function poi($tenant) {

        echo "\nSeeding POI";

        $user = User::inRandomOrder()->where('tenant_id',$tenant->id)->first();

        $faker = Faker\Factory::create();
        $poiData['name'] = $faker->word.' ('.$tenant->name.')';
        $poiData['description'] = $faker->catchPhrase.' '.$faker->bs;
        $poiData['meta'] = [
            "description" => $faker->realText($faker->numberBetween(10,200)),
            "title" => $faker->word,
        ];
        $poiData['has_channel'] = $faker->boolean;
        $poiData['tenant_id'] = $tenant->id;
        $poiData['fallback_language'] = $this->substanceLanguage(rand(0,3));

        $poi = Poi::create($poiData);

        PoiService::addUsersToPoi($poi->id,$tenant->id,$user->id);

        $beacon = Beacon::inRandomOrder()->where('tenant_id',$tenant->id)->where('poi_id',null)->first();

        if($beacon != null) {

            $beacon->poi_id = $poi->id;
            $beacon->save();
            $this->publishPoiUrlConnection($poi->id,$beacon->url,$tenant->id,true);

        }

        $designController = new DesignController();
        $design = $designController->store(DesignService::getPoiDesignBlueprint(), 'App\Models\PoiDesign',$poi->tenant_id);
        $poi->poi_design_id = $design->id;
        $poi->save();

    }

    private function poiContents($tenant) {

        echo "\nSeeding POI Contents";

        $pois = Poi::where('tenant_id',$tenant->id)->get();

        foreach($pois as $poi) {

            $faker = Faker\Factory::create();

            for($i = 0; $i < 4; $i++) {

                $preparedData = GroupService::preparePoiContent($this->poiContentsData($tenant),$poi->id,$this->substanceLanguage($i));

                $pc = new PoiContent();
                $pc->poi_id = $poi->id;
                $pc->language = $this->substanceLanguage($i);
                $pc->data = $preparedData['data'];
                $pc->active = true;
                $has_url = $faker->boolean;
                $pc->has_url = $has_url;
                $pc->url = $has_url ? 'https://'.$faker->word.'.'.$faker->tld : null;
                $pc->meta = [
                    "description" => $faker->bs,
                    "title" => $faker->word,
                ];
                $pc->save();

            }

            $poi->active = $faker->boolean;
            $poi->save();

            $this->publishPoiToEdwardStone($poi->id,$poi->active);

            $this->dispatch(new SetPoiInteractionFlagsJob($poi->id));

        }

    }

    private function poiContentsData($tenant) {

        echo "\nSeeding POI Contents Data";

        $c = rand(1,3);

        $data['groups'] = [];

        for($i=0;$i<$c;$i++) {

            $groups[] = $this->poiContentGroup($tenant);

        }

        $data['groups'] = $groups;

        return $data;

    }

    private function poiContentGroup($tenant) {

        $faker = Faker\Factory::create();
        $weekdaysActive = $faker->boolean;
        return [
            'time_from' => time(),
            'time_to' => $faker->boolean ? time() + rand(604800,604800*30) : null,
            'weekdays' => $this->poiContentsWeekdays($weekdaysActive),
            'weekdays_active' => $weekdaysActive,
            'label' => $faker->word,
            'modules' => $this->poiContentModules($tenant),
            'css' => [
                'wrapper' => [
                    'contents' => 'none'
                ]
            ],
            'html_id' => $faker->word,
            'html_classes' => $faker->word.' '.$faker->word
        ];

    }

    private function poiContentModules($tenant) {

        $c = rand(1,6);

        for($i=0;$i<$c;$i++) {

            $modules[] = $this->randomModule($tenant);

        }

        return $modules;

    }

    private function randomModule($tenant) {

        $c = rand(0,14);

        $faker = Faker\Factory::create();

        $modules = [
            0 => [
                'label'=> 'Text'.$faker->word,
                'type'=> 'text',
                'data'=> [
                    'text'=> $faker->realText($faker->numberBetween(100,2000)),
                    'css'=> [
                        'wrapper'=> [],
                        'text'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            1 => [
                'label'=> 'Image'.$faker->word,
                'type'=> 'image',
                'data'=> [
                    'url'=> $faker->imageUrl(600, 400, 'cats'),
                    'type' => 'url',
                    'alt' => $faker->bs,
                    'caption' => $faker->bs,
                    'css'=> [
                        'wrapper'=> [],
                        'image'=> [],
                        'caption'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            2 => [
                'label'=> 'Video'.$faker->word,
                'type'=> 'video',
                'data'=> [
                    'url'=> 'https://www.youtube.com/watch?v=C8NAYW-Z54o',
                    'type'=> 'youtube',
                    'css'=> [
                        'wrapper'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            3 => [
                'label'=> 'Button'.$faker->word,
                'type'=> 'button',
                'data'=> [
                    'type'=> 'web',
                    'target'=> $faker->url,
                    'text'=> $faker->word,
                    'css'=> [
                        'wrapper'=> [],
                        'button'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            4 => [
                'label'=> 'Audio'.$faker->word,
                'type'=> 'audio',
                'data'=> [
                    'type' => 'external',
                    'url'=> 'http://eddy.raphbib.us/audio/ziv.mp3',
                    'css'=> [
                        'wrapper'=> [],
                        'player'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            6 => [
                'label'=> 'Divider'.$faker->word,
                'type'=> 'divider',
                'data'=> [
                    'line'=> $faker->boolean,
                    'css'=> [
                        'wrapper'=> [],
                        'divider'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            7 => [
                'label'=> 'Headline'.$faker->word,
                'type'=> 'heading',
                'data'=> [
                    'align'=> $faker->boolean ? 'left' : 'right',
                    'size'=> rand(1,3),
                    'text' => $faker->word,
                    'css'=> [
                        'wrapper'=> [],
                        'heading'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            8 => [
                'label'=> 'Social Icons'.$faker->word,
                'type'=> 'social_icons',
                'data'=> [
                    'facebook' => 'https://facebook.com/'.$faker->slug,
                    'twitter' => 'https://twitter.com/'.$faker->slug,
                    'instagram' => 'https://instagram.com/'.$faker->slug,
                    'youtube' => 'https://youtube.com/'.$faker->slug,
                    'css'=> [
                        'wrapper'=> [],
                        'icons'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            9 => [
                'label'=> 'Feedback'.$faker->word,
                'type'=> 'feedback',
                'data'=> [
                    'text' => $faker->word,
                    'positive_label' => $faker->word,
                    'negative_label' => $faker->word,
                    'icons' => $faker->boolean ? 'thumbs' : 'smileys',
                    'css'=> [
                        'wrapper'=> [],
                        'feedbackIcons'=> [],
                        'feedbackText'=> [],
                        'feedbackLabels'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            5 => [
                'label'=> 'Contact Form'.$faker->word,
                'type'=> 'contact',
                'data'=> [
                    'placeholder_name' => $faker->word.' name',
                    'placeholder_email' => $faker->word.' email',
                    'placeholder_text' => $faker->word.' text',
                    'with_captcha' => $faker->boolean,
                    'recipient' => $faker->safeEmail,
                    'button_label' => $faker->word,
                    'success_label' => $faker->realText($faker->numberBetween(50,128)),
                    'css'=> [
                        'wrapper'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            10 => [
                'label'=> 'Sweepstake'.$faker->word,
                'type'=> 'sweepstake',
                'data'=> [
                    'css'=> [
                        'wrapper'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word,
                    'placeholder_firstname' => $faker->word.' firstname',
                    'placeholder_lastname' => $faker->word.' lastname',
                    'placeholder_street' => $faker->word.' street',
                    'placeholder_zip' => $faker->word.' zip',
                    'placeholder_city' => $faker->word.' city',
                    'with_firstname' => true,
                    'with_lastname' => true,
                    'with_street' => true,
                    'with_zip' => true,
                    'with_city' => true,
                    'required_firstname' => $faker->boolean,
                    'required_lastname' => $faker->boolean,
                    'required_street' => $faker->boolean,
                    'required_zip' => $faker->boolean,
                    'required_city' => $faker->boolean,
                    'placeholder_email' => $faker->word.' email',
                    'question' => $faker->realText($faker->numberBetween(50,200)),
                    'answer' => $faker->boolean ? 'none' : 'text',
                    'placeholder_text' => $faker->realText($faker->numberBetween(50,128)),
                    'with_terms' => $faker->boolean,
                    'terms_hint' => $faker->realText($faker->numberBetween(50,256)),
                    'terms_text' => $faker->realText($faker->numberBetween(50,512)),
                    'store_participation' => $faker->boolean,
                    'with_captcha' => $faker->boolean,
                    'recipient' => $faker->safeEmail,
                    'button_label' => $faker->realText($faker->numberBetween(50,128)),
                    'success_label' => $faker->realText($faker->numberBetween(50,128)),
                ]
            ],
            11 => [
                'label'=> 'Symbol'.$faker->word,
                'type'=> 'icon',
                'data'=> [
                    'icon' => 'credit-card',
                    'icon_set' => 'fa',
                    'css'=> [
                        'wrapper'=> [],
                        'heading'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ],
            12 => [
                'label'=> 'Symbol'.$faker->word,
                'type'=> 'spotify',
                'data'=> [
                    'icon' => 'fa-spotify',
                    'icon_set' => 'fa',
                    'css'=> [
                        'wrapper'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word,
					'theme' => $faker->boolean ? 'dark' : 'light',
					'uri' =>  $faker->boolean ? 'spotify:track:1A4y0eCgwxgII0iRCt6oEk' : 'spotify:album:5uGF0WPjVL3rAP5pGEP24k'
                ]
            ],
        ];

        if($c<=12) {
            return $modules[$c];
        } else if ($c==13) {
            $nlc = NewsletterConfiguration::inRandomOrder()->where('tenant_id',$tenant->id)->first();
            return [
                'label'=> 'Newsletter '.$faker->word,
                'type'=> 'newsletter',
                'data'=> [
                    'id' => $nlc->id,
                    'text' => $faker->realText($faker->numberBetween(50,200)),
                    'with_name' => $faker->boolean,
                    'with_captcha' => $faker->boolean,
                    'placeholder' => $faker->word,
                    'button' => $faker->word,
                    'message' => $faker->word,
                    'css'=> [
                        'wrapper'=> [],
                        'form'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ];
        } else if ($c==14) {
            $mp = MicroPayment::inRandomOrder()->where('tenant_id',$tenant->id)->first();
            return [
                'label'=> 'Micro Payment '.$faker->word,
                'type'=> 'micro_payment',
                'data'=> [
                    'name' => $faker->word,
                    'description' => $faker->realText($faker->numberBetween(50,200)),
                    'micro_payment_id' => $mp->id,
                    'css'=> [
                        'wrapper'=> []
                    ],
                    'html_id' => $faker->word,
                    'html_classes' => $faker->word
                ]
            ];
        }

    }

    private function poiContentsWeekdays($active) {

        $t = time()-rand(0,7200);
        $faker = Faker\Factory::create();

        return [
            'mon'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'tue'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'wed'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'thu'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'fri'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'sat'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
            'sun'=>[
                'active' => $active ? $faker->boolean : false,
                'times' => $this->poiContentTimes($t)
            ],
        ];

    }

    private function poiContentTimes($t) {

        $c = rand(1,3);

        if($c==1) {
            return [
                [
                    'time_from'=>date('H:i',$t),
                    'time_to'=>date('H:i',$t+rand(3600,7200)),
                ],
            ];
        } else if ($c==2) {
            return [
                [
                    'time_from'=>date('H:i',$t),
                    'time_to'=>date('H:i',$t+rand(3600,7200)),
                ],
                [
                    'time_from'=>date('H:i',$t),
                    'time_to'=>date('H:i',$t+rand(3600,7200)),
                ]
            ];
        } else {
            return [];
        }

    }

}
