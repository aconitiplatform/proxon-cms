<?php

use Illuminate\Database\Seeder;
use App\Content;

class MediaSeeder extends Seeder {

    private $tenantId = 'c206a8a5-fb82-45ca-810b-521af45dc311';
    private $userId = '5a74c905-aaac-4233-b916-c28862f87003';
    private $url = 'c206a8a5-fb82-45ca-810b-521af45dc311/677c78faa1f92b1854cd989da03ffd841865fd2d.jpeg';
    private $storagePath = '677c78faa1f92b1854cd989da03ffd841865fd2d.jpeg';
    private $type = 'image/jpeg';
    private $seedAmount = 1000;

    public function run() {

        $faker = Faker\Factory::create();

        for($i = 0; $i<$this->seedAmount; $i++) {

            // 'type', 'url', 'name','tenant_id','user_id','storage_path'

            $data = array(
                'type'=>$this->type,
                'url'=>$this->url,
                'name'=>$faker->word,
                'tenant_id'=>$this->tenantId,
                'user_id'=>$this->userId,
                'storage_path'=>$this->storagePath
            );

            Content::create($data);

        }

    }
}
