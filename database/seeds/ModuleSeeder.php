<?php

use Illuminate\Database\Seeder;

use App\Models\Features\Module;
use App\Models\Features\Feature;

class ModuleSeeder extends Seeder {

    public function run() {

        $this->createModules();
        $this->createFeatures();

    }

    private function createModules() {

        $definitions = json_decode(file_get_contents(__DIR__.'/ModuleDefinitions.json'));

        foreach($definitions as $key => $definition) {

            $m = Module::where('type',$definition->type)->first();

            if($m == null) {
                $m = new Module();
            }

            $m->name = $definition->type;
            $m->slug = str_replace("_","-",$m->name);
            $m->label = $definition->label;
            $m->type = $definition->type;
            $m->data = $definition->data;
            $m->icon = $definition->icon;
            $m->group = $definition->group;
            $m->tags = $definition->tags;
            $m->default = $definition->default;
            $m->save();

		}

    }

    private function createFeatures() {

        $features = config('substance.features');

        foreach($features as $key => $definition) {

            $f = Feature::where('name',$definition['name'])->first();

            if($f == null) {
                $f = new Feature();
            }

            $f->name = $definition['name'];
            $f->slug = str_slug($f->name);
            $f->default = $definition['default'];
            $f->save();

            $this->attachModulesToFeatures($definition,$f);

        }

    }

    private function attachModulesToFeatures($definition,$f) {

        $ids = [];

        foreach($definition['related_modules'] as $slug) {

            $m = Module::where('slug',$slug)->first();

            $ids[] = $m->id;

        }

        $f->modules()->sync($ids);

    }

}
