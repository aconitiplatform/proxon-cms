<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\User;

class InvalidateCacheJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    public $entityId;
    public $tenantId;
    public $entity;

    public function __construct($entityId,$tenantId,$entity) {

        $this->entityId = $entityId;
        $this->tenantId = $tenantId;
        $this->entity = $entity;

    }

    public function handle() {

        $users = User::where('tenant_id',$this->tenantId)->get();

        if(!$users->isEmpty()) {

            foreach($users as $user) {

                $cacheKeyIndex = $this->tenantId.':users:'.$user->id.':'.$this->entity.':index';
                $cacheKeyEntity = $this->tenantId.':users:'.$user->id.':'.$this->entity.':'.$this->entityId;
                if(Cache::has($cacheKeyIndex)) Cache::forget($cacheKeyIndex);
                if(Cache::has($cacheKeyEntity)) Cache::forget($cacheKeyEntity);

            }
        }

    }
}
