<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\Poi;
use App\Models\Tenant;

class DeliverPoiToDistributionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;

    public function __construct($poiId) {
        $this->poiId = $poiId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:pois:'.$this->poiId;
        $poi = Poi::find($this->poiId);

        if($poi != null && $poi->active) {

            $poi->poiContents;
            $poi->poiDesign;
            Cache::forever($cacheKey,json_encode($poi));

        }

    }

}
