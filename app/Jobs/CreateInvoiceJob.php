<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Console\Traits\CreatesInvoices;

class CreateInvoiceJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, CreatesInvoices;

    protected $type;
    protected $args;

    public function __construct($type,$args) {

        $this->type = $type;
        $this->args = $args;

    }


    public function handle() {

        switch ($this->type) {
            case 'monthly_reseller':
                $this->createMonthlyResellerInvoice($this->args['reseller_id']);
                break;
            case 'monthly_tenant':
                $this->createMonthlyTenantInvoice($this->args['tenant_id']);
                break;
            case 'single_reseller':
                $this->createSingleResellerInvoice($this->args['reseller_id'],$this->args['tenant_id'],$this->args['amount_of_new_beacons']);
                break;
            case 'single_tenant':
                $this->createSingleTenantInvoice($this->args['tenant_id'],$this->args['amount_of_new_beacons']);
                break;
            default:
                Log::info('Wrong invoice type.');
                break;
        }

    }
}
