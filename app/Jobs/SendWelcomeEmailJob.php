<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class SendWelcomeEmailJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $tenant;
    protected $user;

    public function __construct($tenant,$user) {

        $this->tenant = $tenant;
        $this->user = $user;

    }

    public function handle() {

        $email = $this->user->email;
        $emailData['tenant'] = $this->tenant;
        $emailData['user'] = $this->user;

        Mail::send('emails.registered', $emailData, function ($mail) use ($email) {
            $mail->to($email)
            ->from(config('substance.mail_address'))
            ->subject(trans('emails.welcome_subject',['substance' => config('substance.name')]));
        });

    }
}
