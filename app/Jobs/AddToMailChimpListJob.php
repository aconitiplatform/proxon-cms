<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mailchimp;
use App\Jobs\Job;

class AddToMailChimpListJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $apiKey;
    protected $listId;
    protected $email;
    protected $mergeVars;

    public function __construct($apiKey,$listId,$email,$mergeVars = array()) {

        $this->apiKey = $apiKey;
        $this->listId = $listId;
        $this->email = $email;
        $this->mergeVars = $mergeVars;

    }

    public function handle() {

        $mailchimp = new Mailchimp($this->apiKey);

        try {
            $mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $this->email],
                    $this->mergeVars
                );

        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
        } catch (\Mailchimp_Error $e) {
            // do something
        }

    }
}
