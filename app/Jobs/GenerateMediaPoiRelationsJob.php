<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\Job;
use Log;

use App\Models\Poi;
use App\Models\PoiContent;

use App\Http\Services\MediaService;
use App\Http\Services\PoiContentModules\ModuleService;

class GenerateMediaPoiRelationsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;

    public function __construct($poiId) {

        $this->poiId = $poiId;

    }

    public function handle() {

        try {

            $poi = Poi::findOrFail($this->poiId);

            $relations = MediaService::getAllMediaPoiRelations($poi->id);

            foreach($relations as $media) {
                MediaService::deleteMediaPoiRelation($media->media_id,$poi->id);
            }

            foreach($poi->poiContents as $poiContent) {

                if(isset($poiContent->meta->image_type) && $poiContent->meta->image_type == 'id') {
                    MediaService::insertMediaPoiRelation($poiContent->meta->image_id,$poi->id);
                }

                foreach($poiContent->data['groups'] as $group) {

                    foreach($group['modules'] as $module) {

                        $ids = ModuleService::isRelationableModule($module);

                        foreach($ids as $id) {
                            MediaService::insertMediaPoiRelation($id,$poi->id);
                        }

                    }

                }

            }

        } catch(\Exception $e) {
            Log::info('GenerateMediaPoiRelationsJob failed for '.$this->poiId);
        }

    }
}
