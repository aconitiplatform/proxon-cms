<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

use App\Jobs\Job;

use App\Jobs\GetChartDataJob;

/**
 * @deprecated
 */
class PrepareChartDataJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenantId;

    public function __construct($tenantId) {
        $this->tenantId = $tenantId;
    }

    public function handle() {

        $subusers = User::where('tenant_id',$this->tenantId)->where('role','subuser')->where('active',true)->get();

        foreach($subusers as $subuser) {
            $this->dispatchGetChartDataJobs($subuser);
        }

        $user = User::where('tenant_id',$this->tenantId)->where('role','user')->where('active',true)->first();

        if($user != null) {
            $this->dispatchGetChartDataJobs($user);
        }

        $admin = User::where('tenant_id',$this->tenantId)->where('role','admin')->where('active',true)->first();

        if($admin != null) {
            $this->dispatchGetChartDataJobs($admin);
        }

    }

    private function dispatchGetChartDataJobs($user) {

        $chartMethods = [
            'showMonthChart',
            'showWeekChart',
        ];

        foreach($chartMethods as $chartMethod) {

            $job = (new GetChartDataJob($this->tenantId,$user->id,$chartMethod))->onQueue(config('substance.queue_prefix').'firebase');
            dispatch($job);

        }

    }
}
