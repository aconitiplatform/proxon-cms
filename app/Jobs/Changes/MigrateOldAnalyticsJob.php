<?php

namespace App\Jobs\Changes;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use Log;

use App\Models\Poi;
use App\Models\UrlAccess;
use App\Models\Analytics\AnalyticsScan;
use App\Models\Analytics\AnalyticsEvent;

use App\Jobs\Job;

class MigrateOldAnalyticsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    private $tenantId;
    private $migrated;
    private $pageSize = 100;

    public function __construct($tenantId,$migrated = 0) {

        $this->tenantId = $tenantId;
        $this->migrated = $migrated;

    }

    public function handle() {

        // $oldAnalytics = UrlAccess::where('tenant_id',$this->tenantId)->skip($this->migrated)->take($this->pageSize)->get();

        // if(!$oldAnalytics->isEmpty()) {

        //     $oldAnalytics->each(function($item,$key) {

        //         if(Poi::where('id',$item->poi_id)->exists()) {

        //             switch ($item->access_type) {
        //                 case 'beacon':
        //                     $this->toAnalyticsScan($item);
        //                     break;
        //                 case 'web':
        //                     $this->toAnalyticsEvent($item,'poi');
        //                     break;
        //                 case 'channel':
        //                     $this->toAnalyticsEvent($item,'channel');
        //                     break;
        //             }

        //         } else {
        //             Log::warning("MIGRATE OLD ANALYTICS: POI with ID {$item->poi_id} of tenant with ID {$this->tenantId} is missing. Skipping this event.");
        //         }

        //     });

        //     $this->dispatchJob(new MigrateOldAnalyticsJob($this->tenantId,$this->migrated + $this->pageSize),'analytics');

        // }

    }

    private function toAnalyticsEvent(UrlAccess $urlAccess, string $accessed) {

        $event = new AnalyticsEvent();
        $event->tenant_id = $urlAccess->tenant_id;
        $event->poi_id = $urlAccess->poi_id;
        $event->accessed = $accessed;
        $event->url = $urlAccess->url;
        $event->user_agent = $urlAccess->user_agent;

        $firedAt = Carbon::parse($urlAccess->created_at);
        $event->fired_at = $firedAt->timestamp;

        $event->from = strlen($urlAccess->url) == 36 ? 'web' : 'beacon';

        $event->event = $event->from == 'web' ? "{$accessed}_via_browser" : "{$accessed}_via_beacon";

        $event->save();

    }

    private function toAnalyticsScan(UrlAccess $urlAccess) {

        $scan = new AnalyticsScan();
        $scan->tenant_id = $urlAccess->tenant_id;
        $scan->poi_id = $urlAccess->poi_id;
        $scan->url = $urlAccess->url;
        $scan->user_agent = $urlAccess->user_agent;

        $firedAt = Carbon::parse($urlAccess->created_at);
        $scan->fired_at = $firedAt->timestamp;

        $scan->save();

    }

}
