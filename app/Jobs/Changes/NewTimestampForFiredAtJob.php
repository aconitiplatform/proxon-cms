<?php

namespace App\Jobs\Changes;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use Log;

use App\Models\Poi;
use App\Models\UrlAccess;
use App\Models\Analytics\AnalyticsScan;
use App\Models\Analytics\AnalyticsEvent;

use App\Jobs\Job;

class NewTimestampForFiredAtJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    private $done;
    private $model;
    private $pageSize = 200;

    public function __construct($model,$done = 0) {

        $this->model = $model;
        $this->done = $done;

    }

    public function handle() {

        $model = $this->model;

        $analyticsWithoutTimestamp = $model::where('fired_at_timestamp',null)->take($this->pageSize)->get();

        if(!$analyticsWithoutTimestamp->isEmpty()) {

            foreach($analyticsWithoutTimestamp as $eventOrScan) {

                $eventOrScan->fired_at_timestamp = Carbon::createFromTimestamp($eventOrScan->fired_at)->toDateTimeString();
                $eventOrScan->save();

            }

            Log::info("Took another 200 events, starting at {$this->done} from {$this->model}");

            $this->dispatchJob(new NewTimestampForFiredAtJob($model,$this->done + $this->pageSize),'analytics');

        } else {
            Log::info("Empty colelction for {$this->model}");
        }

    }

}
