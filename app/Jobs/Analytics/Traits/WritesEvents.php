<?php

namespace App\Jobs\Analytics\Traits;

use Carbon\Carbon;

use App\Models\Analytics\AnalyticsEvent;

use App\Http\Services\Service;

use App\Jobs\Analytics\Traits\CachesCounts;

trait WritesEvents {

    protected function event($payload) {

        if($this->isValidEvent($payload)) {
            $this->writeEvent($payload);
        }

    }

    private function isValidEvent($payload) {

        $rules = [
            'tenant_id' => 'required|exists:tenants,id',
            'poi_id' => 'required|exists:pois,id',
            'journey_id' => 'sometimes|string|size:36',
            'accessed' => 'required|in:poi,channel',
            'url' => 'required|string',
            'fired_at' => 'required|numeric',
            'user_agent' => 'sometimes|string',
            'from' => 'sometimes|in:beacon,web',
            'event' => 'sometimes|in:poi_via_beacon,poi_via_browser,channel_via_beacon,channel_via_browser',
            'os' => 'sometimes|string',
        ];

        try {
            Service::validateArray($payload,$rules);
        } catch (\Exception $e) {
            return false;
        }

        return true;

    }

    private function writeEvent($payload) {

        if(!isset($payload['event'])) {
            $payload['event'] = $this->recognizeEvent($payload);
        }

        $payload['fired_at_timestamp'] = Carbon::createFromTimestamp($payload['fired_at'])->toDateTimeString();

        $event = AnalyticsEvent::create($payload);
        CachesCounts::increaseCounts($event->tenant_id,$event->fired_at,$event->poi_id,$event->journey_id,$event->url,$event->accessed,$event->from,$event->event);

    }

    private function recognizeEvent($payload) {

        if(isset($payload['from']) && $payload['from'] == 'beacon') {

            if($payload['accessed'] == 'poi') {
                return 'poi_via_beacon';
            } else if($payload['accessed'] == 'channel') {
                return 'channel_via_beacon';
            }

        } else if(isset($payload['from']) && $payload['from'] == 'web') {

            if($payload['accessed'] == 'poi') {
                return 'poi_via_browser';
            } else if($payload['accessed'] == 'channel') {
                return 'channel_via_browser';
            }

        }

        return null;

    }

}
