<?php

namespace App\Jobs\Analytics\Traits;

use Carbon\Carbon;

use Cache;

class CachesCounts {

    public static function increaseCounts($tenantId,$firedAt,$poiId,$journeyId,$url,$accessed = 'scan',$from = null,$event = null) {

        foreach(self::getPaths($tenantId,$firedAt,$poiId,$journeyId,$url,$accessed,$from,$event) as $path) {

            if(self::getCache()->has($path)) {

                $count = self::getCache()->get($path) + 1;
                self::getCache()->forever($path,$count);

            } else {
                self::getCache()->forever($path,1);
            }

        }

    }

    private static function getPaths($tenantId,$firedAt,$poiId,$journeyId,$url,$accessed = 'scan',$from = null,$event = null) {

        $prefix = "{$accessed}:{$tenantId}";

        $dt = Carbon::createFromTimestamp($firedAt);
        $dt->startOfDay();

        $prefixWithTime = "{$prefix}:{$dt->timestamp}";

        $paths = [
            "{$prefix}:count",
            "{$prefix}:{$poiId}:count",
            "{$prefixWithTime}:count",
            "{$prefixWithTime}:{$poiId}:count",
            "{$prefixWithTime}:{$url}:count",
        ];

        if($journeyId != null) {
            $paths[] = "{$prefix}:journeys:{$journeyId}:count";
            $paths[] = "{$prefixWithTime}:journeys:{$journeyId}:count";
        }

        if($from) {
            $paths[] = "{$prefixWithTime}:{$from}:count";
            $paths[] = "{$prefixWithTime}:{$from}:{$poiId}:count";
        }

        if($event) {
            $paths[] = "{$prefixWithTime}:{$event}:count";
            $paths[] = "{$prefixWithTime}:{$event}:{$poiId}:count";
        }

        return $paths;

    }

    private static function getCache() {
        return Cache::store('analytics');
    }

}
