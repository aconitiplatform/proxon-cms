<?php

namespace App\Jobs\Analytics\Traits;

use App\Models\Analytics\AnalyticsJourney;

use App\Http\Services\Service;

trait WritesJourneys {

    protected function journey($payload) {

        if($this->isValidJourney($payload)) {
            $this->writeJourney($payload);
        }

    }

    private function isValidJourney($payload) {

        $rules = [
            'tenant_id' => 'required|exists:tenants,id',
        ];

        try {
            Service::validateArray($payload,$rules);
        } catch (\Exception $e) {
            return false;
        }

        return true;

    }

    private function writeJourney($payload) {
        $event = AnalyticsJourney::create($payload);
    }

}
