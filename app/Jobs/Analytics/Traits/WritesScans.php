<?php

namespace App\Jobs\Analytics\Traits;

use Carbon\Carbon;

use App\Models\Analytics\AnalyticsScan;

use App\Http\Services\Service;

use App\Jobs\Analytics\Traits\CachesCounts;

trait WritesScans {

    protected function scan($payload) {

        if($this->isValidScan($payload)) {
            $this->writeScan($payload);
        }

    }

    private function isValidScan($payload) {

        $rules = [
            'tenant_id' => 'required|exists:tenants,id',
            'poi_id' => 'required|exists:pois,id',
            'journey_id' => 'sometimes|string|size:36',
            'fired_at' => 'required|numeric',
            'url' => 'required|string',
            'user_agent' => 'sometimes|string',
        ];

        try {
            Service::validateArray($payload,$rules);
        } catch (\Exception $e) {
            return false;
        }

        return true;

    }

    private function writeScan($payload) {

        $payload['fired_at_timestamp'] = Carbon::createFromTimestamp($payload['fired_at'])->toDateTimeString();
        $event = AnalyticsScan::create($payload);
        CachesCounts::increaseCounts($event->tenant_id,$event->fired_at,$event->poi_id,$event->journey_id,$event->url);

    }

}
