<?php

namespace App\Jobs\Analytics;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\Analytics\Traits\WritesEvents;
use App\Jobs\Analytics\Traits\WritesScans;
use App\Jobs\Analytics\Traits\WritesJourneys;

class WriteAnalyticsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, WritesEvents, WritesScans, WritesJourneys;

    protected $type;
    protected $payload;

    public function __construct($type,$payload) {

        $this->type = $type;
        $this->payload = $payload;

    }

    public function handle() {

        switch ($this->type) {
            case 'event':
                $this->event($this->payload);
                break;
            case 'scan':
                $this->scan($this->payload);
                break;
            case 'journey':
                $this->journey($this->payload);
                break;
            default:
                break;
        }

    }




}
