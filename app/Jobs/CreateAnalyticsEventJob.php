<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\Models\UrlAccess;

class CreateAnalyticsEventJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $type;
    protected $time;
    protected $payload;

    public function __construct($type,$time,$payload) {

        $this->type = $type;
        $this->time = $time;
        $this->payload = $payload;

    }

    public function handle() {

        switch ($this->type) {
            case 'browser':
                $this->createAccessEvent();
                break;
            case 'scanner':
                $this->createAccessEvent();
                break;
            default:
                # code...
                break;
        }

    }

    private function createAccessEvent() {

        $urlAccess = UrlAccess::create($this->payload);
        $urlAccess->created_at = Carbon::createFromTimestamp($this->time)->toDateTimeString();
        $urlAccess->save();

    }

}
