<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\Tenant;

class DeliverTenantToDistributionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenantId;

    public function __construct($tenantId) {
        $this->tenantId = $tenantId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:tenants:'.$this->tenantId;

        $tenant = Tenant::find($this->tenantId);

        if($tenant != null) {

            $tenant->tenantDesign;
            $tenant = json_encode($tenant);
            Cache::forever($cacheKey,$tenant);

        } else {
            Cache::forget($cacheKey);
        }

    }
}
