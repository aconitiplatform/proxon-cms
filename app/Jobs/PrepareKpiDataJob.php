<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

use App\Jobs\Job;

use App\Http\Controllers\KpiController;

use App\Jobs\GetKpiDataJob;

/**
 * @deprecated
 */
class PrepareKpiDataJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenantId;

    public function __construct($tenantId) {
        $this->tenantId = $tenantId;
    }

    public function handle() {

        $subusers = User::where('tenant_id',$this->tenantId)->where('role','subuser')->where('active',true)->get();

        foreach($subusers as $subuser) {
            $this->dispatchGetKpiDataJobs($subuser);
        }

        $user = User::where('tenant_id',$this->tenantId)->where('role','user')->where('active',true)->first();

        if($user != null) {
            $this->dispatchGetKpiDataJobs($user);
        }

        $admin = User::where('tenant_id',$this->tenantId)->where('role','admin')->where('active',true)->first();

        if($admin != null) {
            $this->dispatchGetKpiDataJobs($admin);
        }

    }

    private function dispatchGetKpiDataJobs($user) {

        $kpiMethods = [
            'showScansToday',
            'showDeliveredContentToday',
            'showAccessedChannelsToday',
        ];

        foreach($kpiMethods as $kpiMethod) {

            $job = (new GetKpiDataJob($this->tenantId,$user->id,$kpiMethod))->onQueue(config('substance.queue_prefix').'firebase');
            dispatch($job);

        }

    }
}
