<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;

use App\Models\User;
use App\Models\PoiEvent;

use App\Traits\SavesPoiEvent;
use App\Traits\InvalidatesCache;

class PoiEventJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, SavesPoiEvent, InvalidatesCache;

    protected $poiEventData;
    protected $user;

    public function __construct(array $poiEventData,User $user) {

        $this->poiEventData = $poiEventData;
        $this->user = $user;

    }

    public function handle() {

        Auth::setUser($this->user);

        $poiEvent = $this->savePoiEvent($this->poiEventData,$this->user);

        if($poiEvent != null) {

            $this->invalidateAndModify(
                [
                    'id' => $poiEvent->id,
                    'key' => 'poi-log',
                    'object' => new PoiEvent()
                ]
            );

        }

    }
}
