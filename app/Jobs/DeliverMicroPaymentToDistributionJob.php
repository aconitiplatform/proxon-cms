<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\MicroPayment;

use App\Jobs\RemoveMicroPaymentFromDistributionJob;

class DeliverMicroPaymentToDistributionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $microPaymentId;

    public function __construct($microPaymentId) {
        $this->microPaymentId = $microPaymentId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:micro-payments:'.$this->microPaymentId;
        $microPayment = MicroPayment::find($this->microPaymentId);

        if($microPayment != null) {

            Cache::forever($cacheKey,json_encode($microPayment));

        } else {
            $this->dispatchJob(new RemoveMicroPaymentFromDistributionJob($this->microPaymentId));
        }

    }

}
