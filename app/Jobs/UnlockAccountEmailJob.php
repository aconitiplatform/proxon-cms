<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App;

use App\Models\User;

use App\Http\Services\TenantService;

use App\Traits\Resellable;

class UnlockAccountEmailJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, Resellable;

    public $emailData;

    public function __construct($emailData) {
        $this->emailData = $emailData;
    }

    public function handle() {

        $email = $this->emailData['email'];

        $user = User::where('email',$email)->first();

        if($user != null) {

            $this->updateConfigForReseller($user->tenant);

            $this->emailData['substanceUrl'] = TenantService::getSubstanceAppUrl($user->tenant_id);

            App::setLocale($user->language);

            Mail::send('auth.unlock_account', $this->emailData, function ($mail) use ($email) {
                $mail->to($email)
                ->from(config('substance.mail_address'))
                ->subject(trans('emails.unlock_account_subject',['substance' => config('substance.name')]));
            });

        }

    }

}
