<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

use App\Models\Poi;
use App\Models\PoiContent;

use App\Http\Services\PoiContentModules\ModuleService;

class SetPoiInteractionFlagsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;

    public function __construct($poiId) {

        $this->poiId = $poiId;

    }

    public function handle() {

        try {

            $poi = Poi::findOrFail($this->poiId);

            $flags = [];

            foreach(ModuleService::getFlaggableModules() as $flaggableModule) {
                $flags[$flaggableModule] = false;
            }

            if(isset($poi->poiContents)) {

                foreach($poi->poiContents as $poiContent) {

                    foreach(ModuleService::getFlaggableModules() as $flaggableModule) {

                        $key = 'has_'.$flaggableModule;

                        if($poiContent->$key == true) {
                            $poi->$key = true;
                        }

                    }

                }

            }

            $poi->save();


        } catch(\Exception $e) {
            Log::info('SetPoiInteractionFlagsJob failed.');
        }

    }
}
