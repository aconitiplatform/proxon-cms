<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

class RemoveSurveyFromDistributionJob extends Job implements ShouldQueue {

    protected $surveyId;

    public function __construct($surveyId) {
        $this->surveyId = $surveyId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:surveys:'.$this->surveyId;
        if(Cache::has($cacheKey)) Cache::forget($cacheKey);

    }

}
