<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

use App\Jobs\Job;

class SendCartToAdminJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenant;
    protected $cart;

    public function __construct($tenant,$cart) {

        $this->tenant = $tenant;
        $this->cart = $cart;

    }

    public function handle() {

        $emails[] = config('substance.admin_email');
        $emailData['tenant'] = $this->tenant;
        $emailData['cart'] = $this->cart;

        foreach($emails as $email) {

            Mail::send('cart.order', $emailData, function ($mail) use ($email) {
                $mail->to($email)
                ->from(config('substance.mail_address'))
                ->subject('New order details: '.$this->tenant->name);
            });

        }

    }

}
