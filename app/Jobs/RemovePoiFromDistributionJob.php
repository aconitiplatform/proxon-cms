<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

class RemovePoiFromDistributionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;

    public function __construct($poiId) {
        $this->poiId = $poiId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:pois:'.$this->poiId;
        if(Cache::has($cacheKey)) Cache::forget($cacheKey);

    }

}
