<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Traits\CreatesDataAtFirebase;

class CreateFeedbackAtFirebaseJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, CreatesDataAtFirebase;

    protected $feedbackId;
    protected $poiId;
    protected $language;
    protected $label;

    public function __construct($feedbackId, $poiId, $language, $label = 'Feedback') {

        $this->feedbackId = $feedbackId;
        $this->poiId = $poiId;
        $this->language = $language;
        $this->label = $label;

    }

    public function handle() {

        $this->createFirebaseObject('feedbacks/'.$this->poiId.'/'.$this->feedbackId,array(
            'positive'=>0,
            'negative'=>0,
            'language'=>$this->language,
            'label'=>$this->label
        ));

    }
}
