<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\Job;

use Log;
use Auth;

use App\Models\Poi;
use App\Models\User;
use App\Models\PoiContent;

use App\Http\Services\PoiContentModules\GroupService;

use App\Jobs\GenerateMediaPoiRelationsJob;
use App\Jobs\SetPoiInteractionFlagsJob;

use App\Traits\AffectsEdwardStone;
use App\Traits\InvalidatesCache;

class RemoveExpiredGroupsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, AffectsEdwardStone, InvalidatesCache;

    private $poi;

    public function __construct(Poi $poi) {

        $this->poi = $poi;

    }

    public function handle() {

        // dd($this->poiContent->data);

        $hasExpiredGroup = false;

        foreach($this->poi->poiContents as $poiContent) {

            $data['groups'] = [];

            foreach($poiContent->data['groups'] as $groupKey => $groupValue) {

                if(isset($poiContent->data['groups'][$groupKey]['delete_when_expired'])
                    && $poiContent->data['groups'][$groupKey]['delete_when_expired']
                    && $poiContent->data['groups'][$groupKey]['time_to'] < time()) {

                    $hasExpiredGroup = true;

                } else {
                    $data['groups'][] = $poiContent->data['groups'][$groupKey];
                }

            }

            $preparedData = GroupService::preparePoiContent($data,$this->poi->id,$poiContent->language);
            $data = $preparedData['data'];

            $poiContent->data = $data;
            $poiContent->save();

        }

        if($hasExpiredGroup) {
            $this->updatePoi();
        }

    }

    private function updatePoi() {

        $admin = User::where('role','admin')->first();
        Auth::setUser($admin);

        $this->invalidateAndModify(
            [
                'id' => $this->poi->id,
                'key' => 'pois',
                'object' => new Poi()
            ]
        );

        $this->publishPoiToEdwardStone($this->poi->id,$this->poi->active);

        $this->dispatchJob(new GenerateMediaPoiRelationsJob($this->poi->id),'relations');
        $this->dispatchJob(new SetPoiInteractionFlagsJob($this->poi->id),'pois');

    }

}
