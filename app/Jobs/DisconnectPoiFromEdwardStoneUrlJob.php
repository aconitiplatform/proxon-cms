<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\Tenant;

class DisconnectPoiFromEdwardStoneUrlJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;
    protected $url;

    public function __construct($poiId,$url) {

        $this->poiId = $poiId;
        $this->url = $url;

    }

    public function handle() {

        $cacheKey = 'edwardstone:urls:'.$this->url;
        if(Cache::has($cacheKey)) Cache::forget($cacheKey);

        $cacheKey = 'edwardstone:slugs:'.$this->poiId;
        if(Cache::has($cacheKey)) Cache::forget($cacheKey);

    }

}
