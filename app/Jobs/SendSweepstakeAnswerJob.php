<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Mail;

class SendSweepstakeAnswerJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $requestData;

    public function __construct($requestData) {
        $this->requestData = $requestData;
    }

    public function handle() {

        $data['emailData'] = $this->requestData;
        $email = $data['emailData']['recipient'];

        Mail::send('sweepstakes.answer', $data, function ($mail) use ($email) {
            $mail->to($email)
            ->from(config('substance.mail_address'))
            ->subject(trans('emails.sweepstakes_answer_subject'));
        });

    }

}
