<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;
use Redis;
use Log;

use App\Models\User;
use App\Models\Poi;
use App\Models\PoiLock;

use App\Events\PoiWasLeft;
use App\Events\PoisAreLocked;

use App\Traits\FiresPoisAreLockedEvent;

class PusherPoiLeftJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, FiresPoisAreLockedEvent;

    protected $channel;
    protected $userId;
    protected $poiId;

    public function __construct($channel,$userId) {

        $this->channel = $channel;
        $this->userId = $userId;
        $this->poiId = str_replace('presence-poi-','',$channel);

    }

    public function handle() {

        if(config('broadcasting.should_broadcast')) {

            try {

                $user = User::findOrFail($this->userId);
                $poi = Poi::findOrFail($this->poiId);
                event(new PoiWasLeft($poi,$user));

                $poiLock = PoiLock::where('user_id', $user->id)->where('tenant_id', $poi->tenant_id)->where('poi_id', $poi->id)->first();

                if($poiLock != null) {
                    $poiLock->delete();
                }

                $this->firePoisAreLockedEvent($user);

            } catch(\Exception $e) {
                Log::error($e);
            }

        }

    }

}
