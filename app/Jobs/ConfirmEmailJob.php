<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App;

use App\Models\User;
use App\Models\EmailConfirmation;

use App\Http\Services\TenantService;

use App\Traits\Resellable;

class ConfirmEmailJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, Resellable;

    public $user;
    public $emailConfirmation;
    public $emailData;

    public function __construct(User $user, $emailConfirmation) {
        $this->user = $user;
        $this->emailConfirmation = $emailConfirmation;
    }

    public function handle() {

        $email = $this->user->email;

        $this->updateConfigForReseller($this->user->tenant);

        $this->emailData['substanceUrl'] = TenantService::getSubstanceAppUrl($this->user->tenant_id);
        $this->emailData['token'] = $this->emailConfirmation->token;

        App::setLocale($this->user->language);

        Mail::send('emails.confirm_email', $this->emailData, function ($mail) use ($email) {
            $mail->to($email)
            ->from(config('substance.mail_address'))
            ->subject(trans('emails.confirm_email_subject',['substance' => config('substance.name')]));
        });

    }

}
