<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

use App\Jobs\Job;

class SendCartToUserJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenant;
    protected $cart;
    protected $user;

    public function __construct($tenant,$cart,$user) {

        $this->tenant = $tenant;
        $this->cart = $cart;
        $this->user = $user;

    }

    public function handle() {

        $admins = $this->tenant->users()->where('role','admin')->get();

        foreach($admins as $admin) {
            $emails[] = $admin->email;
        }

        $emailData['tenant'] = $this->tenant;
        $emailData['cart'] = $this->cart;
        $emailData['user'] = $this->user;

        foreach($emails as $email) {

            Mail::send('cart.orderinfo', $emailData, function ($mail) use ($email) {
                $mail->to($email)
                ->from(config('substance.mail_address'))
                ->subject('Your order in '.config('substance.name'));
            });

        }

    }

}
