<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

use App\Http\Services\UserService;

class AdjustBeaconUserRelationJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $becomingSubuser;

    public function __construct(User $user,$becomingSubuser) {

        $this->user = $user;
        $this->becomingSubuser = $becomingSubuser;

    }

    public function handle() {

        if(!$this->becomingSubuser) {

            UserService::removeAccessFromBeacons($this->user);
            UserService::giveAccessToBeacons($this->user);

        } else {

            UserService::removeAccessFromBeacons($this->user,null,true);

        }

    }
}
