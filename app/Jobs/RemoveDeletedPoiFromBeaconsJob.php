<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Models\Beacon;

use App\Http\Services\BeaconService;

use App\Traits\AffectsEdwardStone;
use App\Traits\InvalidatesCache;

class RemoveDeletedPoiFromBeaconsJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, AffectsEdwardStone, InvalidatesCache, DispatchesJobs;

    protected $poiId;

    public function __construct($poiId) {
        $this->poiId = $poiId;
    }

    public function handle() {

        $beacons = Beacon::where('poi_id',$this->poiId)->get();

        if(!$beacons->isEmpty()) {

            foreach($beacons as $beacon) {

                $this->publishPoiUrlConnection($beacon->poi_id,$beacon->url,$beacon->tenant_id,false);
                $beacon->poi_id = null;
                $beacon->save();
                $this->invalidate($beacon->id,$beacon->tenant_id,'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx','beacons');

            }

            BeaconService::updateLastModified($beacon->tenant_id,new Beacon());

        }

    }
}
