<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Image;
use Storage;
use Auth;

use App\Models\Media;
use App\Models\User;

use App\Http\Services\MediaService;

use App\Events\ImageIsReady;

class ResizeImageJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $originalPath;
    protected $thumbnailPath;
    protected $resizedPath;
    protected $media;


    public function __construct($media) {

        $this->media = $media;
        $this->originalPath = $this->media->tenant_id.'/'.$this->media->storage_path;
        $this->thumbnailPath = $this->media->tenant_id.'/'.config('substance.image_preview').$this->media->storage_path;
        $this->resizedPath = $this->media->tenant_id.'/'.config('substance.image_resized').$this->media->storage_path;

    }

    public function handle() {

        if(Storage::disk(config('substance.storage'))->exists($this->originalPath)) {

            $user = User::where('tenant_id',$this->media->tenant_id)->where('role','admin')->first();

            Auth::setUser($user);

            $originalFile = Storage::disk(config('substance.storage'))->get($this->originalPath);

            $thumbnail = Image::make($originalFile)->fit(200,200);

            //solution from https://laracasts.com/discuss/channels/laravel/saving-an-intervention-image-instance-into-amazon-s3
            $thumbnail = $thumbnail->stream(null,60);
            Storage::disk(config('substance.storage'))->put($this->thumbnailPath,$thumbnail->__toString());

            $resized = Image::make($originalFile)->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            //solution from https://laracasts.com/discuss/channels/laravel/saving-an-intervention-image-instance-into-amazon-s3
            $resized = $resized->stream(null,80);
            Storage::disk(config('substance.storage'))->put($this->resizedPath,$resized->__toString());

            MediaService::updateLastModified($this->media->tenant_id,new Media());

            if(config('broadcasting.should_broadcast')) event(new ImageIsReady($this->media));

        }

    }
}
