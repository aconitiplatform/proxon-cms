<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Auth;
use Log;

use App\Models\User;

use App\Jobs\Job;

use App\Http\Controllers\KpiController;

/**
 * @deprecated
 */
class GetKpiDataJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    protected $tenantId;
    protected $userId;
    protected $method;

    public function __construct($tenantId,$userId,$method) {
        $this->tenantId = $tenantId;
        $this->userId = $userId;
        $this->method = $method;
    }

    public function handle() {

        $user = User::findOrFail($this->userId);

        Auth::setUser($user);
        $method = $this->method;
        $kc = new KpiController();
        $kc->$method(new Request(),true,true);

    }
}
