<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

use App\Models\Reseller;

use App\Jobs\Job;

class SendTenantDataToAdminJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenant;
    protected $user;
    protected $beaconInfos;

    public function __construct($tenant,$user,$beaconInfos) {

        $this->tenant = $tenant;
        $this->user = $user;
        $this->beaconInfos = $beaconInfos;

    }

    public function handle() {

        $emails[] = config('substance.admin_email');
        $emailData['tenant'] = $this->tenant;
        $emailData['user'] = $this->user;
        $emailData['beaconInfos'] = $this->beaconInfos;

        if($this->tenant->reseller_id != null) {

            $reseller = Reseller::findOrFail($this->tenant->reseller_id);
            $emailData['reseller']['name'] = $reseller->firstname." ".$reseller->lastname;
            $emailData['reseller']['settings'] = $reseller->settings;
            $emails[] = $reseller->email;

        }

        foreach($emails as $email) {

            Mail::send('admin.tenant_created', $emailData, function ($mail) use ($email) {
                $mail->to($email)
                ->from(config('substance.mail_address'))
                ->subject('New tenant details: '.$this->tenant->name);
            });

        }

    }

}
