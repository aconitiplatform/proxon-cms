<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\Tenant;
use App\Models\Poi;

class ConnectPoiToEdwardStoneUrlJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $poiId;
    protected $url;
    protected $tenantId;

    public function __construct($poiId,$url,$tenantId) {

        $this->poiId = $poiId;
        $this->url = $url;
        $this->tenantId = $tenantId;

    }

    public function handle() {

        $cacheKey = 'edwardstone:urls:'.$this->url;
        Cache::forever($cacheKey,$this->poiId);

        $tenant = Tenant::find($this->tenantId);
        $poi = Poi::find($this->poiId);

        if($poi != null) {

            $cacheKey = 'edwardstone:slugs:'.$this->poiId;
            Cache::forever($cacheKey,json_encode(array(
                'tenant_slug' => $tenant->slug,
                'poi_slug' => str_slug($poi->name)
            )));

        }

    }

}
