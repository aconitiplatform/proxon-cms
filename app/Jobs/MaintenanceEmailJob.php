<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App;

use App\Models\User;

class MaintenanceEmailJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $when;
    protected $howLong;

    public function __construct(User $user,$when,$howLong) {

        $this->user = $user;
        $this->when = $when;
        $this->howLong = $howLong;

    }

    public function handle() {

        $emailData['name'] = $this->user->firstname.' '.$this->user->lastname;
        $emailData['when'] = $this->when;
        $emailData['howLong'] = $this->howLong;

        $email = $this->user->email;

        App::setLocale($this->user->language);

        Mail::send('emails.planned_maintenance', $emailData, function ($mail) use ($email) {
            $mail->to($email)
            ->from(config('substance.mail_address'))
            ->subject(trans('emails.planned_maintenance_subject'));
        });

    }

}
