<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

use App\Models\Survey;

use App\Jobs\RemoveSurveyFromDistributionJob;

class DeliverSurveyToDistributionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $surveyId;

    public function __construct($surveyId) {
        $this->surveyId = $surveyId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:surveys:'.$this->surveyId;
        $survey = Survey::find($this->surveyId);

        if($survey != null && ($survey->status == 'started' || $survey->status == 'completed')) {

            Cache::forever($cacheKey,json_encode($survey));

        } else {
            $this->dispatchJob(new RemoveSurveyFromDistributionJob($this->surveyId));
        }

    }

}
