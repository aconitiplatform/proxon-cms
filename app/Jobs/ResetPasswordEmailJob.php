<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App;

use App\Models\User;

use App\Http\Services\TenantService;

use App\Traits\Resellable;
use Log;

class ResetPasswordEmailJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, Resellable;

    public $emailData;
    public $isNewUser;

    public function __construct($emailData, $isNewUser = false) {
        $this->emailData = $emailData;
        $this->isNewUser = $isNewUser;
    }

    public function handle() {

        Log::info("RESET PASSWORD");

        $email = $this->emailData['email'];

        $user = User::where('email',$email)->first();

        if($user != null) {

            $this->updateConfigForReseller($user->tenant);

            $this->emailData['substanceUrl'] = TenantService::getSubstanceAppUrl($user->tenant_id);

            App::setLocale($user->language);

            if($this->isNewUser == true) {

                $template = 'emails.welcome';
                Mail::send($template, $this->emailData, function ($mail) use ($email) {
                    $mail->to($email)
                    ->from(config('substance.mail_address'))
                    ->subject(trans('emails.welcome_subject',['substance' => config('substance.name')]));
                });

            } else {

                $template = 'auth.reset_link';
                Mail::send($template, $this->emailData, function ($mail) use ($email) {
                    $mail->to($email)
                    ->from(config('substance.mail_address'))
                    ->subject(trans('emails.password_reset_subject',['substance' => config('substance.name')]));
                });

            }

        }

    }

}
