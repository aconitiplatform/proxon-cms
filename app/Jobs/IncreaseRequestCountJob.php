<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

use App\Models\RequestCount;
use App\Models\Tenant;

class IncreaseRequestCountJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $tenantId;

    public function __construct($tenantId) {
        $this->tenantId = $tenantId;
    }

    public function handle() {

        $dt = Carbon::now();
        $year = $dt->year;
        $month = $dt->month;

        $requestCount = RequestCount::where('tenant_id',$this->tenantId)->where('year',$year)->where('month',$month)->first();

        $tenant = Tenant::findOrFail($this->tenantId);

        if($requestCount == null) {

            $requestCount = new RequestCount();
            $requestCount->month = $month;
            $requestCount->year = $year;
            $requestCount->limit = $tenant->request_limit;
            $requestCount->tenant_id = $this->tenantId;
            $requestCount->count = 1;

        } else {

            $requestCount->count = $requestCount->count + 1;

        }

        $this->informApproachingLimit($tenant,$requestCount);

    }

    private function informApproachingLimit(Tenant $tenant,RequestCount $requestCount) {

        $threshold = $requestCount->count / $requestCount->limit;

        if($threshold >= 0.8 && $threshold <= 1 && !$requestCount->notified_approach) {
            Log::error("Tenant {$tenant->name} is approaching the request limit of {$requestCount->limit}. Current number of requests is {$requestCount->count} for this month.");
            $requestCount->notified_approach = true;
        } else if ($threshold >= 1 && !$requestCount->notified_limit) {
            Log::error("Tenant {$tenant->name} has reached the request limit of {$requestCount->limit}. Current number of requests is {$requestCount->count} for this month.");
            $requestCount->notified_limit = true;
        }

        $requestCount->save();

    }

}
