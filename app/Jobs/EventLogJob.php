<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\EventLog;
use App\Models\User;

class EventLogJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    public $user;
    public $request;
    public $event;
    public $ip;
    public $resource;

    public function __construct(User $user,string $event,string $ip,array $resource) {

        $this->user = $user;
        $this->event = $event;
        $this->ip = $ip;
        $this->resource = $resource;

    }

    public function handle() {

        EventLog::create([
            'tenant_id' => $this->user->tenant_id,
            'user_id' => $this->user->id,
            'event' => $this->event,
            'ip_hash' => hash('sha256',$this->ip),
            'resource' => $this->resource,
        ]);

    }

}
