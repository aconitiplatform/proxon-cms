<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Cache;

class RemoveMicroPaymentFromDistributionJob extends Job implements ShouldQueue {

    protected $microPaymentId;

    public function __construct($microPaymentId) {
        $this->microPaymentId = $microPaymentId;
    }

    public function handle() {

        $cacheKey = 'edwardstone:micro-payments:'.$this->microPaymentId;
        if(Cache::has($cacheKey)) Cache::forget($cacheKey);

    }

}
