<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

use App\Models\NewsletterConfiguration;

use App\Jobs\AddToMailChimpListJob;

class SubscribeToNewsletterJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $email;
    protected $newsletterConfigurationId;
    protected $withName;
    protected $name;

    public function __construct($email,$newsletterConfigurationId,$withName = false,$name = '') {
        $this->email = $email;
        $this->newsletterConfigurationId = $newsletterConfigurationId;
        $this->withName = $withName;
        $this->name = $name;
    }

    public function handle() {

        $options = array();

        $nc = NewsletterConfiguration::find($this->newsletterConfigurationId);

        if($nc != null) {

            switch ($nc->type) {
                case 'mailchimp':
                    $this->dispatchJob(new AddToMailChimpListJob($nc->configuration['apikey'],$nc->configuration['list'],$this->email,$options),'subscriptions');
                    break;
                default:
                    Log::info('No support for '.$nc->type);
                    break;
            }

        }

    }

}
