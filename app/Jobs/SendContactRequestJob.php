<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Mail;

class SendContactRequestJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $email;
    protected $name;
    protected $text;
    protected $recipient;

    public function __construct($email,$name,$text,$recipient) {
        $this->email = $email;
        $this->name = $name;
        $this->text = $text;
        $this->recipient = $recipient;
    }

    public function handle() {

        $emailData = array(
            'email' => $this->email,
            'name' => $this->name,
            'text' => $this->text
        );
        $email = $this->recipient;

        Mail::send('contacts.request', $emailData, function ($mail) use ($email) {
            $mail->to($email)
            ->from(config('substance.mail_address'))
            ->subject(trans('emails.contact_request_subject'));
        });

    }

}
