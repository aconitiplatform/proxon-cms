<?php

namespace App\Jobs;

use Stripe\Stripe;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Mail;
use App;

use App\Jobs\Job;

use App\Models\Tenant;
use App\Models\MicroPayment;
use App\Models\MicroPaymentTransaction;

use App\Traits\InvalidatesCache;

class CreateMicroPaymentTransactionJob extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels, InvalidatesCache;

    protected $tenantId;
    protected $microPaymentId;
    protected $amount;
    protected $currency;
    protected $stripeId;
    protected $customerEmail;
    protected $language;

    public function __construct($tenantId,$microPaymentId,$amount,$currency,$stripeId,$customerEmail,$language) {
        $this->tenantId = $tenantId;
        $this->microPaymentId = $microPaymentId;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->stripeId = $stripeId;
        $this->customerEmail = $customerEmail;
        $this->language = $language;
    }

    public function handle() {

        App::setLocale($this->language);

        MicroPaymentTransaction::create([
            'tenant_id' => $this->tenantId,
            'micro_payment_id' => $this->microPaymentId,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'stripe_id' => $this->stripeId,
        ]);

        $this->invalidate($this->microPaymentId,$this->tenantId,'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx','micro-payments');

        $charge = $this->getStripeCharge();

        $microPayment = MicroPayment::find($this->microPaymentId);

        if($microPayment != null) {

            if($microPayment->emails_payee) {
                $this->sendEmailToPayee($microPayment->email,$microPayment,$charge);
            }

            if($microPayment->emails_payer) {
                $this->sendEmailToPayer($microPayment,$charge);
            }

        }

    }

    private function sendEmailToPayer(MicroPayment $microPayment, \Stripe\Charge $charge) {

        $tenant = Tenant::find($this->tenantId);

        if($tenant != null) {

            $emailData['email'] = $this->customerEmail;
            $emailData['microPayment']['name'] = $microPayment->name;
            $emailData['microPayment']['amount'] = $charge->amount;
            $emailData['microPayment']['currency'] = $charge->currency;
            $emailData['microPayment']['created_at'] = $charge->created;
            $emailData['microPayment']['sender'] = $microPayment->email;
            $emailData['tenant']['name'] = $tenant->name;

            Mail::send('micropayments.payer', $emailData, function ($mail) use ($emailData) {
                $mail->to($emailData['email'])
                ->from($emailData['microPayment']['sender'])
                ->subject(trans('micropayments.PAYER_SUBJECT', [
                    'TENANTNAME'=>$emailData['tenant']['name'],
                    'PAYMENTNAME'=>$emailData['microPayment']['name']]));
            });

        }

    }

    private function sendEmailToPayee($email, MicroPayment $microPayment, \Stripe\Charge $charge) {

        $emailData['email'] = $email;
        $emailData['microPayment']['name'] = $microPayment->name;
        $emailData['microPayment']['charge'] = $charge->id;
        $emailData['microPayment']['amount'] = $charge->amount;
        $emailData['microPayment']['currency'] = $charge->currency;
        $emailData['microPayment']['created_at'] = $charge->created;
        $emailData['microPayment']['paid'] = $charge->paid;

        Mail::send('micropayments.payee', $emailData, function ($mail) use ($emailData) {
            $mail->to($emailData['email'])
            ->from(config('substance.mail_address'))
            ->subject(trans('micropayments.PAYEE_SUBJECT', ['CHARGE'=>$emailData['microPayment']['charge'],'PAYMENTNAME'=>$emailData['microPayment']['name']]));
        });

    }

    private function getStripeCharge() {

        Stripe::setApiKey(config('substance.stripe_secret'));

        $charge = \Stripe\Charge::retrieve($this->stripeId);

        return $charge;

    }

}
