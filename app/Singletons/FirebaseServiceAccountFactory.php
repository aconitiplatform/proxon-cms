<?php

namespace App\Singletons;

use Firebase;

final class FirebaseServiceAccountFactory {

    private $serviceAccountConfig;

    protected static $inst = null;

    public static function Instance() {

        if(self::$inst === null) {
            self::$inst = new FirebaseServiceAccountFactory();
        }

        return self::$inst;

    }

    public function getServiceAccountConfig() {
        return $this->serviceAccountConfig;
    }

    private function __construct() {

        $this->serviceAccountConfig = str_replace('\\\\','\\',json_encode(array(
            'type' => config('substance.firebase.type'),
            'project_id' => config('substance.firebase.project_id'),
            'private_key_id' => config('substance.firebase.private_key_id'),
            'private_key' => config('substance.firebase.private_key'),
            'client_email' => config('substance.firebase.client_email'),
            'client_id' => config('substance.firebase.client_id'),
            'auth_uri' => config('substance.firebase.auth_uri'),
            'token_uri' => config('substance.firebase.token_uri'),
            'auth_provider_x509_cert_url' => config('substance.firebase.auth_provider_x509_cert_url'),
            'client_x509_cert_url' => config('substance.firebase.client_x509_cert_url'),
        ),JSON_UNESCAPED_UNICODE));

    }

    private function __clone() {}

}
