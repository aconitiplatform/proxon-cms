<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

use App\Models\Poi;
use App\Models\User;

class PoisAreLocked extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $pois;
    public $tenantId;

    public function __construct($tenantId, $pois) {

        $this->pois = $pois;
        $this->tenantId = $tenantId;

    }

    public function broadcastOn() {
        return ['pois-channel-'.$this->tenantId];
    }

    public function broadcastAs() {
        return 'locked-pois';
    }

}
