<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

use App\Models\Poi;
use App\Models\User;

class ImageIsReady extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $media;

    public function __construct($media) {

        $this->media = $media;

    }

    public function broadcastOn() {
        return ['tenants-channel-'.$this->media->tenant_id];
    }

    public function broadcastAs() {
        return 'image-ready';
    }

}
