<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\Poi;
use App\Models\User;

class PoiWasSaved extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $poi;
    public $user;

    public function __construct(Poi $poi, User $savingUser) {

        $this->poi = [
            'id' => $poi->id,
            'name' => $poi->name,
            'updated_at' => $poi->updated_at
        ];
        $this->user = [
            'id' => $savingUser->id,
            'firstname' => $savingUser->firstname,
            'lastname' => $savingUser->lastname,
            'tenant_id' => $savingUser->tenant_id
        ];

    }

    public function broadcastOn() {
        return ['presence-poi-'.$this->poi['id']];
    }

    public function broadcastAs() {
        return 'poi-saved';
    }

}
