<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

use App\Models\Poi;
use App\Models\User;

class PoiWasAccessed extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $poi;
    public $user;

    public function __construct(Poi $poi, User $accessingUser) {

        $this->poi = $poi;
        $this->user = $accessingUser;

    }

    public function broadcastOn() {
        return ['pois-channel-'.$this->user->tenant_id];
    }

    public function broadcastAs() {
        return 'poi-accessed';
    }

}
