<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Log;

use App\Models\Poi;
use App\Models\User;

class TenantHasChanged extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $tenant;

    public function __construct($tenant) {

        $this->tenant = $tenant;

    }

    public function broadcastOn() {
        return ['tenants-channel-'.$this->tenant->id];
    }

    public function broadcastAs() {
        return 'tenant-changed';
    }

}
