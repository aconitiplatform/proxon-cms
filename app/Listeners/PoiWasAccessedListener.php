<?php

namespace App\Listeners;

use App\Events\PoiWasAccessed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PoiWasAccessedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PoiWasAccessed  $event
     * @return void
     */
    public function handle(PoiWasAccessed $event)
    {
        //
    }
}
