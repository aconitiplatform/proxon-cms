<?php

namespace App\Listeners;

use Carbon\Carbon;

use App\Events\UserEmailHasChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\EmailConfirmation;

use App\Jobs\ConfirmEmailJob;

use App\Traits\WorksWithQueue;

class UserEmailHasChangedListener {

    use WorksWithQueue;

    public function __construct()
    {
        //
    }

    public function handle(UserEmailHasChanged $event) {

        $user = $event->user;
        $now = Carbon::now();

        $existingConfirmations = EmailConfirmation::where('user_id',$user->id)->get();

        if(!$existingConfirmations->isEmpty()) {

            foreach($existingConfirmations as $existingConfirmation) {
                $existingConfirmation->delete();
            }

        }

        $confirmation = new EmailConfirmation();
        $confirmation->user_id = $user->id;
        $confirmation->token = str_random(rand(16,64));
        $confirmation->save();

        $user->email_confirmed = false;
        $user->email_confirmation_sent_at = $now->toDateTimeString();
        $user->save();

        $this->dispatchJob(new ConfirmEmailJob($user,$confirmation),'emails');

    }

}
