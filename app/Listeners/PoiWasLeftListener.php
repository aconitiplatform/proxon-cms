<?php

namespace App\Listeners;

use App\Events\PoiWasLeft;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PoiWasLeftListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PoiWasLeft  $event
     * @return void
     */
    public function handle(PoiWasLeft $event)
    {
        //
    }
}
