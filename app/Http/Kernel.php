<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:500,1',
            \Barryvdh\Cors\HandleCors::class,
            \App\Http\Middleware\BySuperadmin::class,
        ],

        'public' => [
            'throttle:100,1',
            \Barryvdh\Cors\HandleCors::class,
        ],

        'cdn' => [
            'throttle:500,1',
        ],

        'webhooks' => [

        ],

        'url_parser' => [
            'throttle:200,1',
        ]

    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'attempts' => \App\Http\Middleware\CheckLoginAttempts::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'active_user' => \App\Http\Middleware\IsUserActive::class,
        'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
        'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
        'refresh.requested' => \App\Http\Middleware\CheckIfRefreshTokenIsRequested::class,
        'log_event' => \App\Http\Middleware\LogEvent::class,
        'app_only' => \App\Http\Middleware\IsApp::class,
        'transform_response' => \App\Http\Middleware\TransformResponse::class,
        'feature' => \App\Http\Middleware\HasFeature::class,
        'confirmed' => \App\Http\Middleware\IsEmailConfirmed::class,
        'first_login' => \App\Http\Middleware\FirstLogin::class
    ];

}
