<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'AngularController@serveApp');

    Route::get('/unsupported-browser', 'AngularController@unsupported');

    Route::get('/tests/mailchimp', 'TestStuffController@mailchimp');

    Route::get('/tests/kontaktiovenue', 'TestStuffController@kontaktiovenue');

    Route::get('/tests/monolog', 'TestStuffController@monolog');

    Route::get('/tests/logging', 'TestStuffController@logging');

    Route::get('/tests/stripe', 'TestStuffController@stripe');

    Route::get('/tests/module-definitions', 'ModuleController@index');

    Route::get('/tests/scans/{times}', 'TestStuffController@analyticsScan');
    Route::get('/tests/journeys/{times}', 'TestStuffController@analyticsJourney');
    Route::get('/tests/events/{times}', 'TestStuffController@analyticsEvent');

});

Route::group(['middleware' => ['webhooks']], function () {

    //Webhooks from Third-Party-Systems

    Route::post('webhooks/pusher/presence', 'PusherController@presenceChannelWebhook');

});

// Route::group(['middleware' => []], function () {
//
//     Route::get('api/modules','ModuleController@index');
//
// });

$api->version('v1', function ($api) {
    //public API routes
    $api->group(['middleware' => ['public']], function ($api) {

        // Authentication Routes...
        $api->post('auth/login', 'Auth\UserAuthController@login')->middleware('attempts')->middleware('confirmed')->middleware('transform_response')->middleware('first_login');
        $api->post('auth/login/apps', 'Auth\AppAuthController@login')->middleware('transform_response');

        // Password Reset Routes...
        $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
        $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
        $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');

        // Unlock account
        $api->post('auth/unlock-account', 'Auth\UserAuthController@unlockAccount');
        $api->post('auth/resend-unlock-account', 'Auth\UserAuthController@resendUnlockAccountEmail');

        // Email confirmation
        $api->post('auth/confirm-email', 'Auth\EmailConfirmationController@confirmEmail');
        $api->post('auth/resend-email-confirmation', 'Auth\EmailConfirmationController@resendEmailConfirmation');

        // Check SHA
        $api->get('auth/sha/latest','AngularController@latestCommit');

        // Substance Configuration
        $api->get('auth/config','AngularController@configByUrl');
        $api->post('auth/register','TenantController@register');
        $api->post('auth/verify-recaptcha','Auth\AuthController@verifyReCaptcha');
        $api->post('auth/registerFromMlm','UserController@MlmRegistration');

    });

    $api->group(['middleware' => ['url_parser']], function ($api) {
        $api->get('url-parser','MetaController@show');
    });

    $api->group(['middleware' => ['cdn']], function ($api) {
        $api->get('contents/{mediaId}/file','MediaController@getFile');
        $api->get('media/{mediaId}/file','MediaController@getFile');
        $api->get('vouchers/{mediaId}/file','VoucherController@getFile');
    });

    //protected API routes with JWT (must be logged in)
    $api->group(['middleware' => ['api', 'api.auth', 'active_user', 'refresh.requested', 'log_event']], function ($api) {

        $api->post('auth/logout', 'Auth\AuthController@logout');
        $api->post('auth/logout/apps', 'Auth\AuthController@logout');
        $api->post('auth/refresh', 'Auth\UserAuthController@refresh');
        $api->post('auth/refresh/apps', 'Auth\AppAuthController@refresh');

        $api->get('superadmin/tenants', 'SuperAdminController@tenants');
        $api->post('superadmin/login', 'Auth\UserAuthController@loginSuperAdminAsUser');

        // Blueprint Routes
        $api->get('blueprints/designs/{type}', 'DesignController@getDesignBlueprint');

        // Invoices
        $api->get('invoices', 'InvoiceController@index');
        $api->get('invoices/last-modified', 'InvoiceController@lastModified');
        $api->get('invoices/{documentId}/pdf', 'InvoiceController@pdf');

        // Subscription & Plans
        $api->get('plans', 'PlanController@index');
        $api->post('plans/subscribe', 'PlanController@subscribe');
        $api->post('plans/unsubscribe', 'PlanController@unsubscribe');
        $api->post('plans/update-card', 'PlanController@updateCard');
        $api->get('plans/invoices', 'PlanController@listSubscriptionInvoices');
        $api->patch('plans/invoices', 'PlanController@updateInvoiceInformation');
        $api->get('plans/invoices/{invoiceId}', 'PlanController@subscriptionInvoice');

        // Order hardware
        $api->post('carts', 'CartController@orderCart');

        // Pusher
        $api->post('pusher/authenticate-presence', 'PusherController@authenticatePresenceChannel');

        // Tenant Routes
        //
        $api->post('tenants','TenantController@store');
		$api->get('tenants','TenantController@index');
        $api->post('tenants/initialize','TenantController@syncInitial');
        $api->post('tenants/add-beacons', 'TenantController@addBeaconsToTenant');
        $api->post('tenants/sync-batches','TenantController@syncBatches');
        $api->post('tenants/end-trial','TenantController@endTrial');
        $api->patch('tenants/activity','TenantController@activity');
        $api->get('tenants/search','TenantController@search');
        $api->get('tenants/batches/{tenantId}','TenantController@showBatches');

        $api->get('facades/permissions/{tenantId}','Features\PermissionsFacadeController@show')->middleware('transform_response');

            $api->get('tenants/modules','Features\ModuleTenantController@index')->middleware('transform_response');
            $api->get('tenants/modules/last-modified','Features\ModuleTenantController@lastModified');
            $api->get('tenants/modules/{tenantId}','Features\ModuleTenantController@show')->middleware('transform_response');
            $api->post('tenants/modules','Features\ModuleTenantController@store');
            $api->delete('tenants/modules','Features\ModuleTenantController@destroy');

            $api->get('tenants/features','Features\FeatureTenantController@index');
            $api->get('tenants/features/last-modified','Features\FeatureTenantController@lastModified');
            $api->get('tenants/features/{tenantId}','Features\FeatureTenantController@show');
            $api->post('tenants/features','Features\FeatureTenantController@store');
            $api->delete('tenants/features','Features\FeatureTenantController@destroy');

        $api->group(['middleware' => ['feature:analytics']], function ($api) {

            // KPIs
            $api->get('kpis', 'Analytics\KpiController@getDayKpis');
            $api->get('kpis/scans', 'KpiController@showScansToday');
            $api->get('kpis/delivered-contents', 'KpiController@showDeliveredContentToday');
            $api->get('kpis/accessed-channels', 'KpiController@showAccessedChannelsToday');
            $api->get('kpis/conversion-rate', 'KpiController@showConversionRateToday');

            // Analytics
            $api->get('analytics/delivered-content', 'AnalyticsController@showDeliveredContent');
            $api->get('analytics/delivered-content/{url}', 'AnalyticsController@showDeliveredContentByUrl');
            $api->get('analytics/delivered-content/pois/{poiId}', 'AnalyticsController@showDeliveredContentByPoi');
            $api->get('analytics/viewed-channels', 'AnalyticsController@showViewedChannels');
            $api->get('analytics/viewed-channels/{url}', 'AnalyticsController@showViewedChannelsByUrl');
            $api->get('analytics/viewed-channels/pois/{poiId}', 'AnalyticsController@showViewedChannelsByPoi');
            $api->get('analytics/scanned-beacons', 'AnalyticsController@showScans');
            $api->get('analytics/scanned-beacons/{url}', 'AnalyticsController@showScansByUrl');
            $api->get('analytics/scanned-beacons/pois/{poiId}', 'AnalyticsController@showScansByPoi');
            $api->get('analytics/pois-with-events', 'AnalyticsController@showPoisWithEvents');

            // Charts
            $api->get('charts/seven-days/viewed-channels/{start}', 'Analytics\ChartController@getSevenDaysViewedChannelsChart');
            $api->get('charts/four-weeks/viewed-channels/{start}', 'Analytics\ChartController@getFourWeeksViewedChannelsChart');
            $api->get('charts/seven-days/delivered-content/{start}', 'Analytics\ChartController@getSevenDaysViewedPoisChart');
            $api->get('charts/four-weeks/delivered-content/{start}', 'Analytics\ChartController@getFourWeeksViewedPoisChart');
            $api->get('charts/seven-days/scanned-beacons/{start}', 'Analytics\ChartController@getSevenDaysScannedBeaconsChart');
            $api->get('charts/four-weeks/scanned-beacons/{start}', 'Analytics\ChartController@getFourWeeksScannedBeaconsChart');

            $api->get('new-charts/four-weeks/{start}', 'Analytics\ChartController@getFourWeeksViewedPoisChart');

        });

        $api->group(['middleware' => ['feature:media']], function ($api) {

            // Content & Mediathek Routes
            $api->get('media/last-modified','MediaController@lastModified');
            $api->get('media','MediaController@index');
            $api->get('media/search','MediaController@search');
            $api->get('media/{mediaId}','MediaController@show');
            $api->post('media','MediaController@store');
            $api->patch('media/{mediaId}','MediaController@update');
            $api->delete('media/{mediaId}','MediaController@destroy');

                // Content POI Routes
                $api->post('media/{mediaId}/pois','MediaPoiController@store');
                $api->delete('media/{mediaId}/pois/{poiId}','MediaPoiController@destroy');

        });

        $api->group(['middleware' => ['feature:poi']], function ($api) {

            // POI Routes
            $api->get('pois/last-modified','PoiController@lastModified');
            $api->get('pois','PoiController@index');
            $api->get('pois/search','PoiController@search');
            $api->get('pois/{poiId}','PoiController@show');
            $api->post('pois','PoiController@store');
            $api->post('pois/previews/{language}','PoiController@createPreview');
            $api->delete('pois/{poiId}','PoiController@destroy');
            $api->patch('pois/{poiId}','PoiController@update');
            $api->patch('facades/pois/{poiId}','PoiFacadeController@update');

                // POI User Routes
                $api->get('pois/{poiId}/users','PoiUserController@index');
                $api->post('pois/{poiId}/users','PoiUserController@store');
                $api->delete('pois/{poiId}/users/{userId}','PoiUserController@destroy');

                // POI Log Routes
                $api->get('pois/{poiId}/log/last-modified','PoiEventController@lastModified');
                $api->get('pois/{poiId}/log','PoiEventController@index');
                $api->post('pois/{poiId}/log','PoiEventController@store');

                // POI Content Routes
                $api->get('pois/{poiId}/contents','PoiContentController@index');
                $api->post('pois/{poiId}/contents','PoiContentController@store');
                $api->patch('pois/{poiId}/contents/{contentId}','PoiContentController@update');
                $api->delete('pois/{poiId}/contents/{contentId}','PoiContentController@destroy');

                // POI Beacon Routes
                $api->get('pois/{poiId}/beacons','PoiBeaconController@index');
                $api->post('pois/{poiId}/beacons/bulk','PoiBeaconController@bulk');
                $api->post('pois/{poiId}/beacons','PoiBeaconController@store');
                $api->delete('pois/{poiId}/beacons/{beaconId}','PoiBeaconController@destroy');

                // POI Feedback Routes
                $api->get('pois/{poiId}/feedbacks','PoiContentFeedbackController@index');

                // POI Sweepstake Routes
                $api->get('pois/{poiId}/sweepstakes','PoiContentSweepstakeController@index');

                // POI Design Routes
                $api->get('pois/{poiId}/design','DesignController@showForPoi');
                $api->post('pois/{poiId}/design','DesignController@storeForPoi');
                $api->patch('pois/{poiId}/design','DesignController@updateForPoi');
                $api->delete('pois/{poiId}/design/','DesignController@destroyForPoi');

                // POI Lock Routes
                $api->get('locks','PoiLockController@index');

                // Module Routes
                $api->post('modules/validate','ModuleController@validateModule');

            // Vouchers
            $api->post('vouchers/proxy', 'VoucherController@proxyBarcode');

        });

        $api->group(['middleware' => ['feature:payment']], function ($api) {

            // Micro Payment Routes
            $api->get('micro-payments/last-modified','MicroPaymentController@lastModified');
            $api->get('micro-payments','MicroPaymentController@index');
            $api->get('micro-payments/search','MicroPaymentController@search');
            $api->get('micro-payments/{microPaymentId}','MicroPaymentController@show');
            $api->post('micro-payments','MicroPaymentController@store');
            $api->delete('micro-payments/{microPaymentId}','MicroPaymentController@destroy');
            $api->patch('micro-payments/{microPaymentId}','MicroPaymentController@update');
            $api->get('micro-payments/{microPaymentId}/transactions','MicroPaymentController@showTransactions');

        });

        $api->group(['middleware' => ['feature:beacon']], function ($api) {

            // Beacon Routes
            $api->get('beacons/last-modified','BeaconController@lastModified');
            $api->get('beacons','BeaconController@index');
            $api->get('beacons/search','BeaconController@search');
            $api->get('beacons/{beaconId}','BeaconController@show');
            $api->post('beacons','BeaconController@store');
            $api->delete('beacons/{beaconId}','BeaconController@destroy');
            $api->patch('beacons/{beaconId}','BeaconController@update');
            $api->patch('facades/beacons/{beaconId}','BeaconFacadeController@update');

            // Beacon User Routes
            $api->get('beacons/{beaconId}/users','BeaconUserController@index');
            $api->post('beacons/{beaconId}/users','BeaconUserController@store');
            $api->delete('beacons/{beaconId}/users/{userId}','BeaconUserController@destroy');

            // Beacon Location Routes
            $api->get('beacons/{beaconId}/location','BeaconLocationController@show');
            $api->post('beacons/{beaconId}/location','BeaconLocationController@store');
            $api->patch('beacons/{beaconId}/location','BeaconLocationController@update');
            $api->delete('beacons/{beaconId}/location','BeaconLocationController@destroy');
            $api->get('beacon-locations/last-modified','BeaconLocationController@lastModified');

        });

        $api->group(['middleware' => ['feature:survey']], function ($api) {

            // Survey Routes
            $api->get('surveys/last-modified','SurveyController@lastModified');
            $api->get('surveys','SurveyController@index');
            $api->get('surveys/{surveyId}','SurveyController@show');
            $api->post('surveys','SurveyController@store');
            $api->delete('surveys/{surveyId}','SurveyController@destroy');
            $api->patch('surveys/{surveyId}','SurveyController@update');

        });

        $api->group(['middleware' => ['feature:admin']], function ($api) {

            // Tenant Routes
            $api->get('tenants/design','DesignController@showForTenant');
            $api->post('tenants/design','DesignController@storeForTenant');
            $api->patch('tenants/design','DesignController@updateForTenant');
            $api->delete('tenants/design','DesignController@resetForTenant');
			$api->get('tenants/last-modified','TenantController@lastModified');
            $api->get('tenants/current','TenantController@show');
			$api->get('tenants/{tenantId}','TenantController@showById');
            $api->patch('tenants/{tenantId}','TenantController@update');
            // $api->post('tenants','TenantController@store');

            // App Routes
            $api->get('apps/last-modified','AppController@lastModified');
            $api->get('apps','AppController@index');
            $api->get('apps/search','AppController@search');
            $api->get('apps/{userId}','AppController@show');
            $api->post('apps','AppController@store');
            $api->patch('apps/{userId}','AppController@update');
            $api->delete('apps/{userId}','AppController@destroy');

            // SDK Routes
            $api->get('sdk/beacons', 'SdkController@showAvailableBeacons')->middleware('app_only');
            $api->post('sdk/beacons/associate', 'SdkController@connectContentToBeacon')->middleware('app_only');
            $api->post('sdk/beacons/disassociate', 'SdkController@disconnectContentFromBeacon')->middleware('app_only');

            // User Routes
            $api->get('users/search','UserController@search');
            $api->get('users','UserController@index');
            $api->post('users','UserController@store');
            $api->delete('users/{userId}','UserController@destroy');

        });

        $api->group(['middleware' => ['feature:newsletter']], function ($api) {

            // Newsletter Routes
            $api->get('newsletter-configurations/last-modified','NewsletterConfigurationController@lastModified');
            $api->get('newsletter-configurations','NewsletterConfigurationController@index');
            $api->get('newsletter-configurations/search','NewsletterConfigurationController@search');
            $api->get('newsletter-configurations/{newsletterConfigurationId}','NewsletterConfigurationController@show');
            $api->post('newsletter-configurations','NewsletterConfigurationController@store');
            $api->patch('newsletter-configurations/{newsletterConfigurationId}','NewsletterConfigurationController@update');
            $api->delete('newsletter-configurations/{newsletterConfigurationId}','NewsletterConfigurationController@destroy');
            $api->post('newsletter-configurations/{newsletterConfigurationId}/subscribe','NewsletterConfigurationController@subscribe');

        });

        // User Routes
        $api->get('users/last-modified','UserController@lastModified');
        $api->get('users/{userId}','UserController@show');
        $api->patch('users/{userId}','UserController@update');

    });

});
