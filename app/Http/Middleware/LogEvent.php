<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\Jobs\EventLogJob;

use App\Traits\WorksWithQueue;

class LogEvent {

    use WorksWithQueue;

    public function handle($request, Closure $next) {

        $response = $next($request);

        if($request->method() != 'GET') {
            $this->dispatchJob(new EventLogJob(Auth::user(),$request->method(),$request->ip(),$request->segments()),'eventlogs');
        }

        return $response;

    }
}
