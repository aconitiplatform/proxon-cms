<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App;
use Cache;

use App\Models\Tenant;

use App\Traits\GeneratesCacheKeys;

class IsUserActive {

    use GeneratesCacheKeys;

    public function handle($request, Closure $next) {

        $user = Auth::user();

        if(Cache::has($this->getTenantRootCacheKey('tenant'))) {
            $tenant = Cache::get($this->getTenantRootCacheKey('tenant'));
        } else {
            $tenant = Tenant::find($user->tenant_id);
            Cache::forever($this->getTenantRootCacheKey('tenant'),$tenant);
        }

        if($user->active && $tenant->active) {
            App::setLocale($user->language);
            return $next($request);
        }

        return response()->error(trans('api.NOT_ACTIVE'),401);

    }
}
