<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasFeature {

    public function handle($request, Closure $next, $featureSlug = null) {

        if($featureSlug == null || config('substance.feature_check_active') == false) {
            return $next($request);
        } else {

            $tenant = Auth::user()->tenant;

            $feature = $tenant->features()->where('slug',$featureSlug)->count();

            if($feature == 0) {
                return response()->error(trans('api.NOT_FEATURE'),401);
            } else {
                return $next($request);
            }

        }

    }
}
