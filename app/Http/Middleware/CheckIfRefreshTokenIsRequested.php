<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Middleware\BaseMiddleware as ExternalBaseMiddleware;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

use Closure;

class CheckIfRefreshTokenIsRequested extends ExternalBaseMiddleware {

    public function handle($request, Closure $next) {

        $response = $next($request);

        if($request->header('X-JWT-Token') == 'refresh') {

            try {
                $newToken = $this->auth->setRequest($request)->parseToken()->refresh();
            } catch (TokenExpiredException $e) {
                return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
            } catch (JWTException $e) {
                return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
            }

            $response->headers->set('X-JWT-Token', $newToken);
            $response->headers->set('X-JWT-TTL', config('jwt.ttl'));

        }

        return $response;

    }
}
