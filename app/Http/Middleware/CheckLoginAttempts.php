<?php

namespace App\Http\Middleware;

use Closure;
use Cache;

use App\Models\PasswordReset;

use App\Jobs\UnlockAccountEmailJob;

use App\Traits\WorksWithQueue;
use App\Traits\SendsUnlockAccountEmail;

class CheckLoginAttempts {

    use WorksWithQueue;
	use SendsUnlockAccountEmail;

    public function handle($request, Closure $next) {

        $email = $request->input('email');
        $cacheKey = 'login-attempts:'.$email;

        if(!Cache::has($cacheKey) || (Cache::has($cacheKey) && Cache::get($cacheKey) < 3)) {

            return $next($request);

        }

        $this->sendResetToken($email);
        return response()->error(trans('api.429'),429);

    }

	/*
    private function sendResetToken($email) {

        $cacheKey = 'unlock-emails:'.$email;

        if(!Cache::has($cacheKey)) {

            $reset = PasswordReset::create([
                'email' => $email,
                'token' => str_random(10),
            ]);

            $token = $reset->token;

            $this->dispatchJob(new UnlockAccountEmailJob(compact('email', 'token')));

            Cache::forever($cacheKey,time());

        }

    }
	*/

}
