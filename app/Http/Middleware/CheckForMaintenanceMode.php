<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckForMaintenanceMode {

    protected $app;

    public function __construct(Application $app) {
        $this->app = $app;
    }

    public function handle($request, Closure $next) {

        if ($this->app->isDownForMaintenance() && $request->header('Accept') == 'application/x.'.config('api.subtype').'.v1+json') {
            return response()->maintenance();
        }

        if ($this->app->isDownForMaintenance()) {
            throw new HttpException(503);
        }

        return $next($request);
    }

}
