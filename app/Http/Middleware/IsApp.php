<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsApp {

    public function handle($request, Closure $next) {

        if(Auth::user()->role == 'app') {
            return $next($request);
        }

        return response()->error(trans('api.APP_ONLY'),401);

    }
}
