<?php

namespace App\Http\Middleware;

use Carbon\Carbon;

use Closure;
use Auth;

use App\Models\User;

class IsEmailConfirmed {

    public function handle($request, Closure $next) {

        $email = $request->input('email');

        $user = User::where('email',$email)->first();

        if($user != null) {

            $now = Carbon::now();
            $confirmation = Carbon::parse($user->email_confirmation_sent_at);

            if($user->email_confirmed == false && $confirmation->diffInDays($now) > config('substance.email.confirmation_period')) {

                $user->active = false;
                $user->save();
                return response()->error(trans('api.CONFIRMATION_EXPIRED'),401);

            }

        }

        return $next($request);

    }
}
