<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\User;

class BySuperadmin {

    public function handle($request, Closure $next) {

        if($request->header('X-Superadmin')) {

            $superadmin = User::where('id',$request->header('X-Superadmin'))->where('role','superadmin')->first();

            if($superadmin != null) {
                $request['by_superadmin'] = $superadmin->id;
            }

        }

        return $next($request);

    }
}
