<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;

use App\Models\Tenant;

use App\Traits\GeneratesCacheKeys;
use App\Traits\InvalidatesCache;

class FirstLogin {

    use GeneratesCacheKeys, InvalidatesCache;

    public function handle($request, Closure $next) {

        $response = $next($request);

        if(Auth::user() != null) {

            $tenant = Auth::user()->tenant;

            if($tenant->first_login == true && Auth::user()->role == 'admin') {

                $tenant->first_login = false;
                $tenant->save();
                Cache::forever($this->getTenantRootCacheKey('tenant'),$tenant);

                $this->invalidateAndModify(
                    [
                        'object' => new Tenant()
                    ]
                );

            }

        }

        return $response;

    }
}
