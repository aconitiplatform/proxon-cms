<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TransformResponse {

    public function handle($request, Closure $next) {

        $response = $next($request);
        config(['substance.request.forces_objects' => true]);
        return $response;

    }
}
