<?php

namespace App\Http\Services\Apis;

use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;

use App\Http\Services\Service;

class ReCaptchaService extends Service {

    const URL = 'https://www.google.com/recaptcha/api/';

    public static function verify($payload) {

        $endpoint = 'siteverify';

        $client = self::getHttpClient(true);

        $response = $client->request('POST', $endpoint, [
            'form_params' => $payload
        ]);

        $body = $response->getBody();
        $data = json_decode($body->getContents());
        return $data;

    }


    private static function getHttpClient($withBodyHeaders = false) {

        return new Client([
            'base_uri' => self::URL,
            'timeout'  => 2.0,
            'headers' => self::getHeaders($withBodyHeaders)
        ]);

    }

    private static function getHeaders($withBody = false) {

        $headers = [
            'Accept' => self::getAcceptHeader(),
        ];

        if($withBody) {
            $headers['Content-Type'] = self::getContentType();
        }

        return $headers;

    }

    private static function getAcceptHeader() {

        return 'application/json';

    }


    private static function getContentType() {
        return 'application/x-www-form-urlencoded';
    }

}
