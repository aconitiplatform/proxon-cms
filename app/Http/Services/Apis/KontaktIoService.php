<?php

namespace App\Http\Services\Apis;

use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;

use Cache;
use Auth;

use App\Http\Services\Service;

class KontaktIoService extends Service {

    const URL = 'https://api.kontakt.io/';

    public static function createVenue($name,$description) {

        $endpoint = 'venue/create';

        $client = self::getHttpClient(true);

        $response = $client->request('POST', $endpoint, [
            'form_params' => [
                'name' => $name." ".config('substance.url'),
                'description' => $description
            ]
        ]);

        if($response->getStatusCode() == 201) {

            $body = $response->getBody();

            $data = json_decode($body->getContents());

            return $data;

        }

        return null;

    }

    public static function getVenue($id) {

        $endpoint = 'venue/'.$id;

        $client = self::getHttpClient(true);

        $response = $client->request('GET', $endpoint);

        if($response->getStatusCode() == 200) {

            $body = $response->getBody();

            $data = json_decode($body->getContents());

            return $data;

        }

        return null;

    }

    public static function getVenueDevices($id) {

        $venue = self::getVenue($id);

        if($venue != null) {
            return $venue->devices;
        }

        return null;

    }

    public static function updateDeviceUrl($uniqueId,$url) {

        $endpoint = 'config/create';

        $client = self::getHttpClient(true);

        $response = $client->request('POST', $endpoint, [
            'form_params' => [
                'uniqueId' => $uniqueId,
                'url' => $url,
                'deviceType' => 'beacon',
                'txPower' => 5,
                'interval' => 1000,
                'packets' => 'eddystone_url',
                'profiles' => 'EDDYSTONE',
                'name' => 'Proxon'
            ]
        ]);

        return $response->getStatusCode();

    }

    private static function getHttpClient($withBodyHeaders = false) {

        return new Client([
            'base_uri' => self::URL,
            'timeout'  => 2.0,
            'headers' => self::getHeaders($withBodyHeaders)
        ]);

    }

    private static function getHeaders($withBody = false) {

        $headers = [
            'Accept' => self::getAcceptHeader(),
            'Api-Key' => self::getApiKey(),
        ];

        if($withBody) {
            $headers['Content-Type'] = self::getContentType();
        }

        return $headers;

    }

    private static function getAcceptHeader() {

        return 'application/vnd.com.kontakt+json; version='.config('substance.kontakt_io.api_version');

    }

    private static function getApiKey() {
        return config('substance.kontakt_io.api_key');
    }

    private static function getContentType() {
        return 'application/x-www-form-urlencoded';
    }

}
