<?php

namespace App\Http\Services\Apis;

use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Client;

use Cache;
use Auth;

use App\Http\Services\Service;

class EasybillService extends Service {

    const URL = 'https://api.easybill.de/rest/v1/';

    public static function getCustomers($limit=1000) {

        $endpoint = 'customers?limit='.$limit;

        $client = self::getHttpClient(false);
		try {
			$response = $client->request('GET', $endpoint);

			if($response->getStatusCode() == 200) {

				$body = $response->getBody();

				$data = json_decode($body->getContents());

				return $data;

			}
		} catch (\Exception $e) {
			return null;
		}

        return null;

    }

    public static function getDocumentPdf($documentId) {

        $endpoint = 'documents/'.$documentId.'/pdf';

        $client = self::getHttpClient(false);

        $response = $client->request('GET', $endpoint);

        if($response->getStatusCode() == 200) {

            $body = $response->getBody();

            $data = $body->getContents();

            return $data;

        }

        return null;

    }

    public static function createDocument($document) {

        $endpoint = 'documents';

        $client = self::getHttpClient(true);

        do {

            $response = $client->request('POST', $endpoint, [
                'json' => $document
            ]);

            if($response->getStatusCode() != 201) {
                sleep(10);
            }

        } while ($response->getStatusCode() != 201);

        $body = $response->getBody();
        $data = json_decode($body->getContents());
        return $data;

    }

    public static function setDocumentDone($documentId) {

        $endpoint = 'documents/'.$documentId.'/done';

        $client = self::getHttpClient(true);

        do {

            $response = $client->request('PUT', $endpoint, []);
            $doneStatus = $response->getStatusCode();

            if($doneStatus != 200) {
                sleep(10);
            }

        } while ($doneStatus != 200);

        $body = $response->getBody();
        $data = json_decode($body->getContents());
        return $data;

    }

    public static function sendDocumentPerEmail($documentId) {

        $endpoint = 'documents/'.$documentId.'/send/email';

        $client = self::getHttpClient(true);

        do {

            $response = $client->request('POST', $endpoint, []);

            if($response->getStatusCode() != 204) {
                sleep(10);
            }

        } while ($response->getStatusCode() != 204);

        return $response->getStatusCode();

    }

    private static function getHttpClient($withBodyHeaders = false) {

        return new Client([
            'base_uri' => self::URL,
            'timeout'  => 10.0,
            'headers' => self::getHeaders($withBodyHeaders)
        ]);

    }

    private static function getHeaders($withBody = false) {

        $headers = [
            'Authorization' => self::getBearerToken(),
        ];

        if($withBody) {
            $headers['Content-Type'] = self::getContentType();
        }

        return $headers;

    }

    private static function getBearerToken() {

        return 'Bearer '.self::getApiKey();

    }

    private static function getApiKey() {

        return config('substance.easybill.api_key');

    }

    private static function getContentType() {

        return 'application/json';

    }

}
