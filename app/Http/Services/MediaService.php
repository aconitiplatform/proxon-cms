<?php

namespace App\Http\Services;

use Image;
use Storage;
use DB;

use App\Models\User;

use App\Http\Services\Service;

class MediaService extends Service {

    public static function getStoreMediaValidationRules() {
        return [
            'name' => 'required|string|min:3|max:255',
            'file' => 'required|file',
            'language' => 'sometimes|string|size:2',
            'locale' => 'sometimes|string|size:5',
            'used_for' => 'required|in:media,voucher'
        ];
    }

    public static function getUpdateMediaValidationRules() {
        return [
            'name' => 'sometimes|string|min:3|max:255',
            'language' => 'sometimes|string|size:2',
            'locale' => 'sometimes|string|size:5',
        ];
    }

    public static function getStoreMediaPoiValidationRules() {
        return [
            'id' => 'required|string|size:36|exists:pois,id',
        ];
    }

    public static function getDeleteMediaPoiValidationRules() {
        return [
            'poi_id' => 'required|string|size:36|exists:pois,id',
        ];
    }

    public static function getAllowedMimeTypes() {
        return [
            'audio/mpeg',
            'audio/mp3',
            'audio/wav',
            'video/mp4',
            'video/webm',
            'image/jpg',
            'image/jpeg',
            'image/png',
            'image/gif',
        ];
    }

    public static function insertMediaPoiRelation($mediaId,$poiId) {
        self::insertObjectObjectRelation($mediaId,$poiId,'media_id','poi_id','media_poi');
    }

    public static function deleteMediaPoiRelation($mediaId,$poiId) {
        self::deleteObjectObjectRelation($mediaId,$poiId,'media_id','poi_id','media_poi');
    }

    public static function getMediaPoiRelation($mediaId,$poiId) {
        return self::getObjectObjectRelation($mediaId,$poiId,'media_id','poi_id','media_poi');
    }

    public static function getAllMediaPoiRelations($poiId) {
        return DB::table('media_poi')->select('media_id')->where('poi_id',$poiId)->get();
    }

    public static function addUsersToMedia($mediaId,$tenantId,$createdByUserId) {

        $creatorHasBeenAdded = false;

        $users = User::where('tenant_id',$tenantId)->whereIn('role',self::getRolesButSubuser())->get();

        foreach($users as $user) {

            self::insertMediaUserRelation($mediaId,$user->id);
            if($user->id == $createdByUserId) {
                $creatorHasBeenAdded = true;
            }

        }

        if(!$creatorHasBeenAdded) {
            self::insertMediaUserRelation($mediaId,$createdByUserId);
        }

    }

    public static function insertMediaUserRelation($mediaId,$userId) {
        self::insertObjectUserRelation($mediaId,$userId,'media_user','media_id');
    }

    public static function deleteMediaUserRelation($mediaId,$userId) {
        self::deleteObjectUserRelation($mediaId,$userId,'media_user','media_id');
    }

    public static function getMediaUserRelation($mediaId,$userId) {
        $relation = self::getObjectUserRelation($mediaId,$userId,'media_user','media_id');
        return $relation;
    }

    public static function storeFile($file,$tenantId,$fileNameWithExtension,$isImage) {

        if($isImage) {
            $pathReal = $tenantId.'/'.$fileNameWithExtension;
            $image = Image::make($file->getRealPath())->orientate();
            $image = $image->stream();
            Storage::disk(config('substance.storage'))->put($pathReal,$image->__toString());
        } else {
            Storage::disk(config('substance.storage'))->put(
                $tenantId.'/'.$fileNameWithExtension,
                file_get_contents($file->getRealPath())
            );
        }
        return $tenantId.'/'.$fileNameWithExtension;

    }

    public static function removeFileFromStorage($media) {

        $pathReal = $media->tenant_id.'/'.$media->storage_path;
        Storage::disk(config('substance.storage'))->delete($pathReal);
        $pathToThumb = $media->tenant_id.'/preview_'.$media->storage_path;

        if(Storage::disk(config('substance.storage'))->exists($pathToThumb)) {
            Storage::disk(config('substance.storage'))->delete($pathToThumb);
        }

    }

    public static function generateMediaFileNameWithExtension($file,$user) {

        $fileName = hash('sha1',$user->id.time().$file->getClientOriginalName());
        $extension = $file->guessExtension();
        return $fileName.'.'.$extension;

    }

}
