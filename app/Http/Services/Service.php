<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use Dingo\Api\Exception\ValidationHttpException;

use Validator;
use Cache;
use DB;
use Log;

use App\Traits\GeneratesCacheKeys;
use App\Traits\WorksWithQueue;

/*
    Dependencies shall be declared in the following order; groups shall be separated by an empty line:
    Laravel & External
    Laravel Facades
    Models
    Controllers
    Services
    Traits
    Jobs
*/

class Service {

    use GeneratesCacheKeys, WorksWithQueue;

    public static function getRolesButSubuser() {
        return [
            'user',
            'admin',
            'app',
            'superadmin'
        ];
    }

    public static function getInvalidatableUserRoles() {
        return self::getRolesButSubuser();
    }

    public static function getPartialUpdateValues(Request $request,$rules,$setNull = false,$setEmptyString = false,$sets = []) {

        $updateData = [];

        foreach($rules as $key => $value) {

            if($request->has($key)) {
                $updateData[$key] = $request->input($key);
            } else {
                if(in_array($key,array_keys($sets))) {
                    $updateData[$key] = $sets[$key];
                } else {
                    if($setNull) {
                        $updateData[$key] = null;
                    }
                    if($setEmptyString) {
                        $updateData[$key] = '';
                    }
                }

            }

        }

        return $updateData;

    }

    public static function generateRandomString($length = 6,$withUpperCase = false,$prefix = "") {

		$str = "";
        $characters = array_merge(range('a','z'), range('0','9'));

        if($withUpperCase) {
            $characters = array_merge($characters,range('A','Z'));
        }

		$max = count($characters) - 1;

		for ($i = 0; $i < $length; $i++) {

			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];

		}

		return $prefix.$str;

    }

    public static function sanitizeArray($data, array $rules, array $messages = [], array $customAttributes = [], $strict = true) {

        if(is_array($data)) {
            $data = self::removeInvalidIndizes($data,$rules);
            if(!$strict) $data['errors'] = self::validateArray($data,$rules,$messages,$customAttributes,$strict);
        }

        return $data;

    }

    public static function removeInvalidIndizes(array $data, array $rules) {

        foreach($data as $k => $v) {

            if(!array_key_exists($k,$rules)) {
                unset($data[$k]);
            }

        }

        return $data;

    }

    public static function validateArray(array $data, array $rules, array $messages = [], array $customAttributes = [], $strict = true) {

        $validator = Validator::make($data, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            if(!$strict) {
                return $validator->errors();
            } else {
                throw new ValidationHttpException($validator->errors());
            }
        }

    }

    public static function insertObjectObjectRelation($oId1,$oId2,$oCol1,$oCol2,$table) {
        self::insertRelation($oId1,$oId2,$oCol1,$oCol2,$table);
    }

    public static function deleteObjectObjectRelation($oId1,$oId2,$oCol1,$oCol2,$table) {

        $cacheKey = 'relations:'.$table.':'.$oId1.':'.$oCol2.':'.$oId2;
        self::deleteRelation($oId1,$oId2,$oCol1,$oCol2,$table);

        if(Cache::has($cacheKey)) {
            Cache::forget($cacheKey);
        }

    }

    public static function getObjectObjectObjectRelation($oId1,$oId2,$oCol1,$oCol2,$table) {

        $cacheKey = 'relations:'.$table.':'.$oId1.':'.$oCol2.':'.$oId2;

        if(Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        } else {

            $relation = DB::table($table)->select($oCol1,$oCol2)->where($oCol1,$oId1)->where($oCol2,$oId2)->first();

            if(empty($relation)) {
                return null;
            }

            Cache::forever($cacheKey,$relation);

            return $relation;

        }

    }

    public static function insertObjectUserRelation($objectId,$userId,$table,$objectColumn) {
        self::insertRelation($objectId,$userId,$objectColumn,'user_id',$table);
    }

    public static function deleteObjectUserRelation($objectId,$userId,$table,$objectColumn) {

        $cacheKey = 'relations:'.$table.':'.$userId.':'.$objectColumn.':'.$objectId;
        self::deleteRelation($objectId,$userId,$objectColumn,'user_id',$table);

        if(Cache::has($cacheKey)) {
            Cache::forget($cacheKey);
        }

    }

    public static function getObjectUserRelation($objectId,$userId,$table,$objectColumn) {

        $cacheKey = 'relations:'.$table.':'.$userId.':'.$objectColumn.':'.$objectId;

        if(Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        } else {

            $relation = DB::table($table)->select($objectColumn,'user_id','added_automatically')->where($objectColumn,$objectId)->where('user_id',$userId)->first();

            if(empty($relation)) {
                return null;
            }

            Cache::forever($cacheKey,$relation);

            return $relation;

        }

    }

    public static function invalidateCache($entityId,$tenantId,$triggeringUserId,$redisKey) {

        $roles = self::getInvalidatableUserRoles();

        foreach($roles as $role) {

            $index = $tenantId.':users:'.$role.':'.$redisKey.':index';
            $entity = $tenantId.':users:'.$role.':'.$redisKey.':'.$entityId;
            self::deleteCacheKey($index);
            self::deleteCacheKey($entity);

        }

        $index = $tenantId.':users:'.$triggeringUserId.':'.$redisKey.':index';
        $entity = $tenantId.':users:'.$triggeringUserId.':'.$redisKey.':'.$entityId;
        self::deleteCacheKey($index);
        self::deleteCacheKey($entity);

    }

    public static function deleteCacheKey($cacheKey) {
        Cache::forget($cacheKey);
    }

    public static function updateLastModified($tenantId,$model) {

        $modelAsString = get_class($model);

        $result = DB::table('last_modified')->select('model','timestamp')->where('model',$modelAsString)->where('tenant_id',$tenantId)->get();

        $updatedAt = time();

        if($result == null) {

            DB::table('last_modified')->insert([
                'model' => $modelAsString,
                'tenant_id' => $tenantId,
                'timestamp' => $updatedAt
            ]);

        } else {

            DB::table('last_modified')->where('model',$modelAsString)->where('tenant_id',$tenantId)->update([
                'timestamp' => $updatedAt
            ]);

        }

        Cache::forever(self::getTenantRootCacheKeyStatically('last-modified:'.$modelAsString,$tenantId),$updatedAt);

    }

    public static function getLastModified($tenantId,$model,$inMilliseconds = true) {

        $modelAsString = get_class($model);

        if(Cache::has(self::getTenantRootCacheKeyStatically('last-modified:'.$modelAsString))) {
            $updatedAt = Cache::get(self::getTenantRootCacheKeyStatically('last-modified:'.$modelAsString));
        } else {

            $result = DB::table('last_modified')->select('timestamp')->where('model',$modelAsString)->where('tenant_id',$tenantId)->get();

            if($result != null) {

                $updatedAt = $result[0]->timestamp;
                Cache::forever(self::getTenantRootCacheKeyStatically('last-modified:'.$modelAsString),$updatedAt);

            } else {
                $updatedAt = null;
            }

        }

        if($inMilliseconds && $updatedAt != null) {
            $updatedAt = $updatedAt * 1000;
        }

        return array(
            'updated_at' => $updatedAt
        );

    }

    public static function filterEmptyValues($array) {

        $array = array_filter($array);

        unset($array['contents']);

        if(empty($array)) {
            $array['contents'] = 'none';
        }

        return $array;

    }

    public static function toBooleanKeys($hasOldKeys,$getsBooleanKeys = []) {

        foreach($hasOldKeys as $k => $v) {
            $getsBooleanKeys['has_'.$k] = $v;
        }

        return $getsBooleanKeys;

    }

    private static function insertRelation($id1,$id2,$col1,$col2,$table) {

        DB::table($table)->insert([
            $col1 => $id1,
            $col2 => $id2
        ]);

    }

    private static function deleteRelation($id1,$id2,$col1,$col2,$table) {
        DB::table($table)->where($col1,$id1)->where($col2,$id2)->delete();
    }

    public static function getLogoSettingsValidationRules($isDesign = false) {

        $default = '';

        if($isDesign) {
            $default = ',default';
        }

        return [
            'type' => 'required|in:none,url,id'.$default,
            'id' => 'required_if:type,id|exists:media,id',
            'url' => 'required_if:type,url|url',
        ];
    }

}
