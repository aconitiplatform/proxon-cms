<?php

namespace App\Http\Services;

use Illuminate\Http\Request;

use App\Http\Services\Service;

class DesignService extends Service {

    public static function getStoreDesignValidationRules($model) {

        if($model == 'App\Models\PoiDesign') {
            $default = ',default';
        } else {
            $default = '';
        }
        // dd($model);
        return [
            'header_fixed' => 'required|in:fixed,scrollable'.$default,
            'header_logo' => 'required|in:visible,invisible'.$default,
            'header_title' => 'required|in:visible,invisible'.$default,
            'header_visible' => 'required|in:visible,invisible'.$default,
            'header_format' => 'sometimes|string|in:all_center,all_left,all_right'.$default,
            'header' => 'sometimes|array',
            'h1' => 'sometimes|array',
            'h2' => 'sometimes|array',
            'h3' => 'sometimes|array',
            'body' => 'sometimes|array',
            'footer' => 'sometimes|array',
            'footer_format' => 'sometimes|string|in:all_center,logo_left_content_right,logo_right_content_left'.$default,
            'header_logo_settings' => 'sometimes|array',
            'footer_logo_settings' => 'sometimes|array',
            'footer_logo' => 'required|in:visible,invisible'.$default,
            'header_logo_height' => 'required|string',
            'footer_logo_height' => 'required|string',
            'fonts' => 'required|font_list',
            'edward_button' => 'sometimes|array',
            'edward_app' => 'sometimes|array',
        ];
    }

    public static function getUpdateDesignValidationRules($model) {

        if($model == 'App\Models\PoiDesign') {
            $default = ',default';
        } else {
            $default = '';
        }

        return [
            'header_fixed' => 'sometimes|in:fixed,scrollable'.$default,
            'header_logo' => 'sometimes|in:visible,invisible'.$default,
            'header_title' => 'sometimes|in:visible,invisible'.$default,
            'header_visible' => 'sometimes|in:visible,invisible'.$default,
            'header_format' => 'sometimes|string|in:all_center,all_left,all_right'.$default,
            'header' => 'sometimes|array',
            'h1' => 'sometimes|array',
            'h2' => 'sometimes|array',
            'h3' => 'sometimes|array',
            'body' => 'sometimes|array',
            'footer' => 'sometimes|array',
            'footer_format' => 'sometimes|string|in:all_center,logo_left_content_right,logo_right_content_left'.$default,
            'header_logo_settings' => 'sometimes|array',
            'footer_logo_settings' => 'sometimes|array',
            'footer_logo' => 'sometimes|in:visible,invisible'.$default,
            'header_logo_height' => 'sometimes|string',
            'footer_logo_height' => 'sometimes|string',
            'fonts' => 'sometimes|font_list',
            'edward_button' => 'sometimes|array',
            'edward_app' => 'sometimes|array',
        ];
    }

    public static function getTenantDesignBlueprint() {
        return [
            'header_fixed' => 'fixed', //scrollable, default
            'header_logo' => 'visible', //invisible, default
            'header_title' => 'visible', //invisible, default
            'header_visible' => 'visible', //invisible, default
            'header_format' => 'all_left', //all_center, all_right, default
            'header_logo_height' => '50px',
            'header' => [
                'color' => '#ffffff',
                'background-color' => '#bb1c3e',
                'height' => '70px',
                'padding-top' => '10px',
                'padding-left' => '10px',
                'padding-right' => '10px',
                'padding-bottom' => '10px',
            ],
            'h1' => [
                'color' => '#021A24',
                'font-size' => '26px',
            ],
            'h2' => [
                'color' => '#021A24',
                'font-size' => '24px',
            ],
            'h3' => [
                'color' => '#021A24',
                'font-size' => '22px',
            ],
            'body' => [
                'color' => '#021A24',
                'font-family' => '',
            ],
            'footer' => [
                'color' => '#FFFFFF',
                'background-color' => '#021A24',
                'height' => '110px',
                'padding-top' => '10px',
                'padding-left' => '10px',
                'padding-right' => '10px',
                'padding-bottom' => '10px',
                'font-size' => '11px',
            ],
            'footer_format' => 'all_center',
            'header_logo_settings' => [
                'type' => 'none'
            ],
            'footer_logo_settings' => [
                'type' => 'none'
            ],
            'footer_logo' => 'visible',
            'footer_logo_height' => '50px',
            'fonts' => [],
            'edward_button' => [
                'color' => '',
                'font-size' => '',
                'font-weight' => '',
                'text-transformation' => '',
                'background-color' => '',
                'border-color' => '',
                'border-width' => '',
                'border-radius' => '',
                'padding-top' => '',
                'padding-right' => '',
                'padding-bottom' => '',
                'padding-left' => '',
            ],
            'edward_app' => [
            	'background' => '',
                'padding-left' => '',
                'padding-right' => '',
            ],
        ];
    }

    public static function getPoiDesignBlueprint() {
        return [
            'header_fixed' => 'default',
            'header_logo' => 'default',
            'header_title' => 'default',
            'header_visible' => 'default',
            'header_format' => 'default',
            'header' => [
                'color' => '',
                'background-color' => '',
                'height' => '',
                'padding-top' => '',
                'padding-left' => '',
                'padding-right' => '',
                'padding-bottom' => '',
            ],
            'h1' => null,
            'h2' => null,
            'h3' => null,
            'body' => [
                'color' => '',
                'font-family' => '',
            ],
            'footer' => [
                'color' => '',
                'background-color' => '',
                'height' => '',
                'padding-top' => '',
                'padding-left' => '',
                'padding-right' => '',
                'padding-bottom' => '',
                'font-size' => '',
            ],
            'footer_format' => 'default',
            'header_logo_settings' => [
                'type' => 'default'
            ],
            'footer_logo_settings' => [
                'type' => 'default'
            ],
            'footer_logo' => 'default',
            'fonts' => [],
            'edward_button' => [
                'color' => '',
                'font-size' => '',
                'font-weight' => '',
                'text-transform' => '',
                'background-color' => '',
                'border-color' => '',
                'border-width' => '',
                'border-radius' => '',
                'padding-top' => '',
                'padding-right' => '',
                'padding-bottom' => '',
                'padding-left' => '',
            ],
            'edward_app' => [
            	'background' => '',
                'padding-left' => '',
                'padding-right' => '',
            ],
        ];
    }

    public static function validateLogoSettings(Request $request) {

        if($request->has('header_logo_settings')) {
            self::validateArray($request['header_logo_settings'], self::getLogoSettingsValidationRules(true));
        }

        if($request->has('footer_logo_settings')) {
            self::validateArray($request['footer_logo_settings'], self::getLogoSettingsValidationRules(true));
        }

    }

}
