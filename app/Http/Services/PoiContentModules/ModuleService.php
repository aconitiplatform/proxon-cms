<?php

namespace App\Http\Services\PoiContentModules;

use App\Http\Services\Service;

use Log;

use App\Traits\ValidatesModules;
use App\Traits\EscapesModules;

class ModuleService extends Service {

    use ValidatesModules, EscapesModules;

    public static function getPoiContentModuleValidationRules() {
        return [
            'label' => 'sometimes|string|min:1|max:128',
            'type' => 'required|in:text,image,video,audio,button,wikipedia,link,survey,newsletter,divider,heading,social_icons,feedback,micro_payment,contact,sweepstake,icon,spotify,voucher,image_gallery,vag',
            'data' => 'required|array',
            'hidden' => 'sometimes|boolean',
        ];
    }

    public static function isRelationableModule(array $module) {

        $relationableModule = [
            'video',
            'audio',
            'image',
            'voucher',
            'image_gallery',
        ];

        $ids = [];

        if(in_array($module['type'],$relationableModule)) {

            switch ($module['type']) {
                case 'video':
                    if(isset($module['data']['webm_id'])) {
                        $ids[] = $module['data']['webm_id'];
                    }
                    if(isset($module['data']['mp4_id'])) {
                        $ids[] = $module['data']['mp4_id'];
                    }
                    break;
                case 'audio':
                    if(isset($module['data']['id'])) {
                        $ids[] = $module['data']['id'];
                    }
                    break;
                case 'image':
                    if(isset($module['data']['id'])) {
                        $ids[] = $module['data']['id'];
                    }
                    break;
                case 'voucher':
                    if(isset($module['data']['image_id'])) {
                        $ids[] = $module['data']['image_id'];
                    }
                    break;
                case 'image_gallery':
                    foreach($module['data']['images'] as $image) {
                        $ids[] = $image['id'];
                    }
                    break;
                default:
                    return $ids;
            }

            return $ids;

        } else {
            return $ids;
        }

    }

    public static function getFlaggableModules() {

        $flaggableModules = [
            'sweepstake',
            'feedback',
            'contact',
            'micro_payment'
        ];

        return $flaggableModules;

    }

    public static function getUuidableModules() {

        $uuidableModules = [
            'sweepstake',
            'feedback',
            'voucher',
        ];

        return $uuidableModules;

    }

}
