<?php

namespace App\Http\Services\PoiContentModules;

use Ramsey\Uuid\Uuid;

use Log;

use App\Http\Services\Service;
use App\Http\Services\PoiContentModules\ModuleService;

use App\Jobs\CreateFeedbackAtFirebaseJob;

class GroupService extends Service {

    public static function getPoiContentGroupValidationRules() {
        return [
            'time_from' => 'required|numeric',
            'time_to' => 'sometimes|numeric',
            'weekdays' => 'required|array',
            'weekdays_active' => 'required|boolean',
            'label' => 'required|string|max:255',
            'modules' => 'required|array',
            'css' => 'sometimes|array',
			'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
            'show' => 'required|string|in:beacon,not_beacon,always',
            'hidden' => 'sometimes|boolean',
			'delete_when_expired' => 'sometimes|boolean',
        ];
    }

    public static function getPoiContentGroupWeekdaysValidationRules() {

        return [
            'mon' => 'required|array',
            'tue' => 'required|array',
            'wed' => 'required|array',
            'thu' => 'required|array',
            'fri' => 'required|array',
            'sat' => 'required|array',
            'sun' => 'required|array',
        ];

    }

    public static function getPoiContentGroupWeekdayValidationRules() {

        return [
            'active' => 'required|boolean',
            'times' => 'sometimes|array',
        ];

    }

    public static function getPoiContentGroupWeekdayTimeValidationRules() {

        return [
            'time_from' => 'required|string|size:5',
            'time_to' => 'required|string|size:5',
        ];

    }

    public static function preparePoiContent(array $dataArray, string $poiId, string $language, $isPreview = false) {

        $flags = self::falseFlagModules();

        foreach($dataArray['groups'] as $groupKey => $group) {

            if(!isset($dataArray['groups'][$groupKey]['show'])) {
                $dataArray['groups'][$groupKey]['show'] = 'always';
            }

            $dataArray['groups'][$groupKey] = self::sanitizeArray($dataArray['groups'][$groupKey],self::getPoiContentGroupValidationRules());
            $dataArray['groups'][$groupKey]['weekdays'] = self::sanitizeArray($dataArray['groups'][$groupKey]['weekdays'],self::getPoiContentGroupWeekdaysValidationRules());

            $dataArray['groups'][$groupKey]['css']['x'] = ['k'=>'v'];
            $dataArray['groups'][$groupKey]['css'] = self::removeEmptyCssValues($dataArray['groups'][$groupKey]['css']);

            $dataArray['groups'][$groupKey]['weekdays'] = self::sanitizeWeekdays($dataArray['groups'][$groupKey]['weekdays']);

            $preparedModuleData = self::prepareModules($dataArray['groups'][$groupKey]['modules'], $poiId, $language, $flags, $isPreview);

            $dataArray['groups'][$groupKey]['modules'] = $preparedModuleData['modules'];
            $flags = $preparedModuleData['flags'];

        }

        $preparedData['data'] = $dataArray;
        $preparedData['flags'] = $flags;

        return $preparedData;

    }

    private static function prepareModules($modules, $poiId, $language, $flags, $isPreview = false) {

        $data['flags'] = $flags;

        foreach($modules as $k => $module) {

            $modules[$k] = self::sanitizeArray($module,ModuleService::getPoiContentModuleValidationRules(),[],[],false);

            if(isset($modules[$k]['data'])) {
                $modules[$k]['data'] = self::sanitizeArray($module['data'],ModuleService::getPoiContentModuleValidationRulesByType($module['type']),[],[],false);

                if(self::isUuidable($module)) {
                    $modules[$k]['data']['uuid'] = Uuid::uuid4();
                }

                if($module['type'] == 'feedback' && !isset($module['data']['uuid']) && !$isPreview) {
                    self::dispatchJobStatically(new CreateFeedbackAtFirebaseJob($modules[$k]['data']['uuid'],$poiId,$language,$modules[$k]['label']),'firebase');
                }

                if(!$isPreview) {
                    $data['flags'] = self::flagForInteraction($data['flags'],$module);
                }

                $modules[$k]['data'] = ModuleService::escapePoiContentModuleStringsByType($module['type'],$modules[$k]['data']);

                if(isset($modules[$k]['data']['css'])) {

                    $modules[$k]['data']['css']['x'] = ['k'=>'v'];
                    $modules[$k]['data']['css'] = self::removeEmptyCssValues($modules[$k]['data']['css']);

                }

            }

        }

        $data['modules'] = $modules;

        return $data;

    }

    private static function sanitizeWeekdays($weekdays) {

        foreach($weekdays as $k => $weekday) {

            $weekdays[$k] = self::sanitizeArray($weekday,self::getPoiContentGroupWeekdayValidationRules());

            if(isset($weekdays[$k]['times'])) {
                $weekdays[$k]['times'] = self::sanitizeTimes($weekdays[$k]['times']);
            }

        }

        return $weekdays;

    }

    private static function sanitizeTimes($times) {

        foreach($times as $k => $time) {
            $times[$k] = self::sanitizeArray($time,self::getPoiContentGroupWeekdayTimeValidationRules());
        }

        return $times;

    }

    public static function removeEmptyCssValues($css) {

        foreach($css as $k => $value) {
            $css[$k] = Service::filterEmptyValues($css[$k]);
        }

        return $css;

    }

    private static function falseFlagModules() {

        $flags = [];

        foreach(ModuleService::getFlaggableModules() as $flaggableModule) {
            $flags[$flaggableModule] = false;
        }

        return $flags;

    }

    private static function isUuidable(array $module) {

        if(in_array($module['type'],ModuleService::getUuidableModules()) && !isset($module['data']['uuid'])) {
            return true;
        }
        return false;

    }

    private static function flagForInteraction(array $flags, array $module) {

        if(in_array($module['type'],ModuleService::getFlaggableModules())) {
            $flags[$module['type']] = true;
        }

        return $flags;

    }

}
