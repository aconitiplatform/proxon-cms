<?php

namespace App\Http\Services;

use App\Http\Services\Service;

class ServerService extends Service {

    public static function getServerOptions() {

        return [
            'q',
            'qm',
            'ui',
            'qmui',
        ];

    }

    public static function getServerIp() {
        return config('app-server.ip') != null ? config('app-server.ip') : 'Unknown IP';
    }

    public static function getCommit() {
        return config('app-server.commit') != null ? config('app-server.commit') : 'commit-id-unavailable';
    }

    public static function getServerType() {
        return config('app-server.server') != null ? config('app-server.server') : 'qmui';
    }

}
