<?php

namespace App\Http\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Survey;
use App\Models\User;

use App\Http\Services\Service;

class SurveyService extends Service {

    public static function getStoreSurveyValidationRules() {
        return [
            'name' => 'required|string|max:255',
            'fallback_language' => 'required|string|min:2|max:5',
            'description' => 'sometimes|string|min:1|max:65000',
        ];
    }

    public static function getUpdateSurveyValidationRules() {
        return [
            'name' => 'sometimes|string|max:255',
            'fallback_language' => 'sometimes|string|min:2|max:5',
            'status' => 'sometimes|in:draft,started,completed',
            'description' => 'sometimes|string|min:1|max:65000',
            'survey_questions' => 'sometimes|array',
        ];
    }

    public static function getStoreSurveyQuestionValidationRules() {
        return [
            'key' => 'required|string|min:1|max:32',
            'language' => 'required|string|min:2|max:5',
            'question' => 'required|string|min:1',
            'type' => 'required|in:single_choice',
            'survey_question_options' => 'sometimes|array',
        ];
    }

    public static function getStoreSurveyQuestionOptionValidationRules() {
        return [
            'key' => 'required|string|min:1|max:32',
            'label' => 'required|string|min:1|max:255',
        ];
    }

    public static function validateSurveyQuestionsArray(array $surveyQuestions) {

        $surveyQuestionsIndex = 0;

        foreach($surveyQuestions as $surveyQuestion) {

            $surveyQuestions[$surveyQuestionsIndex]['errors'] = self::validateArray($surveyQuestion, self::getStoreSurveyQuestionValidationRules(),[],[],false);

            if(isset($surveyQuestion['survey_question_options'])) {

                $surveyQuestionOptionsIndex = 0;

                foreach($surveyQuestion['survey_question_options'] as $surveyQuestionOption) {

                    $surveyQuestions[$surveyQuestionsIndex]['survey_question_options'][$surveyQuestionOptionsIndex]['errors']
                        = self::validateArray($surveyQuestionOption, self::getStoreSurveyQuestionOptionValidationRules(),[],[],false);

                    $surveyQuestionOptionsIndex++;

                }

            }

            $surveyQuestionsIndex++;

        }

        return $surveyQuestions;

    }

    public static function addUsersToSurvey($surveyId,$tenantId,$createdByUserId) {

        $creatorHasBeenAdded = false;

        $users = User::where('tenant_id',$tenantId)->whereIn('role',self::getRolesButSubuser())->get();

        foreach($users as $user) {

            self::insertSurveyUserRelation($surveyId,$user->id);
            if($user->id == $createdByUserId) {
                $creatorHasBeenAdded = true;
            }

        }

        if(!$creatorHasBeenAdded) {
            self::insertSurveyUserRelation($surveyId,$createdByUserId);
        }

    }

    public static function insertSurveyUserRelation($surveyId,$userId) {
        self::insertObjectUserRelation($surveyId,$userId,'survey_user','survey_id');
    }

    public static function deleteSurveyUserRelation($surveyId,$userId) {
        self::deleteObjectUserRelation($surveyId,$userId,'survey_user','survey_id');
    }

    public static function getSurveyUserRelation($surveyId,$userId) {
        $relation = self::getObjectUserRelation($surveyId,$userId,'survey_user','survey_id');
        return $relation;
    }

}
