<?php

namespace App\Http\Services;

use Cache;

use App\Models\Poi;
use App\Models\User;

use App\Http\Services\Service;

class PoiService extends Service {

    public static function addAllRelationsToPoi($poi) {

        $poi->poiContents;
        $poi->beacons;
        $poi->media;
        $poi->poiDesign;

        return $poi;
    }

    public static function getStorePoiValidationRules() {
        return [
            'name' => 'required|string|min:3|max:255',
            'description' => 'sometimes|string',
            'meta' => 'sometimes|array',
            'has_channel' => 'required|boolean',
            'fallback_language' => 'sometimes|string|min:2|max:5',
            'overwrite_imprint' => 'sometimes|boolean',
            'imprint' => 'required_if:overwrite_imprint,true|string',
            'overwrite_privacy' => 'sometimes|boolean',
            'privacy' => 'required_if:overwrite_privacy,true|string',
            'overwrite_copyright' => 'sometimes|boolean',
            'copyright' => 'required_if:overwrite_copyright,true|string',
        ];
    }

    public static function getUpdatePoiValidationRules() {
        return [
            'active' => 'sometimes|boolean',
            'show' => 'sometimes|string|in:always,beacon,not_beacon',
            'name' => 'sometimes|string|min:3|max:255',
            'description' => 'sometimes|string',
            'meta' => 'sometimes|array',
            'has_channel' => 'sometimes|boolean',
            'fallback_language' => 'sometimes|string|min:2|max:5',
            'overwrite_imprint' => 'sometimes|boolean',
            'imprint' => 'required_if:overwrite_imprint,true|string',
            'overwrite_privacy' => 'sometimes|boolean',
            'privacy' => 'required_if:overwrite_privacy,true|string',
            'overwrite_copyright' => 'sometimes|boolean',
            'copyright' => 'required_if:overwrite_copyright,true|string',
            'has_sweepstake' => 'sometimes|boolean',
            'has_feedback' => 'sometimes|boolean',
        ];
    }

    public static function getStorePoiUserValidationRules() {
        return [
            'id' => 'required|string|size:36|exists:users,id',
        ];
    }

    public static function getStorePoiEventValidationRules() {
        return [
            'type' => 'required|in:note',
            'text' => 'required_if:type,note|string|min:1|max:2000',
        ];
    }

    public static function getStorePoiBeaconValidationRules() {
        return [
            'id' => 'required|string|size:36|exists:beacons,id',
        ];
    }

    public static function getDestroyPoiBeaconValidationRules() {
        return [
            'beacon_id' => 'required|string|size:36|exists:beacons,id',
        ];
    }

    public static function getBulkPoiBeaconValidationRules() {
        return [
            'beacons' => 'sometimes|array'
        ];
    }

    public static function getStorePoiContentValidationRules() {
        return [
            'poi_id' => 'required|string|size:36|exists:pois,id',
            'language' => 'required|string|min:1|max:5',
            'data' => 'required|array',
            'active' => 'required|boolean',
            'has_url' => 'required|boolean',
            'url' => 'required_if:has_url,true|url',
            'meta' => 'sometimes|array',
        ];
    }

    public static function getUpdatePoiContentValidationRules() {
        return [
            'language' => 'sometimes|string|min:1|max:5',
            'data' => 'required|array',
            'active' => 'required|boolean',
            'has_url' => 'sometimes|boolean',
            'url' => 'required_if:has_url,true|url',
            'meta' => 'sometimes|array',
            'has_sweepstake' => 'sometimes|boolean',
            'has_feedback' => 'sometimes|boolean',
            'has_contact' => 'sometimes|boolean',
            'has_micro_payment' => 'sometimes|boolean',
        ];
    }

    public static function poiBelongsToTenant($poiId,$tenantId) {

        $cacheKey = self::getTenantRootCacheKeyStatically('owns:'.$poiId);

        if(Cache::has($cacheKey)) {

            return Cache::get($cacheKey);

        } else {

            $belongsToTenant = false;

            $poi = Poi::find($poiId);

            if($poi == null) return $belongsToTenant;

            if($poi->tenant_id == $tenantId) {

                $belongsToTenant = true;
                Cache::put($cacheKey,$poi,60);

            }

            return $belongsToTenant ? $poi : false;

        }

    }

    public static function addUsersToPoi($poiId,$tenantId,$createdByUserId) {

        $creatorHasBeenAdded = false;

        $users = User::where('tenant_id',$tenantId)->whereIn('role',self::getRolesButSubuser())->get();

        foreach($users as $user) {

            self::insertPoiUserRelation($poiId,$user->id);
            if($user->id == $createdByUserId) {
                $creatorHasBeenAdded = true;
            }

        }

        if(!$creatorHasBeenAdded) {
            self::insertPoiUserRelation($poiId,$createdByUserId);
        }

    }

    public static function insertPoiUserRelation($poiId,$userId) {
        self::insertObjectUserRelation($poiId,$userId,'poi_user','poi_id');
    }

    public static function deletePoiUserRelation($poiId,$userId) {
        self::deleteObjectUserRelation($poiId,$userId,'poi_user','poi_id');
    }

    public static function getPoiUserRelation($poiId,$userId) {
        $relation = self::getObjectUserRelation($poiId,$userId,'poi_user','poi_id');
        return $relation;
    }

}
