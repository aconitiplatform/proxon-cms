<?php

namespace App\Http\Services;

use App\Models\User;

use App\Http\Services\Service;

class NewsletterConfigurationService extends Service {

    public static function getUpdateNewsletterConfigurationValidationRules() {
        return [
            'name'    => 'sometimes|string',
            'type' => 'required|in:mailchimp',
            'configuration' => 'sometimes|array',
            'user_id' => 'sometimes|string|size:36|exists:users,id'
        ];
    }

    public static function getStoreNewsletterConfigurationValidationRules() {
        return [
            'name'    => 'required|string',
            'type' => 'required|in:mailchimp',
            'configuration' => 'required|array',
        ];
    }

    public static function getSubscribeToNewsletterValidationRules() {
        return [
            'newsletter_configuration' => 'required|integer|exists:newsletter_configurations,id',
            'email' => 'required|email',
            'options' => 'sometimes|array'
        ];
    }

    public static function addUsersToNewsletterConfiguration($nlcId,$tenantId,$createdByUserId) {

        $creatorHasBeenAdded = false;

        $users = User::where('tenant_id',$tenantId)->whereIn('role',self::getRolesButSubuser())->get();

        foreach($users as $user) {

            self::insertNewsletterConfigurationUserRelation($nlcId,$user->id);
            if($user->id == $createdByUserId) {
                $creatorHasBeenAdded = true;
            }

        }

        if(!$creatorHasBeenAdded) {
            self::insertNewsletterConfigurationUserRelation($nlcId,$createdByUserId);
        }

    }

    public static function insertNewsletterConfigurationUserRelation($nlcId,$userId) {
        self::insertObjectUserRelation($nlcId,$userId,'newsletter_configuration_user','nlc_id');
    }

    public static function deleteNewsletterConfigurationUserRelation($nlcId,$userId) {
        self::deleteObjectUserRelation($nlcId,$userId,'newsletter_configuration_user','nlc_id');
    }

    public static function getNewsletterConfigurationUserRelation($nlcId,$userId) {
        $relation = self::getObjectUserRelation($nlcId,$userId,'newsletter_configuration_user','nlc_id');
        return $relation;
    }

}
