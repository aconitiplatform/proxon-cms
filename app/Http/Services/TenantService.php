<?php

namespace App\Http\Services;

use Cache;

use App\Models\Tenant;
use App\Models\Reseller;

use App\Http\Services\Service;

class TenantService extends Service {

    public static function getUpdateTenantValidationRules() {
        return [
            'name'    => 'sometimes|string',
            'url' => 'sometimes|url',
            'css' => 'sometimes|url',
            'logo' => 'sometimes|url',
            'imprint' => 'sometimes|string',
            'email' => 'sometimes|email',
            'fallback_language' => 'sometimes|string|min:2|max:5',
            'locale' => 'sometimes|string|min:2|max:10',
            'ga_tracking_id' => 'sometimes|string',
            'ga_utm_source' => 'sometimes|string',
            'substance_url' => 'sometimes|url',
            'edward_stone_url' => 'sometimes|url',
            'logo_settings' => 'sometimes|array',
            'privacy' => 'sometimes|string',
            'copyright_label' => 'sometimes|string',
        ];
    }

    public static function getSubstanceAppUrl($tenantId) {

        if(Cache::has(self::getTenantRootCacheKeyStatically('tenant',$tenantId))) {

            $tenant = Cache::get(self::getTenantRootCacheKeyStatically('tenant',$tenantId));

        } else {

            $tenant = Tenant::find($tenantId);
            Cache::forever(self::getTenantRootCacheKeyStatically('tenant',$tenantId),$tenant);

        }

        if($tenant->substance_url != null) {
            return $tenant->substance_url;
        } else if ($tenant->reseller_id != null) {

            try {

                $reseller = Reseller::findOrFail($tenant->reseller_id);

                if($reseller->settings->substance_url != null) {
                    return $reseller->settings->substance_url;
                }

            } catch (\Exception $e) {
                //fallback to standard url
            }

        }

        return config('substance.url');

    }

    public static function getEdwardStoneAppUrl($tenantId) {

        if(Cache::has(self::getTenantRootCacheKeyStatically('tenant',$tenantId))) {

            $tenant = Cache::get(self::getTenantRootCacheKeyStatically('tenant',$tenantId));

        } else {

            $tenant = Tenant::find($tenantId);
            Cache::forever(self::getTenantRootCacheKeyStatically('tenant',$tenantId),$tenant);

        }

        if($tenant->edward_stone_url != null) {
            return $tenant->edward_stone_url;
        } else if ($tenant->reseller_id != null) {

            try {

                $reseller = Reseller::findOrFail($tenant->reseller_id);

                if($reseller->settings->edward_stone_url != null) {
                    return $reseller->settings->edward_stone_url;
                }

            } catch (\Exception $e) {
                //fallback to standard url
            }

        }
        
        return config('substance.edward_stone_url');

    }

}
