<?php

namespace App\Http\Services;

use App\Http\Services\Service;

class VoucherService extends Service {

    public static function getProxyBarcodeValidationRules() {
        return [
            'bc_type' => 'required|string|min:1',
            'bc_content' => 'required|string|min:1',
        ];
    }

}
