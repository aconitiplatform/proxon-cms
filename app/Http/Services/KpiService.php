<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Cache;
use Auth;

use App\Http\Controllers\AnalyticsController;

use App\Http\Services\Service;

use App\Traits\GeneratesCacheKeys;

class KpiService extends Service {

    use GeneratesCacheKeys;

    public static function getKpiData(Request $request,$key = 'access-today',$isQueue = false) {

        $cacheKey = self::getRoleBasedCacheKeyStatically('kpis:'.$key);

        if(Cache::has($cacheKey) && !$isQueue) {

            $analytics = Cache::get($cacheKey);

        } else {

            switch ($key) {
                case 'scans-today':
                    $listMethod = 'showScans';
                    break;
                case 'content-delivered-today':
                    $listMethod = 'showDeliveredContent';
                    break;
                case 'channel-access-today':
                    $listMethod = 'showViewedChannels';
                    break;
                default:
                    $listMethod = 'showScans';
                    break;
            }

            $analytics = self::getKpiDataWithRequestConstraints($request, $listMethod);

            Cache::put($cacheKey,$analytics,config('substance.cache_analytics_defaul_time'));

        }

        return self::getKpiValues($analytics);

    }

    protected static function getKpiDataWithRequestConstraints(Request $request, $listMethod) {

        $isInternal = true;
        $analyticsController = new AnalyticsController();

        $timeToday = Carbon::today();
        $request['start'] = $timeToday->timestamp;
        $request['end'] = Carbon::now()->timestamp;
        $today = $analyticsController->$listMethod($request,$isInternal);


        $timeYesterday = Carbon::yesterday();
        $request['start'] = $timeYesterday->timestamp;
        $request['end'] = Carbon::now()->timestamp - 86400;
        $yesterday =  $analyticsController->$listMethod($request,$isInternal);

        return compact('today','yesterday');

    }

    public static function getKpiValues($analytics,$dd = false) {

        $data['today'] = (is_float($analytics['today']) || is_int($analytics['today'])) ? round($analytics['today'],2) : count($analytics['today']);
        $data['yesterday'] = (is_float($analytics['yesterday']) || is_int($analytics['yesterday'])) ? round($analytics['yesterday'],2) : count($analytics['yesterday']);

        $data['difference'] = abs($data['today'] - $data['yesterday']);
        if($data['yesterday'] == 0) {
            $data['percent'] = 100;
        } else {
            $data['percent'] = round(($data['today'] / $data['yesterday'] * 100),2);
        }

        if($data['percent'] > 100 && $data['today'] > $data['yesterday']) {
            $trend = 'up';
            $data['percent'] = $data['percent'] - 100;
        } else if($data['percent'] == 100 && $data['today'] > $data['yesterday']) {
            $trend = 'up';
            $data['percent'] = $data['percent'];
        } else if ($data['percent'] == 100 && $data['today'] == $data['yesterday']) {
            $trend = 'flat';
            $data['percent'] = 0;
        } else {
            $trend = 'down';
            $data['percent'] = $data['percent'] - 100;
        }

        $data['difference'] = round($data['difference'],2);
        $data['percent'] = round($data['percent'],2);

        $data['trend'] = $trend;

        return $data;

    }

}
