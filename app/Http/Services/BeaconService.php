<?php

namespace App\Http\Services;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Beacon;

use App\Http\Services\Service;

class BeaconService extends Service {

    public static function addAllRelationsToBeacon($beacon) {

        $beacon->beaconLocation;
        $beacon->poi;

        return $beacon;
    }

    public static function getStoreBeaconValidationRules() {
        return [
            'name' => 'required|string|min:1|max:128',
            'tenant_id' => 'required|string|size:36|exists:tenants,id',
            'config' => 'sometimes|array',
            'description' => 'sometimes|string',
            'alias' => 'sometimes|string|max:255',
        ];
    }

    public static function getUpdateBeaconValidationRules() {
        return [
            'config' => 'sometimes|array',
            'poi_id' => 'sometimes|string|size:36|exists:pois,id',
            'name' => 'sometimes|string|min:1|max:128',
            'description' => 'sometimes|string',
            'alias' => 'sometimes|string|max:255',
        ];
    }

    public static function getStoreBeaconUserValidationRules() {
        return [
            'id' => 'required|string|size:36|exists:users,id',
        ];
    }

    public static function getStoreBeaconLocationValidationRules() {
        return [
            'beacon_id' => 'required|string|size:36|exists:beacons,id',
            'latitude' => 'required_with:longitude|numeric|min:-90|max:90',
            'longitude' => 'required_with:latitude|numeric|min:-180|max:180',
            'indoor' => 'sometimes|boolean',
            'level' => 'sometimes|integer',
            'note' => 'sometimes|string'
        ];
    }

    public static function getUpdateBeaconLocationValidationRules() {
        return [
            'latitude' => 'required_with:longitude|numeric|min:-90|max:90',
            'longitude' => 'required_with:latitude|numeric|min:-180|max:180',
            'indoor' => 'sometimes|boolean',
            'level' => 'sometimes|integer',
            'note' => 'sometimes|string'
        ];
    }

    public static function addTenantUsersAndAdminsToBeacon($beaconId,$tenantId) {

        $users = User::where('tenant_id',$tenantId)->where('role','admin')->orWhere('tenant_id',$tenantId)->where('role','superadmin')->orWhere('tenant_id',$tenantId)->where('role','user')->get();

        foreach($users as $user) {
            self::insertBeaconUserRelation($beaconId,$user->id);
        }

    }

    public static function getBeaconByUrl($url,$user = null) {

        $beacon = Beacon::where('url',$url)->first();

        if($beacon != null) {

            if($user != null) {
                if(self::getBeaconUserRelation($beacon->id,$user->id) != null) {
                    return $beacon;
                } else {
                    return null;
                }
            }

            return $beacon;

        } else {
            return null;
        }

    }

    public static function insertBeaconUserRelation($beaconId,$userId) {
        self::insertObjectUserRelation($beaconId,$userId,'beacon_user','beacon_id');
    }

    public static function deleteBeaconUserRelation($beaconId,$userId) {
        self::deleteObjectUserRelation($beaconId,$userId,'beacon_user','beacon_id');
    }

    public static function getBeaconUserRelation($beaconId,$userId) {
        $relation = self::getObjectUserRelation($beaconId,$userId,'beacon_user','beacon_id');
        return $relation;
    }

}
