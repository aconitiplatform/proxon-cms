<?php

namespace App\Http\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;

use App\Models\Beacon;
use App\Models\Poi;
use App\Models\MicroPayment;

use App\Http\Services\BeaconService;
use App\Http\Services\PoiService;
use App\Http\Services\Service;

class UserService extends Service {

    public static function getStoreUserValidationRules() {
        return [
            'firstname' => 'required|string|min:3|max:255',
            'lastname' => 'required|string|min:3|max:255',
            'email' => 'required|email|unique:users,email',
            'role' => 'required|in:admin,user,subuser',
            'language' => 'required|string|size:2',
            'locale' => 'required|string|min:1|max:10',
        ];
    }

    public static function getUpdateUserValidationRules($userId) {
        return [
            'firstname' => 'sometimes|string|min:3|max:255',
            'lastname' => 'sometimes|string|min:3|max:255',
            'email' => 'sometimes|email|unique:users,email,'.$userId,
            'role' => 'sometimes|in:admin,user,subuser',
            'language' => 'sometimes|string|size:2',
            'locale' => 'sometimes|string|min:1|max:10',
            'active' => 'sometimes|boolean',
        ];
    }

    public static function getStoreAppValidationRules() {
        return [
            'appname' => 'required|string|min:3|max:255',
            'language' => 'required|string|size:2',
            'locale' => 'sometimes|string|min:1|max:10',
        ];
    }

    public static function getUpdateAppValidationRules() {
        return [
            'appname' => 'sometimes|string|min:3|max:255',
            'language' => 'sometimes|string|size:2',
            'locale' => 'sometimes|string|min:1|max:10',
            'active' => 'sometimes|boolean',
        ];
    }

    public static function giveAccessToAllEntities($user) {

        self::giveAccessToBeacons($user);
        self::giveAccessToPois($user);
        self::giveAccessToMicroPayments($user);
        self::giveAccessToNewsletterConfigurations($user);
        self::giveAccessToSurveys($user);
        self::giveAccessToMedias($user);

    }

    public static function removeAccessFromAllEntities($user) {

        self::removeAccessFromBeacons($user);
        self::removeAccessFromPois($user);
        self::removeAccessFromMicroPayments($user);
        self::removeAccessFromNewsletterConfigurations($user);
        self::removeAccessFromSurveys($user);
        self::removeAccessFromMedias($user);

    }

    public static function giveAccessToBeacons($user,$beacons = null) {
        self::giveAccessToEntity($user,$beacons,'App\Models\Beacon','App\Http\Services\BeaconService','insertBeaconUserRelation');
    }

    public static function removeAccessFromBeacons($user,$beacons = null) {
        self::removeAccessToEntity($user,$beacons,'App\Models\Beacon','App\Http\Services\BeaconService','deleteBeaconUserRelation');
    }

    public static function giveAccessToPois($user,$pois = null) {
        self::giveAccessToEntity($user,$pois,'App\Models\Poi','App\Http\Services\PoiService','insertPoiUserRelation');
    }

    public static function removeAccessFromPois($user,$pois = null) {
        self::removeAccessToEntity($user,$pois,'App\Models\Poi','App\Http\Services\PoiService','deletePoiUserRelation');
    }

    public static function giveAccessToMicroPayments($user,$microPayments = null) {
        self::giveAccessToEntity($user,$microPayments,'App\Models\MicroPayment','App\Http\Services\MicroPaymentService','insertMicroPaymentUserRelation');
    }

    public static function removeAccessFromMicroPayments($user,$microPayments = null) {
        self::removeAccessToEntity($user,$microPayments,'App\Models\MicroPayment','App\Http\Services\MicroPaymentService','deleteMicroPaymentUserRelation');
    }

    public static function giveAccessToNewsletterConfigurations($user,$nlcs = null) {
        self::giveAccessToEntity($user,$nlcs,'App\Models\NewsletterConfiguration','App\Http\Services\NewsletterConfigurationService','insertNewsletterConfigurationUserRelation');
    }

    public static function removeAccessFromNewsletterConfigurations($user,$nlcs = null) {
        self::removeAccessToEntity($user,$nlcs,'App\Models\NewsletterConfiguration','App\Http\Services\NewsletterConfigurationService','deleteNewsletterConfigurationUserRelation');
    }

    public static function giveAccessToSurveys($user,$surveys = null) {
        self::giveAccessToEntity($user,$surveys,'App\Models\Survey','App\Http\Services\SurveyService','insertSurveyUserRelation');
    }

    public static function removeAccessFromSurveys($user,$surveys = null) {
        self::removeAccessToEntity($user,$surveys,'App\Models\Survey','App\Http\Services\SurveyService','deleteSurveyUserRelation');
    }

    public static function giveAccessToMedias($user,$medias = null) {
        self::giveAccessToEntity($user,$medias,'App\Models\Media','App\Http\Services\MediaService','insertMediaUserRelation');
    }

    public static function removeAccessFromMedias($user,$medias = null) {
        self::removeAccessToEntity($user,$medias,'App\Models\Media','App\Http\Services\MediaService','deleteMediaUserRelation');
    }

    private static function giveAccessToEntity($user,$entityCollection,$model,$modelService,$modelServiceInsertRelationMethod) {

        if(!isset($entityCollection)) {

            $entityCollection = $model::where('tenant_id',$user->tenant_id)->get();

            if(!$entityCollection->isEmpty()) {

                foreach($entityCollection as $entity) {
                    $modelService::$modelServiceInsertRelationMethod($entity->id,$user->id);
                }

            }

        } else {

            foreach($entityCollection as $entityId) {

                try {

                    $entity = $model::findOrFail($entityId);

                    if($entity->tenant_id == $user->tenant_id) {
                        $modelService::$modelServiceInsertRelationMethod($entity->id,$user->id);
                    }

                } catch(ModelNotFoundException $modelNotFoundException) {
                    continue;
                }

            }

        }

    }

    private static function removeAccessToEntity($user,$entityCollection,$model,$modelService,$modelServiceDeleteRelationMethod) {

        if(!isset($entityCollection)) {

            $entityCollection = $model::where('tenant_id',$user->tenant_id)->get();

            if(!$entityCollection->isEmpty()) {

                foreach($entityCollection as $entity) {
                    $modelService::$modelServiceDeleteRelationMethod($entity->id,$user->id);
                }

            }

        } else {

            foreach($entityCollection as $entityId) {

                try {

                    $entity = $model::findOrFail($entityId);

                    if($entity->tenant_id == $user->tenant_id) {
                        $modelService::$modelServiceDeleteRelationMethod($entity->id,$user->id);
                    }

                } catch(ModelNotFoundException $modelNotFoundException) {
                    continue;
                }

            }

        }

    }

    public static function storeAppCredentials($userId,$tenantId,$key,$secret) {

        DB::table('app_credentials')->insert([
            'user_id' => $userId,
            'tenant_id' => $tenantId,
            'key' => $key,
            'secret' => $secret
        ]);

    }

    public static function getUserIdByAppCredentials($key,$secret) {

        $relation = DB::table('app_credentials')->select('user_id')->where('key',$key)->where('secret',$secret)->first();

        if($relation != null) {
            return $relation->user_id;
        } else {
            return null;
        }

    }

}
