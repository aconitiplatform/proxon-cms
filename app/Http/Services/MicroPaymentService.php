<?php

namespace App\Http\Services;

use App\Models\MicroPayment;
use App\Models\User;

use App\Http\Services\Service;

class MicroPaymentService extends Service {

    public static function getStoreMicroPaymentValidationRules() {
        return [
            'name' => 'required|string|min:3|max:255',
            'description' => 'required|string|min:1|max:1000',
            'amount' => 'required|integer|min:1',
            'type' => 'required|string|in:single,subscription',
            'currency' => 'required|string|in:EUR,USD',
            'allowed_payment_types' => 'required|array|min:1',
            'allowed_payment_types.*' => 'required_with:allowed_payment_types|string|in:credit_card,paypal',
            'fields' => 'sometimes|array',
            'emails_payee' => 'required|boolean',
            'emails_payer' => 'required|boolean',
            'email' => 'required_if:emails_payee,true|email',
            'callback_url' => 'sometimes|url',
            'heartbeat_url' => 'sometimes|url',
        ];
    }

    public static function getUpdateMicroPaymentValidationRules() {
        return [
            'name' => 'sometimes|string|min:3|max:255',
            'description' => 'sometimes|string|min:1|max:1000',
            'amount' => 'sometimes|integer|min:1',
            'type' => 'sometimes|string|in:single,subscription',
            'currency' => 'sometimes|string|in:EUR,USD',
            'allowed_payment_types' => 'sometimes|array|min:1',
            'allowed_payment_types.*' => 'required_with:allowed_payment_types|string|in:credit_card,paypal',
            'fields' => 'sometimes|array',
            'emails_payee' => 'sometimes|boolean',
            'emails_payer' => 'sometimes|boolean',
            'email' => 'sometimes|email',
            'callback_url' => 'sometimes|url',
            'heartbeat_url' => 'sometimes|url',
        ];
    }

    public static function addUsersToMicroPayment($microPaymentId,$tenantId,$createdByUserId) {

        $creatorHasBeenAdded = false;

        $users = User::where('tenant_id',$tenantId)->whereIn('role',self::getRolesButSubuser())->get();

        foreach($users as $user) {

            self::insertMicroPaymentUserRelation($microPaymentId,$user->id);
            if($user->id == $createdByUserId) {
                $creatorHasBeenAdded = true;
            }

        }

        if(!$creatorHasBeenAdded) {
            self::insertMicroPaymentUserRelation($microPaymentId,$createdByUserId);
        }

    }

    public static function insertMicroPaymentUserRelation($microPaymentId,$userId) {
        self::insertObjectUserRelation($microPaymentId,$userId,'micro_payment_user','micro_payment_id');
    }

    public static function deleteMicroPaymentUserRelation($microPaymentId,$userId) {
        self::deleteObjectUserRelation($microPaymentId,$userId,'micro_payment_user','micro_payment_id');
    }

    public static function getMicroPaymentUserRelation($microPaymentId,$userId) {
        $relation = self::getObjectUserRelation($microPaymentId,$userId,'micro_payment_user','micro_payment_id');
        return $relation;
    }

}
