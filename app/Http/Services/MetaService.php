<?php

namespace App\Http\Services;

use App\Http\Services\Service;

class MetaService extends Service {

    public static function getUrlParserValidationRules() {
        return [
            'url' => 'required|url',
            'language' => 'sometimes|string|min:1|max:5',
        ];
    }

}
