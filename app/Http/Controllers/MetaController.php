<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\MetaService;

use App\Traits\ParsesUrls;

class MetaController extends Controller {

    use ParsesUrls;

    public function show(Request $request) {

        $this->validate($request,MetaService::getUrlParserValidationRules());

        $url = urldecode($request->input('url'));

        $request->has('language') ? $language = $request->input('language') : $language = config('app.locale');

        try {
            return response()->data($this->parseUrl($url,$language));
        } catch (\Exception $e) {
            return response()->data([],400,false,['error'=>trans('api.URL_PARSER_NO_URL')]);
        }

    }

}
