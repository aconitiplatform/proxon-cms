<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use DB;
use Cache;

use App\Models\App;
use App\Models\User;

use App\Http\Controllers\Auth\PasswordResetController;

use App\Http\Services\UserService;

use App\Traits\PaginatesLists;
use App\Traits\HasSearch;

class AppController extends Controller {

    use PaginatesLists, HasSearch;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(UserService::getLastModified(Auth::user()->tenant_id,new App(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('apps:index');

        if(Cache::has($cacheKey)) {
            $apps = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();
            $apps = App::where('role','app')->get();
            Cache::forever($cacheKey,$apps);

        }

        return response()->data($this->paginate($apps,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $userId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('apps:'.$userId);

        if(Cache::has($cacheKey)) {
            $app = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();

            try {

                $app = App::findOrFail($userId);
                if($app->role != 'app') throw new ModelNotFoundException();
                Cache::forever($cacheKey,$app);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($app, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, UserService::getStoreAppValidationRules());

            $newUser = new App();
            $newUser->firstname = trim($request->input('appname'));
            $newUser->lastname = 'App';
            $newUser->email = md5(time().config('app.key')).'@'.Auth::user()->tenant_id.'.com';
            $newUser->password = bcrypt(time().config('app.key'));
            $newUser->role = 'app';
            $newUser->language = trim($request->input('language'));
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->active = true;

            if($request->has('locale')) $newUser->locale = trim($request->input('locale'));

            $newUser->save();

            UserService::giveAccessToBeacons($newUser);
            UserService::giveAccessToPois($newUser);
            UserService::storeAppCredentials($newUser->id,$newUser->tenant_id,hash('sha1',$newUser->email),hash('sha1',$newUser->password));

            $this->invalidateAndModify(
                [
                    'id' => $newUser->id,
                    'key' => 'apps',
                    'object' => new App()
                ]
            );

            return response()->data($newUser, 201, $isInternal);

        }

        return response()->data([], 403, $isInternal);

    }

    public function update(Request $request, $userId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id) || Auth::user()->id == $userId) {

            $this->validate($request, UserService::getUpdateAppValidationRules($userId));

            try {

                $updatableApp = App::findOrFail($userId);

                if($updatableApp->role != 'app') throw new ModelNotFoundException();
                if($request->has('appname')) $request['firstname'] = $request->input('appname');

                $updatableApp->update(UserService::getPartialUpdateValues($request,UserService::getUpdateUserValidationRules($userId)));

                $this->invalidateAndModify(
                    [
                        'id' => $updatableApp->id,
                        'key' => 'apps',
                        'object' => new App()
                    ]
                );

                $updatableApp->beacons;

                return response()->data($updatableApp, 200, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }


    }

    public function destroy(Request $request, $userId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $deletableApp = User::findOrFail($userId);
                $tenantId = $deletableApp->tenant_id;
                $deletableApp->delete();

                $this->invalidateAndModify(
                    [
                        'id' => $userId,
                        'key' => 'apps',
                        'object' => new App()
                    ]
                );

                return response()->data(null, 204, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
