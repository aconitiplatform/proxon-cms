<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Tenant;
use App\Models\User;

use App\Http\Services\TenantService;
use App\Http\Services\UserService;

use App\Traits\AffectsEdwardStone;

class SuperAdminController extends Controller {

    public function tenants(Request $request, $isInternal = false) {

        if($this->isSuperAdmin(Auth::user())) {

            $data = [];

            $tenants = Tenant::where('active',true)->where('id','!=',Auth::user()->tenant_id)->get();

            if(!$tenants->isEmpty()) {

                $i = 0;

                foreach($tenants as $tenant) {

                    $data[$i] = [
                        'id' => $tenant->id,
                        'name' => $tenant->name,
                    ];

                    foreach($tenant->users()->where('role','!=','app')->get() as $user) {

                        $data[$i]['users'][] = [
                            'id' => $user->id,
                            'email' => $user->email,
                            'lastname' => $user->lastname,
                            'firstname' => $user->firstname,
                            'active' => $user->active,
                            'role' => $user->role,
                        ];

                    }

                    $i++;

                }

            }

            return response()->data($data, 200, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
