<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Beacon;
use App\Models\Poi;
use App\Models\Tenant;
use App\Models\PoiEvent;

use App\Http\Services\PoiService;
use App\Http\Services\BeaconService;
use App\Http\Services\TenantService;

use App\Traits\AffectsEdwardStone;

class PoiBeaconController extends Controller {

    use AffectsEdwardStone;

    public function index(Request $request, $poiId, $isInternal = false) {

        if(PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $beacons = Beacon::where('poi_id',$poiId)->get();
            return response()->data($beacons, 200, $isInternal);

        } else {
            return response()->data([], 403, $isInternal, ['exception' => $e->getMessage()]);
        }

    }

    public function bulk(Request $request, $poiId, $isInternal = false) {

        if(PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $this->validate($request, PoiService::getBulkPoiBeaconValidationRules());

            $bulkBeacons = $request->input('beacons');
            $added = 0;
            $removed = 0;
            $result = array();
            $anythingChanged = false;

            foreach($bulkBeacons as $bulkBeaconId => $bulkBeaconAction) {

                $relation = BeaconService::getBeaconUserRelation($bulkBeaconId,Auth::user()->id);

                if($relation == null) {
                    $result[$bulkBeaconId]['status'] = trans('api.403');
                    continue;
                }

                $beacon = Beacon::find($bulkBeaconId);
                $hasChanged = false;

                if(($beacon->poi_id == null || $beacon->poi_id != null) && $bulkBeaconAction == 'add') {
                    $added++;
                    if($beacon->poi_id != $poiId) {
                        $beacon->poi_id = $poiId;
                        $hasChanged = true;
                        $anythingChanged = true;
                    }
                    $publish = true;
                } else if ($beacon->poi_id != null && $bulkBeaconAction == 'remove') {
                    $removed++;
                    if($beacon->poi_id != null) {
                        $beacon->poi_id = null;
                        $hasChanged = true;
                        $anythingChanged = true;
                    }
                    $publish = false;
                }

                $result[$beacon->id] = array(
                    'action' => $bulkBeaconAction
                );

                if($hasChanged) {

                    $url = $beacon->url;

                    if($beacon->save()) {
                        $result[$beacon->id]['status'] = "OK";
                    } else {
                        $result[$beacon->id]['status'] = "Failed";
                    }

                    $this->invalidateAndModify(
                        [
                            'id' => $beacon->id,
                            'key' => 'beacons',
                            'object' => new Beacon()
                        ]
                    );

                    $this->publishPoiUrlConnection($poiId,$url,Auth::user()->tenant_id,$publish);

                } else {
                    $result[$beacon->id]['status'] = "Unchanged";
                }

            }

            if($anythingChanged) {

                $this->invalidateAndModify(
                    [
                        'id' => $poiId,
                        'key' => 'pois',
                        'object' => new Poi()
                    ],
                    [
                        'object' => new Tenant()
                    ],
                    [
                        'object' => new Beacon()
                    ],
                    [
                        'id' => $poiId,
                        'key' => 'poi-log-'.$poiId,
                        'object' => new PoiEvent()
                    ]
                );

            }

            return response()->data($result, 200, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function store(Request $request, $poiId, $isInternal = false) {

        if(PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $this->validate($request, PoiService::getStorePoiBeaconValidationRules());

            $relation = BeaconService::getBeaconUserRelation($request->input('id'),Auth::user()->id);

            if($relation == null) {
                return response()->data([], 404, $isInternal);
            }

            $beacon = Beacon::find($request->input('id'));

            $beacon->poi_id = $poiId;

            $url = $beacon->url;

            $beacon->save();

            $this->publishPoiUrlConnection($poiId,$url,Auth::user()->tenant_id,true);

            $this->invalidateAndModify(
                [
                    'id' => $poiId,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'id' => $beacon->id,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ],
                [
                    'id' => $poiId,
                    'key' => 'poi-log-'.$poiId,
                    'object' => new PoiEvent()
                ]
            );

            return response()->data($beacon, 200, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function destroy(Request $request, $poiId, $beaconId, $isInternal = false) {

        if(PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $request['beacon_id'] = $beaconId;

            $this->validate($request, PoiService::getDestroyPoiBeaconValidationRules());

            $relation = BeaconService::getBeaconUserRelation($beaconId,Auth::user()->id);

            if($relation == null) {
                return response()->data([], 404, $isInternal);
            }

            $beacon = Beacon::find($beaconId);
            $beacon->poi_id = null;

            $url = $beacon->url;

            $beacon->save();

            $this->publishPoiUrlConnection($poiId,$url,Auth::user()->tenant_id,false);

            $this->invalidateAndModify(
                [
                    'id' => $poiId,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'id' => $beacon->id,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ],
                [
                    'id' => $poiId,
                    'key' => 'poi-log-'.$poiId,
                    'object' => new PoiEvent()
                ]
            );

            return response()->data(null, 204, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
