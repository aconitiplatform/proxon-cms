<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

use App\Models\Beacon;
use App\Models\User;

use App\Http\Controllers\BeaconController;

use App\Http\Services\BeaconService;
use App\Http\Services\UserService;

class BeaconUserController extends Controller {

    public function index(Request $request, $beaconId, $isInternal = false) {

        $bc = new BeaconController();

        $beacon = $bc->show($request,$beaconId,true);

        if($beacon != null) {

            $users = $beacon->users;
            return response()->data($users, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function store(Request $request, $beaconId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, BeaconService::getStoreBeaconUserValidationRules());

            $bc = new BeaconController();
            $beacon = $bc->show($request,$beaconId,true);

            $uc = new UserController();
            $addableUser = $uc->show($request,$request->input('id'),true);

            if($beacon->tenant_id == $addableUser->tenant_id) {

                BeaconService::insertBeaconUserRelation($beacon->id,$addableUser->id,false);

                $this->invalidateAndModify(
                    [
                        'object' => new Beacon()
                    ],
                    [
                        'object' => new User()
                    ]
                );

                return response()->data(trans('api.201'), 201, $isInternal);

            }

        }

        return response()->data([], 403, $isInternal);

    }

    public function destroy(Request $request, $beaconId, $userId, $isInternal = false) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            BeaconService::deleteBeaconUserRelation($beaconId,$userId,false);

            $this->invalidateAndModify(
                [
                    'id' => $beaconId,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ],
                [
                    'object' => new User()
                ]
            );

            return response()->data(null, 204, $isInternal);

        }

        return response()->data([], 403, $isInternal);

    }

}
