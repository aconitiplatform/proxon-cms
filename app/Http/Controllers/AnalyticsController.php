<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Beacon;
use App\Models\UrlAccess;

use App\Http\Controllers\PoiController;

use App\Http\Services\BeaconService;

use App\Traits\GetsAnalyticsEvents;
use App\Traits\GetsAnalyticsEventsNew;

class AnalyticsController extends Controller {

    use GetsAnalyticsEventsNew;

    public function showDeliveredContent(Request $request, $isInternal = false) {
        return $this->showEvents('poi',$request,$this->getUrl($request),$isInternal,$this->getPoi($request));
    }

    public function showViewedChannels(Request $request, $isInternal = false) {
        return $this->showEvents('channel',$request,$this->getUrl($request),$isInternal,$this->getPoi($request));
    }

    public function showScans(Request $request, $isInternal = false) {
        return $this->showEvents('scans',$request,$this->getUrl($request),$isInternal,$this->getPoi($request));
    }

    public function showPoisWithEvents(Request $request, $isInternal = false) {

        $pc = new PoiController();

        $pois = Auth::user()->pois;

        $data = [];

        if(!$pois->isEmpty()) {

            foreach($pois as $poi) {

                $p['id'] = $poi->id;
                $p['name'] = $poi->name;
                $p['events'] = $this->getAnalyticsCount('poi',Auth::user()->tenant_id,$poi->id);
                $p['scans'] = $this->getAnalyticsCount('scan',Auth::user()->tenant_id,$poi->id);
                $data[] = $p;

            }

        }

        return response()->data($data, 200, $isInternal);

    }

    private function getAnalyticsCount($accessed,$tenantId,$poiId) {

        $cacheKey = "{$accessed}:{$tenantId}:{$poiId}:count";

        if(Cache::store('analytics')->has($cacheKey)) {
            return Cache::store('analytics')->get($cacheKey);
        }

        return 0;

    }

    private function showEvents($type, Request $request, $url = null, $isInternal = false, $poiId = null) {

        switch ($type) {
            case 'poi':
                $analytics = $this->getAnalyticsEvents('poi',$request,$url,true,$poiId);
                break;
            case 'channel':
                $analytics = $this->getAnalyticsEvents('channel',$request,$url,true,$poiId);
                break;
            case 'scans':
                $analytics = $this->getAnalyticsScans($request,$url,true,$poiId);
                break;
            default:
                # code...
                break;
        }

        return response()->data($analytics, 200, $isInternal);

    }

    private function getUrl(Request $request) {
        return $request->has('url') ? $request->input('url') : null;
    }

    private function getPoi(Request $request) {
        return $request->has('poi') ? $request->input('poi') : null;
    }

}
