<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;
use Cache;

use App\Models\Media;

use App\Http\Services\MediaService;
use App\Http\Services\TenantService;

use App\Jobs\ResizeImageJob;

use App\Traits\HasSearch;
use App\Traits\PaginatesLists;

class MediaController extends Controller {

    use HasSearch, PaginatesLists;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(MediaService::getLastModified(Auth::user()->tenant_id,new Media(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('media:index');

        $medias = array();

        if(Cache::has($cacheKey)) {
            $medias = Cache::get($cacheKey);
        } else {

            $medias = Auth::user()->media()->where('used_for','media')->orderBy('created_at','desc')->get();

            foreach($medias as $media) {
                $media->pois;
            }

            Cache::forever($cacheKey,$medias);

        }

        return response()->data($this->paginate($medias,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $mediaId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('media:'.$mediaId);

        if(Cache::has($cacheKey)) {
            $media = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();

            try {

                $media = Auth::user()->media()->findOrFail($mediaId);

                $media->pois;

                Cache::forever($cacheKey,$media);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($media, 200, $isInternal);

    }

    public function getFile(Request $request, $mediaId) {

        $cacheKey = 'media:'.$mediaId;

        if(Cache::has($cacheKey)) {
            $media = Cache::get($cacheKey);
        } else {

            try {

                $media = Media::findOrFail($mediaId);
                Cache::forever($cacheKey,$media);

            } catch (\Exception $e) {
                return response()->data([], 404, false, ['exception' => $e->getMessage()]);
            }

        }

        if($media->simple_type == 'image') {

            if($request->has('preview') && $request->input('preview') == true) {
                $path = $media->tenant_id.'/'.config('substance.image_preview').$media->storage_path;
            } else if($request->has('original') && $request->input('original') == true) {
                $path = $media->tenant_id.'/'.$media->storage_path;
            } else {
                $path = $media->tenant_id.'/'.config('substance.image_resized').$media->storage_path;
            }

            if(!Storage::disk(config('substance.storage'))->exists($path)) {
                $path = $media->tenant_id.'/'.$media->storage_path;
            }

        } else {

            if($request->has('preview') && $request->input('preview') == true) {
                $path = null;
            } else {
                $path = $media->tenant_id.'/'.$media->storage_path;
            }

        }

        if(Storage::disk(config('substance.storage'))->exists($path)) {

            if(config('substance.storage')=='public') {
                $url = Storage::disk(config('substance.storage'))->url($path);
                return redirect(TenantService::getSubstanceAppUrl($media->tenant_id).$url)->header('Access-Control-Allow-Origin', '*');
            } else {
                $url = Storage::disk(config('substance.storage'))->url($path);
                return redirect($url)->header('Access-Control-Allow-Origin', '*');
            }

        } else {
            return response()->file('img/no_image.png');
        }

    }

    public function store(Request $request, $isInternal = false) {

        $request->has('used_for') ?: $request['used_for'] = 'media';

        $this->validate($request, MediaService::getStoreMediaValidationRules());

        if ($request->file('file')->isValid()) {

            $file = $request->file('file');

            if(!in_array($file->getMimeType(),MediaService::getAllowedMimeTypes())) {
                return response()->data(array(), 400, $isInternal, ['error' =>trans('api.FILE_UNSUPPORTED')]);
            }

            $isImage = false;
            if(strpos($file->getMimeType(), 'image') !== false) {
                $isImage = true;
            }

            $fileNameWithExtension = MediaService::generateMediaFileNameWithExtension($file,Auth::user());

            Storage::disk(config('substance.storage'))->makeDirectory(Auth::user()->tenant_id);

            $urlToFile = MediaService::storeFile($file,Auth::user()->tenant_id,$fileNameWithExtension,$isImage);

            $media = Media::create([
                'type' => $file->getMimeType(),
                'name' => $request->input('name'),
                'url' => $urlToFile,
                'user_id' => Auth::user()->id,
                'tenant_id' => Auth::user()->tenant_id,
                'storage_path' => $fileNameWithExtension,
                'used_for' => $request->input('used_for'),
            ]);

            if($isImage) {
                $this->dispatchJob((new ResizeImageJob($media))->delay(config('substance.resize_image_wait')));
            }

            MediaService::addUsersToMedia($media->id,Auth::user()->tenant_id,Auth::user()->id);

            $this->invalidateAndModify(
                [
                    'id' => $media->id,
                    'key' => 'media',
                    'object' => new Media()
                ]
            );

            return response()->data($media, 201, $isInternal);

        } else {
            return response()->data(array(), 400, $isInternal, ['error' =>trans('api.FILE_INVALID')]);
        }

    }

    public function update(Request $request, $mediaId, $isInternal = false) {

        $this->validate($request, MediaService::getUpdateMediaValidationRules());

        $this->scopeRequest();

        $cacheKey = 'media:'.$mediaId;

        try {

            $media = Auth::user()->media()->findOrFail($mediaId);
            $mediaData = MediaService::getPartialUpdateValues($request,MediaService::getUpdateMediaValidationRules());
            $media->update($mediaData);

            $this->invalidateAndModify(
                [
                    'id' => $media->id,
                    'key' => 'media',
                    'object' => new Media()
                ]
            );

            if(Cache::has($cacheKey)) {
                Cache::forever($cacheKey,$media);
            }

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

        return response()->data($media, 200, $isInternal);

    }

    public function destroy(Request $request, $mediaId, $isInternal = false) {

        $this->scopeRequest();

        $cacheKey = 'media:'.$mediaId;

        try {

            $media = Auth::user()->media()->findOrFail($mediaId);
            MediaService::removeFileFromStorage($media);
            $media->delete();

            $this->invalidateAndModify(
                [
                    'id' => $mediaId,
                    'key' => 'media',
                    'object' => new Media()
                ]
            );

            Cache::forget($cacheKey);

            return response()->data(null, 204, $isInternal);

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

    }

}
