<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;

use App\Http\Services\ServerService;

use App\Traits\Resellable;

class AngularController extends Controller {

    use Resellable;

    public function serveApp(Request $request) {

        $this->updateConfigForResellerByUrl($request->root());
        return response()->view('index')->header('Cache-Control','no-cache, must-revalidate');

    }

    public function unsupported() {
        return response()->view('unsupported_browser');
    }

    public function latestCommit(Request $request) {
        return response()->data(['sha' => ServerService::getCommit()]);
    }

    public function configByUrl(Request $request) {

        $this->updateConfigForResellerByUrl($request->root());
        return response()->data(config('substance.public'));

    }

}
