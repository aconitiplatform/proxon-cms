<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Poi;
use App\Models\PoiEvent;
use App\Models\Beacon;

use App\Http\Controllers\PoiController;
use App\Http\Controllers\PoiBeaconController;
use App\Http\Controllers\PoiContentController;
use App\Http\Controllers\DesignController;

use App\Http\Services\PoiService;

use App\Jobs\PoiEventJob;

use App\Traits\AffectsEdwardStone;

use App\Events\PoiWasSaved;

class PoiFacadeController extends Controller {

    use AffectsEdwardStone;

    public function update(Request $request, $poiId, $isInternal = false) {

        $poiController = new PoiController(true);

        $poi = $poiController->update($request, $poiId, true);

        $this->updatePoiContents($poi,$request->input('poi_contents'));

        if($request->has('beacons')) {
            $this->updatePoiBeacons($poi,$request->input('beacons'));
        }

        if($request->has('poi_design')) {
            $this->updatePoiDesign($poi,$request->input('poi_design'));
        }

        $this->invalidateAndModify(
            [
                'id' => $poiId,
                'key' => 'pois',
                'object' => new Poi()
            ],
            [
                'id' => $poi->id,
                'key' => 'beacons',
                'object' => new Beacon()
            ],
            [
                'id' => $poiId,
                'key' => 'poi-log-'.$poiId,
                'object' => new PoiEvent()
            ]
        );

        $this->publishPoiToEdwardStone($poiId,$poi->active);
        $this->dispatchJob(new PoiEventJob(['by_superadmin' => $request->input('by_superadmin'),'type' => 'updated', 'text' => null, 'poi_id' => $poi->id],Auth::user()),'eventlogs');

        $poi = Poi::find($poi->id);

        $poi->poiContents;
        $poi->beacons;
        $poi->poiDesign;

        if(config('broadcasting.should_broadcast')) event(new PoiWasSaved($poi,Auth::user()));

        return response()->data($poi, 200, $isInternal);

    }

    private function updatePoiContents(Poi $poi,$poiContents) {

        foreach($poiContents as $poiContent) {

            $poiContentRequest = new \Dingo\Api\Http\Request();

            foreach($poiContent as $k => $v) {
                $poiContentRequest[$k] = $v;
            }

            $poiContentController = new PoiContentController(true);

            if(isset($poiContent['id']) && isset($poiContent['action']) && $poiContent['action'] == 'delete') {
                $poiContentController->destroy($poiContentRequest, $poi->id, $poiContent['id'], true);
            } else if (isset($poiContent['id']) && !isset($poiContent['action'])) {
                $poiContentController->update($poiContentRequest, $poi->id, $poiContent['id'], true);
            } else if (!isset($poiContent['id'])) {
                $poiContentController->store($poiContentRequest, $poi->id, true);
            }

        }

    }

    private function updatePoiBeacons(Poi $poi, $poiBeacons) {

        $poiBeaconRequest = new \Dingo\Api\Http\Request();
        $poiBeaconRequest['beacons'] = $poiBeacons;

        $poiBeaconController = new PoiBeaconController(true);
        $poiBeaconController->bulk($poiBeaconRequest,$poi->id,true);

    }

    private function updatePoiDesign(Poi $poi,$poiDesign) {

        $poiDesignRequest = new \Dingo\Api\Http\Request();

        foreach($poiDesign as $k => $v) {
            $poiDesignRequest[$k] = $v;
        }

        $designController = new DesignController(true);
        $designController->updateForPoi($poiDesignRequest, $poi->id, true);

    }

}
