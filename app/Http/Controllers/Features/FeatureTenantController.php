<?php

namespace App\Http\Controllers\Features;

use Illuminate\Http\Request;

use Auth;

use App\Http\Controllers\Features\FeatureBaseController;

use App\Models\Features\Feature;

use App\Http\Services\Service;

use App\Traits\Validators\FeatureTenantValidationRules;

class FeatureTenantController extends FeatureBaseController {

    use FeatureTenantValidationRules;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(Service::getLastModified(Auth::user()->tenant_id,new Feature(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $tenant = Auth::user()->tenant;
        $features = $tenant->features()->get();
        $features = $features->unique();
        return response()->data($features,200,$isInternal);

    }

    public function show(Request $request, $tenantId, $isInternal = false) {

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($tenantId);

        if($tenant != null) {
            return $this->getResponse($tenant,'features',$isInternal);
        }

        return response()->data([],403,$isInternal);

    }

    public function store(Request $request) {

        $this->validate($request, $this->getStoreFeatureTenantValidationRules());

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($request->input('tenant_id'));

        if($tenant != null) {

            $tenant->features()->sync($this->getFeatureIdsFromSlugs($request->input('features')));
            return $this->getResponse($tenant,'features');

        }

        return response()->data([], 403);

    }

    public function destroy(Request $request) {

        $this->validate($request, $this->getDestroyFeatureTenantValidationRules());

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($request->input('tenant_id'));

        if($tenant != null) {

            $tenant->features()->detach($this->getFeatureIdsFromSlugs($request->input('features')));
            return $this->getResponse($tenant,'features');

        }

        return response()->data([], 403);

    }

    protected function getFeatureIdsFromSlugs($slugs) {

        $ids = [];

        foreach($slugs as $slug) {

            $module = Feature::where('slug',$slug)->first();

            if($module != null) {
                $ids[] = $module->id;
            }

        }

        return $ids;

    }

}
