<?php

namespace App\Http\Controllers\Features;

use Illuminate\Http\Request;

use Auth;

use App\Http\Controllers\Features\FeatureBaseController;

use App\Models\Features\Module;
use App\Models\Features\Feature;

use App\Http\Services\Service;

use App\Traits\Validators\ModuleTenantValidationRules;

class ModuleTenantController extends FeatureBaseController {

    use ModuleTenantValidationRules;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(Service::getLastModified(Auth::user()->tenant_id,new Module(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $tenant = Auth::user()->tenant;
        $modules = $tenant->modules()->get();
        $modules = $modules->unique();
        return response()->data($modules,200,$isInternal);

    }

    public function show(Request $request, $tenantId, $isInternal = false) {

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($tenantId);

        if($tenant != null) {
            return $this->getResponse($tenant,'modules',$isInternal);
        }

        return response()->data([],403,$isInternal);

    }

    public function store(Request $request) {

        $this->validate($request, $this->getStoreModuleTenantValidationRules());

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($request->input('tenant_id'));

        if($tenant != null) {

            $tenant->modules()->sync($this->getModuleIdsFromSlugs($request->input('modules')));
            return $this->getResponse($tenant,'modules');

        }

        return response()->data([], 403);

    }

    public function destroy(Request $request) {

        $this->validate($request, $this->getDestroyModuleTenantValidationRules());

        $tenant = $this->getTenantIfBelongsToSuperadminOrReseller($request->input('tenant_id'));

        if($tenant != null) {

            $tenant->modules()->detach($this->getModuleIdsFromSlugs($request->input('modules')));
            return $this->getResponse($tenant,'modules');

        }

        return response()->data([], 403);

    }

    protected function getModuleIdsFromSlugs($slugs) {

        $ids = [];

        foreach($slugs as $slug) {

            $module = Module::where('slug',$slug)->first();

            if($module != null) {
                $ids[] = $module->id;
            }

        }

        return $ids;

    }

}
