<?php

namespace App\Http\Controllers\Features;

use Illuminate\Http\Request;

use Auth;

use App\Http\Controllers\Features\FeatureBaseController;
use App\Http\Controllers\Features\FeatureTenantController;
use App\Http\Controllers\Features\ModuleTenantController;

use App\Models\Features\Feature;
use App\Models\Features\Module;

use App\Http\Services\Service;

class PermissionsFacadeController extends FeatureBaseController {

    public function show(Request $request, $tenantId) {

        $featureTenantController = new FeatureTenantController(true);
        $featuresOfAuthenticatedTenant = $featureTenantController->index($request,true);
        $featuresOfQueriedTenant = $featureTenantController->show($request,$tenantId,true);

        $moduleTenantController = new ModuleTenantController(true);
        $modulesOfAuthenticatedTenant = $moduleTenantController->index($request,true);
        $modulesOfQueriedTenant = $moduleTenantController->show($request,$tenantId,true);


        return response()->data([
            'features_of_authenticated_tenant' => $featuresOfAuthenticatedTenant,
            'features_of_queried_tenant' => $featuresOfQueriedTenant,
            'modules_of_authenticated_tenant' => $modulesOfAuthenticatedTenant,
            'modules_of_queried_tenant' => $modulesOfQueriedTenant,
        ]);

    }

}
