<?php

namespace App\Http\Controllers\Features;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Auth;

use App\Models\Tenant;
use App\Models\Reseller;
use App\Models\Features\Feature;
use App\Models\Features\Module;

class FeatureBaseController extends Controller {

    public function getFeaturesToAssign(Request $request) {

        if(Auth::user()->role == 'superadmin') {
            $features = Feature::all();
        } else if (Auth::user()->role == 'reseller') {
            $features = Auth::user()->features;
        } else {
            return response()->data([], 403);
        }

        return response()->data($features->unique());

    }

    public function getModulesToAssign(Request $request) {

        if(Auth::user()->role == 'superadmin') {
            $modules = Module::all();
        } else if (Auth::user()->role == 'reseller') {
            $modules = Auth::user()->modules;
        } else {
            return response()->data([], 403);
        }

        return response()->data($modules->unique());

    }

    protected function getTenantIfBelongsToSuperadminOrReseller($tenantId) {

        if(Auth::user()->role == 'superadmin') {
            return Tenant::findOrFail($tenantId);
        } else if (Auth::user()->role == 'reseller') {
            $reseller = Reseller::findOrFail(Auth::user()->id);
            return $reseller->tenants()->findOrFail($tenantId);
        } else {
            return null;
        }

    }

    protected function getResponse($tenant,$relation,$isInternal = false) {

        switch ($relation) {
            case 'features':
                $invalidatableObject = new Feature;
                break;
            case 'modules':
                $invalidatableObject = new Module;
                break;
        }

        $this->invalidateAndModify(
            [
                'object' => $invalidatableObject,
                'tenant_id' => $tenant->id
            ]
        );

        $collection = $tenant->$relation;
        $collection = $collection->unique();
        return response()->data($collection,200,$isInternal);

    }

}
