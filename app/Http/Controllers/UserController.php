<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Cache;

use App\Models\User;

use App\Http\Controllers\Auth\PasswordResetController;

use App\Http\Services\UserService;

use App\Events\UserEmailHasChanged;

use App\Traits\PaginatesLists;
use App\Traits\HasSearch;

class UserController extends Controller {

    use PaginatesLists, HasSearch;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(UserService::getLastModified(Auth::user()->tenant_id,new User(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('users:index');

        if(Cache::has($cacheKey)) {
            $users = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();
            $users = User::where('role','!=','app')->get();
            Cache::forever($cacheKey,$users);

        }

        return response()->data($this->paginate($users,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $userId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('users:'.$userId);

        if(Cache::has($cacheKey)) {
            $user = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();

            try {

                $user = User::findOrFail($userId);
                if($user->role == 'app') throw new ModelNotFoundException();
                $user->beacons;
                Cache::forever($cacheKey,$user);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($user, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, UserService::getStoreUserValidationRules());

            $newUser = new User();
            $newUser->firstname = trim($request->input('firstname'));
            $newUser->lastname = trim($request->input('lastname'));
            $newUser->email = trim($request->input('email'));
            $newUser->password = bcrypt(time().config('app.key'));
            $newUser->role = trim($request->input('role'));
            $newUser->language = trim($request->input('language'));
            $newUser->tenant_id = Auth::user()->tenant_id;
            $newUser->active = true;
            $newUser->locale = trim($request->input('locale'));

            $newUser->save();

            $passwordResetController = new PasswordResetController();
            $passwordResetController->triggerInternalSendResetLinkEmail($newUser->email,true);

            if($newUser->role != 'subuser') {
                UserService::giveAccessToAllEntities($newUser);
            }

            event(new UserEmailHasChanged($newUser));

            $this->invalidateAndModify(
                [
                    'id' => $newUser->id,
                    'key' => 'users',
                    'object' => new User()
                ]
            );

            $newUser->beacons;

            return response()->data($newUser, 201, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function update(Request $request, $userId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id) || Auth::user()->id == $userId) {

            $this->validate($request, UserService::getUpdateUserValidationRules($userId));

            try {

                $updatableUser = User::findOrFail($userId);

                if($request->has('email') && $updatableUser->email != $request->input('email')) {
                    event(new UserEmailHasChanged($updatableUser));
                }

                if($request->has('role') && $updatableUser->role != $request->input('role')) {

                    if ($updatableUser->role != 'subuser' && $request->input('role') == 'subuser') {
                        //remove all automatically assigned beacons
                        UserService::removeAccessFromAllEntities($updatableUser);
                    } else {
                        UserService::removeAccessFromAllEntities($updatableUser);
                        UserService::giveAccessToAllEntities($updatableUser);
                    }

                }

                $updatableUser->update(UserService::getPartialUpdateValues($request,UserService::getUpdateUserValidationRules($userId)));

                $this->invalidateAndModify(
                    [
                        'id' => $updatableUser->id,
                        'key' => 'users',
                        'object' => new User()
                    ]
                );

                $updatableUser->beacons;

                return response()->data($updatableUser, 200, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }


    }

    public function destroy(Request $request, $userId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id) && $userId != Auth::user()->id) {

            try {

                $deletableUser = User::findOrFail($userId);
                $tenantId = $deletableUser->tenant_id;

                $this->invalidateAndModify(
                    [
                        'id' => $deletableUser->id,
                        'key' => 'users',
                        'object' => new User()
                    ]
                );

                $deletableUser->delete();

                return response()->data(null, 204, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function MlmRegistration(Request $request, $isInternal = false) {

        

            $this->validate($request, UserService::getStoreUserValidationRules());

            $newUser = new User();
            $newUser->firstname = trim($request->input('firstname'));
            $newUser->lastname = trim($request->input('lastname'));
            $newUser->email = trim($request->input('email'));
            $newUser->password = bcrypt(time().config('app.key'));
            $newUser->role = trim($request->input('role'));
            $newUser->language = trim($request->input('language'));
            $newUser->tenant_id = $request->input('tenant_id');
            $newUser->active = true;
            $newUser->locale = trim($request->input('locale'));

            $newUser->save();

            // $passwordResetController = new PasswordResetController();
            // $passwordResetController->triggerInternalSendResetLinkEmail($newUser->email,true);

            if($newUser->role != 'subuser') {
                UserService::giveAccessToAllEntities($newUser);
            }

            event(new UserEmailHasChanged($newUser));

            // $this->invalidateAndModify(
            //     [
            //         'id' => $newUser->id,
            //         'key' => 'users',
            //         'object' => new User()
            //     ]
            // );

            $newUser->beacons;

            return response()->data($newUser, 201, $isInternal);

        

    }

}
