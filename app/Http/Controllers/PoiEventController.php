<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cache;
use Auth;

use App\Models\Poi;
use App\Models\PoiEvent;
use App\Models\User;

use App\Http\Services\PoiService;
use App\Http\Services\UserService;

use App\Traits\PaginatesLists;
use App\Traits\SavesPoiEvent;

class PoiEventController extends Controller {

    use PaginatesLists, SavesPoiEvent;

    public function lastModified(Request $request, $poiId, $isInternal = false) {
        return response()->data(PoiService::getLastModified(Auth::user()->tenant_id,new PoiEvent(),true), 200, $isInternal);
    }

    public function index(Request $request, $poiId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('poi-log-'.$poiId.':index');

        $poiEvents = [];

        if(Cache::has($cacheKey)) {
            $poiEvents = Cache::get($cacheKey);
        } else {

            try {

                $poi = Auth::user()->pois()->findOrFail($poiId);

                $poiEvents = $poi->poiEvents()->orderBy('created_at','desc')->get();

                $poiEvents = $poiEvents->each(function($item,$key) {
                    $item->user;
                    $item->superadmin;
                });

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal);
            }

            Cache::forever($cacheKey,$poiEvents);

        }

        return response()->data($this->paginate($poiEvents,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function store(Request $request, $poiId) {

        $this->validate($request, PoiService::getStorePoiEventValidationRules());

        try {
            $poi = Auth::user()->pois()->findOrFail($poiId);
        } catch (\Exception $e) {
            return response()->data([], 404);
        }

        $poiEventData = $request->only('type','text','by_superadmin');
        $poiEventData['poi_id'] = $poi->id;

        $poiEvent = $this->savePoiEvent($poiEventData,Auth::user());

        $this->invalidateAndModify(
            [
                'id' => $poiEvent->id,
                'key' => 'poi-log-'.$poiId,
                'object' => new PoiEvent()
            ]
        );

        return response()->data($poiEvent, 201);

    }

}
