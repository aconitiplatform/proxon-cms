<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Opengraph;

use Redis;
use Auth;
use App;
use Log;
use Faker;

use App\Models\Tenant;
use App\Models\Poi;
use App\Models\Beacon;
use App\Models\Features\Module;
use App\Models\Features\Feature;
use App\Models\Analytics\AnalyticsJourney;

use App\Http\Services\Apis\KontaktIoService;

use App\Jobs\AddToMailChimpListJob;
use App\Jobs\Analytics\WriteAnalyticsJob;

use App\Events\WebSocketEvent;

class TestStuffController extends Controller {

    public function stripe(Request $request) {

        try {

            \Stripe\Stripe::setApiKey("sk_test_7fitgqSwYdEweQjSY5PhCAi9");

            $token = \Stripe\Token::create(array("card"=>["number" => "4242424242424242","exp_month" => 1,"exp_year" => 2018,"cvc" => "314"]));
            var_dump($token);
            $customer = \Stripe\Customer::create(array(
                "description" => "Customer for stripe@partner.rakete7.com",
                "source" => $token->id,
                "email" => "stripe@partner.rakete7.com"
            ));
            var_dump($customer);
            // $tenant = Tenant::findOrFail('99b598c5-ce2e-418c-aee0-7c2725482a8d');
            // var_dump($tenant);
            \Stripe\Subscription::create(array(
                "customer" => $customer->id,
                "items" => array(
                    array(
                        "plan" => "mesostest",
                    ),
                )
            ));

        } catch (\Exception $e) {
            dd($e);
        }



    }

    public function moduleDefinitions(Request $request) {

        if(App::environment('local')) {

            $modules = Module::all();
            return response()->data($modules);

        }

    }

    public function analyticsScan(Request $request, $times) {

        if(App::environment('local')) {

            for($i = 0; $i < $times; $i++) {

                $faker = Faker\Factory::create();

                $tenant = Tenant::inRandomOrder()->first();
                $poi = $tenant->pois()->inRandomOrder()->first();

                if($poi == null) {
                    continue;
                }

                $journeyId = $this->getJourneyId();

                if(rand(1,6) % 2 == 0) {
                    $beacon = $tenant->beacons()->inRandomOrder()->first();
                    $url = $beacon->url;
                } else {
                    $url = $poi->id;
                }

                $payload = [
                    'tenant_id' => $tenant->id,
                    'poi_id' => $poi->id,
                    'journey_id' => $journeyId,
                    'url' => $url,
                    'user_agent' => $faker->userAgent,
                    'fired_at' => time() - rand(0,2419200),
                ];

                $this->dispatchJob(new WriteAnalyticsJob('scan',$payload));
                $number = $i + 1;
                echo "Dispatched Analytics Scan {$number}<br>";

            }

        }

    }

    // public function analyticsJourney(Request $request, $times) {
    //
    //     if(App::environment('local')) {
    //
    //         for($i = 0; $i < $times; $i++) {
    //
    //
    //             $tenant = Tenant::inRandomOrder()->first();
    //
    //             $payload = [
    //                 'tenant_id' => $tenant->id,
    //             ];
    //
    //             $this->dispatchJob(new WriteAnalyticsJob('journey',$payload));
    //             $number = $i + 1;
    //             echo "Dispatched Analytics Journey {$number}<br>";
    //
    //         }
    //
    //     }
    //
    // }

    public function analyticsEvent(Request $request, $times) {

        if(App::environment('local')) {

            for($i = 0; $i < $times; $i++) {

                $faker = Faker\Factory::create();

                $tenant = Tenant::inRandomOrder()->first();
                $poi = $tenant->pois()->inRandomOrder()->first();

                if($poi == null) {
                    continue;
                }

                $journeyId = $this->getJourneyId();;

                if(rand(1,6) % 2 == 0) {
                    $beacon = $tenant->beacons()->inRandomOrder()->first();
                    $url = $beacon->url;
                } else {
                    $url = $poi->id;
                }

                if(rand(1,6) % 2 == 0) {
                    $accessed = 'poi';
                    $events = [
                        'beacon_via_scanner',
                        'beacon_via_browser',
                        'poi_via_beacon',
                        'poi_via_browser',
                    ];
                } else {
                    $accessed = 'channel';
                    $events = [
                        'channel_via_beacon',
                        'channel_via_browser',
                    ];
                }

                if(rand(1,6) % 2 == 0) {
                    $from = 'web';
                } else {
                    $from = 'beacon';
                }

                if(rand(1,6) % 2 == 0) {
                    $os = 'iOS';
                } else {
                    $os = 'Android';
                }

                $payload = [
                    'tenant_id' => $tenant->id,
                    'poi_id' => $poi->id,
                    'journey_id' => $journeyId,
                    'url' => $url,
                    'user_agent' => $faker->userAgent,
                    'accessed' => $accessed,
                    'from' => $from,
                    'event' => $events[rand(0,count($events)-1)],
                    'os' => $os,
                    'fired_at' => time() - rand(0,2419200),
                ];

                $this->dispatchJob(new WriteAnalyticsJob('event',$payload));
                $number = $i + 1;
                echo "Dispatched Analytics Event {$number}<br>";

            }

        }

    }

    public function mailchimp(Request $request) {

        if(App::environment('local')) {
            $this->dispatchJob(new AddToMailChimpListJob("04d7618eedec894ddfd1bc4ab5804d27-us14","6f2bc7d397"));
        }

    }

    public function kontaktiovenue(Request $request) {

        if(App::environment('local')) {

            $venue = KontaktIoService::createVenue("test".time(),"a great description");

            return response()->json($venue);

        }

    }

    public function monolog(Request $request) {

        if(!App::environment('production')) {
            throw new \Exception();
        }

    }

    public function logging(Request $request) {

        if(App::environment('local')) {
            return response()->json(array('hi'=>'servus'));
        }

    }

    public function fireTestEvent(Request $request) {
        if(config('broadcasting.should_broadcast')) event(new WebSocketEvent());
        return response()->json(["response" => "event fired"]);
    }

    public function redisCollection(Request $request) {

        $lm = Redis::keys(config('substance.cache_prefix').':'.Auth::user()->tenant_id.':last-modified:*');
        var_dump($lm);
        return response()->json($lm);

    }


    private function getJourneyId() {

        $journeyIds = [
            '761c83e2-bfcb-4ead-82a0-c9ce9316617c',
            'b9b61ff8-f981-45b6-84c1-e979c0508e05',
            '7d66c35f-6c89-43de-8f3b-ef18e263c7de',
            '7a12f2b8-ba23-4fd8-bdfb-0b072e171c49',
            '26417c87-bcaa-45b4-ac99-f87b5be1071e',
            'ae640737-77fc-4d52-9df6-012a1e1e1516',
            '58a9fc25-6c1f-4f49-af9c-1ea3d74142ee',
            '1eb6d649-e76a-453f-96f8-eab5a8285d3d',
            'c222fe1c-1a91-4e71-87c3-391c6e0c7956',
            'd735898a-6184-4c8c-8a2d-8ebafbb86cbe',
            '24dcc258-2216-4aa4-97fc-bba0017db484',
            '91fea55b-3f5a-403b-a81c-910232ee1008',
            '4d99de73-a6dc-499d-bd9c-0937a8fdc9a6',
            '8bb284a4-fea0-41ca-aa69-18a9e0cf7092',
            '50789b0f-770b-4967-a252-fd88033224de',
            'b8fd5207-e16b-46ff-8e8f-d7f3671805e8',
            '7ebf20ca-9a3a-4077-b4a5-f3da5bb82dce',
            '4a0a41e6-fcdb-43cb-bbc8-2650f705b85a',
            '2b5e8247-71d7-465d-a280-02879ad02ca8',
            '878e823f-a6db-4cfa-9672-12013c2d6d1d',
            '34f94bd3-492b-4da7-93a2-dbd66ac759bc',
            '1abd5399-38c1-4ee7-aa01-b363662598b4',
            'c40a6d65-f444-48ea-8bbf-8deb1ea75ee1',
            '253a5d56-ffc2-49b3-9aea-90e413547a3e',
            '6065ce20-c8b6-40d3-a115-209a0c139723',
            '0dc54894-183e-4654-babb-b463973f8d57',
        ];

        return $journeyIds[rand(0,count($journeyIds)-1)];

    }

}
