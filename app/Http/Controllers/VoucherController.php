<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Intervention\Image\ImageManager;

use Storage;
use Cache;
use Response;

use App\Models\Media;

use App\Http\Services\VoucherService;
use App\Http\Services\TenantService;

class VoucherController extends Controller {

    public function getFile(Request $request, $mediaId) {

        $cacheKey = 'media:'.$mediaId;

        if(Cache::has($cacheKey)) {
            $media = Cache::get($cacheKey);
        } else {

            try {

                $media = Media::findOrFail($mediaId);
                Cache::forever($cacheKey,$media);

            } catch (\Exception $e) {
                return response()->data([], 404, false, ['exception' => $e->getMessage()]);
            }

        }

        if($media->used_for == 'media' && $media->simple_type == 'image') {

            if($request->has('preview') && $request->input('preview') == true) {
                $path = $media->tenant_id.'/'.config('substance.image_preview').$media->storage_path;
            } else if($request->has('original') && $request->input('original') == true) {
                $path = $media->tenant_id.'/'.$media->storage_path;
            } else {
                $path = $media->tenant_id.'/'.config('substance.image_resized').$media->storage_path;
            }

            if(Storage::disk(config('substance.storage'))->exists($path)) {

                $url = Storage::disk(config('substance.storage'))->url($path);
                header("Content-Type: ".$media->type);
                return readfile($url);

            }

        }

        return response()->file('img/no_image.png');

    }

    public function proxyBarcode(Request $request) {

        $this->validate($request, VoucherService::getProxyBarcodeValidationRules());

        $url = config('substance.barcode_api');

        $url = str_replace(config('substance.barcode_api_type'),htmlentities($request->input('bc_type')),$url);
        $url = str_replace(config('substance.barcode_api_text'),htmlentities($request->input('bc_content')),$url);
        // dd($url);
        $client = new Client([
            'base_uri' => $url,
            'timeout'  => 2.0,
            'headers' => ['Accept' => 'image/png']
        ]);

        $response = $client->request('GET','',['stream' => true]);

        $img = "";
        $body = $response->getBody();

        while (!$body->eof()) {
            $img .= $body->read(1024);
        }

        $imageManager = new ImageManager(array('driver' => 'imagick'));
        $image = $imageManager->make($img);

        $png = $image->encode('png');
        $base64 = 'data:image/png;base64,'.base64_encode($png);

        $response = Response::make($base64);
        return $response;

    }

}
