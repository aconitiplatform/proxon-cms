<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Beacon;
use App\Models\BeaconLocation;

use App\Http\Controllers\BeaconController;
use App\Http\Controllers\BeaconLocationController;

use App\Http\Services\BeaconService;

use App\Traits\AffectsEdwardStone;

class BeaconFacadeController extends Controller {

    use AffectsEdwardStone;

    public function update(Request $request, $beaconId, $isInternal = false) {

        $beaconController = new BeaconController(true);

        $beacon = $beaconController->update($request, $beaconId, true);

        if($request->has('beacon_location')) {
            $this->updateBeaconLocation($beacon,$request->input('beacon_location'));
        }

        $this->invalidateAndModify(
            [
                'id' => $beacon->id,
                'key' => 'beacons',
                'object' => new Beacon()
            ],
            [
                'id' => $beacon->id,
                'key' => 'beaconlocations',
                'object' => new BeaconLocation()
            ]
        );

        $beacon = Beacon::find($beacon->id);
        $beacon->beaconLocation;
        $beacon->poi;

        return response()->data($beacon, 200, $isInternal);

    }

    private function updateBeaconLocation(Beacon $beacon, $beaconLocation) {

        $beaconLocationRequest = new \Dingo\Api\Http\Request();

        foreach($beaconLocation as $k => $v) {
            $beaconLocationRequest[$k] = $v;
        }

        $beaconLocationController = new BeaconLocationController(true);

        if($beaconLocationController->show($beaconLocationRequest, $beacon->id, true) == null) {
            $beaconLocationController->store($beaconLocationRequest, $beacon->id, true);
        } else {
            $beaconLocationController->update($beaconLocationRequest, $beacon->id, true);
        }

    }

}
