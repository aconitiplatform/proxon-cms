<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\NewsletterConfiguration;

use App\Http\Services\NewsletterConfigurationService;

use App\Jobs\SubscribeToNewsletterJob;

use App\Traits\PaginatesLists;
use App\Traits\HasSearch;

class NewsletterConfigurationController extends Controller {

    use PaginatesLists, HasSearch;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(NewsletterConfigurationService::getLastModified(Auth::user()->tenant_id,new NewsletterConfiguration(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $this->scopeRequest();

        $cacheKey = $this->getRoleBasedCacheKey('newsletter-configurations:index');

        $ncs = array();

        if(Cache::has($cacheKey)) {
            $ncs = Cache::get($cacheKey);
        } else {

            $ncs = Auth::user()->newsletterConfigurations;

            Cache::forever($cacheKey,$ncs);

        }

        return response()->data($this->paginate($ncs,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $newsletterConfigurationId, $isInternal = false) {

        $this->scopeRequest();

        $cacheKey = $this->getRoleBasedCacheKey('newsletter-configurations:'.$newsletterConfigurationId);

        if(Cache::has($cacheKey)) {
            $nc = Cache::get($cacheKey);
        } else {

            try {

                $nc = Auth::user()->newsletterConfigurations()->findOrFail($newsletterConfigurationId);

                Cache::forever($cacheKey,$nc);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($nc, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        $this->scopeRequest();

        $this->validate($request, NewsletterConfigurationService::getStoreNewsletterConfigurationValidationRules());

        $ncData = $request->only('name','type','configuration');
        $ncData['user_id'] = Auth::user()->id;
        $ncData['tenant_id'] = Auth::user()->tenant_id;

        $nc = NewsletterConfiguration::create($ncData);

        $this->invalidateAndModify(
            [
                'id' => $nc->id,
                'key' => 'newsletter-configurations',
                'object' => new NewsletterConfiguration()
            ]
        );

        NewsletterConfigurationService::addUsersToNewsletterConfiguration($nc->id,Auth::user()->tenant_id,Auth::user()->id);

        return response()->data($nc, 201, $isInternal);

    }

    public function update(Request $request, $newsletterConfigurationId, $isInternal = false) {

        $this->scopeRequest();

        $this->validate($request, NewsletterConfigurationService::getUpdateNewsletterConfigurationValidationRules());

        try {

            $nc = Auth::user()->newsletterConfigurations()->findOrFail($newsletterConfigurationId);

            $ncData = NewsletterConfigurationService::getPartialUpdateValues($request,NewsletterConfigurationService::getUpdateNewsletterConfigurationValidationRules());
            $nc->update($ncData);

            $this->invalidateAndModify(
                [
                    'id' => $nc->id,
                    'key' => 'newsletter-configurations',
                    'object' => new NewsletterConfiguration()
                ]
            );

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

        return response()->data($nc, 200, $isInternal);

    }

    public function destroy(Request $request, $newsletterConfigurationId, $isInternal = false) {

        $this->scopeRequest();

        try {

            $nc = Auth::user()->newsletterConfigurations()->findOrFail($newsletterConfigurationId);

            $this->invalidateAndModify(
                [
                    'id' => $nc->id,
                    'key' => 'newsletter-configurations',
                    'object' => new NewsletterConfiguration()
                ]
            );

            $nc->delete();

            return response()->data(null, 204, $isInternal);

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

    }

    public function subscribe(Request $request, $newsletterConfigurationId, $isInternal = false) {

        $this->scopeRequest();

        $request['newsletter_configuration'] = $newsletterConfigurationId;
        $this->validate($request, NewsletterConfigurationService::getSubscribeToNewsletterValidationRules());

        if($request->has('options')) {
            $options = $request->input('options');
        } else {
            $options = array();
        }

        $this->dispatchJob(new SubscribeToNewsletterJob($request->input('newsletter_configuration'),$request->input('email')),'subscriptions');

        return response()->data(trans('api.201'), 201, $isInternal);

    }

}
