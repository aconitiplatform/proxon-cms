<?php

namespace App\Http\Controllers\Analytics;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Cache;
use Log;
use Redis;
use App;

use App\Models\Analytics\AnalyticsEvent;
use App\Models\Analytics\AnalyticsScan;
use App\Models\Analytics\Chart;

use App\Http\Controllers\Controller;

class KpiController extends Controller {

    private $meta = [];

    public function getDayKpis(Request $request) {

        $poiId = $request->has('poi') ? $request->input('poi') : "";

        $cdt = $this->getCarbonCurrent();

        $current = $this->withPoiFromCache(Auth::user()->tenant_id,$cdt,$poiId);
        $yesterday = $this->withPoiFromCache(Auth::user()->tenant_id,$cdt->subDay(),$poiId);
        $day_before_yesterday = $this->withPoiFromCache(Auth::user()->tenant_id,$cdt->subDay(),$poiId);

        return response()->data(compact('current','yesterday','day_before_yesterday'));

    }

    private function getCarbonCurrent() {

        $current = (App::environment('local')) ? Carbon::createFromTimestamp(1498003200) : Carbon::now();
        $current->startOfDay();
        return $current;

    }

    private function withPoiFromCache($tenantId,$dt,$poiId = "") {

        $counts = [
            'poi' => [
                "poi_via_beacon:%poi%count",
                "poi_via_browser:%poi%count",
                "beacon:%poi%count",
                "web:%poi%count",
                "%poi%count",
            ],
            'scan' => [
                "%poi%count"
            ]
        ];

        $poiIdWithColon = ($poiId == "") ? $poiId : $poiId.":";

        foreach($counts as $accessed => $fields) {

            foreach($fields as $field) {

                $dataKey = "{$accessed}_".str_replace(":","_",str_replace("%poi%","",$field));
                $redisKey = config('substance.cache_prefix').":{$accessed}:{$tenantId}:{$dt->timestamp}:";
                $fieldKey = str_replace("%poi%","{$poiIdWithColon}",$field);
                $value = Redis::connection('analytics')->get($redisKey.$fieldKey);
                $data[$dataKey] = ($value != null) ? (int)$value : 0;

            }

        }

        $data['poi_scan_conversion'] = ($data['poi_count'] == 0 || $data['scan_count'] == 0) ? 0 : round($data['poi_count']/$data['scan_count']*100,2);
        $data['poi_beacon_scan_conversion'] = ($data['poi_beacon_count'] == 0 || $data['scan_count'] == 0) ? 0 : round($data['poi_beacon_count']/$data['scan_count']*100,2);
        $data['poi_web_scan_conversion'] = ($data['poi_web_count'] == 0 || $data['scan_count'] == 0) ? 0 : round($data['poi_web_count']/$data['scan_count']*100,2);

        return $data;

    }

}
