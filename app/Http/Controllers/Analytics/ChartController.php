<?php

namespace App\Http\Controllers\Analytics;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Cache;
use Log;

use App\Models\Analytics\AnalyticsEvent;
use App\Models\Analytics\AnalyticsScan;
use App\Models\Analytics\Chart;

use App\Http\Controllers\Controller;

class ChartController extends Controller {

    private $meta = [];

    public function getFourWeeksViewedPoisChart(Request $request, $until) {
        return response()->data($this->createDeliveredContentChart($until,28,$request->input('poi'),'poi',$request->segments()),200,false,$this->meta);
    }

    public function getSevenDaysViewedPoisChart(Request $request, $until) {
        return response()->data($this->createDeliveredContentChart($until,7,$request->input('poi'),'poi',$request->segments()),200,false,$this->meta);
    }

    public function getFourWeeksViewedChannelsChart(Request $request, $until) {
        return response()->data($this->createDeliveredContentChart($until,28,$request->input('poi'),'channel',$request->segments()),200,false,$this->meta);
    }

    public function getSevenDaysViewedChannelsChart(Request $request, $until) {
        return response()->data($this->createDeliveredContentChart($until,7,$request->input('poi'),'channel',$request->segments()),200,false,$this->meta);
    }

    public function getFourWeeksScannedBeaconsChart(Request $request, $until) {
        return response()->data($this->createScannedBeaconsChart($until,28,$request->input('poi'),$request->segments()),200,false,$this->meta);
    }

    public function getSevenDaysScannedBeaconsChart(Request $request, $until) {
        return response()->data($this->createScannedBeaconsChart($until,7,$request->input('poi'),$request->segments()),200,false,$this->meta);
    }

    private function createDeliveredContentChart($until,$days,$poiId = null,$accessed,$segments) {

        $actual = $this->getCarbonActual($until);
        $compare = $this->getCarbonCompare($until,$days);

        // $cacheKey = $this->getChartCacheKey($segments,$actual,$poiId);
        //
        // if(!!$chart = $this->getStoredChart($cacheKey)) {
        //     return $chart;
        // }

        for($i = 1; $i <= $days; $i++) {
            // $series[0][] = $this->withPoi(AnalyticsEvent::where('tenant_id',Auth::user()->tenant_id)->where('accessed',$accessed)->where('fired_at_timestamp','<',$actual->toDateTimeString())->where('fired_at_timestamp','>=',$actual->subDay()->toDateTimeString()),$poiId);
            // $series[1][] = $this->withPoi(AnalyticsEvent::where('tenant_id',Auth::user()->tenant_id)->where('accessed',$accessed)->where('fired_at_timestamp','<',$compare->toDateTimeString())->where('fired_at_timestamp','>=',$compare->subDay()->toDateTimeString()),$poiId);
            $actual->subDay();
            $compare->subDay();
            $series[0][] = $this->withPoiFromCache($accessed,Auth::user()->tenant_id,$actual->timestamp,$poiId);
            $series[1][] = $this->withPoiFromCache($accessed,Auth::user()->tenant_id,$compare->timestamp,$poiId);
            $this->setMetaDates($actual,$compare);
            $labels[] = $actual->dayOfWeek;
        }

        $chart = $this->reverseForChartJs($labels,$series);
        // $this->storeChart($cacheKey,$chart);
        return $chart;

    }

    private function createScannedBeaconsChart($until,$days,$poiId = null,$segments) {

        $actual = $this->getCarbonActual($until);
        $compare = $this->getCarbonCompare($until,$days);

        // $cacheKey = $this->getChartCacheKey($segments,$actual,$poiId);
        //
        // if(!!$chart = $this->getStoredChart($cacheKey)) {
        //     return $chart;
        // }

        for($i = 1; $i <= $days; $i++) {
            // $series[0][] = $this->withPoi(AnalyticsScan::where('tenant_id',Auth::user()->tenant_id)->where('fired_at_timestamp','<',$actual->toDateTimeString())->where('fired_at_timestamp','>=',$actual->subDay()->toDateTimeString()),$poiId);
            // $series[1][] = $this->withPoi(AnalyticsScan::where('tenant_id',Auth::user()->tenant_id)->where('fired_at_timestamp','<',$compare->toDateTimeString())->where('fired_at_timestamp','>=',$compare->subDay()->toDateTimeString()),$poiId);
            $actual->subDay();
            $compare->subDay();
            $series[0][] = $this->withPoiFromCache('scan',Auth::user()->tenant_id,$actual->timestamp,$poiId);
            $series[1][] = $this->withPoiFromCache('scan',Auth::user()->tenant_id,$compare->timestamp,$poiId);
            $this->setMetaDates($actual,$compare);
            $labels[] = $actual->dayOfWeek;
        }

        $chart = $this->reverseForChartJs($labels,$series);
        // $this->storeChart($cacheKey,$chart);
        return $chart;

    }

    private function withPoiFromCache($accessed,$tenantId,$timestamp,$poiId = null) {

        if($poiId == null) {
            $cacheKey = "{$accessed}:{$tenantId}:{$timestamp}:count";
        } else {
            $cacheKey = "{$accessed}:{$tenantId}:{$timestamp}:{$poiId}:count";
        }

        if(Cache::store('analytics')->has($cacheKey)) {
            return Cache::store('analytics')->get($cacheKey);
        }

        return 0;

    }

    private function withPoi($queryBuilder,$poiId = null) {

        if($poiId == null) {
            return $queryBuilder->count();
        } else {
            return $queryBuilder->where('poi_id',$poiId)->count();
        }

    }

    private function getCarbonActual($until) {

        $actual = Carbon::createFromTimestamp($until);
        $actual->startOfDay();
        return $actual;

    }

    private function getCarbonCompare($until,$days) {

        $compare = Carbon::createFromTimestamp($until);
        $compare->subDays($days);
        $compare->startOfDay();
        return $compare;

    }

    private function setMetaDates($actual,$compare) {

        //will not be displayed in cached responses. Just for testing purposes.
        $this->meta['dates'][] = [
            'series_1_day' => $actual->toDateTimeString(),
            'series_1_timestamp' => $actual->timestamp,
            'series_2_day' => $compare->toDateTimeString(),
            'series_2_timestamp' => $compare->timestamp,
        ];

    }

    private function reverseForChartJs($labels,$series) {

        $this->meta['dates'] = array_reverse($this->meta['dates']);

        $labels = array_reverse($labels);

        foreach($series as $k => $s) {
            $series[$k] = array_reverse($s);
        }

        return compact('labels','series');

    }

}
