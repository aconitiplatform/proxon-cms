<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Poi;
use App\Models\PoiEvent;
use App\Models\PoiContent;

use App\Http\Services\PoiService;
use App\Http\Services\PoiContentModules\GroupService;

use App\Traits\AffectsEdwardStone;

use App\Jobs\GenerateMediaPoiRelationsJob;
use App\Jobs\SetPoiInteractionFlagsJob;

class PoiContentController extends Controller {

    use AffectsEdwardStone;

    public function index(Request $request, $poiId, $isInternal = false) {

        $cacheKey = $this->getTenantRootCacheKey('pois:'.$poiId.':contents:index');

        if(!!$poi = PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            if(Cache::has($cacheKey)) {
                return response()->success(Cache::get($cacheKey));
            } else {

                $poiContents = PoiContent::where('poi_id',$poiId)->get();

                if($poiContents->isEmpty()) {
                    return response()->data([], 404, $isInternal);
                }

                Cache::forever($cacheKey,$poiContents);

                return response()->data($poiContents, 200, $isInternal);

            }

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function store(Request $request, $poiId, $isInternal = false) {

        if(!!$poi = PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $request['poi_id'] = $poiId;
            $this->validate($request,PoiService::getStorePoiContentValidationRules());

            $preparedData = GroupService::preparePoiContent($request->input('data'),$poiId,$request->input('language'));

            $request['data'] = $preparedData['data'];

            $poiContentData = $request->only('poi_id','language','data','active','has_url','url','meta');

            $poiContentData = GroupService::toBooleanKeys($preparedData['flags'],$poiContentData);

            $poiContent = PoiContent::create($poiContentData);

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $poiId,
                        'key' => 'pois',
                        'object' => new Poi()
                    ],
                    [
                        'id' => $poiId,
                        'key' => 'poi-log-'.$poiId,
                        'object' => new PoiEvent()
                    ]
                );

                $this->publishPoiToEdwardStone($poiId,$poi->active);

            }

            $this->dispatchJob(new GenerateMediaPoiRelationsJob($poiId),'relations');
            $this->dispatchJob(new SetPoiInteractionFlagsJob($poiId),'pois');

            return response()->data($poiContent, 201, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function update(Request $request, $poiId, $contentId, $isInternal = false) {

        if(!!$poi = PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            $this->validate($request,PoiService::getUpdatePoiContentValidationRules());

            try {

                $poiContent = PoiContent::findOrFail($contentId);

                $preparedData = GroupService::preparePoiContent($request->input('data'),$poiId,$poiContent->language);

                $request['data'] = $preparedData['data'];

                $request = GroupService::toBooleanKeys($preparedData['flags'],$request);

                if($poiContent->poi_id == $poiId) {

                    $cacheKey = $this->getTenantRootCacheKey('pois:'.$poiId.':contents:index');

                    if(Cache::has($cacheKey)) {
                        Cache::forget($cacheKey);
                    }

                    $poiContent->update(PoiService::getPartialUpdateValues($request,PoiService::getUpdatePoiContentValidationRules()));

                    if(!$this->isFacade) {

                        $this->invalidateAndModify(
                            [
                                'id' => $poiId,
                                'key' => 'pois',
                                'object' => new Poi()
                            ],
                            [
                                'id' => $poiId,
                                'key' => 'poi-log-'.$poiId,
                                'object' => new PoiEvent()
                            ]
                        );

                        $this->publishPoiToEdwardStone($poiId,$poi->active);

                    }

                    $this->dispatchJob(new GenerateMediaPoiRelationsJob($poiId),'relations');
                    $this->dispatchJob(new SetPoiInteractionFlagsJob($poiId),'pois');

                    return response()->data($poiContent, 200, $isInternal);

                }

                return response()->data([], 403, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function destroy(Request $request, $poiId, $contentId, $isInternal = false) {

        if(!!$poi = PoiService::poiBelongsToTenant($poiId,Auth::user()->tenant_id)) {

            try {

                $poiContent = PoiContent::findOrFail($contentId);
                $poiContent->delete();

                $cacheKey = $this->getTenantRootCacheKey('pois:'.$poiId.':contents:index');

                if(Cache::has($cacheKey)) {
                    Cache::forget($cacheKey);
                }

                if(!$this->isFacade) {

                    $this->invalidateAndModify(
                        [
                            'id' => $poiId,
                            'key' => 'pois',
                            'object' => new Poi()
                        ],
                        [
                            'id' => $poiId,
                            'key' => 'poi-log-'.$poiId,
                            'object' => new PoiEvent()
                        ]
                    );

                    $this->publishPoiToEdwardStone($poiId,$poi->active);

                }

                return response()->data(null, 204, $isInternal);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
