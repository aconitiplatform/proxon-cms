<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use DB;
use Auth;
use Cache;

use App\Models\Poi;
use App\Models\Beacon;
use App\Models\PoiEvent;

use App\Http\Controllers\PoiContentController;

use App\Http\Services\PoiService;
use App\Http\Services\PoiContentModules\GroupService;

use App\Jobs\RemoveDeletedPoiFromBeaconsJob;
use App\Jobs\PoiEventJob;

use App\Traits\AffectsEdwardStone;
use App\Traits\FiresPoisAreLockedEvent;
use App\Traits\HasSearch;
use App\Traits\PaginatesLists;

use App\Events\PoiWasSaved;

class PoiController extends Controller {

    use AffectsEdwardStone, FiresPoisAreLockedEvent, HasSearch, PaginatesLists;

    public function lastModified(Request $request, $isInternal = false) {
        $this->firePoisAreLockedEvent(Auth::user());
        return response()->data(PoiService::getLastModified(Auth::user()->tenant_id,new Poi(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('pois:index');

        $pois = array();

        if(Cache::has($cacheKey)) {
            $pois = Cache::get($cacheKey);
        } else {

            $pois = Auth::user()->pois;

            foreach($pois as $poi) {

                $poi = PoiService::addAllRelationsToPoi($poi);

            }

            Cache::forever($cacheKey,$pois);
        }

        return response()->data($this->paginate($pois,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $poiId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('pois:'.$poiId);

        if(Cache::has($cacheKey)) {
            $poi = Cache::get($cacheKey);
        } else {

            try {

                $poi = Auth::user()->pois()->findOrFail($poiId);
                $poi = PoiService::addAllRelationsToPoi($poi);

                Cache::forever($cacheKey,$poi);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($poi, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false, $isActive = false) {

        $this->validate($request, PoiService::getStorePoiValidationRules());

        $poiData = $request->only('name','description','meta','has_channel','fallback_language');
        $poiData['active'] = $isActive;
        $poiData['tenant_id'] = Auth::user()->tenant_id;
        $poiData['user_id'] = Auth::user()->id;

        $poi = Poi::create($poiData);
      

        PoiService::addUsersToPoi($poi->id,Auth::user()->tenant_id,Auth::user()->id);

        $this->invalidateAndModify(
            [
                'id' => $poi->id,
                'key' => 'pois',
                'object' => new Poi()
            ]
        );

        $this->storePoiDesign($poi);

        if($request->has('poi_contents')) {
            $this->storePoiContents($poi,$request->input('poi_contents'));
        }

        $poi = Poi::find($poi->id);
        $poi = PoiService::addAllRelationsToPoi($poi);
        $this->dispatchJob(new PoiEventJob(['by_superadmin' => $request->input('by_superadmin'),'type' => 'created', 'text' => null, 'poi_id' => $poi->id],Auth::user()),'eventlogs');

        if(config('broadcasting.should_broadcast')) event(new PoiWasSaved($poi,Auth::user()));

        return response()->data($poi, 201, $isInternal);

    }

    public function update(Request $request, $poiId, $isInternal = false) {

        $this->validate($request, PoiService::getUpdatePoiValidationRules());

        try {
            $poi = Auth::user()->pois()->findOrFail($poiId);
        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

        $poiData = PoiService::getPartialUpdateValues($request,PoiService::getUpdatePoiValidationRules());

        if(isset($poiData['name']) && $this->hasFieldChanged($poiData['name'],$poi->name)) {
            $this->changePoiSlugsOnRename($poi->id,$poi->tenant_id);
        }

        $poi->update($poiData);

        if(!$this->isFacade) {

            $this->invalidateAndModify(
                [
                    'id' => $poi->id,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'id' => $poi->id,
                    'key' => 'poi-log-'.$poi->id,
                    'object' => new PoiEvent()
                ],
                [
                    'id' => $poi->id,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ]
            );

            $this->publishPoiToEdwardStone($poi->id,$poi->active);
            $this->dispatchJob(new PoiEventJob(['by_superadmin' => $request->input('by_superadmin'),'type' => 'updated', 'text' => null, 'poi_id' => $poi->id],Auth::user()),'eventlogs');

            if(config('broadcasting.should_broadcast')) event(new PoiWasSaved($poi,Auth::user()));

        }

        return response()->data($poi, 200, $isInternal);

    }

    public function destroy(Request $request, $poiId, $isInternal = false) {

        try {
            $poi = Auth::user()->pois()->findOrFail($poiId);
        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

        $poi->delete();

        $this->invalidateAndModify(
            [
                'id' => $poiId,
                'key' => 'pois',
                'object' => new Poi()
            ]
        );

        $this->publishPoiToEdwardStone($poiId,false);

        $this->dispatchJob(new RemoveDeletedPoiFromBeaconsJob($poiId));

        return response()->data(null, 204, $isInternal);

    }

    public function createPreview(Request $request, $language) {

        $preparedData = GroupService::preparePoiContent($request['poi_contents'][0]['data'],$request->input('id'),$request['poi_contents'][0]['language'],true);
        // $request['poi_contents'][0]['data'] = $preparedData['data'];

        $previewPayload = $request->all();
        $previewPayload['poi_contents'][0]['data'] = $preparedData['data'];

        $client = new Client([
            'base_uri' => config('substance.edward_stone_url'),
            'timeout'  => 10.0,
            'headers' => ['Accept' => 'application/json']
        ]);

        $response = $client->request('POST', 'api/previews/'.$language, [
            'json' => $previewPayload
        ]);

        $body = $response->getBody();

        $data = json_decode($body->getContents());

        return response()->success($data,$response->getStatusCode());

    }

    private function storePoiContents(Poi $poi,$poiContents) {

        foreach($poiContents as $poiContent) {

            $poiContentRequest = new \Dingo\Api\Http\Request();

            foreach($poiContent as $k => $v) {
                $poiContentRequest[$k] = $v;
            }

            $poiContentController = new PoiContentController();
            $poiContentController->store($poiContentRequest, $poi->id, true);

        }

    }

    private function storePoiDesign(Poi $poi) {

        $poiDesignRequest = new \Dingo\Api\Http\Request();
        $designController = new DesignController(true);
        $designController->storeForPoi($poiDesignRequest, $poi->id, true);

    }

    private function changePoiSlugsOnRename($poiId,$tenantId) {

        $beacons = Beacon::where('poi_id',$poiId)->get();

        if(!$beacons->isEmpty()) {

            foreach($beacons as $beacon) {
                $this->publishPoiUrlConnection($poiId,$beacon->url,$tenantId,true);
            }

        }

    }

}
