<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Invoice;
use App\Models\Tenant;
use App\Models\User;

use App\Http\Services\Service;
use App\Http\Services\Apis\EasybillService;

use App\Traits\PaginatesLists;

class InvoiceController extends Controller {

    use PaginatesLists;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(Service::getLastModified(Auth::user()->tenant_id,new Invoice(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        if($billableIdentifiers = $this->getBillableIdentifier(Auth::user())) {

            $cacheKey = $this->getRoleBasedCacheKey('invoices:index');

            $invoices = array();

            if(Cache::has($cacheKey)) {
                $invoices = Cache::get($cacheKey);
            } else {

                $invoices = Invoice::where('billable_object_type',$billableIdentifiers['billable_object_type'])->where('billable_object_id',$billableIdentifiers['billable_object_id'])->orderBy('created_at','desc')->get();
                Cache::forever($cacheKey,$invoices);

            }

            return response()->data($this->paginate($invoices,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

        }

        return response()->data([], 403, $isInternal);

    }

    public function pdf(Request $request, $documentId, $isInternal = false) {

        if($billableIdentifiers = $this->getBillableIdentifier(Auth::user())) {

            $invoice = Invoice::where('billable_object_type',$billableIdentifiers['billable_object_type'])->where('billable_object_id',$billableIdentifiers['billable_object_id'])->where('id',$documentId)->first();

            if($invoice != null && $invoice->easybill_invoice_id != null) {

                $pdf = EasybillService::getDocumentPdf($invoice->easybill_invoice_id);

                if($pdf != null) {
                    return response($pdf)->header('Content-Type','application/pdf');
                } else {
                    return response()->data([], 404, $isInternal);
                }

            } else {
                return response()->data([], 404, $isInternal);
            }

        }

        return response()->data([], 403, $isInternal);

    }

    private function getBillableIdentifier($user) {

        $billableIdentifiers = null;

        if($user->role == 'reseller') {

            $billableIdentifiers['billable_object_type'] = 'reseller';
            $billableIdentifiers['billable_object_id'] = $user->id;

        } else if ($this->isTenantAdmin($user,$user->tenant_id)) {

            $billableIdentifiers['billable_object_type'] = 'tenant';
            $billableIdentifiers['billable_object_id'] = $user->tenant_id;

        }

        return $billableIdentifiers;

    }


}
