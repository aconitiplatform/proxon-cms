<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Survey;

use App\Http\Services\SurveyService;

use App\Traits\AffectsEdwardStone;
use App\Traits\PaginatesLists;

class SurveyController extends Controller {

    use AffectsEdwardStone, PaginatesLists;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(SurveyService::getLastModified(Auth::user()->tenant_id,new Survey(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('surveys:index');

        if(Cache::has($cacheKey)) {
            $surveys = Cache::get($cacheKey);
        } else {

            $surveys = Auth::user()->surveys;

            Cache::forever($cacheKey,$surveys);

        }

        return response()->data($this->paginate($surveys,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $surveyId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('surveys:'.$surveyId);

        if(Cache::has($cacheKey)) {
            $survey = Cache::get($cacheKey);
        } else {

            $this->scopeRequest();

            try {

                $survey = Auth::user()->surveys()->findOrFail($surveyId);

                Cache::forever($cacheKey,$survey);

            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
            }

        }

        return response()->data($survey, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        $this->validate($request, SurveyService::getStoreSurveyValidationRules());

        $surveyData = $request->only('name', 'fallback_language', 'description');
        $surveyData['tenant_id'] = Auth::user()->tenant_id;
        $surveyData['user_id'] = Auth::user()->id;
        $surveyData['status'] = 'draft';

        $survey = Survey::create($surveyData);

        SurveyService::addUsersToSurvey($survey->id,Auth::user()->tenant_id,Auth::user()->id);

        $this->invalidateAndModify(
            [
                'id' => $survey->id,
                'key' => 'surveys',
                'object' => new Survey()
            ]
        );

        return response()->data($survey, 201, $isInternal);

    }

    public function update(Request $request, $surveyId, $isInternal = false) {

        $this->scopeRequest();

        $this->validate($request, SurveyService::getUpdateSurveyValidationRules());

        try {

            $survey = Auth::user()->surveys()->findOrFail($surveyId);

            if($request->has('survey_questions')) {
                $request['survey_questions'] = SurveyService::validateSurveyQuestionsArray($request->input('survey_questions'));
            }

            $surveyData = SurveyService::getPartialUpdateValues($request,SurveyService::getUpdateSurveyValidationRules());
            $survey->update($surveyData);

            $this->invalidateAndModify(
                [
                    'id' => $survey->id,
                    'key' => 'surveys',
                    'object' => new Survey()
                ]
            );

            $this->publishSurveyToEdwardStone($survey->id,true);

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

        return response()->data($survey, 200, $isInternal);

    }

    public function destroy(Request $request, $surveyId, $isInternal = false) {

        $this->scopeRequest();

        try {

            $survey = Auth::user()->surveys()->findOrFail($surveyId);

            $this->invalidateAndModify(
                [
                    'id' => $survey->id,
                    'key' => 'surveys',
                    'object' => new Survey()
                ]
            );

            $this->publishSurveyToEdwardStone($survey->id,false);

            $survey->delete();

            return response()->data(null, 204, $isInternal);

        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
        }

    }

}
