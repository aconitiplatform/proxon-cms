<?php

namespace App\Http\Controllers;

use Dingo\Api\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Dingo\Api\Routing\Helpers;

/*
    Dependencies shall be declared in the following order; groups shall be separated by an empty line:
    Laravel & External
    Laravel Facades
    Models
    Controllers
    Services
    Traits
    Jobs
*/

use App\Traits\GeneratesCacheKeys;
use App\Traits\InvalidatesCache;
use App\Traits\WorksWithQueue;
use App\Traits\PaginatesLists;
use App\Traits\ScopesRequests;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, GeneratesCacheKeys, PaginatesLists, InvalidatesCache, Helpers, WorksWithQueue, ScopesRequests;

    public $isFacade = false;

    public function __construct($isFacade = false) {

        $this->isFacade = $isFacade;

    }

    /*Fixes dingo/api form request validation https://github.com/dingo/api/wiki/Errors-And-Error-Responses#form-requests*/
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = []) {

        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors());
        }

    }

    protected function isTenantAdmin($user,$tenantId) {
        return (($user->tenant_id == $tenantId && $user->is_tenant_admin) || $this->isSuperAdmin($user));
    }

    protected function isSuperAdmin($user) {
        return ($user->is_super_admin);
    }

    protected function isReseller($user) {
        return ($user->role == 'reseller');
    }

    protected function hasFieldChanged($newValue,$oldValue) {
        return $newValue != $oldValue;
    }

}
