<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;
use Log;

use App\Models\Tenant;
use App\Models\User;
use App\Models\Beacon;
use App\Models\BeaconBatch;
use App\Models\Reseller;
use App\Models\ResellerSetting;
use App\Models\Billable;

use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Controllers\DesignController;

use App\Http\Services\TenantService;
use App\Http\Services\UserService;
use App\Http\Services\Apis\KontaktIoService;
use App\Http\Services\DesignService;
use App\Http\Services\BeaconService;

use App\Jobs\DeliverTenantToDistributionJob;
use App\Jobs\SendTenantDataToAdminJob;
use App\Jobs\SendWelcomeEmailJob;

use App\Events\TenantHasChanged;
use App\Events\UserEmailHasChanged;

use App\Traits\AffectsEdwardStone;
use App\Traits\CreatesTenantBeacons;
use App\Traits\Validators\TenantValidationRules;
use App\Traits\SyncsFeaturesAndModules;
use App\Traits\SyncsTenantBeacons;
use App\Traits\Resellable;
use App\Traits\PaginatesLists;
use App\Traits\HasSearch;
use App\Console\Traits\CreatesInvoices;

class TenantController extends Controller {

	use AffectsEdwardStone, CreatesTenantBeacons, TenantValidationRules, SyncsFeaturesAndModules, SyncsTenantBeacons, CreatesInvoices, PaginatesLists, Resellable, HasSearch;

	public function lastModified(Request $request, $isInternal = false) {
		return response()->data(TenantService::getLastModified(Auth::user()->tenant_id,new Tenant(),true), 200, $isInternal);
	}

	public function index(Request $request, $isInternal = false) {

		if($this->isReseller(Auth::user())) {

			$reseller = Reseller::find(Auth::user()->id);
			$tenants = $reseller->tenants;

		} else if ($this->isSuperAdmin(Auth::user())) {

			$tenants = Tenant::all();

		} else {
			return response()->data([], 403);
		}

		return response()->data($this->paginate($tenants,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

	}

	public function show(Request $request, $isInternal = false) {

		if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id) || $isInternal == true) {

			try {

				if(Cache::has($this->getTenantRootCacheKey('tenant'))) {
					$tenant = Cache::get($this->getTenantRootCacheKey('tenant'));
				} else {
					$tenant = Tenant::findOrFail(Auth::user()->tenant_id);
					Cache::forever($this->getTenantRootCacheKey('tenant'),$tenant);
				}

				return response()->data($tenant, 200, $isInternal);

			} catch (\Exception $e) {
				return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
			}

		} else {
			return response()->data([], 403, $isInternal);
		}

	}

	public function showById(Request $request, $tenantId = null, $isInternal = false) {

		if ($tenantId == null) {
			return $this->show($request, $isInternal);
		} else {

			if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id) || $this->isReseller(Auth::user()) || $isInternal == true) {

				try {

					if(Cache::has($this->getTenantRootCacheKey('tenant', $tenantId))) {
						$tenant = Cache::get($this->getTenantRootCacheKey('tenant', $tenantId));
					} else {
						$tenant = Tenant::findOrFail($tenantId);
						Cache::forever($this->getTenantRootCacheKey('tenant', $tenantId),$tenant);
					}

					return response()->data($tenant, 200, $isInternal);

				} catch (\Exception $e) {
					return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
				}

			} else {
				return response()->data([], 403, $isInternal);
			}

		}

	}

	public function register(Request $request) {

		$this->validate($request, $this->getRegisterTenantValidationRules());
		return $this->store($request,true);

	}

	public function store(Request $request, $isRegistration = false) {

		if($isRegistration || $this->isReseller(Auth::user()) || $this->isSuperAdmin(Auth::user())) {

			if(!$isRegistration) $this->updateConfigAsReseller(Auth::user());

			$this->validate($request, $this->getStoreTenantValidationRules($isRegistration));

			$tenantData = $request->only('tenant');
			$tenantData = $tenantData['tenant'];

			$userData = $request->only('user');
			$userData = $userData['user'];

			if($isRegistration) {

				$tenantData['email'] = $userData['email'];
				$tenantData['allowed_beacons'] = 2;
				$tenantData['kontakt_io'] = false;
				$tenantData['substance_url'] = null;
				$tenantData['edward_stone_url'] = null;

			}

			$tenant = new Tenant();
			$tenant->name = $tenantData['name'];

			$tenant->slug = str_slug($tenant->name);
			$tenant->email = $tenantData['email'];
			$tenant->fallback_language = $tenantData['fallback_language'];
			$tenant->locale = $tenantData['locale'];
			$tenant->allowed_beacons = $tenantData['allowed_beacons'];
			$tenant->active = !$tenantData['kontakt_io'];
			$tenant->initial_kontakt_io_sync = false;
			$tenant->url = $tenant->css = $tenant->logo = $tenant->imprint = $tenant->privacy = '';
			$tenant->first_login = true;
			$isRegistration ? $tenant->registered = true : $tenant->registered = false;
			$tenant->trial = true;
			$tenant->logo_settings = [
				'type' => 'none',
			];
			$tenant->copyright_label = '';

			if(config('substance.reseller.by')) {
				$tenant->reseller_id = Auth::user()->id;
			}

			if(isset($tenantData['substance_url'])) {
				$tenant->substance_url = $tenantData['substance_url'];
			} else if (config('substance.reseller.by')) {
				$tenant->substance_url = config('substance.url');
			}

			if(isset($tenantData['edward_stone_url'])) {
				$tenant->edward_stone_url = $tenantData['edward_stone_url'];
			} else if (config('substance.reseller.by')) {
				$tenant->edward_stone_url = config('substance.edward_stone_url');
			}

			$tenant->save();


			$designController = new DesignController();
			$design = $designController->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',$tenant->id);

			$tenant->tenant_design_id = $design->id;
			$tenant->save();

			if($tenantData['kontakt_io']) {

				try {
					$venue = KontaktIoService::createVenue($tenant->name,$tenant->id." (Substance Tenant ID)");
				} catch(\Exception $e) {
					$tenant->delete();
					return response()->data(trans('api.KIO_KEY_WRONG'), 400);
				}

				if($venue == null) {
					Log::error('Could not create venue at Kontakt IO');
				} else {
					$tenant->kontakt_io_venue_id = $venue->id;
					$tenant->save();
				}

			}

			if(!$tenantData['kontakt_io']) {
				$this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
			}

			$user = new User();
			$user->firstname = $userData['firstname'];
			$user->lastname = $userData['lastname'];
			$user->email = $userData['email'];
			$user->password = $isRegistration ? bcrypt($userData['password']) : bcrypt(config('app.key').time());
			$user->language = $tenant->fallback_language;
			$user->locale = $tenant->locale;
			$user->role = 'admin';
			$user->tenant_id = $tenant->id;
			$user->active = true;

			$user->save();

			$beaconInfos = $this->createTenantBeacons($tenant,false);

			if(!$tenantData['kontakt_io']) {

				if(!$isRegistration) {
					$tenant = $this->endTrialWithWhitelabelSettingsAsReseller($tenant);
					$passwordResetController = new PasswordResetController();
					$passwordResetController->triggerInternalSendResetLinkEmail($user->email,true);
					$user->email_confirmed = true;
					$user->save();
				} else {
					$this->dispatchJob(new SendWelcomeEmailJob($tenant,$user),'emails');
					event(new UserEmailHasChanged($user));
				}
				$this->dispatchJob(new SendTenantDataToAdminJob($tenant,$user,$beaconInfos),'emails');

			}

			$this->syncDefaults($tenant);

			if(!$isRegistration) {
				$this->invalidateAndModify(
					[
						'object' => new Tenant()
					]
				);
			}

			return response()->data(compact('tenant','user'),201);

		} else {
			return response()->data([], 403);
		}

	}

	public function syncInitial(Request $request) {

		$tenantId = $request->input('tenant_id');

		$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			if($tenant->active == false) {

				$this->updateConfigAsReseller(Auth::user());
				$tenant = $this->endTrialWithWhitelabelSettingsAsReseller($tenant);

				return $this->syncBeacons($tenant);

			} else {
				return response()->data([],200);
			}


		} else {
			return response()->data([], 403);
		}

	}

	public function syncBatches(Request $request) {

		$tenantId = $request->input('tenant_id');
		$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			$this->updateConfigAsReseller(Auth::user());
			return $this->syncBeaconBatches($tenant);

		} else {
			return response()->data([], 403);
		}

	}

	public function showBatches(Request $request, $tenantId) {

    	$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			$this->updateConfigAsReseller(Auth::user());

			$batches = $tenant->beaconBatches()->where('synced',false)->get();
			return response()->data($batches,200);

		} else {
			return response()->data([], 403);
		}

    }

	public function addBeaconsToTenant(Request $request) {

		$tenantId = $request->input('tenant_id');
		$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			$this->validate($request,$this->getAddBeaconsToTenantValidationRules());
			$this->updateConfigAsReseller(Auth::user());

			$batch = $this->createBeaconBatch($tenant,$request->input('amount'),$request->input('kontakt_io'));
			$tenant->allowed_beacons = $tenant->allowed_beacons + $request->input('amount');
			$tenant->save();

			$this->invalidateAndModify(
                [
                    'id' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                    'key' => 'beacons',
                    'object' => new Beacon(),
					'tenant_id' => $tenant->id,
                ],
				[
					'tenant_id' => $tenant->id,
					'object' => new Tenant()
				]
            );

			$this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));

			Cache::forever($this->getTenantRootCacheKey('tenant', $tenantId),$tenant);

			if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

			return response()->data($batch,201);

		} else {
			return response()->data([], 403);
		}

	}

	public function endTrial(Request $request) {

		$tenantId = $request->input('tenant_id');

		$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			if(!$tenant->active) return response()->data(trans('api.ACTIVATE_TENANT_FIRST'),400);

			if($tenant->trial) {

				$tenant->trial = false;
				$tenant->save();

				$billable = Billable::where('tenant_id',$tenant->id)->first();

				if($billable == null) {
					$billable = new Billable();
				}

				$billable->tenant_id = $tenantId;

				if($this->isReseller(Auth::user())) {

						$settings = $this->updateConfigAsReseller(Auth::user());

						$billable->billable_object_type = 'reseller';
						$billable->billable_object_id = $settings->reseller_id;
						$billable->easybill_customer_id = $settings->easybill_customer_id;

				} else {

					$billable->billable_object_type = 'tenant';
					$billable->billable_object_id = $tenant->id;
					$billable->easybill_customer_id = $request->input('easybill_customer_id');

				}

				$billable->price_per_beacon = $request->input('price_per_beacon');
				$billable->discount = $request->input('discount');

				$billable->save();


				$success = false;
				if(config('substance.reseller.by') && $request->input('bill_rest_of_month') == true) {
					$success = $this->createSingleResellerInvoice($settings->reseller_id,$tenant->id,$tenant->allowed_beacons);
				} else if (!config('substance.reseller.by') && $request->input('bill_rest_of_month') == true) {
					$success = $this->createSingleTenantInvoice($tenant->id,$tenant->allowed_beacons);
				}

				if(!$success) {
					return response()->data(trans('api.EASYBILL_ERROR'),400);
				}

                Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
            	TenantService::updateLastModified($tenant->id,new Tenant());
            	$this->publishTenantToEdwardStone($tenant->id,true);

                return response()->data($billable,201);

			} else {
				return response()->data([],200);
			}


		} else {
			return response()->data([], 403);
		}

	}

	public function activity(Request $request) {

		$tenantId = $request->input('tenant_id');

		$tenant = Tenant::findOrFail($tenantId);

		if(($this->isReseller(Auth::user()) && $tenant->reseller_id == Auth::user()->id) || $this->isSuperAdmin(Auth::user())) {

			$tenant->active = !$tenant->active;

			$tenant->save();

			if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));
			Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
			TenantService::updateLastModified($tenant->id,new Tenant());
			$this->publishTenantToEdwardStone($tenant->id,true);

			return response()->data($tenant,200);

		} else {
			return response()->data([], 403);
		}

	}

	public function update(Request $request, $tenantId, $isInternal = false) {

		if($this->isTenantAdmin(Auth::user(),$tenantId)) {

			$this->validate($request, $this->getUpdateTenantValidationRules());
			TenantService::validateArray($request['logo_settings'], TenantService::getLogoSettingsValidationRules());

			try {

				$tenant = Tenant::findOrFail($tenantId);
				$tenant->update(TenantService::getPartialUpdateValues($request,$this->getUpdateTenantValidationRules(),false,true));

				Cache::forever($this->getTenantRootCacheKey('tenant', $tenantId),$tenant);

				$this->invalidateAndModify(
					[
						'object' => new Tenant()
					]
				);

				$this->publishTenantToEdwardStone($tenant->id,true);

				if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

				return response()->data($tenant, 200, $isInternal);

			} catch (\Exception $e) {
				return response()->data([], 404, $isInternal, ['exception' => $e->getMessage()]);
			}

		} else {
			return response()->data([], 403, $isInternal);
		}

	}

}
