<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;

use App\Models\Features\Module;

use App\Http\Services\PoiContentModules\ModuleService;
use App\Http\Services\PoiContentModules\GroupService;

class ModuleController extends Controller {

    public function validateModule(Request $request) {

        $this->validate($request, ModuleService::getPoiContentModuleValidationRules());

        $module = $request->all();

        $validatedModule = ModuleService::sanitizeArray($module,ModuleService::getPoiContentModuleValidationRules(),[],[],false);

        if(isset($module['data'])) {
            $validatedModule['data'] = ModuleService::sanitizeArray($module['data'],ModuleService::getPoiContentModuleValidationRulesByType($module['type']),[],[],false);
        }

        $validatedModule['data'] = ModuleService::escapePoiContentModuleStringsByType($module['type'],$validatedModule['data']);

        if(isset($module['data']['css'])) {

            $validatedModule['data']['css']['x'] = ['k'=>'v'];
            $validatedModule['data']['css'] = GroupService::removeEmptyCssValues($module['data']['css']);

        }

        return response()->success($validatedModule);

    }

    public function index(Request $request) {

        $tenant = Auth::user()->tenant;
        $modules = $tenant->modules()->get();
        $modules = $modules->unique();
        return response()->success($modules);

    }

}
