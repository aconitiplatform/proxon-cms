<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Mail;

use App\Models\User;
use App\Models\PasswordReset;

use App\Http\Controllers\Auth\AuthController;

use App\Jobs\ResetPasswordEmailJob;


class PasswordResetController extends AuthController {

    public function sendResetLinkEmail(Request $request) {

        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
        ]);

        $this->triggerInternalSendResetLinkEmail($request->email);

        return response()->success(true);

    }

    public function verify(Request $request) {

        $this->validate($request, [
            'email' => 'required|email',
            'token' => 'required',
        ]);

        $check = PasswordReset::whereEmail($request->email)
        ->whereToken($request->token)
        ->first();

        if(!$check) {
            return response()->error(trans('api.WRONG_RESET_TOKEN'), 422);
        }

        return response()->success(true);

    }

    public function reset(Request $request) {

        $this->validate($request, [
            'email'    => 'required|email',
            'token'    => "required|exists:password_resets,token,email,{$request->email}",
            'password' => 'required|min:8|confirmed',
        ]);

        $user = User::whereEmail($request->email)->firstOrFail();
        $user->password = bcrypt($request->password);
        $user->save();

        $this->deleteLoginAttempts($request->email);

        PasswordReset::whereEmail($request->email)->delete();

        return response()->success(true);

    }

    public function triggerInternalSendResetLinkEmail($email,$isNewUser = false) {

        PasswordReset::whereEmail($email)->delete();

        $reset = PasswordReset::create([
            'email' => $email,
            'token' => str_random(10),
        ]);

        $token = $reset->token;

        $this->dispatchJob(new ResetPasswordEmailJob(compact('email', 'token'),$isNewUser));

    }
}
