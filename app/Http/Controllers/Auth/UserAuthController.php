<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\User;
use App\Models\Tenant;
use App\Models\PasswordReset;

use App\Http\Controllers\Auth\AuthController;

use App\Traits\WorksWithQueue;
use App\Traits\SendsUnlockAccountEmail;

use App\Jobs\ResendUnlockAccountEmailJob;

class UserAuthController extends AuthController {

	use WorksWithQueue;
	use SendsUnlockAccountEmail;

	public function userRole(Request $request) {

		$this->validate($request, [
			'email'    => 'required|email',
			'password' => 'required|min:8',
		]);

	}

	public function login(Request $request) {

		$this->validate($request, [
			'email'    => 'required|email',
			'password' => 'required|min:8',
		]);

		$credentials = $request->only('email', 'password');

		try {

			if(!$token = JWTAuth::attempt($credentials)) {

				$this->setLoginAttempts($credentials['email']);
				return response()->error(trans('api.INVALID_USER_CREDENTIALS'), 401);

			}

		} catch (\JWTException $e) {
			return response()->error(trans('api.TOKEN_FAILED'), 500);
		}

		$this->deleteLoginAttempts($credentials['email']);

		$user = Auth::user();

		if(Auth::user()->role == 'app') return response()->error(trans('api.NOT_USER_BUT_APP'), 400);

		$tenant = Tenant::find(Auth::user()->tenant_id);
		$tenant->features;
		$tenant->modules;

		if(!$this->isUserAndTenantActive($user,$tenant)) {
			return response()->error(trans('api.NOT_ALLOWED_SYSTEM'), 403);
		}

		return response()->success(compact('user', 'token', 'tenant'),201)->header('X-JWT-TTL',config('jwt.ttl'))->header('X-JWT-Token',$token);

	}

	public function refresh(Request $request) {

		$user = Auth::user();

		if($user->role == 'app') return response()->error(trans('api.NOT_USER_BUT_APP_REFRESH'), 400);

		$tenant = Tenant::find($user->tenant_id);

		$meta['info'] = trans('api.NEW_TOKEN_IN_HEADER');

		if(!$this->isUserAndTenantActive($user,$tenant)) {
			return response()->error(trans('api.NOT_ALLOWED_SYSTEM'), 403);
		}

		return response()->success(compact('user', 'tenant'),201,$meta)->header('X-JWT-TTL',config('jwt.ttl'));

	}

	public function unlockAccount(Request $request) {

		$this->validate($request, [
			'email' => 'required|email',
			'token' => "required|exists:password_resets,token,email,{$request->email}",
		]);

		$this->deleteLoginAttempts($request->email);

		PasswordReset::whereEmail($request->email)->where('token',$request->token)->delete();

		return response()->success(true);

	}

	public function loginSuperAdminAsUser(Request $request) {

		if($this->isSuperAdmin(Auth::user())) {

			$this->validate($request, [
				'user_id' => 'required|exists:users,id',
			]);

			$user = User::find($request->input('user_id'));

			try {

				if(!$token = JWTAuth::fromUser($user)) {

					return response()->error(trans('api.INVALID_USER_CREDENTIALS'), 401);

				}

				Auth::setUser($user);

			} catch (\JWTException $e) {
				return response()->error(trans('api.TOKEN_FAILED'), 500);
			}

			$user = Auth::user();

			if(Auth::user()->role == 'app') return response()->error(trans('api.NOT_USER_BUT_APP'), 400);

			$tenant = Tenant::find(Auth::user()->tenant_id);

			return response()->success(compact('user', 'token', 'tenant'),201)->header('X-JWT-TTL',config('jwt.ttl'))->header('X-JWT-Token',$token);

		} else {
			return response()->error(trans('api.403'), 403);
		}

	}

	public function resendUnlockAccountEmail(Request $request) {

		$this->validate($request, [
			'email'	=> 'required|email'
		]);

		$email = $request->only('email')['email'];

        $this->sendResetToken($email, true);

		return response()->success(true);

	}

}
