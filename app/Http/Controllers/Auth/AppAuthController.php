<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Auth;
use JWTAuth;

use App\Models\User;
use App\Models\Tenant;

use App\Http\Controllers\Auth\AuthController;

use App\Http\Services\UserService;

class AppAuthController extends AuthController {

    public function login(Request $request) {

        $this->validate($request, [
            'key'    => 'required|size:40',
            'secret' => 'required|size:40',
        ]);

        $userId = UserService::getUserIdByAppCredentials($request->input('key'),$request->input('secret'));

        if($userId) {

            try {

                $user = User::findOrFail($userId);
                $tenant = Tenant::find($user->tenant_id);

                if(!$this->isUserAndTenantActive($user,$tenant)) {
                    return response()->error(trans('api.NOT_ALLOWED_SYSTEM'), 403);
                }

            } catch (\Exception $e) {
                Log::error($e);
                return response()->error(trans('api.404'), 404);
            }

            try {

                if (!$token = JWTAuth::fromUser($user)) return response()->error(trans('api.INVALID_APP_CREDENTIALS'), 401);

            } catch (\JWTException $e) {
                Log::error($e);
                return response()->error(trans('api.TOKEN_FAILED'), 500);
            }

            return response()->success(array('token'=>$token),201)->header('X-JWT-TTL',config('jwt.ttl'))->header('X-JWT-Token',$token);

        }

        return response()->error(trans('api.404'), 404);

    }

    public function refresh(Request $request) {

        $user = Auth::user();
        $tenant = Tenant::find(Auth::user()->tenant_id);

        if(!$this->isUserAndTenantActive($user,$tenant)) {
            return response()->error(trans('api.NOT_ALLOWED_SYSTEM'), 403);
        }

        $meta['info'] = trans('api.NEW_TOKEN_IN_HEADER');

        return response()->success(array(),201,$meta)->header('X-JWT-TTL',config('jwt.ttl'));

    }

}
