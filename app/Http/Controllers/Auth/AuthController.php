<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use JWTAuth;
use Carbon\Carbon;

use Cache;
use App\Models\User;
use App\Models\Tenant;

use App\Http\Controllers\Controller;

use App\Http\Services\Apis\ReCaptchaService;

use App\Jobs\DeliverTenantToDistributionJob;

class AuthController extends Controller {

    protected function setLoginAttempts($email) {

        $cacheKey = 'login-attempts:'.$email;

        if(!Cache::has($cacheKey)) {
            Cache::forever($cacheKey,1);
        } else {

            $attempts = Cache::get($cacheKey);
            Cache::forever($cacheKey,$attempts+1);

        }

    }

    protected function deleteLoginAttempts($email) {

        $cacheKey = 'login-attempts:'.$email;
        Cache::forget($cacheKey);
        $cacheKey = 'unlock-emails:'.$email;
        Cache::forget($cacheKey);

    }

    protected function isUserAndTenantActive(User $user, Tenant $tenant) {

        if($tenant->canceled_at != null) {

            $canceledAt = Carbon::parse($tenant->canceled_at);
            if($canceledAt->isPast()) {
                $tenant->active = false;
                $tenant->save();
                $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
            }

        }

        return ($user->active && $tenant->active);
    }

    public function logout(Request $request) {

        JWTAuth::parseToken()->invalidate();
        return response()->success([],201);

    }

    public function verifyReCaptcha(Request $request) {

        $this->validate($request, [
			'user_response'    => 'required|string',
		]);

        return response()->data(ReCaptchaService::verify([
            'secret' => config('substance.recaptcha.secret'),
            'response' => $request->input('user_response'),
        ]));

    }

}
