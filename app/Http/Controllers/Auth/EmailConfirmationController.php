<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Auth;

use App\Models\User;
use App\Models\EmailConfirmation;

use App\Http\Controllers\Auth\AuthController;

use App\Events\UserEmailHasChanged;

use App\Traits\Validators\EmailConfirmationValidationRules;

class EmailConfirmationController extends AuthController {

    use EmailConfirmationValidationRules;

    public function confirmEmail(Request $request) {

        $this->validate($request,$this->getConfirmEmailValidationRules());

        $confirmation = EmailConfirmation::whereToken($request->input('token'))->first();

        if($confirmation != null) {

            $user = User::where('id',$confirmation->user_id)->first();

            if($user != null && $user->email_confirmed == false) {
                $user->email_confirmed = true;
                $user->save();
                $confirmation->delete();

                Auth::setUser($user);

                $this->invalidateAndModify(
                    [
                        'id' => $user->id,
                        'key' => 'users',
                        'object' => new User()
                    ]
                );

                return response()->data($user,200);

            }

        }

        return response()->data([],400);

    }

    public function resendEmailConfirmation(Request $request) {

        $this->validate($request,$this->getResendEmailConfirmationValidationRules());

        $user = User::where('email',$request->input('email'))->first();

        if($user != null && $user->email_confirmed == false) {
            event(new UserEmailHasChanged($user));
            return response()->data([],200);
        }

        return response()->data([],400);

    }

}
