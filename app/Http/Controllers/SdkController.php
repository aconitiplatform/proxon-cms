<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;

use App\Traits\Validators\SdkValidationRules;
use App\Traits\SdkHelpers;

class SdkController extends Controller {

    use SdkValidationRules, SdkHelpers;

    private $data = [];

    public function showAvailableBeacons(Request $request) {

        $beacons = Auth::user()->beacons;

        $beacons->each(function($item,$key) {
            $this->data[$key]['id'] = $item->id;
            $this->data[$key]['name'] = $item->name;
            $this->data[$key]['alias'] = $item->alias;
            $this->data[$key]['associated'] = $item->poi_id != null;
        });

        return response()->data($this->data);

    }

    public function connectContentToBeacon(Request $request) {

        $this->validate($request, $this->getConnectContentToBeaconValidationRules());

        try {

            $beacon = Auth::user()->beacons()->findOrFail($request->input('beacon_id'));

            if($beacon->poi_id != null) {
                $this->deletePoi($beacon);
            }

            $poi = $this->createPoiAndContent($request->input('url'),$request->input('title'),$request->input('description'));
            $connection = $this->connectBeacon($beacon,$poi);

            return response()->data($connection, 200);

        } catch (\Exception $e) {
            return response()->data([], 404, false, ['exception' => $e->getMessage()]);
        }

    }

    public function disconnectContentFromBeacon(Request $request) {

        $this->validate($request, $this->getDisconnectContentFromBeaconValidationRules());

        try {

            $beacon = Auth::user()->beacons()->findOrFail($request->input('beacon_id'));

            if($beacon->poi_id != null) {

                $this->deletePoi($beacon);
                $beacon->poi_id == null;
                $beacon->save();

            }

            $connection = [
                'id' => $beacon->id,
                'name' => $beacon->name,
                'alias' => $beacon->alias,
                'associated' => false,
            ];

            return response()->data($connection, 200);

        } catch (\Exception $e) {
            return response()->data([], 404, false, ['exception' => $e->getMessage()]);
        }

    }

}
