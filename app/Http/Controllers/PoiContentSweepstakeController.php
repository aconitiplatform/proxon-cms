<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cache;

use App\Http\Controllers\PoiController;

use App\Traits\GetsDataFromFirebase;

class PoiContentSweepstakeController extends Controller {

    use GetsDataFromFirebase;

    public function index(Request $request, $poiId, $isInternal = false) {

        $pc = new PoiController();
        $poi = $pc->show($request, $poiId, true);

        if($poi != null) {

            $cacheKey = $this->getTenantRootCacheKey('pois:'.$poi->id.':sweepstake-participations');

            if(Cache::has($cacheKey)) {
                $meta['yes'] = true;
                $sweepstakes = Cache::get($cacheKey);
            } else {

                $sweepstakes = $this->getFirebaseList($request,'sweepstakes/'.$poi->id,false);

                Cache::put($cacheKey,$sweepstakes,60);

            }

            $data = [];
            $meta['count'] = 0;
            $meta['key'] = $cacheKey;

            if($sweepstakes) {

                $meta['count'] = count($sweepstakes);

                foreach($sweepstakes as $sweepstake) {

                    $meta[$sweepstake['payload']['sweepstake']] = isset($meta[$sweepstake['payload']['sweepstake']]) ? $meta[$sweepstake['payload']['sweepstake']]+1 : 1;

                    $data[] = [
                        'sweepstake' => $sweepstake['payload']['sweepstake'],
                        'time' => $sweepstake['time'] * 1000
                    ];

                }

            }
            

            return response()->data($data, 200, $isInternal, $meta);

        }

        return response()->data([], 404, $isInternal);

    }

}
