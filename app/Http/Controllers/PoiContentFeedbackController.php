<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cache;

use App\Http\Controllers\PoiController;

use App\Traits\GetsDataFromFirebase;

class PoiContentFeedbackController extends Controller {

    use GetsDataFromFirebase;

    public function index(Request $request, $poiId, $isInternal = false) {

        $pc = new PoiController();
        $poi = $pc->show($request, $poiId, true);

        if($poi != null) {

            $cacheKey = $this->getTenantRootCacheKey('pois:'.$poi->id.':feedbacks');

            if(Cache::has($cacheKey)) {
                $data = Cache::get($cacheKey);
            } else {

                $feedbacks = $this->getFirebaseObject('feedbacks/'.$poi->id);

                $data = [];

                if($feedbacks) {

                    foreach($feedbacks as $feedbackKey => $feedbackValue) {

                        $data[] = [
                            'language' => isset($feedbackValue['language']) ? $feedbackValue['language'] : trans('api.GENERAL_FEEDBACK'),
                            'feedback_id' => $feedbackKey,
                            'label' => isset($feedbackValue['label']) ? $feedbackValue['label'] : trans('api.GENERAL_FEEDBACK'),
                            'positive' => isset($feedbackValue['positive']) ? $feedbackValue['positive'] : trans('api.GENERAL_FEEDBACK'),
                            'negative' => isset($feedbackValue['negative']) ? $feedbackValue['negative'] : trans('api.GENERAL_FEEDBACK'),
                        ];

                    }

                }

                Cache::put($cacheKey,$data,60);

            }

            return response()->data($data, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

}
