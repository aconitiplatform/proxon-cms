<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Log;
use Cache;
use PDF;

use App\Models\Tenant;
use App\Models\Beacon;

use App\Events\TenantHasChanged;

use App\Traits\CreatesTenantBeacons;
use App\Traits\AffectsEdwardStone;
use App\Traits\Validators\PlanValidationRules;

use App\Jobs\DeliverTenantToDistributionJob;

class PlanController extends Controller {

    use PlanValidationRules, CreatesTenantBeacons, AffectsEdwardStone;

    private $plans;

    public function index(Request $request) {

        return response()->data($this->getPlans());

    }

    private function getPlans() {

        return [
            'startrampe' => [
                'name' => trans('plans.startrampe'),
                'description' => trans('plans.plan_description',['amount' => 2]),
                'id' => 'startrampe',
                'price_per_month' => 0,
                'amount_of_urls' => 2,
                'stripe' => false,
                'rank' => 0,
            ],
            'troposphere' => [
                'name' => trans('plans.troposphere'),
                'description' => trans('plans.plan_description',['amount' => 3]),
                'id' => 'troposphere',
                'price_per_month' => 10.00,
                'amount_of_urls' => 3,
                'stripe' => true,
                'rank' => 1,
            ],
            'stratosphere' => [
                'name' => trans('plans.stratosphere'),
                'description' => trans('plans.plan_description',['amount' => 10]),
                'id' => 'stratosphere',
                'price_per_month' => 30.00,
                'amount_of_urls' => 10,
                'stripe' => true,
                'rank' => 2,
            ],
            'mesosphere' => [
                'name' => trans('plans.mesosphere'),
                'description' => trans('plans.plan_description',['amount' => 18]),
                'id' => 'mesosphere',
                'price_per_month' => 50.00,
                'amount_of_urls' => 18,
                'stripe' => true,
                'rank' => 3,
            ],
            'thermosphere' => [
                'name' => trans('plans.thermosphere'),
                'description' => trans('plans.plan_description',['amount' => 30]),
                'id' => 'thermosphere',
                'price_per_month' => 80.00,
                'amount_of_urls' => 30,
                'stripe' => true,
                'rank' => 4,
            ],
        ];

    }

    public function subscribe(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, $this->getSubscribeValidationRules());

            try {

                $this->plans = $this->getPlans();

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                $tenant = Auth::user()->tenant;

                if($tenant->stripe_token_id == null || $tenant->stripe_token_id != $request->input('stripe_token_id')) {

                    $tenant->stripe_token_id = $request->input('stripe_token_id');
                    $tenant->save();

                }

                $token = \Stripe\Token::retrieve($tenant->stripe_token_id);

                if($tenant->stripe_customer_id == null) {

                    $customer = \Stripe\Customer::create(array(
                        "description" => "Customer for ".$tenant->name,
                        "source" => $token->id,
                        "email" => Auth::user()->email
                    ));

                    $tenant->stripe_customer_id = $customer->id;
                    $tenant->save();
                    $this->setCardForTenant($customer,$tenant);

                }

                if($tenant->stripe_subscription == null && $tenant->stripe_plan == null) {

                    $subscription = \Stripe\Subscription::create([
                        'customer' => $tenant->stripe_customer_id,
                        'items' => [
                            [
                                'plan' => $request->input('plan')
                            ]
                        ],
                        'tax_percent' => 19,
                    ]);

                    $tenant->stripe_plan = $request->input('plan');
                    $tenant->stripe_subscription = $subscription->id;
                    $tenant->save();

                } else if ($tenant->stripe_subscription != null && $tenant->stripe_plan != $request->input('plan')) {

                    if($this->plans[$request->input('plan')]['rank'] < $this->plans[$tenant->stripe_plan]['rank']) {
                        return response()->data(trans('plans.NO_DOWNGRADE'), 400);
                    }

                    $subscription = \Stripe\Subscription::retrieve($tenant->stripe_subscription);
                    $itemId = $subscription->items->data[0]->id;

                    $subscription->items = [
                        [
                            'id' => $itemId,
                            'plan' => $request->input('plan')
                        ]
                    ];
                    $subscription->save();
                    $tenant->stripe_plan = $request->input('plan');
                    $tenant->save();

                } else {
                    return response()->data(trans('plans.ALREADY_SUBSCRIBED'), 400);
                }

                $beaconInfos = $this->upgradeTenant($tenant);

                Cache::forever($this->getTenantRootCacheKey('tenant', $tenant->id),$tenant);

				$this->invalidateAndModify(
					[
						'object' => new Tenant()
					],
                    [
                        'id' => Auth::user()->id,
                        'key' => 'beacons',
						'object' => new Beacon()
					]
				);

				$this->publishTenantToEdwardStone($tenant->id);

				if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

                return response()->data(compact('tenant','beaconInfos'), 201);

            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
			return response()->data([], 403);
		}

    }

    public function unsubscribe(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $this->plans = $this->getPlans();

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                $tenant = Auth::user()->tenant;

                if($tenant->stripe_subscription != null) {

                    $subscription = \Stripe\Subscription::retrieve($tenant->stripe_subscription);
                    $subscription->cancel([
                        'at_period_end' => true,
                    ]);
                    $canceledAt = Carbon::createFromTimestamp($subscription->current_period_end);
                    $tenant->canceled_at = $canceledAt->toDateTimeString();
                    $tenant->save();

                    Cache::forever($this->getTenantRootCacheKey('tenant', $tenant->id),$tenant);

    				$this->invalidateAndModify(
    					[
    						'object' => new Tenant()
    					],
                        [
                            'id' => Auth::user()->id,
                            'key' => 'beacons',
    						'object' => new Beacon()
    					]
    				);

    				$this->publishTenantToEdwardStone($tenant->id);

    				if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

                    return response()->data($tenant, 200);

                } else {
                    return response()->data(trans('plans.NO_SUBSCRIPTION'), 400);
                }

            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
            return response()->data([], 403);
        }

    }

    public function updateCard(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $this->validate($request, $this->getUpdateCardValidationRules());

                $tenant = Auth::user()->tenant;

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                if($tenant->stripe_customer_id != null) {

                    $customer = \Stripe\Customer::retrieve($tenant->stripe_customer_id);

                    $card = $customer->sources->create(array("source" => $request->input('stripe_token_id')));
                    $customer->default_source = $card->id;
                    $customer->save();
                    $this->setCardForTenant($customer,$tenant);

                    Cache::forever($this->getTenantRootCacheKey('tenant', $tenant->id),$tenant);

    				$this->invalidateAndModify(
    					[
    						'object' => new Tenant()
    					]
    				);

    				$this->publishTenantToEdwardStone($tenant->id);

    				if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

                    return response()->data($tenant);

                } else {
                    return response()->data(trans('plans.NO_CUSTOMER'), 400);
                }


            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
            return response()->data([], 403);
        }

    }

    public function listSubscriptionInvoices(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $tenant = Auth::user()->tenant;

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                if($tenant->stripe_customer_id != null) {

                    $list = \Stripe\Invoice::all(array("customer" => $tenant->stripe_customer_id));

                    $invoices = $list->data;

                    foreach($invoices as $invoice) {

                        $date = Carbon::createFromTimestamp($invoice->date);
                        $period_end = Carbon::createFromTimestamp($invoice->lines->data[0]->period->end);
                        $period_start = Carbon::createFromTimestamp($invoice->lines->data[0]->period->start);

                        $item = [
                            'id' => $invoice->id,
                            'paid' => $invoice->paid,
                            'currency' => $invoice->currency,
                            'date' => $date->toDateTimeString(),
                            'period_end' => $period_end->toDateTimeString(),
                            'period_start' => $period_start->toDateTimeString(),
                            'subtotal' => $invoice->subtotal,
                            'tax' => $invoice->tax,
                            'tax_percent' => $invoice->tax_percent,
                            'total' => $invoice->total,
                        ];

                        $subscriptions[] = $item;

                    }

                    return response()->data($subscriptions);

                } else {
                    return response()->data(trans('plans.NO_CUSTOMER'), 400);
                }


            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
            return response()->data([], 403);
        }

    }

    public function updateInvoiceInformation(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $this->validate($request, $this->getUpdateInvoiveInformationValidationRules());

                $tenant = Auth::user()->tenant;
                $tenant->invoice_information = $request->input('invoice_information');
                $tenant->save();

                Cache::forever($this->getTenantRootCacheKey('tenant', $tenant->id),$tenant);

                $this->invalidateAndModify(
                    [
                        'object' => new Tenant()
                    ]
                );

                $this->publishTenantToEdwardStone($tenant->id);

                if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

                return response()->data($tenant);


            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
            return response()->data([], 403);
        }

    }

    public function subscriptionInvoice(Request $request, $invoiceId) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            try {

                $tenant = Auth::user()->tenant;

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                if($tenant->stripe_customer_id != null) {

                    $invoice = \Stripe\Invoice::retrieve($invoiceId);

                    $date = Carbon::createFromTimestamp($invoice->date);
                    $period_end = Carbon::createFromTimestamp($invoice->lines->data[0]->period->end);
                    $period_start = Carbon::createFromTimestamp($invoice->lines->data[0]->period->start);

                    $data = [
                        'id' => $invoice->id,
                        'paid' => $invoice->paid,
                        'currency' => $invoice->currency,
                        'date' => $date->toDateString(),
                        'period_end' => $period_end->toDateString(),
                        'period_start' => $period_start->toDateString(),
                        'subtotal' => $invoice->subtotal,
                        'tax' => $invoice->tax,
                        'tax_percent' => $invoice->tax_percent,
                        'total' => $invoice->total,
                        'information' => $tenant->invoice_information,
                        'tenant' => $tenant->name,
                        'customer' => $invoice->customer,
                    ];

                    $pdf = PDF::loadView('pdf.invoice', $data);
                    return $pdf->download('invoice.pdf');

                } else {
                    return response()->data(trans('plans.NO_CUSTOMER'), 400);
                }


            } catch (\Exception $e) {
                return response()->data($e,$e->getCode());
            }

        } else {
            return response()->data([], 403);
        }

    }

    private function setCardForTenant($customer,$tenant) {

        $card = $customer->sources->retrieve($customer->default_source);

        $tenant->card_brand = $card->brand;
        $tenant->card_last_four = $card->last4;
        $tenant->save();

    }

    private function upgradeTenant(Tenant $tenant) {

        $amountOfNewBeacons = $this->plans[$tenant->stripe_plan]['amount_of_urls'] - $tenant->allowed_beacons;
        $tenant->allowed_beacons = $this->plans[$tenant->stripe_plan]['amount_of_urls'];
        $tenant->save();
        return $this->createTenantBeacons($tenant,false,$amountOfNewBeacons);

    }

}
