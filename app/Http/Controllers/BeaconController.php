<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Cache;

use App\Models\Beacon;
use App\Models\BeaconBatch;

use App\Http\Services\BeaconService;
use App\Http\Services\PoiService;

use App\Traits\HasSearch;
use App\Traits\PaginatesLists;
use App\Traits\GeneratesBeaconUrl;

class BeaconController extends Controller {

    use HasSearch, PaginatesLists, GeneratesBeaconUrl;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(BeaconService::getLastModified(Auth::user()->tenant_id,new Beacon(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('beacons:index');

        $beacons = array();

        if(Cache::has($cacheKey)) {
            $beacons = Cache::get($cacheKey);
        } else {

            $beacons = Auth::user()->beacons;

            $beacons->each(function($item,$key) {
                $item = BeaconService::addAllRelationsToBeacon($item);
            });

            Cache::forever($cacheKey,$beacons);

        }

        return response()->data($this->paginate($beacons,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $beaconId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('beacons:'.$beaconId);

        if(Cache::has($cacheKey)) {
            $beacon = Cache::get($cacheKey);
        } else {

            $relation = BeaconService::getBeaconUserRelation($beaconId,Auth::user()->id);

            if($relation == null) {
                return response()->data([], 404, $isInternal);
            }

            $beacon = Beacon::find($relation->beacon_id);
            $beacon->beaconLocation;
            $beacon->poi;

            Cache::forever($cacheKey,$beacon);

        }

        return response()->data($beacon, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        if($this->isSuperAdmin(Auth::user())) {

            $this->validate($request, BeaconService::getStoreBeaconValidationRules());

            $beacon = new Beacon();
            $beacon->name = trim($request->input('name'));
            $beacon->tenant_id = trim($request->input('tenant_id'));
            $beacon->config = trim($request->input('config'));
            $beacon->url = $this->newUniqueUrl();
            $beacon->url_hash = md5($beacon->url);
            $beacon->description = trim($request->input('description'));
            $beacon->poi_id = null;
            $beacon->save();
            BeaconService::addTenantUsersAndAdminsToBeacon($beacon->id,$beacon->tenant_id);

            $this->invalidateAndModify(
                [
                    'id' => $relation->beacon_id,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ]
            );

            return response()->data($beacon, 201, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function update(Request $request, $beaconId, $isInternal = false) {

        $this->validate($request, BeaconService::getUpdateBeaconValidationRules());

        $relation = BeaconService::getBeaconUserRelation($beaconId,Auth::user()->id);

        if($relation == null) {
            return response()->data([], 404, $isInternal);
        }

        $beacon = Beacon::find($relation->beacon_id);

        if($request->has('poi_id') && !PoiService::poiBelongsToTenant($request->input('poi_id'),Auth::user()->tenant_id)) {
            return response()->data([], 403, $isInternal);
        }

        if(Auth::user()->tenant_id == $beacon->tenant_id) {

            $meta = [];

            $beaconData = BeaconService::getPartialUpdateValues($request,BeaconService::getUpdateBeaconValidationRules(),true);

            if($beacon->substance_beacon) {
                unset($beaconData['name']);
                $meta['beacon_name'] = trans('api.BEACON_NAME');
            }

            $beacon->update($beaconData);

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $relation->beacon_id,
                        'key' => 'beacons',
                        'object' => new Beacon()
                    ]
                );

            }

            return response()->data($beacon, 200, $isInternal, $meta);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function destroy(Request $request, $beaconId, $isInternal = false) {

        if($this->isSuperAdmin(Auth::user())) {

            $relation = BeaconService::getBeaconUserRelation($beaconId,Auth::user()->id);

            if($relation == null) {
                return response()->data([], 404, $isInternal);
            }

            $beacon = Beacon::find($relation->beacon_id);
            $beacon->delete();

            $this->invalidateAndModify(
                [
                    'id' => $relation->beacon_id,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ]
            );

            return response()->data(null, 204, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
