<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

use App\Models\Poi;
use App\Models\User;

use App\Http\Services\PoiService;
use App\Http\Services\UserService;

class PoiUserController extends Controller {

    public function index(Request $request, $poiId, $isInternal = false) {

        $pc = new PoiController();

        $poi = $pc->show($request,$poiId,true);

        if($poi != null) {

            $users = $poi->users;
            return response()->data($users, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function store(Request $request, $poiId, $isInternal = false) {

        $this->scopeRequest();

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, PoiService::getStorePoiUserValidationRules());
            $poi = Poi::find($poiId);
            $addableUser = User::find($request->input('id'));

            if($poi->tenant_id == $addableUser->tenant_id) {
                PoiService::insertPoiUserRelation($poi->id,$addableUser->id);

                $this->invalidateAndModify(
                    [
                        'object' => new Poi()
                    ],
                    [
                        'object' => new User()
                    ]
                );

                return response()->data(trans('api.201'), 201, $isInternal);
            }

        }

        return response()->data([], 403, $isInternal);

    }

    public function destroy(Request $request, $poiId, $userId, $isInternal = false) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            PoiService::deletePoiUserRelation($poiId,$userId);

            $this->invalidateAndModify(
                [
                    'id' => $poiId,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'object' => new User()
                ]
            );

            return response()->data(null, 204, $isInternal);

        }
        return response()->data([], 403, $isInternal);
    }

}
