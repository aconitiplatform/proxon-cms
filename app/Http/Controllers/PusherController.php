<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Pusher;

use DB;
use Auth;
use Cache;
use Log;

use App\Jobs\PusherPoiAccessedJob;
use App\Jobs\PusherPoiLeftJob;

use App\Traits\AffectsEdwardStone;

class PusherController extends Controller {

    use AffectsEdwardStone;

    public function authenticatePresenceChannel(Request $request) {

        $pusher = new Pusher(config('broadcasting.connections.pusher.key'),config('broadcasting.connections.pusher.secret'),config('broadcasting.connections.pusher.app_id'),array('cluster'=>config('broadcasting.connections.pusher.options.cluster')));

        $payload = [
            'id' => Auth::user()->id,
            'firstname' => Auth::user()->firstname,
            'lastname' => Auth::user()->lastname,
        ];

        $data = $pusher->presence_auth($request->input('channel_name'), $request->input('socket_id'), Auth::user()->id, $payload);

        return response()->json($data,200);

    }

    public function presenceChannelWebhook(Request $request, $isInternal = false) {

        $webhookSignature = $_SERVER['HTTP_X_PUSHER_SIGNATURE'];

        $body = $request->getContent();

        $expectedSignature = hash_hmac('sha256',$body,config('broadcasting.connections.pusher.secret'),false);

        if($webhookSignature == $expectedSignature) {

            foreach($request->input('events') as $event) {

                switch ($event['name']) {
                    case 'member_added':
                        $this->dispatchJob(new PusherPoiAccessedJob($event['channel'],$event['user_id']));
                        break;
                    case 'member_removed':
                        $this->dispatchJob(new PusherPoiLeftJob($event['channel'],$event['user_id']));
                        break;
                    default:
                        Log::info($event['name']);
                        break;
                }

            }

            return response()->data([], 201, $isInternal);

        }

        return response()->data([], 403, $isInternal);

    }

}
