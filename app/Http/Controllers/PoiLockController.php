<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\PoiLock;


class PoiLockController extends Controller {

    public function index(Request $request, $isInternal = false) {

        $poiLocks = PoiLock::where('tenant_id',Auth::user()->tenant_id)->orderBy('created_at','asc')->get();

        if(!$poiLocks->isEmpty()) {

            foreach($poiLocks as $poiLock) {

                $user = $poiLock->user;
                $lockedPois[$poiLock->poi_id][] = [
                    'id' => $user->id,
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                ];

            }

        } else {
            $lockedPois = [];
        }

        return response()->data(['pois' => $lockedPois, 'tenantId' => Auth::user()->tenant_id]);

    }

}
