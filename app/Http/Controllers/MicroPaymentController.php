<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;

use Auth;
use Cache;

use App\Models\MicroPayment;

use App\Http\Services\MicroPaymentService;

use App\Traits\AffectsEdwardStone;
use App\Traits\PaginatesLists;
use App\Traits\HasSearch;

class MicroPaymentController extends Controller {

    use AffectsEdwardStone, PaginatesLists, HasSearch;

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(MicroPaymentService::getLastModified(Auth::user()->tenant_id,new MicroPayment(),true), 200, $isInternal);
    }

    public function index(Request $request, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('micro-payments:index');

        if(Cache::has($cacheKey)) {
            $microPayments = Cache::get($cacheKey);
        } else {

            $microPayments = Auth::user()->microPayments;

            Cache::forever($cacheKey,$microPayments);

        }

        return response()->data($this->paginate($microPayments,$request->input('skip'),$request->input('take'),$request->input('since')), 200, $isInternal, $this->getListMetaInformation());

    }

    public function show(Request $request, $microPaymentId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('micro-payments:'.$microPaymentId);

        if(Cache::has($cacheKey)) {
            $microPayment = Cache::get($cacheKey);
        } else {

            try {
                $microPayment = Auth::user()->microPayments()->findOrFail($microPaymentId);
            } catch (\Exception $e) {
                return response()->data([], 404, $isInternal);
            }

            Cache::forever($cacheKey,$microPayment);

        }

        return response()->data($microPayment, 200, $isInternal);

    }

    public function showTransactions(Request $request, $microPaymentId, $isInternal = false) {

        $skip = $request->has('skip') ? $request->input('skip') : 0;

        $cacheKey = $this->getRoleBasedCacheKey('micro-payments:'.$microPaymentId.':transactions:'.$skip);

        if(Cache::has($cacheKey)) {
            $data = Cache::get($cacheKey);
        } else {

            Stripe::setApiKey(config('substance.stripe_secret'));

            $microPayment = $this->show($request, $microPaymentId, true);

            $i = 0;

            $data = array();

            if(isset($microPayment->transactions)) {

                $transactions = $microPayment->transactions()->orderBy('created_at','desc')->take(10)->skip($skip)->get();

                foreach($transactions as $transaction) {

                    $charge = \Stripe\Charge::retrieve($transaction->stripe_id);

                    $data[$i]['transaction'] = $transaction;
                    $data[$i]['charge']['paid'] = $charge->paid;
                    $data[$i]['charge']['outcome'] = $charge->outcome;
                    $data[$i]['charge']['refunded'] = $charge->refunded;
                    $i++;

                }

            }

            Cache::put($cacheKey,$data,10);

        }

        return response()->data($data, 200, $isInternal);

    }

    public function store(Request $request, $isInternal = false) {

        $this->validate($request, MicroPaymentService::getStoreMicroPaymentValidationRules());

        if(!$request->input('callback_url')) {
            $request['callback_url'] = config('substance.edward_stone_url').config('substance.payment.standard_callback');
        }

        if(!$request->input('heartbeat_url')) {
            $request['heartbeat_url'] = config('substance.edward_stone_url').config('substance.payment.standard_heartbeat');
        }

        $microPaymentData = $request->only('name', 'description', 'amount', 'type', 'currency', 'allowed_payment_types', 'fields', 'emails_payee', 'emails_payer', 'email', 'callback_url', 'heartbeat_url');
        $microPaymentData['tenant_id'] = Auth::user()->tenant_id;
        $microPaymentData['user_id'] = Auth::user()->id;

        $microPayment = MicroPayment::create($microPaymentData);

        $microPayment->save();

        $this->invalidateAndModify(
            [
                'id' => $microPayment->id,
                'key' => 'micro-payments',
                'object' => new MicroPayment()
            ]
        );

        $this->publishMicroPaymentToEdwardStone($microPayment->id,true);

        MicroPaymentService::addUsersToMicroPayment($microPayment->id,Auth::user()->tenant_id,Auth::user()->id);

        return response()->data($microPayment, 201, $isInternal);

    }

    public function update(Request $request, $microPaymentId, $isInternal = false) {

        $this->validate($request, MicroPaymentService::getUpdateMicroPaymentValidationRules());

        try {
            $microPayment = Auth::user()->microPayments()->findOrFail($microPaymentId);
        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal);
        }

        $microPaymentData = MicroPaymentService::getPartialUpdateValues($request,MicroPaymentService::getUpdateMicroPaymentValidationRules());
        $microPayment->update($microPaymentData);

        $this->invalidateAndModify(
            [
                'id' => $microPaymentId,
                'key' => 'micro-payments',
                'object' => new MicroPayment()
            ]
        );

        $this->publishMicroPaymentToEdwardStone($microPaymentId,true);

        return response()->data($microPayment, 200, $isInternal);

    }

    public function destroy(Request $request, $microPaymentId, $isInternal = false) {

        try {
            $microPayment = Auth::user()->microPayments()->findOrFail($microPaymentId);
        } catch (\Exception $e) {
            return response()->data([], 404, $isInternal);
        }

        if($microPayment->stripe_product_id != null) {

            Stripe::setApiKey(config('substance.stripe_secret'));
            $product = \Stripe\Product::retrieve($microPayment->stripe_product_id);
            $product->delete();

        }

        $microPayment->delete();

        $this->invalidateAndModify(
            [
                'id' => $microPaymentId,
                'key' => 'micro-payments',
                'object' => new MicroPayment()
            ]
        );

        $this->publishMicroPaymentToEdwardStone($microPaymentId,false);

        return response()->data(null, 204, $isInternal);

    }

}
