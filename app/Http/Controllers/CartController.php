<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Log;

use App\Models\Tenant;
use App\Models\Cart;

use App\Traits\Validators\CartValidationRules;

use App\Jobs\SendCartToAdminJob;
use App\Jobs\SendCartToUserJob;

class CartController extends Controller {

    use CartValidationRules;

    public function orderCart(Request $request) {

        if($this->isTenantAdmin(Auth::user(),Auth::user()->tenant_id)) {

            $this->validate($request, $this->getCartValidationRules());

            try {

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                $tenant = Auth::user()->tenant;

                if($tenant->stripe_token_id == null || $tenant->stripe_token_id != $request->input('stripe_token_id')) {

                    $tenant->stripe_token_id = $request->input('stripe_token_id');
                    $tenant->save();

                }

                $token = \Stripe\Token::retrieve($tenant->stripe_token_id);

                \Stripe\Charge::create([
                    "amount" => $this->getAmount($request->input('cart')),
                    "currency" => "eur",
                    "source" => $token,
                    "description" => "Charge for ".$tenant->name,
                    "receipt_email" => $tenant->email,
                ]);

                $items = $this->adjustPrices($request->input('cart'));

                $cart = new Cart();
                $cart->items = $items;
                $cart->stripe_token_id = $token->id;
                $cart->address_name = $request->input('address_name');
                $cart->address_optional = $request->input('address_optional');
                $cart->address_street = $request->input('address_street');
                $cart->address_zip = $request->input('address_zip');
                $cart->address_city = $request->input('address_city');
                $cart->address_country = $request->input('address_country');
                $cart->shipped = false;
                $cart->tenant_id = $tenant->id;
                $cart->save();

                $this->dispatchJob(new SendCartToAdminJob($tenant,$cart),'emails');
                $this->dispatchJob(new SendCartToUserJob($tenant,$cart,Auth::user()),'emails');

                return response()->data($cart, 201);

            } catch (\Exception $e) {
                return response()->data($e);
            }

        } else {
            return response()->data([], 403);
        }

    }

    private function adjustPrices($items) {

        for($i = 0; $i < count($items); $i++) {
            $items[$i]['price'] = config('substance.prices.'.$items[$i]['hardware']);
        }

        return $items;

    }

    private function getAmount($items) {

        $amount = 0;

        foreach($items as $item) {
            $amount += config('substance.prices.'.$item['hardware']);
        }

        return $amount;

    }

}
