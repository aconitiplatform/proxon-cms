<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\KpiService;

class KpiController extends Controller {

    public function showKpis(Request $request, $isInternal = false) {

        $isInternal = true;
        $data = array();
        $data['scans_today'] = $this->showScansToday($request,$isInternal);
        $data['content_delivered_today'] = $this->showDeliveredContentToday($request,$isInternal);
        $data['accessed_channels_today'] = $this->showAccessedChannelsToday($request,$isInternal);
        $data['conversion_rate_scans_to_content'] = $this->showConversionRateToday($request,$isInternal);

        return response()->data($data, 200, $isInternal);

    }

    public function showScansToday(Request $request, $isInternal = false, $isQueue = false) {

        $analytics = KpiService::getKpiData($request,'scans-today',$isQueue);
        return response()->data($analytics, 200, $isInternal);

    }

    public function showDeliveredContentToday(Request $request, $isInternal = false, $isQueue = false) {

        $analytics = KpiService::getKpiData($request,'content-delivered-today',$isQueue);
        return response()->data($analytics, 200, $isInternal);

    }

    public function showAccessedChannelsToday(Request $request, $isInternal = false, $isQueue = false) {

        $analytics = KpiService::getKpiData($request,'channel-access-today',$isQueue);
        return response()->data($analytics, 200, $isInternal);

    }

    public function showConversionRateToday(Request $request, $isInternal = false, $isQueue = false) {

        $scans = KpiService::getKpiData($request,'scans-today',$isQueue);
        $contents = KpiService::getKpiData($request,'content-delivered-today',$isQueue);

        $analytics['today'] = $scans['today'] > 0 ? $contents['today'] / $scans['today'] * 100 : $scans['today'];
        $analytics['yesterday'] = $scans['yesterday'] > 0 ? $contents['yesterday'] / $scans['yesterday'] * 100 : $scans['yesterday'];

        $analytics = KpiService::getKpiValues($analytics,true);
        return response()->data($analytics, 200, $isInternal, array(
            'scans' => $scans,
            'contents' => $contents,
        ));

    }

}
