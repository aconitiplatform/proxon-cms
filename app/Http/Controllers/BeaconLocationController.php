<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\Beacon;
use App\Models\BeaconLocation;

use App\Http\Services\BeaconService;

class BeaconLocationController extends Controller {

    public function lastModified(Request $request, $isInternal = false) {
        return response()->data(BeaconService::getLastModified(Auth::user()->tenant_id,new BeaconLocation(),true), 200, $isInternal);
    }

    public function show(Request $request, $beaconId, $isInternal = false) {

        $cacheKey = $this->getRoleBasedCacheKey('beaconlocations:'.$beaconId);

        if(Cache::has($cacheKey)) {
            $beaconLocation = Cache::get($cacheKey);
            return response()->data($beaconLocation, 200, $isInternal);
        } else {

            $beaconLocation = BeaconLocation::where('beacon_id',$beaconId)->first();

            if($beaconLocation != null) {

                Cache::forever($cacheKey,$beaconLocation);
                return response()->data($beaconLocation, 200, $isInternal);

            }

            return response()->data([], 404, $isInternal);

        }

    }

    public function store(Request $request, $beaconId, $isInternal = false) {

        $request['beacon_id'] = $beaconId;
        $this->validate($request,BeaconService::getStoreBeaconLocationValidationRules());

        $beaconLocation = BeaconLocation::where('beacon_id',$beaconId)->first();

        if($beaconLocation == null) {

            $beaconLocation = BeaconLocation::create($request->only('note','latitude','longitude','level','indoor','beacon_id'));

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $beaconId,
                        'key' => 'beacons',
                        'object' => new Beacon()
                    ],
                    [
                        'id' => $beaconId,
                        'key' => 'beaconlocations',
                        'object' => new BeaconLocation()
                    ]
                );

            }

            return response()->data($beaconLocation, 201, $isInternal);

        } else {
            return response()->data([], 400, $isInternal);
        }

        return response()->data([], 404, $isInternal);

    }

    public function update(Request $request, $beaconId, $isInternal = false) {

        $this->validate($request,BeaconService::getUpdateBeaconLocationValidationRules());

        $beaconLocation = BeaconLocation::where('beacon_id',$beaconId)->first();

        if($beaconLocation != null) {

            $beaconLocation->update(BeaconService::getPartialUpdateValues($request,BeaconService::getUpdateBeaconLocationValidationRules()));

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $beaconId,
                        'key' => 'beacons',
                        'object' => new Beacon()
                    ],
                    [
                        'id' => $beaconId,
                        'key' => 'beaconlocations',
                        'object' => new BeaconLocation()
                    ]
                );

            }

            return response()->data($beaconLocation, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function destroy(Request $request, $beaconId, $isInternal = false) {

        $beaconLocation = BeaconLocation::where('beacon_id',$beaconId)->first();

        if($beaconLocation != null) {

            $beaconLocation->delete();

            $this->invalidateAndModify(
                [
                    'id' => $beaconId,
                    'key' => 'beacons',
                    'object' => new Beacon()
                ],
                [
                    'id' => $beaconId,
                    'key' => 'beaconlocations',
                    'object' => new BeaconLocation()
                ]
            );

            return response()->data(null, 204, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

}
