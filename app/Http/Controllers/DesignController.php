<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Cache;

use App\Models\PoiDesign;
use App\Models\TenantDesign;
use App\Models\Tenant;
use App\Models\Poi;

use App\Http\Controllers\PoiController;
use App\Http\Controllers\TenantController;

use App\Http\Services\PoiService;
use App\Http\Services\DesignService;

use App\Traits\AffectsEdwardStone;

use App\Jobs\GenerateMediaPoiRelationsJob;
use App\Jobs\SetPoiInteractionFlagsJob;

class DesignController extends Controller {

    use AffectsEdwardStone;

    public function showForPoi(Request $request, $poiId, $isInternal = false) {

        $poiController = new PoiController();

        if(!!$poi = $poiController->show($request, $poiId, true)) {

            if($poi->poi_design_id != null) {

                $cacheKey = $this->getTenantRootCacheKey('poi-designs:'.$poi->poi_design_id);
                return $this->show($request,'App\Models\PoiDesign',$poi->poi_design_id,$cacheKey,$isInternal);

            }

        }

        return response()->data([], 404, $isInternal);

    }

    public function showForTenant(Request $request, $isInternal = false) {

        $tenantController = new TenantController();

        if(!!$tenant = $tenantController->show($request, true)) {

            if($tenant->tenant_design_id != null) {

                $cacheKey = $this->getTenantRootCacheKey('tenant-design');
                return $this->show($request,'App\Models\TenantDesign',$tenant->tenant_design_id,$cacheKey,$isInternal);

            }

        }

        return response()->data([], 404, $isInternal);

    }

    protected function show(Request $request, $model, $designId, $cacheKey, $isInternal = false) {

        if(Cache::has($cacheKey)) {
            return response()->data(Cache::get($cacheKey), 200, $isInternal);
        } else {

            $this->scopeRequest();

            $design = $model::find($designId);

            if($design != null) {

                Cache::forever($cacheKey,$design);
                return response()->data($design, 200, $isInternal);

            }

        }

        return response()->data([], 404, $isInternal);

    }

    public function storeForPoi(Request $request, $poiId, $isInternal = false) {

        $poiController = new PoiController();

        if(!!$poi = $poiController->show($request, $poiId, true)) {

            $design = $this->store(DesignService::getPoiDesignBlueprint(),'App\Models\PoiDesign',Auth::user()->tenant_id);
            $poi->poi_design_id = $design->id;
            $poi->save();

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $poi->id,
                        'key' => 'pois',
                        'object' => new Poi()
                    ]
                );

                $this->publishPoiToEdwardStone($poi->id,$poi->active);

            }

            return response()->data($design, 201, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function storeForTenant(Request $request, $isInternal = false) {

        $tenantController = new TenantController();

        if(!!$tenant = $tenantController->show($request, true)) {

            $design = $this->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',Auth::user()->tenant_id);
            $tenant->tenant_design_id = $design->id;
            $tenant->save();

            Cache::forever($this->getTenantRootCacheKey('tenant'),$tenant);

            $this->invalidateAndModify(
                [
                    'object' => new Tenant()
                ]
            );

            $this->publishTenantToEdwardStone($tenant->id,true);

            return response()->data($design, 201, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function store($designData, $model, $tenantId) {

        // DesignService::validateArray($designData, DesignService::getStoreDesignValidationRules($model));

        $designData['tenant_id'] = $tenantId;
        $design = $model::create($designData);

        return $design;

    }

    public function updateForPoi(Request $request, $poiId, $isInternal = false) {

        $poiController = new PoiController();

        if(!!$poi = $poiController->show($request, $poiId, true)) {

            $design = $this->update($request,'App\Models\PoiDesign',$poi->poi_design_id);

            $cacheKey = $this->getTenantRootCacheKey('poi-designs:'.$poi->poi_design_id);
            Cache::forever($cacheKey,$design);

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $poi->id,
                        'key' => 'pois',
                        'object' => new Poi()
                    ]
                );

                $this->publishPoiToEdwardStone($poi->id,$poi->active);

            }

            return response()->data($design, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function updateForTenant(Request $request, $isInternal = false) {

        $tenantController = new TenantController();

        if(!!$tenant = $tenantController->show($request, true)) {

            $design = $this->update($request, 'App\Models\TenantDesign', $tenant->tenant_design_id);

            if($design != null) {

                $cacheKey = $this->getTenantRootCacheKey('tenant-design');
                Cache::forever($cacheKey,$design);

                Cache::forever($this->getTenantRootCacheKey('tenant'),$tenant);

                $this->invalidateAndModify(
                    [
                        'object' => new Tenant()
                    ]
                );

                $this->publishTenantToEdwardStone($tenant->id,true);

                return response()->data($design, 200, $isInternal);

            }

        }

        return response()->data([], 404, $isInternal);

    }

    protected function update(Request $request, $model, $designId) {

        $this->validate($request, DesignService::getUpdateDesignValidationRules($model));
        DesignService::validateLogoSettings($request);

        try {

            $design = $model::findOrFail($designId);

            $design->update(DesignService::getPartialUpdateValues($request,DesignService::getUpdateDesignValidationRules($model),false,true,['fonts'=>[]]));
            // dd($design);
            return $design;

         } catch (\Exception $e) {
            return null;
        }

    }

    public function resetForTenant(Request $request, $isInternal = false) {

        $tenantController = new TenantController();

        if(!!$tenant = $tenantController->show($request, true)) {

            TenantDesign::destroy($tenant->tenant_design_id);

            $design = $this->storeForTenant($request,true);

            return response()->data($design, 200, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function destroyForPoi(Request $request, $poiId, $isInternal = false) {

        $poiController = new PoiController();

        if(!!$poi = $poiController->show($request, $poiId, true)) {

            $cacheKey = $this->getTenantRootCacheKey('poi-designs:'.$poi->poi_design_id);
            Cache::forget($cacheKey);

            PoiDesign::destroy($poi->poi_design_id);
            $poi->poi_design_id = null;
            $poi->save();

            if(!$this->isFacade) {

                $this->invalidateAndModify(
                    [
                        'id' => $poi->id,
                        'key' => 'pois',
                        'object' => new Poi()
                    ]
                );

                $this->publishPoiToEdwardStone($poi->id,$poi->active);

            }

            return response()->data(null, 204, $isInternal);

        }

        return response()->data([], 404, $isInternal);

    }

    public function getDesignBlueprint(Request $request, $type) {
        return $type == 'tenant' ? response()->data(DesignService::getTenantDesignBlueprint()) : response()->data(DesignService::getPoiDesignBlueprint());
    }

}
