<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\Poi;
use App\Models\Media;

use App\Http\Services\MediaService;
use App\Http\Services\PoiService;

class MediaPoiController extends Controller {

    public function store(Request $request, $mediaId, $isInternal = false) {

        $this->validate($request, MediaService::getStoreMediaPoiValidationRules());

        try {
            $poi = Poi::findOrFail($request->input('id'));
            $media = Media::findOrFail($mediaId);
        } catch(\Exception $e) {
            return response()->data([], 404, $isInternal);
        }

        if($poi->tenant_id == $media->tenant_id) {

            MediaService::insertMediaPoiRelation($media->id,$poi->id);

            $this->invalidateAndModify(
                [
                    'id' => $poi->id,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'object' => new Media()
                ]
            );

            return response()->data(trans('api.201'), 201, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

    public function destroy(Request $request, $mediaId, $poiId, $isInternal = false) {

        $request['poi_id'] = $poiId;

        $this->validate($request, MediaService::getDeleteMediaPoiValidationRules());

        try {
            $poi = Poi::findOrFail($poiId);
            $media = Media::findOrFail($mediaId);
        } catch(\Exception $e) {
            return response()->data([], 404, $isInternal);
        }

        if($poi->tenant_id == $media->tenant_id) {

            MediaService::deleteMediaPoiRelation($media->id,$poi->id);

            $this->invalidateAndModify(
                [
                    'id' => $poi->id,
                    'key' => 'pois',
                    'object' => new Poi()
                ],
                [
                    'object' => new Media()
                ]
            );

            return response()->data(null, 204, $isInternal);

        } else {
            return response()->data([], 403, $isInternal);
        }

    }

}
