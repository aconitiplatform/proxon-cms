<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Monolog\Handler\SlackWebhookHandler;
use Monolog\Logger;

use Validator;
use Log;
use App;

use App\Http\Services\Service;
use App\Http\Services\ServerService;

use App\Traits\Validators\TenantValidationRules;

class AppServiceProvider extends ServiceProvider {

    use TenantValidationRules;

    public function boot() {

        $this->bootImageGalleryValidation();
        // $this->bootSlackWebhookLogging();
        $this->bootFontListValidation();
        $this->bootNewTenantValidation();
        $this->bootNewTenantAdminValidation();
        $this->bootRegisterTenantValidation();
        $this->bootNewTenantAdminWithPasswordValidation();

    }

    public function register() {
        //
    }

    private function bootFontListValidation() {

        Validator::extend('font_list', function($attribute, $value, $parameters, $validator) {

            $result = true;
            $standardCount = 0;

            if(!is_array($value) || $value == '') return false;

            if(count($value) == 0) return true;

            $rules = [
                'family' => 'required|string',
                'import' => 'required|string',
                'standard' => 'required|boolean'
            ];

            foreach($value as $fontListItem) {

                try {
                    Service::validateArray($fontListItem,$rules);
                    if($fontListItem['standard'] === true) {
                        $standardCount++;
                    }
                } catch(\Exception $e) {
                    $result = false;
                }

            }

            if($standardCount > 1) {
                $result = false;
            }

            return $result;

        });

    }

    private function bootImageGalleryValidation() {

        Validator::extend('image_gallery', function($attribute, $value, $parameters, $validator) {

            $result = true;

            $rules = [
                'id' => 'required|string|size:36|exists:media,id',
                'caption' => 'sometimes|string'
            ];

            foreach($value as $galleryItem) {

                try {
                    Service::validateArray($galleryItem,$rules);
                } catch(\Exception $e) {
                    $result = false;
                }

            }

            return $result;

        });

    }

    private function bootSlackWebhookLogging() {

        $monolog = Log::getMonolog();

        $ip = ServerService::getServerIp();

        if(App::environment('production')) {
            $monolog->pushHandler(new SlackWebhookHandler(config('substance.slack_prod_logging_webhook'),config('substance.slack_prod_logging_channel'),config('app.url').' on '.$ip,true,':skull_and_crossbones:',true,false,Logger::WARNING,true,[]));
        } else if (!App::environment('local')) {
            $monolog->pushHandler(new SlackWebhookHandler(config('substance.slack_dev_logging_webhook'),config('substance.slack_dev_logging_channel'),config('app.url').' on '.$ip,true,':skull:',true,false,Logger::WARNING,true,[]));
        }

    }

}
