<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

        // protected function internalResponse($data, $isInternal = false, $meta = array(), $code = 200) {
        //
        //     if($isInternal && $code >= 200 && $code <= 299) {
        //         return $data;
        //     } else if($isInternal) {
        //         return null;
        //     }
        //
        //     $meta['count'] = count($data);
        //     $meta['info'] = trans('api.'.$code);
        //
        //     if($code >= 200 && $code <= 299) {
        //         return response()->success($data,$code,$meta);
        //     } else {
        //         return response()->error($data,$code,$meta);
        //     }
        //
        // }

        Response::macro('data', function($data, $code = 200, $isInternal = false, $meta = []) {

            if($isInternal && $code >= 200 && $code <= 299) {
                return $data;
            } else if($isInternal) {
                return null;
            }
            // dd(is_array($data));
            $meta['count'] = isset($data) && is_array($data) ? count($data) : 1;
            $meta['info'] = trans('api.'.$code);

            if($code >= 200 && $code <= 299) {
                return response()->success($data,$code,$meta);
            } else {
                return response()->error($data,$code,$meta);
            }

        });


        Response::macro('success', function ($data, $status = 200, $meta = array()) {

            // Response::setJsonOptions(JSON_FORCE_OBJECT);

            return Response::json(
                [
                    'errors'  => false,
                    'meta' => $meta,
                    'data' => $data,
                    'status_code' => $status,
                ],
                $status
            );

        });

        Response::macro('error', function ($message, $status = 400, $meta = array()) {

            return Response::json(
                [
                    'message' => $status.' error',
                    'errors' => [
                        'message' => [
                            $message
                        ],
                        'meta' => $meta
                    ],
                    'status_code' => $status,
                ],
                $status
            );

        });

        Response::macro('maintenance', function () {

            return Response::json(
                [
                    'errors'  => false,
                    'meta' => [],
                    'data' => trans('api.MAINTENANCE_API'),
                    'status_code' => 503,
                ],
                503
            );

        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
