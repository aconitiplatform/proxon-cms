<?php

namespace App\Traits;

use Auth;
use Landlord;

trait ScopesRequests {

    protected function scopeRequest() {
        Landlord::addTenant('tenant_id',Auth::user()->tenant_id);
    }

}
