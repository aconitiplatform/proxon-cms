<?php

namespace App\Traits;

use App\Jobs\DeliverPoiToDistributionJob;
use App\Jobs\RemovePoiFromDistributionJob;
use App\Jobs\DeliverSurveyToDistributionJob;
use App\Jobs\RemoveSurveyFromDistributionJob;
use App\Jobs\DeliverTenantToDistributionJob;
// use App\Jobs\RemoveTenantFromDistributionJob;
use App\Jobs\ConnectPoiToEdwardStoneUrlJob;
use App\Jobs\DisconnectPoiFromEdwardStoneUrlJob;
use App\Jobs\DeliverMicroPaymentToDistributionJob;
use App\Jobs\RemoveMicroPaymentFromDistributionJob;

trait AffectsEdwardStone {

    protected function publishPoiToEdwardStone($poiId,$toBePublished = true) {

        if($toBePublished) {
            $this->dispatchJob(new DeliverPoiToDistributionJob($poiId));
        } else {
            $this->dispatchJob(new RemovePoiFromDistributionJob($poiId));
        }

    }

    protected function publishSurveyToEdwardStone($surveyId,$toBePublished = true) {

        if($toBePublished) {
            $this->dispatchJob(new DeliverSurveyToDistributionJob($surveyId));
        } else {
            $this->dispatchJob(new RemoveSurveyFromDistributionJob($surveyId));
        }

    }

    protected function publishTenantToEdwardStone($tenantId,$toBePublished = true) {

        if($toBePublished) {
            $this->dispatchJob(new DeliverTenantToDistributionJob($tenantId));
        } else {
            // $this->dispatchJob(new RemoveTenantToDistributionJob($tenantId));
        }

    }

    protected function publishPoiUrlConnection($poiId,$url,$tenantId,$toBePublished = true) {

        if($toBePublished) {
            $this->dispatchJob(new ConnectPoiToEdwardStoneUrlJob($poiId,$url,$tenantId));
        } else {
            $this->dispatchJob(new DisconnectPoiFromEdwardStoneUrlJob($poiId,$url));
        }

    }

    protected function publishMicroPaymentToEdwardStone($microPaymentId,$toBePublished = true) {

        if($toBePublished) {
            $this->dispatchJob(new DeliverMicroPaymentToDistributionJob($microPaymentId));
        } else {
            $this->dispatchJob(new RemoveMicroPaymentFromDistributionJob($microPaymentId));
        }

    }

}
