<?php

namespace App\Traits;

use Log;

use App\Models\PoiEvent;

trait SavesPoiEvent {

    public function savePoiEvent($poiEventData,$user) {

        try {

            $poiEventData['user_id'] = $user->id;
            $poiEventData['tenant_id'] = $user->tenant_id;

            $poiEvent = PoiEvent::create($poiEventData);

            return $poiEvent;

        } catch(\Exception $e) {
            return null;
        }

    }

}
