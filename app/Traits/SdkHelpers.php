<?php

namespace App\Traits;

use Carbon\Carbon;

use Auth;

use App\Http\Controllers\PoiController;
use App\Http\Controllers\PoiBeaconController;

trait SdkHelpers {

    private function connectBeacon($beacon,$poi) {

        $poiBeaconRequest = new \Dingo\Api\Http\Request();
        $poiBeaconRequest['id'] = $beacon->id;

        $poiBeaconController = new PoiBeaconController();
        $beacon = $poiBeaconController->store($poiBeaconRequest, $poi->id, true);

        return [
            'id' => $beacon->id,
            'name' => $beacon->name,
            'alias' => $beacon->alias,
            'associated' => true,
        ];

    }

    private function createPoiAndContent($url,$title = null,$description = null) {

        $now = Carbon::now();

        $poiCreationRequest = new \Dingo\Api\Http\Request();
        $poiController = new PoiController();

        $name = Auth::user()->firstname." ".$now->toDateTimeString();
        $poiCreationRequest['name'] = $title != null ? "{$title} ({$name})" : $name;
        $poiCreationRequest['description'] = $description != null ? $description : "Created by ".Auth::user()->firstname;
        $poiCreationRequest['meta'] = "";
        $poiCreationRequest['has_channel'] = false;
        $poiCreationRequest['fallback_language'] = Auth::user()->language;

        if($title != null && $description != null) {
            $meta = [
                'title' => $title,
                'description' => $description
            ];
        } else {
            $meta = [];
        }

        $poiCreationRequest['poi_contents'] = [
            [
                'language' => Auth::user()->language,
                'data' => [
                    'groups' => []
                ],
                'active' => true,
                'has_url' => true,
                'url' => $url,
                'meta' => $meta
            ]
        ];

        $poi = $poiController->store($poiCreationRequest, true, true);

        return $poi;

    }

    private function deletePoi($beacon) {

        $poiDeletionRequest = new \Dingo\Api\Http\Request();
        $poiController = new PoiController();
        $poiController->destroy($poiDeletionRequest, $beacon->poi_id, true);

    }

}
