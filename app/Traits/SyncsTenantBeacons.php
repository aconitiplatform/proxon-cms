<?php

namespace App\Traits;

use App\Models\Tenant;
use App\Models\User;
use App\Models\Beacon;

use App\Http\Controllers\Auth\PasswordResetController;

use App\Http\Services\Apis\KontaktIoService;

use App\Jobs\DeliverTenantToDistributionJob;
use App\Jobs\SendTenantDataToAdminJob;

trait SyncsTenantBeacons {

    protected function syncBeacons(Tenant $tenant,$fromCli = false) {

        try {
            $devices = KontaktIoService::getVenueDevices($tenant->kontakt_io_venue_id);
        } catch(\Exception $e) {
            if($fromCli) exit;
            return response()->data(trans('api.KIO_KEY_WRONG'), 400);
        }

        $beacons = $tenant->beacons;

        if(count($devices) == count($beacons)) {
            if($fromCli) $this->info("number of beacons and number of devices are equal.");
        } else {
            if($fromCli) $this->error("number of beacons and number of devices are not equal.");
            if($fromCli) exit;
            return response()->data(trans('api.BEACON_AMOUNT_UNEQUAL'),422);
        }

        $beaconInfos = $this->updateBeaconBatch($beacons,$devices,$fromCli);

        $tenant->initial_kontakt_io_sync = true;
        $tenant->active = true;
        $tenant->save();
        if($fromCli) $this->info("activated tenant");

        $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
        if($fromCli) $this->info("delivered tenant to edward stone");

        $user = User::where('tenant_id',$tenant->id)->where('role','admin')->first();

        $passwordResetController = new PasswordResetController();
        $passwordResetController->triggerInternalSendResetLinkEmail($user->email);
        if($fromCli) $this->info("password reset sent to tenant admin");

        $this->dispatchJob(new SendTenantDataToAdminJob($tenant,$user,$beaconInfos));
        if($fromCli) $this->info("tenant data sent to Proxon");

        if(!$fromCli) return response()->data(compact('tenant','user'),201);

    }

    protected function syncBeaconBatches(Tenant $tenant) {

        $beaconBatches = $tenant->beaconBatches()->where('synced',false)->get();

        if(!$beaconBatches->isEmpty()) {

            $syncedBeacons = [];

            foreach($beaconBatches as $batch) {

                try {
                    $devices = KontaktIoService::getVenueDevices($batch->kontakt_io_venue_id);
                } catch(\Exception $e) {
                    $syncedBeacons[$batch->id]['error'] = "problem with kontakt.io API";
                    $syncedBeacons[$batch->id]['batch'] = $batch;
                    continue;
                }

                $beacons = $tenant->beacons()->where('beacon_batch_id',$batch->id)->get();

                if(count($devices) != count($beacons)) {
                    $syncedBeacons[$batch->id]['error'] = "number of substance beacons and kontakt.io devices are not equal";
                    $syncedBeacons[$batch->id]['batch'] = $batch;
                    continue;
                }

                $syncedBeacons[$batch->id]['beacon_infos'] = $this->updateBeaconBatch($beacons,$devices,false);

                $batch->synced = true;
                $batch->save();

                $syncedBeacons[$batch->id]['batch'] = $batch;

            }

            $this->invalidateAndModify(
                [
                    'id' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                    'key' => 'beacons',
                    'object' => new Beacon(),
                    'tenant_id' => $tenant->id,
                ],
                [
                    'tenant_id' => $tenant->id,
                    'object' => new Tenant()
                ]
            );

            $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
            return response()->data($syncedBeacons,200);

        }

        return response()->data(null,204);

    }

    private function updateBeaconBatch($beacons,$devices,$fromCli) {

        $beaconInfos = [];

        for($i = 0; $i < count($beacons); $i++) {

            $beacons[$i]->name = $devices[$i]->uniqueId;

            $updateStatusCode = KontaktIoService::updateDeviceUrl($devices[$i]->uniqueId,$this->createBeaconHexUrl($beacons[$i]));

            if($fromCli) $this->info("Updated ".$devices[$i]->uniqueId." with status code ".$updateStatusCode);

            $beacons[$i]->save();

            $beaconInfos[] = array(
                $devices[$i]->uniqueId,
                $beacons[$i]->url
            );

        }

        return $beaconInfos;

    }

    private function createBeaconHexUrl(Beacon $beacon) {

        return config('substance.beacon_url_prefix').$this->stringToHex(config('substance.beacon_url').$beacon->url);

    }

    private function stringToHex($string){

        $hex='';

        for ($i=0; $i < strlen($string); $i++){
            $hex .= dechex(ord($string[$i]));
        }

        return $hex;

    }

}
