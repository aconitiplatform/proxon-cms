<?php

namespace App\Traits;

use App\Models\Tenant;
use App\Models\Reseller;
use App\Models\Features\Feature;
use App\Models\Features\Module;

trait SyncsFeaturesAndModules {

    public function syncDefaults(Tenant $tenant) {

        $this->syncDefaultFeatures($tenant,'tenant');
        $this->syncDefaultModules($tenant,'tenant');

    }

    public function syncDefaultsForReseller(Reseller $reseller) {

        $this->syncDefaultFeatures($reseller,'reseller');
        $this->syncDefaultModules($reseller,'reseller');

    }

    public function syncDefaultFeatures($model,$type) {

        if($type == 'tenant' && $model->reseller_id != null) {
            $reseller = Reseller::find($model->reseller_id);

            if($reseller == null) {
                $features = $this->getDefaultFeatures();
            } else {
                $features = $reseller->features;
            }

        } else {
            $features = $this->getDefaultFeatures();
        }

        if(!$features->isEmpty()) {

            $featureIds = [];

            foreach($features as $feature) {

                $featureIds[] = $feature->id;

                $modules = $feature->modules;

                if(!$modules->isEmpty()) {

                    foreach($modules as $module) {
                        $model->modules()->attach($module->id);
                    }

                }

            }

            if($type == 'tenant' && $model->reseller_id != null) {
                $model->features()->sync($featureIds);
            } else {
                $model->features()->syncWithoutDetaching($featureIds);
            }

        }

    }

    public function syncDefaultModules($model,$type) {

        if($type == 'tenant' && $model->reseller_id != null) {
            $reseller = Reseller::find($model->reseller_id);

            if($reseller == null) {
                $modules = $this->getDefaultModules();
            } else {
                $modules = $reseller->modules;
            }

        } else {
            $modules = $this->getDefaultModules();
        }

        if(!$modules->isEmpty()) {

            $moduleIds = [];

            foreach($modules as $module) {

                $moduleIds[] = $module->id;

                $features = $module->features;

                if(!$features->isEmpty()) {

                    foreach($features as $feature) {
                        $model->features()->attach($feature->id);
                    }

                }

            }

            if($type == 'tenant' && $model->reseller_id != null) {
                $model->modules()->sync($moduleIds);
            } else {
                $model->modules()->syncWithoutDetaching($moduleIds);
            }

        }

    }

    private function getDefaultFeatures() {
        return Feature::where('default',true)->get();
    }

    private function getDefaultModules() {
        return Module::where('default',true)->get();
    }

}
