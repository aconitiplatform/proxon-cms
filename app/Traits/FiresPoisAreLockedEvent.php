<?php

namespace App\Traits;

use Redis;
use Cache;

use App\Models\User;
use App\Models\PoiLock;

use App\Events\PoisAreLocked;

trait FiresPoisAreLockedEvent {

    protected function firePoisAreLockedEvent(User $user) {

        $poiLocks = PoiLock::where('tenant_id',$user->tenant_id)->orderBy('created_at','asc')->get();

        if(!$poiLocks->isEmpty()) {

            foreach($poiLocks as $poiLock) {

                $user = $poiLock->user;
                $lockedPois[$poiLock->poi_id][] = [
                    'id' => $user->id,
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                ];

            }

        } else {
            $lockedPois = [];
        }

        if(config('broadcasting.should_broadcast')) event(new PoisAreLocked($user->tenant_id,$lockedPois));

    }


}
