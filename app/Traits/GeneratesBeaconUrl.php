<?php

namespace App\Traits;

use App\Models\Beacon;

use App\Http\Services\BeaconService;

trait GeneratesBeaconUrl {

    public function newUniqueUrl() {

        $isUnique = false;

        $string = BeaconService::generateRandomString(config('substance.url_length'));

        while(!$isUnique) {

            $beacon = Beacon::where('url',$string)->first();

            if($beacon == null) {
                $isUnique = true;
            } else {
                $string = BeaconService::generateRandomString(config('substance.url_length'));
            }

        }

        return $string;

    }

}
