<?php

namespace App\Traits;

use Auth;

trait GeneratesCacheKeys {

    protected function getChartCacheKey($segments,$actual,$poiId = null) {

        if(Auth::user()->role == 'subuser' || Auth::user()->role == 'reseller') {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->id;
        } else {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->role;
        }

        for($i = 1;$i < count($segments)-1; $i++) {
            $cacheKey .= ":".$segments[$i];
        }

        $cacheKey .= ":".$actual->timestamp;

        $poiId == null ?: $cacheKey .= ":".$poiId;

        return $cacheKey;

    }

    protected function getRoleBasedCacheKey($path) {

        if(Auth::user()->role == 'subuser' || Auth::user()->role == 'reseller') {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->id.':'.$path;
        } else {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->role.':'.$path;
        }

        return $cacheKey;

    }

    protected function getTenantRootCacheKey($path,$tenantId = null) {

        return $tenantId != null ? $tenantId.':'.$path : Auth::user()->tenant_id.':'.$path;

    }

    protected static function getRoleBasedCacheKeyStatically($path) {

        if(Auth::user()->role == 'subuser' || Auth::user()->role == 'reseller') {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->id.':'.$path;
        } else {
            $cacheKey = Auth::user()->tenant_id.':users:'.Auth::user()->role.':'.$path;
        }

        return $cacheKey;

    }

    protected static function getTenantRootCacheKeyStatically($path,$tenantId = null) {

        return $tenantId != null ? $tenantId.':'.$path : Auth::user()->tenant_id.':'.$path;

    }

}
