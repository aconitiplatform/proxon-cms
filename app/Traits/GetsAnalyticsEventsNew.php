<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Log;
use DB;
use Auth;

use App\Models\Analytics\AnalyticsEvent;
use App\Models\Analytics\AnalyticsScan;

trait GetsAnalyticsEventsNew {

    protected function getAnalyticsEvents($type,Request $request,$url = null,$withStartAndEnd=true,$poiId=null) {

        $events = AnalyticsEvent::where('tenant_id',Auth::user()->tenant_id)->where('accessed',$type);
        return $this->getAccessEvents($events,$request,$url,$withStartAndEnd,$poiId);

    }

    protected function getAnalyticsScans(Request $request,$url = null,$withStartAndEnd=true,$poiId=null) {

        $events = AnalyticsScan::where('tenant_id',Auth::user()->tenant_id);
        return $this->getAccessEvents($events,$request,$url,$withStartAndEnd,$poiId);

    }




    // protected function getAccessEventsByUrl(Request $request,$tenantId,$url,$withStartAndEnd=true,$accessType=null) {
    //     return $this->getAccessEvents($request,$tenantId,$url,$withStartAndEnd,$accessType);
    // }

    // backwards compatibility
    protected function getAccessEvents($events,Request $request,$url=null,$withStartAndEnd=true,$poiId=null) {

        if($withStartAndEnd) {

            $start = time() - 3600;
            $end = time();

            if($request->has('start')) {
                $start = $request->input('start');
            }

            if($request->has('end')) {
                $end = $request->input('end');
            }

            $start = Carbon::createFromTimestamp($start)->toDateTimeString();
            $end = Carbon::createFromTimestamp($end)->toDateTimeString();

            $events = $events->where('fired_at_timestamp','>=',$start)->where('fired_at_timestamp','<=',$end);

        }

        if($url!=null) {
            $events = $events->where('url',$url);
        }

        if($poiId!=null) {
            $events = $events->where('poi_id',$poiId);
        } else {

            $poiIds = [];

            $pois = Auth::user()->pois;

            foreach($pois as $poi) {
                $poiIds[] = $poi->id;
            }

            $events = $events->whereIn('poi_id',$poiIds);

        }

        // if($accessType!=null) {
        //     $events = $events->where('access_type',$accessType);
        // }

        $events = $events->get();

        if(!$events->isEmpty()) {
            return $events;
        }

        return [];

    }

}
