<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\ServiceAccount\Discoverer;

use Log;

use App\Singletons\FirebaseServiceAccountFactory;

trait CreatesDataAtFirebase {

    protected function createFirebaseObject($resource,$data) {

        $firebase = $this->getFirebaseInstance();

        $database = $firebase->getDatabase();
        $reference = $database->getReference($resource)->set($data);

    }

    private function getFirebaseInstance() {

        $discoverer = new Discoverer([
			function() {
				$fbs = FirebaseServiceAccountFactory::Instance();
				$serviceAccount = ServiceAccount::fromJson($fbs->getServiceAccountConfig());
				return $serviceAccount;
			}
		]);

		$firebase = (new Factory())
			->withServiceAccountDiscoverer($discoverer)
			->create();

		return $firebase;

    }

}
