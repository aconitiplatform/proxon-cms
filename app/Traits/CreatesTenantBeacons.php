<?php

namespace App\Traits;

use App\Models\Beacon;
use App\Models\BeaconBatch;

use App\Http\Services\BeaconService;
use App\Http\Services\Apis\KontaktIoService;

use App\Traits\GeneratesBeaconUrl;

trait CreatesTenantBeacons {

    use GeneratesBeaconUrl;

    protected function createBeaconBatch($tenant,$amount,$withKontaktIo = false) {

        $batch = new BeaconBatch();

        if($withKontaktIo) {

            $kontakt_io_venue_name = $tenant->name." ".str_random(4);
            $batch->kontakt_io_venue_name = $kontakt_io_venue_name;
            $batch->synced = false;

            try {
                $venue = KontaktIoService::createVenue($kontakt_io_venue_name,$tenant->id." (Substance Tenant ID)");
            } catch(\Exception $e) {
                return false;
            }

            if($venue == null) {
                Log::error('Could not create venue at Kontakt IO');
                return false;
            } else {
                $batch->kontakt_io_venue_id = $venue->id;
            }

        } else {
            $batch->synced = true;
        }

        $batch->tenant_id = $tenant->id;
        $batch->number_of_beacons = $amount;
        $batch->save();
        $this->createTenantBeacons($tenant,false,$amount,$batch);

        return $batch;

    }

    protected function createTenantBeacons($tenant,$fromCli = false,$amount = null,$batch = null) {

        $beaconInfos = [];

        if($fromCli) $bar = $this->output->createProgressBar($tenant->allowed_beacons);

        $createdBeacons = 0;

        $amountOfBeacons = $amount == null ? $tenant->allowed_beacons : $amount;

        while($amountOfBeacons > $createdBeacons) {

            $beacon = $this->createNewBeacon($tenant,$batch);

            $beaconInfos[] = array(
                $beacon->name,
                $beacon->url
            );

            $createdBeacons++;
            if($fromCli) $bar->advance();

            BeaconService::addTenantUsersAndAdminsToBeacon($beacon->id,$beacon->tenant_id);

        }

        if($fromCli) $bar->finish();

        return $beaconInfos;

    }

    protected function createNewBeacon($tenant,$batch = null) {

        $beacon = new Beacon();
        $beacon->name = BeaconService::generateRandomString(config('substance.beacon_name_length'),true,"B-");
        $beacon->url = $this->newUniqueUrl();
        $beacon->url_hash = md5($beacon->url);
        $beacon->tenant_id = $tenant->id;

        if($tenant->kontakt_io_venue_id == null) {
            $beacon->substance_beacon = false;
        }

        if($batch != null) {
            $beacon->beacon_batch_id = $batch->id;
            if($batch->kontakt_io_venue_id == null) {
                $beacon->substance_beacon = false;
            }
        }

        $beacon->save();

        return $beacon;

    }

}
