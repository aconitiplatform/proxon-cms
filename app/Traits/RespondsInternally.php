<?php

namespace App\Traits;

trait RespondsInternally {

    protected function internalResponse($data, $isInternal = false, $meta = array(), $code = 200) {

        if($isInternal && $code >= 200 && $code <= 299) {
            return $data;
        } else if($isInternal) {
            return null;
        }

        $meta['count'] = count($data);
        $meta['info'] = trans('api.'.$code);

        if($code >= 200 && $code <= 299) {
            return response()->success($data,$code,$meta);
        } else {
            return response()->error($data,$code,$meta);
        }

    }

}
