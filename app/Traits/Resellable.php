<?php

namespace App\Traits;

use Auth;
use Cache;
use Log;

use App\Models\Reseller;
use App\Models\ResellerSetting;
use App\Models\Tenant;
use App\Models\User;
use App\Models\Billable;

use App\Http\Services\TenantService;

trait Resellable {

    public function updateConfigForResellerByUrl($url) {

        if($url != config('substance.url') || config('substance.owner') != 'default') {

            $url = str_replace("http:","https:",$url);

            $settings = ResellerSetting::where('substance_url',$url)->first();

            if($settings != null) {
                return $this->updateSubstanceConfig($settings);
            }

        }

    }

    public function updateConfigForReseller(Tenant $tenant) {

        if($tenant->reseller_id != null) {

            $reseller = Reseller::findOrFail($tenant->reseller_id);
            return $this->updateSubstanceConfig($reseller->settings);

        }

        return null;

    }

    public function updateConfigAsReseller(User $user) {

        if($user->role == 'reseller') {

            $reseller = Reseller::findOrFail($user->id);
            return $this->updateSubstanceConfig($reseller->settings);

        }

        return null;

    }

    public function endTrialWithWhitelabelSettingsAsReseller(Tenant $tenant) {

		if($tenant->trial && config('substance.whitelabel.active') && config('substance.whitelabel.allow_trials') == false) {

			$tenant->trial = false;
			$tenant->save();

			$billable = Billable::where('tenant_id',$tenant->id)->first();

			if($billable == null) {
				$billable = new Billable();
			}

			$billable->tenant_id = $tenant->id;

			$settings = $this->updateConfigAsReseller(Auth::user());

			$billable->billable_object_type = 'reseller';
			$billable->billable_object_id = $settings->reseller_id;
			$billable->easybill_customer_id = $settings->easybill_customer_id;

			$billable->price_per_beacon = config('substance.whitelabel.price_per_beacon');
			$billable->discount = config('substance.whitelabel.discount');

			$billable->save();

			$success = false;
			if(config('substance.reseller.by') && config('substance.whitelabel.bill_rest_of_month') == true && config('substance.whitelabel.bill_manually') == false) {
				$success = $this->createSingleResellerInvoice($settings->reseller_id,$tenant->id,$tenant->allowed_beacons);
			} else if (config('substance.whitelabel.bill_manually') == true) {
                $success = true;
            }

			if(!$success) {
                Log::info("no success");
				return false;
			}

		}

        return $tenant;

    }

    private function updateSubstanceConfig(ResellerSetting $settings) {

        config(['substance.reseller.by' => true]);

        $this->updateConfigKey('substance.kontakt_io.api_key',$settings->kontakt_io_api_key);
        $this->updateConfigKey('substance.url',$settings->substance_url);
        $this->updateConfigKey('substance.edward_stone_url',$settings->edward_stone_url);
        $this->updateConfigKey('substance.sidebar_product_logo',$settings->sidebar_product_logo);
        $this->updateConfigKey('substance.login_screen_product_logo',$settings->login_screen_product_logo);
        $this->updateConfigKey('substance.login_screen_company_logo',$settings->login_screen_company_logo);
        $this->updateConfigKey('substance.login_screen_image',$settings->login_screen_image);
        $this->updateConfigKey('substance.name',$settings->product_name);
        $this->updateConfigKey('substance.company_name',$settings->company_name);
        $this->updateConfigKey('substance.public.url',$settings->substance_url);
        $this->updateConfigKey('substance.public.edward_stone_url',$settings->edward_stone_url);
        $this->updateConfigKey('substance.public.sidebar_product_logo',$settings->sidebar_product_logo);
        $this->updateConfigKey('substance.public.login_screen_product_logo',$settings->login_screen_product_logo);
        $this->updateConfigKey('substance.public.login_screen_company_logo',$settings->login_screen_company_logo);
        $this->updateConfigKey('substance.public.login_screen_image',$settings->login_screen_image);
        $this->updateConfigKey('substance.public.name',$settings->product_name);
        $this->updateConfigKey('substance.public.company_name',$settings->company_name);
        $this->updateConfigKey('substance.mail_address',$settings->mail_address);
        $this->updateConfigKey('mail.host',$settings->mail_host);
        $this->updateConfigKey('mail.port',$settings->mail_port);
        $this->updateConfigKey('mail.username',$settings->mail_username);
        $this->updateConfigKey('mail.password',$settings->mail_password);
        $this->updateConfigKey('mail.encryption',$settings->mail_encryption);

        return $settings;

    }

    private function updateConfigKey($key,$value) {

        if($value != null && $value != "") {
            config([$key => $value]);
        }

    }

}
