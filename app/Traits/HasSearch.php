<?php

namespace App\Traits;

use Illuminate\Http\Request;

use Auth;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Reseller;
use App\Models\App;

use App\Http\Services\PoiService;
use App\Http\Services\BeaconService;

use App\Traits\ScopesRequests;

trait HasSearch {

    use ScopesRequests;

    public function search(Request $request, $isInternal = false) {

        if($request->has('q') && $request->input('q')!='') {

            $entities = $this->getEntityCollection($request->input('q'));

            $meta['q'] = $request->input('q');
            $meta['count'] = $entities->count();

            return response()->data($entities, 200, $isInternal, $meta);

        }

        return response()->data([], 400, $isInternal);

    }

    private function getEntityCollection($search) {

        switch (__CLASS__) {
            case "App\Http\Controllers\MediaController":
                return $this->searchMedia($search);
            case "App\Http\Controllers\PoiController":
                return $this->searchPois($search);
            case "App\Http\Controllers\BeaconController":
                return $this->searchBeacons($search);
            case "App\Http\Controllers\MicroPaymentController":
                return $this->searchMicroPayments($search);
            case "App\Http\Controllers\NewsletterConfigurationController":
                return $this->searchNewsletterConfigurations($search);
            case "App\Http\Controllers\SurveyController":
                return $this->searchSurveys($search);
            case "App\Http\Controllers\UserController":
                return $this->searchUsers($search);
            case "App\Http\Controllers\AppController":
                return $this->searchApps($search);
            case "App\Http\Controllers\TenantController":
                return $this->searchTenants($search);
            default:
                throw new \Exception(trans('api.UNSEARCHABLE_ENTITY'),422);
        }

    }

    private function searchMedia($search) {

        $collection = Auth::user()->media()->where('name', 'LIKE', '%'.$search.'%')->get();
        $collection = $collection->each(function ($item, $key) {
            $item->pois;
        });
        return $collection;

    }

    private function searchPois($search) {

        $collection = Auth::user()->pois()->where('name', 'LIKE', '%'.$search.'%')->get();
        $collection = $collection->each(function ($item, $key) {
            $item = PoiService::addAllRelationsToPoi($item);
        });
        return $collection;

    }

    private function searchBeacons($search) {

        $collection = Auth::user()->beacons()->where('name', 'LIKE', '%'.$search.'%')->orWhere('alias', 'LIKE', '%'.$search.'%')->get();
        $collection = $collection->unique();
        $collection->each(function($item,$key) {
            $item = BeaconService::addAllRelationsToBeacon($item);
        });
        return $collection;

    }

    private function searchMicroPayments($search) {

        $collection = Auth::user()->microPayments()->where('name', 'LIKE', '%'.$search.'%')->get();
        return $collection;

    }

    private function searchNewsletterConfigurations($search) {

        $collection = Auth::user()->newsletterConfigurations()->where('name', 'LIKE', '%'.$search.'%')->get();
        return $collection;

    }

    private function searchSurveys($search) {

        $collection = Auth::user()->surveys()->where('name', 'LIKE', '%'.$search.'%')->get();
        return $collection;

    }

    private function searchUsers($search) {

        $this->scopeRequest();
        $collection = User::where('role','!=','app')->where('firstname', 'LIKE', '%'.$search.'%')->orWhere('role','!=','app')->where('lastname', 'LIKE', '%'.$search.'%')->get();
        return $collection;

    }

    private function searchApps($search) {

        $this->scopeRequest();
        $collection = App::where('role','=','app')->where('firstname', 'LIKE', '%'.$search.'%')->get();
        return $collection;

    }

    public function searchTenants($search) {
        if(Auth::user()->role == 'superadmin') {
            return Tenant::where('name', 'LIKE', '%'.$search.'%')->get();
        } else if (Auth::user()->role == 'reseller') {
            return Tenant::where('reseller_id',Auth::user()->id)->where('name', 'LIKE', '%'.$search.'%')->get();
        } else {
            return collect([]);
        }
    }

}
