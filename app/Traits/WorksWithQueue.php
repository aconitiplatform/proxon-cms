<?php

namespace App\Traits;

use App;

trait WorksWithQueue {

    public function dispatchJob($job,$queue = null) {

        if(App::environment('local') || $queue === null) {
            dispatch($job);
        } else {
            dispatch($job->onQueue(config('substance.queue_prefix').$queue));
        }

    }

    public static function dispatchJobStatically($job,$queue = null) {

        if(App::environment('local') || $queue === null) {
            dispatch($job);
        } else {
            dispatch($job->onQueue(config('substance.queue_prefix').$queue));
        }

    }

}
