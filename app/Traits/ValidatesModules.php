<?php

namespace App\Traits;


trait ValidatesModules {

    public static function getPoiContentModuleValidationRulesByType($type) {

        switch ($type) {
            case 'text':
                return self::getTextPoiContentModuleValidationRules();
            case 'heading':
                return self::getHeadingPoiContentModuleValidationRules();
            case 'image':
                return self::getImagePoiContentModuleValidationRules();
            case 'video':
                return self::getVideoPoiContentModuleValidationRules();
            case 'audio':
                return self::getAudioPoiContentModuleValidationRules();
            case 'wikipedia':
                return self::getWikipediaPoiContentModuleValidationRules();
            case 'button':
                return self::getButtonPoiContentModuleValidationRules();
            case 'survey':
                return self::getSurveyPoiContentModuleValidationRules();
            case 'newsletter':
                return self::getNewsletterConfigurationPoiContentModuleValidationRules();
            case 'divider':
                return self::getDividerPoiContentModuleValidationRules();
			case 'social_icons':
				return self::getSocialIconsPoiContentModuleValidationRules();
            case 'feedback':
				return self::getFeedbackPoiContentModuleValidationRules();
            case 'micro_payment':
				return self::getMicroPaymentPoiContentModuleValidationRules();
            case 'contact':
				return self::getContactPoiContentModuleValidationRules();
            case 'sweepstake':
				return self::getSweepstakePoiContentModuleValidationRules();
			case 'icon':
				return self::getIconPoiContentModuleValidationRules();
            case 'spotify':
                return self::getSpotifyPoiContentModuleValidationRules();
			case 'voucher':
				return self::getVoucherPoiContentModuleValidationRules();
			case 'image_gallery':
				return self::getImageGalleryPoiContentModuleValidationRules();
            case 'vag':
                return self::getVagPoiContentModuleValidationRules();
            default:
                throw new \Exception(trans('api.UNSUPPORTED_MODULE'),422);
        }

    }

    protected static function getTextPoiContentModuleValidationRules() {
        return [
            'text'=>'required|string|min:1',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getHeadingPoiContentModuleValidationRules() {
        return [
            'size'=>'required|integer|in:1,2,3',
            'align'=>'required|string|in:left,center,right',
            'text'=>'required|string|min:1',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getImagePoiContentModuleValidationRules() {
        return [
            'type'=>'required|in:id,url',
            'url'=>'required_if:type,url|url',
            'alt'=>'sometimes|string',
            'caption'=>'sometimes|string',
			'id'=>'required_if:type,id|string|size:36',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getVideoPoiContentModuleValidationRules() {
        return [
            'type'=>'required|in:html5,youtube,vimeo',
            'webm_type' => 'required_if:type,html5|in:id,url',
            'mp4_type' => 'required_if:type,html5|in:id,url',
            'webm_id'=>'required_if:webm_type,id|string|size:36|exists:media,id',
            'mp4_id'=>'required_if:mp4_type,id|string|size:36|exists:media,id',
            'webm_url'=>'required_if:webm_type,url|url',
            'mp4_url'=>'required_if:mp4_type,url|url',
            'url'=>'required_if:type,youtube,vimeo|url',
            'id'=>'sometimes|required_if:type,youtube,vimeo|string',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getAudioPoiContentModuleValidationRules() {
        return [
            'type'=>'required|in:media,external',
            'id'=>'required_if:type,media|string|size:36|exists:media,id',
            'url'=>'required_if:type,external|url',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getWikipediaPoiContentModuleValidationRules() {
        return [
            'url'=>'required|url',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getButtonPoiContentModuleValidationRules() {
        return [
            'type'=>'required|in:mail,web,phone',
            'target'=>'required|string|max:512',
			'text'=>'required|string|max:100',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getNewsletterConfigurationPoiContentModuleValidationRules() {
        return [
            'id'=>'required|size:36|exists:newsletter_configurations,id',
            'text'=>'required|string|max:200',
            'with_name'=>'required|boolean',
            'with_captcha'=>'required|boolean',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
			'placeholder'=>'sometimes|string|max:100',
			'button'=>'sometimes|string|max:100',
			'message'=>'sometimes|string|max:512',
        ];
    }

    protected static function getSurveyPoiContentModuleValidationRules() {
        return [
            'id'=>'required|size:36|exists:surveys,id',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getDividerPoiContentModuleValidationRules() {
        return [
            'line'=>'required|boolean',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

	protected static function getSocialIconsPoiContentModuleValidationRules() {
        return [
            'facebook'=>'sometimes|url|max:255',
			'twitter'=>'sometimes|url|max:255',
			'instagram'=>'sometimes|url|max:255',
			'youtube'=>'sometimes|url|max:255',
			'xing'=>'sometimes|url|max:255',
			'linkedin'=>'sometimes|url|max:255',
			'google_plus'=>'sometimes|url|max:255',
			'pinterest'=>'sometimes|url|max:255',
			'tumblr'=>'sometimes|url|max:255',
			'reddit'=>'sometimes|url|max:255',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getFeedbackPoiContentModuleValidationRules() {
        return [
            'uuid'=>'sometimes|string|size:36',
            'text'=>'required|string|min:3|max:1000',
            'positive_label'=>'required|string|min:1|max:50',
            'negative_label'=>'required|string|min:1|max:50',
            'icons'=>'required|string|in:thumbs,smileys',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getMicroPaymentPoiContentModuleValidationRules() {
        return [
            'name'=>'required|string|min:1|max:128',
            'description'=>'required|string|min:3|max:1000',
            'micro_payment_id'=>'required|string|size:36|exists:micro_payments,id',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getContactPoiContentModuleValidationRules() {
        return [
            'placeholder_name'=>'required|string|min:1|max:128',
            'placeholder_email'=>'required|string|min:1|max:128',
            'placeholder_text'=>'required|string|min:1|max:128',
            'with_captcha'=>'required|boolean',
            'recipient' => 'required|email|max:128',
            'button_label'=>'required|string|min:1|max:128',
            'success_label'=>'required|string|min:1|max:128',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getSweepstakePoiContentModuleValidationRules() {
        return [
            'uuid'=>'sometimes|string|size:36',
            'placeholder_firstname'=>'required_if:with_firstname,true|string|min:1|max:256',
            'placeholder_lastname'=>'required_if:with_lastname,true|string|min:1|max:256',
            'placeholder_street'=>'required_if:with_street,true|string|min:1|max:256',
            'placeholder_zip'=>'required_if:with_zip,true|string|min:1|max:256',
            'placeholder_city'=>'required_if:with_city,true|string|min:1|max:256',
            'placeholder_country'=>'required_if:with_country,true|string|min:1|max:256',
            'with_firstname'=>'sometimes|boolean',
            'with_lastname'=>'sometimes|boolean',
            'with_street'=>'sometimes|boolean',
            'with_zip'=>'sometimes|boolean',
            'with_city'=>'sometimes|boolean',
            'with_country'=>'sometimes|boolean',
            'required_firstname'=>'sometimes|boolean',
            'required_lastname'=>'sometimes|boolean',
            'required_street'=>'sometimes|boolean',
            'required_zip'=>'sometimes|boolean',
            'required_city'=>'sometimes|boolean',
            'required_country'=>'sometimes|boolean',
            'placeholder_email'=>'required|string|min:1|max:256',
            'question'=>'required_unless:answer,none|string|min:1|max:4000',
            'answer'=>'required|string|in:none,text,choice',
            // 'choices'=>'required_if:answer,choice|string',
            // 'choices.*'=>'sometimes|string|min:1',
            'placeholder_text'=>'required_unless:answer,none|string|min:1|max:4000',
			'with_terms'=>'required|boolean',
			'terms_hint'=>'sometimes|string|max:512',
			'terms_text'=>'required_if:with_terms,true|string',
            'store_participation'=>'required|boolean',
            'with_captcha'=>'required|boolean',
            'recipient' => 'required|email',
            'button_label'=>'required|string|min:1|max:128',
            'success_label'=>'required|string|min:1|max:4000',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

	protected static function getIconPoiContentModuleValidationRules() {
        return [
            'icon'=>'required|string|min:1',
			'icon_set'=>'required|string|min:1',
			'link'=>'sometimes|url|max:255',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getSpotifyPoiContentModuleValidationRules() {
        return [
            'uri'=>'required|string|min:1|max:255',
			'theme'=>'required|in:white,dark',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

	protected static function getVoucherPoiContentModuleValidationRules() {
        return [
            'uuid'=>'sometimes|string|size:36',
            'duration'=>'required|integer|min:1',
			'description'=>'sometimes|string|min:1|max:4096',
			'terms'=>'sometimes|string|min:1',
			'image_type'=>'sometimes|in:url,id',
			'image_id'=>'required_if:image_type,id|string|size:36|exists:media,id',
			'image_url'=>'required_if:image_type,url|string|min:1|max:1024',
            'type'=>'required|in:barcode,text',
            'text_content'=>'required_if:type,text|string|min:1|max:128',
            'bc_type'=>'required_if:type,barcode|in:qrcode,code128,code39',
            'bc_content'=>'required_if:type,barcode|string|min:1|max:128',
            'voucher_image_id'=>'required|string|size:36',
            'css'=>'sometimes|array',
            'voucher_font_color' => 'sometimes|string|min:1|max:7',
            'voucher_background_color' => 'sometimes|string|min:1|max:7',
            'voucher_code_background_color' => 'sometimes|string|min:1|max:7',
			'download_button_text'=>'sometimes|string|min:1|max:128',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
			'store_participation' => 'sometimes|boolean'
        ];
    }

	private static function getImageGalleryPoiContentModuleValidationRules() {
        return [
            'images'=>'required|image_gallery',
			'type'=>'required|in:slider,classic,grid',
			'columns'=>'sometimes|in:2,3,4,5',
			'show_caption' => 'sometimes|boolean',
            'css'=>'sometimes|array',
            'html_id' => 'sometimes|string|min:1|max:100',
            'html_classes' => 'sometimes|string|min:1|max:255',
        ];
    }

    protected static function getVagPoiContentModuleValidationRules() {
        return [
            'stop_id'=>'required|integer|min:1|max:999999',
			'stop_name'=>'sometimes|string|min:1|max:100'
        ];
    }

}
