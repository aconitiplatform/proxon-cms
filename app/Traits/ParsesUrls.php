<?php

namespace App\Traits;

use Opengraph;

trait ParsesUrls {

    private $requiredOpenGraphFields = [
        'og:url' => 'url',
        'og:title' => 'title',
        'og:description' => 'description',
        'og:image' => 'images',
    ];

    private $openGraphTags = [];
    private $metaTags = [];

    private $parsedFields = [
        'title' => null,
        'description' => null,
        'images' => null,
        'url' => null,
    ];

    private $missingFields = [];

    private function parseUrl($url,$language) {

        $this->getFieldsFromOpenGraph($url,$language);
        $this->getFieldsFromMetaTags($url,$language);
        $this->getUrlFromRedirectTarget($url);
        return $this->parsedFields;

    }

    private function getFieldsFromOpenGraph($url,$language) {

        $this->setOpenGraphTags($url,$language);
        $nonExistingFields = [];

        foreach($this->requiredOpenGraphFields as $requiredOpenGraphField => $parsedField) {

            if(!array_key_exists($requiredOpenGraphField,$this->openGraphTags)) {
                $this->missingFields[] = $parsedField;
            } else {
                $this->parsedFields[$parsedField] =  $this->openGraphTags[$requiredOpenGraphField];
            }

        }

    }

    private function getFieldsFromMetaTags($url,$language) {

        if(!empty($this->missingFields)) {

            $this->setMetaTags($url,$language);

            foreach($this->parsedFields as $key => $value) {

                if($value == null && isset($this->metaTags['meta_tags'][$key])) {
                    $this->parsedFields[$key] = $this->metaTags['meta_tags'][$key]['value'];
                }

            }

            if($this->parsedFields['title'] == null) {
                $this->parsedFields['title'] = $this->metaTags['title'];
            }

        }

    }

    private function getUrlFromRedirectTarget($url) {

        if($this->parsedFields['url'] == null) {
            $this->parsedFields['url'] = $this->getRedirectTarget($url);
        }

    }

    private function setOpenGraphTags($url,$language) {

        $reader = new Opengraph\Reader();
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: ".$language."\r\n"
            ]
        ];
        $context = stream_context_create($opts);
        $reader->parse(file_get_contents($url,false,$context));
        $this->openGraphTags = $reader->getArrayCopy();

    }

    private function setMetaTags($url,$language) {

        $this->metaTags = $this->getUrlData($url,$language,false);

    }

    function getRedirectTarget($url) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow redirects
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // set referer on redirect
        curl_exec($ch);
        $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        if ($target)
            return $target;
        return false;

    }

    // taken from: http://php.net/manual/de/function.get-meta-tags.php
    private function getUrlData($url, $language, $raw=false) { // $raw - enable for raw display

        $result = false;

        $contents = $this->getUrlContents($url,$language);

        if (isset($contents) && is_string($contents)) {

            $title = null;
            $metaTags = null;
            $metaProperties = null;

            preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );

            if (isset($match) && is_array($match) && count($match) > 0) {
                $title = strip_tags($match[1]);
            }

            preg_match_all('/<[\s]*meta[\s]*(name|property)="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);

            if (isset($match) && is_array($match) && count($match) == 4) {

                $originals = $match[0];
                $names = $match[2];
                $values = $match[3];

                if (count($originals) == count($names) && count($names) == count($values)) {

                    $metaTags = array();
                    $metaProperties = $metaTags;

                    if ($raw) {
                        if (version_compare(PHP_VERSION, '5.4.0') == -1) {
                            $flags = ENT_COMPAT;
                        } else {
                            $flags = ENT_COMPAT | ENT_HTML401;
                        }
                    }

                    for ($i=0, $limiti=count($names); $i < $limiti; $i++) {

                        if ($match[1][$i] == 'name')
                             $meta_type = 'metaTags';
                        else {
                            $meta_type = 'metaProperties';
                        }

                        if ($raw) {
                            ${$meta_type}[$names[$i]] = array (
                                'html' => htmlentities($originals[$i], $flags, 'UTF-8'),
                                'value' => $values[$i]
                            );
                        } else {
                            ${$meta_type}[$names[$i]] = array (
                                'html' => $originals[$i],
                                'value' => $values[$i]
                            );
                        }
                    }
                }
            }

            $result = array (
                'title' => $title,
                'meta_tags' => $metaTags,
                'meta_properties' => $metaProperties,
            );

        }

        return $result;

    }

    private function getUrlContents($url, $language, $maximumRedirections = null, $currentRedirection = 0) {

        $result = false;

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Accept-language: ".$language."\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        $contents = @file_get_contents($url,false,$context);

        // Check if we need to go somewhere else

        if (isset($contents) && is_string($contents)) {

            preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

            if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1) {

                if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections) {
                    return $this->getUrlContents($match[1][0], $language, $maximumRedirections, ++$currentRedirection);
                }

                $result = false;

            } else {
                $result = $contents;
            }

        }

        return $contents;

    }

}
