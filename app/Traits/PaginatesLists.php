<?php

namespace App\Traits;

use Auth;

use App\Models\Media;
use App\Models\Poi;
use App\Models\Beacon;
use App\Models\MicroPayment;
use App\Models\NewsletterConfiguration;
use App\Models\Survey;
use App\Models\User;
use App\Models\Tenant;
use App\Models\App;
use App\Models\Invoice;
use App\Models\PoiEvent;

use App\Http\Services\Service;

trait PaginatesLists {

    private $listLength = null;
    private $paginatedListLength = null;
    private $nextSkip = null;
    private $previousSkip = null;
    private $isDelta = true;
    private $lastModified;
    private $since = null;

    protected function paginate($list,$skip = null,$take = null,$since = null) {

        $this->lastModified = Service::getLastModified(Auth::user()->tenant_id,$this->getModelInstance());

        if($since == null) {
            $this->since = $this->lastModified['updated_at'];
        } else {
            $this->since = $since;
        }

        $this->listLength = count($list);

        if($skip == null) return $list;

        if($take == null) $take = config('substance.page_size');

        if($skip >= $this->listLength || $take <= 0 || $skip < 0) return [];

        if($this->since < $this->lastModified['updated_at']) {

            if($skip > 0) {
                $take = $skip + $take;
                $this->isDelta = false;
            }

            $skip = 0;

        }

        if($skip + $take > $this->listLength) {
            $numberOfTakableListItems = $this->listLength - $skip;
        } else {
            $numberOfTakableListItems = $take;
            $this->nextSkip = $skip + $take;
        }

        if($skip > 0 && $skip <= $take) {
            $this->previousSkip = 0;
        } else if ($skip > $take) {
            $this->previousSkip = $skip - $take;
        }

        for($i = $skip; $i < $numberOfTakableListItems + $skip; $i++) {

            $paginatedList[] = $list[$i];

        }

        $this->paginatedListLength = count($paginatedList);

        return $paginatedList;

    }

    protected function getListMetaInformation() {

        return [
            'actual_length' => $this->listLength,
            'paginated_length' => $this->paginatedListLength,
            'next_skip' => $this->nextSkip,
            'previous_skip' => $this->previousSkip,
            'is_delta' => $this->isDelta,
            'last_modified' => $this->lastModified['updated_at'],
            'since' => (int)$this->since,
        ];

    }

    private function getModelInstance() {

        switch (__CLASS__) {
            case "App\Http\Controllers\MediaController":
                return new Media();
            case "App\Http\Controllers\PoiController":
                return new Poi();
            case "App\Http\Controllers\BeaconController":
                return new Beacon();
            case "App\Http\Controllers\MicroPaymentController":
                return new MicroPayment();
            case "App\Http\Controllers\NewsletterConfigurationController":
                return new NewsletterConfiguration();
            case "App\Http\Controllers\SurveyController":
                return new Survey();
            case "App\Http\Controllers\UserController":
                return new User();
            case "App\Http\Controllers\AppController":
                return new App();
            case "App\Http\Controllers\TenantController":
                return new Tenant();
            case "App\Http\Controllers\InvoiceController":
                return new Invoice();
            case "App\Http\Controllers\PoiEventController":
                return new PoiEvent();
            default:
                throw new \Exception(trans('api.UNPAGINATABLE_ENTITY'),422);
        }

    }

}
