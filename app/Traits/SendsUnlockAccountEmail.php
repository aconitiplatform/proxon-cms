<?php

namespace App\Traits;

use App;
use Cache;

use App\Models\PasswordReset;

use App\Jobs\UnlockAccountEmailJob;

use App\Traits\WorksWithQueue;

trait SendsUnlockAccountEmail {

	protected function sendResetToken($email, $resend = false) {

        $cacheKey = 'unlock-emails:'.$email;

        if(!Cache::has($cacheKey) || $resend) {

            $reset = PasswordReset::create([
                'email' => $email,
                'token' => str_random(10),
            ]);

            $token = $reset->token;

            $this->dispatchJob(new UnlockAccountEmailJob(compact('email', 'token')));

            Cache::forever($cacheKey,time());

        }

    }

}
