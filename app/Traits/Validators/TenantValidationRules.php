<?php

namespace App\Traits\Validators;

use Validator;

use App\Http\Services\Service;

trait TenantValidationRules {

    public function getStoreTenantValidationRules($isRegistration = false) {

        if($isRegistration) {
            return [
                'tenant' => 'required',
                'user' => 'required|new_tenant_admin_with_password',
            ];
        }

        return [
            'tenant' => 'required|new_tenant',
            'user' => 'required|new_tenant_admin',
        ];

    }

    public function getRegisterTenantValidationRules() {

        return [
            'tenant' => 'required|register_tenant',
            'user' => 'required|new_tenant_admin_with_password',
        ];

    }

    public function getUpdateTenantValidationRules() {
        return [
            'name'    => 'sometimes|string',
            'url' => 'sometimes|url',
            'css' => 'sometimes|url',
            'logo' => 'sometimes|url',
            'imprint' => 'sometimes|string',
            'email' => 'sometimes|email',
            'fallback_language' => 'sometimes|string|min:2|max:5',
            'locale' => 'sometimes|string|min:2|max:10',
            'ga_tracking_id' => 'sometimes|string',
            'ga_utm_source' => 'sometimes|string',
            'substance_url' => 'sometimes|url',
            'edward_stone_url' => 'sometimes|url',
            'logo_settings' => 'sometimes|array',
            'privacy' => 'sometimes|string',
            'copyright_label' => 'sometimes|string',
        ];
    }

    public function getAddBeaconsToTenantValidationRules() {
        return [
            'tenant_id' => 'required|string|exists:tenants,id',
            'amount' => 'required|integer',
            'kontakt_io' => 'required|boolean',
        ];
    }

    protected function bootRegisterTenantValidation() {

        Validator::extend('register_tenant', function($attribute, $value, $parameters, $validator) {

            $rules = [
                'name' => 'required|string|max:255',
                'fallback_language' => 'required|string|min:2|max:5',
                'locale' => 'required|string|min:2|max:10',
            ];

            Service::validateArray($value,$rules);

            return true;

        });

    }

    protected function bootNewTenantValidation() {

        Validator::extend('new_tenant', function($attribute, $value, $parameters, $validator) {

            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|email',
                'fallback_language' => 'required|string|min:2|max:5',
                'locale' => 'required|string|min:2|max:10',
                'allowed_beacons' => 'required|integer|min:1',
                'kontakt_io' => 'required|boolean',
                'substance_url' => 'sometimes|url',
                'edward_stone_url' => 'sometimes|url'
            ];

            Service::validateArray($value,$rules);

            return true;

        });

    }

    protected function bootNewTenantAdminValidation() {

        Validator::extend('new_tenant_admin', function($attribute, $value, $parameters, $validator) {

            $rules = [
                'firstname' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'email' => 'required|email|unique:users,email',
            ];

            Service::validateArray($value,$rules);

            return true;

        });

    }

    protected function bootNewTenantAdminWithPasswordValidation() {

        Validator::extend('new_tenant_admin_with_password', function($attribute, $value, $parameters, $validator) {

            $rules = [
                'firstname' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|string|min:8'
            ];

            Service::validateArray($value,$rules);

            return true;

        });

    }

}
