<?php

namespace App\Traits\Validators;

use Validator;

use App\Http\Services\Service;

trait CartValidationRules {

    public function getCartValidationRules() {

        // return [
        //     'number' => 'required|size:16',
        //     'exp_month' => 'required|integer|between:1,12',
        //     'exp_year' => 'required|integer|min:2017',
        //     'cvc' => 'required|integer',
        //     'plan' => 'required|in:troposphere,stratosphere,mesosphere,thermosphere'
        // ];

        return [
            'stripe_token_id' => 'required|string',
            'cart' => 'required|array',
            'cart.*.name' => 'required|string',
            'cart.*.url' => 'required|string',
            'cart.*.hardware' => 'required|in:beacon,beacon_pro,nfc_tag,shipping_cost',
            'cart.*.price' => 'required|numeric',
            'address_name' => 'required|string',
            'address_optional' => 'sometimes|string',
            'address_street' => 'required|string',
            'address_zip' => 'required|string',
            'address_city' => 'required|string',
            'address_country' => 'required|string',
        ];

    }

}
