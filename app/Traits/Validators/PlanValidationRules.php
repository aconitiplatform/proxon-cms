<?php

namespace App\Traits\Validators;

use Validator;

use App\Http\Services\Service;

trait PlanValidationRules {

    public function getSubscribeValidationRules() {

        // return [
        //     'number' => 'required|size:16',
        //     'exp_month' => 'required|integer|between:1,12',
        //     'exp_year' => 'required|integer|min:2017',
        //     'cvc' => 'required|integer',
        //     'plan' => 'required|in:troposphere,stratosphere,mesosphere,thermosphere'
        // ];

        return [
            'stripe_token_id' => 'required|string',
            'plan' => 'required|in:startrampe,troposphere,stratosphere,mesosphere,thermosphere'
        ];

    }

    public function getUpdateCardValidationRules() {

        return [
            'stripe_token_id' => 'required|string',
        ];

    }

    public function getUpdateInvoiveInformationValidationRules() {

        return [
            'invoice_information' => 'required|string',
        ];

    }

}
