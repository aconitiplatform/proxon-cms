<?php

namespace App\Traits\Validators;

trait SdkValidationRules {

    public function getConnectContentToBeaconValidationRules() {

        return [
            'beacon_id' => 'required|string|size:36',
            'url' => 'required|url',
            'title' => 'sometimes|string|min:1|max:40',
            'description' => 'sometimes|string|min:1|max:255',
        ];

    }

    public function getDisconnectContentFromBeaconValidationRules() {

        return [
            'beacon_id' => 'required|string|size:36',
        ];

    }

}
