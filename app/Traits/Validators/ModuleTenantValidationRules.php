<?php

namespace App\Traits\Validators;

trait ModuleTenantValidationRules {

    public function getStoreModuleTenantValidationRules() {

        return [
            'tenant_id' => 'required|string|size:36|exists:tenants,id',
            'modules' => 'required|array',
        ];

    }

    public function getDestroyModuleTenantValidationRules() {

        return [
            'tenant_id' => 'required|string|size:36|exists:tenants,id',
            'modules' => 'required|array',
        ];

    }

}
