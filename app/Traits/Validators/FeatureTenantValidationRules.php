<?php

namespace App\Traits\Validators;

trait FeatureTenantValidationRules {

    public function getStoreFeatureTenantValidationRules() {

        return [
            'tenant_id' => 'required|string|size:36|exists:tenants,id',
            'features' => 'required|array',
        ];

    }

    public function getDestroyFeatureTenantValidationRules() {

        return [
            'tenant_id' => 'required|string|size:36|exists:tenants,id',
            'features' => 'required|array',
        ];

    }

}
