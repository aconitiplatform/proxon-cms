<?php

namespace App\Traits\Validators;

trait EmailConfirmationValidationRules {

    public function getConfirmEmailValidationRules() {

        return [
            'token' => 'required|string|exists:email_confirmations,token',
        ];

    }

    public function getResendEmailConfirmationValidationRules() {

        return [
            'email' => 'required|email|exists:users,email',
        ];

    }

}
