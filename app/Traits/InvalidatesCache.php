<?php

namespace App\Traits;

use App\Http\Services\Service;

use Auth;

use App\Jobs\InvalidateCacheJob;

trait InvalidatesCache {

    protected function invalidateAndModify() {

        $params = func_get_args();

        foreach($params as $invAndModEntity) {

            if(isset($invAndModEntity['id']) && isset($invAndModEntity['key'])) {
                $this->invalidate($invAndModEntity['id'],Auth::user()->tenant_id,Auth::user()->id,$invAndModEntity['key']);
            }

            if(isset($invAndModEntity['object'])) {
                Service::updateLastModified(Auth::user()->tenant_id,$invAndModEntity['object']);
            }

            if(isset($invAndModEntity['tenant_id'])) {

                if(isset($invAndModEntity['object'])) {
                    Service::updateLastModified($invAndModEntity['tenant_id'],$invAndModEntity['object']);
                }

                if(isset($invAndModEntity['id']) && isset($invAndModEntity['key'])) {
                    $this->invalidate($invAndModEntity['id'],$invAndModEntity['tenant_id'],Auth::user()->id,$invAndModEntity['key']);
                }

            }

        }

    }

    protected function invalidate($entityId,$tenantId,$userId,$entity) {

        $this->dispatchJob(new InvalidateCacheJob($entityId,$tenantId,$entity));
        Service::invalidateCache($entityId,$tenantId,$userId,$entity);

    }

}
