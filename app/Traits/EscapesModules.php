<?php

namespace App\Traits;


trait EscapesModules {

    public static function escapePoiContentModuleStringsByType($type,$moduleData) {

        switch ($type) {
            case 'text':
                $fields = self::getEscapeTextPoiContentModuleData();
                break;
            case 'heading':
                $fields = self::getEscapeHeadingPoiContentModuleData();
                break;
            case 'image':
                $fields = self::getEscapeImagePoiContentModuleData();
                break;
            case 'video':
                $fields = self::getEscapeVideoPoiContentModuleData();
                break;
            case 'audio':
                $fields = self::getEscapeAudioPoiContentModuleData();
                break;
            case 'wikipedia':
                $fields = self::getEscapeWikipediaPoiContentModuleData();
                break;
            case 'button':
                $fields = self::getEscapeButtonPoiContentModuleData();
                break;
            case 'survey':
                $fields = self::getEscapeSurveyPoiContentModuleData();
                break;
            case 'newsletter':
                $fields = self::getEscapeNewsletterConfigurationPoiContentModuleData();
                break;
            case 'divider':
                $fields = self::getEscapeDividerPoiContentModuleData();
                break;
			case 'social_icons':
				$fields = self::getEscapeSocialIconsPoiContentModuleData();
                break;
            case 'feedback':
				$fields = self::getEscapeFeedbackPoiContentModuleData();
                break;
            case 'micro_payment':
				$fields = self::getEscapeMicroPaymentPoiContentModuleData();
                break;
            case 'contact':
				$fields = self::getEscapeContactPoiContentModuleData();
                break;
            case 'sweepstake':
				$fields = self::getEscapeSweepstakePoiContentModuleData();
                break;
			case 'icon':
				$fields = self::getEscapeIconPoiContentModuleData();
                break;
            case 'spotify':
                $fields = self::getEscapeSpotifyPoiContentModuleData();
                break;
            case 'voucher':
                $fields = self::getEscapeVoucherPoiContentModuleData();
                break;
            case 'image_gallery':
                $fields = self::getEscapeImageGalleryPoiContentModuleData();
                break;
			case 'vag':
                $fields = self::getEscapeVagPoiContentModuleData();
                break;
            default:
                throw new \Exception(trans('api.UNSUPPORTED_MODULE'),422);
        }

        return self::escape($moduleData,$fields);

    }

    private static function escape($moduleData,$fields) {

        foreach($fields as $field) {

            if(isset($moduleData[$field])) {
                $moduleData[$field] = str_replace('&#34;','"',$moduleData[$field]);
                // $moduleData[$field] = str_replace('"','\"',$moduleData[$field]);
            }

        }

        return $moduleData;

    }

    protected static function getEscapeTextPoiContentModuleData() {
        return ['text'];
    }

    protected static function getEscapeHeadingPoiContentModuleData() {
        return ['text'];
    }

    protected static function getEscapeImagePoiContentModuleData() {
        return ['alt','caption'];
    }

    protected static function getEscapeVideoPoiContentModuleData() {
        return [];
    }

    protected static function getEscapeAudioPoiContentModuleData() {
        return [];
    }

    protected static function getEscapeWikipediaPoiContentModuleData() {
        return [];
    }

    protected static function getEscapeButtonPoiContentModuleData() {
        return ['text','target'];
    }

    protected static function getEscapeNewsletterConfigurationPoiContentModuleData() {
        return ['text','placeholder','button','message'];
    }

    protected static function getEscapeSurveyPoiContentModuleData() {
        return [];
    }

    protected static function getEscapeDividerPoiContentModuleData() {
        return [];
    }

	protected static function getEscapeSocialIconsPoiContentModuleData() {
        return [];
    }

    protected static function getEscapeFeedbackPoiContentModuleData() {
        return ['text','positive_label','negative_label'];
    }

    protected static function getEscapeMicroPaymentPoiContentModuleData() {
        return ['name','description'];
    }

    protected static function getEscapeContactPoiContentModuleData() {
        return ['placeholder_name','placeholder_email','placeholder_text','button_label','success_label'];
    }

    protected static function getEscapeSweepstakePoiContentModuleData() {

        return [
            'placeholder_firstname','placeholder_lastname','placeholder_street','placeholder_zip','placeholder_city','placeholder_country',
            'placeholder_text','question','answer','button_label','success_label'
        ];

    }

	protected static function getEscapeIconPoiContentModuleData() {
        return ['icon','icon_set'];
    }

    protected static function getEscapeSpotifyPoiContentModuleData() {
        return [];
    }

	protected static function getEscapeVoucherPoiContentModuleData() {
        return ['description','terms','text_content'];
    }

    protected static function getEscapeImageGalleryPoiContentModuleData() {
        return [];
    }

	protected static function getEscapeVagPoiContentModuleData() {
        return ['stop_name'];
    }

}
