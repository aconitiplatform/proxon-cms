<?php

namespace App\Traits;

use Illuminate\Http\Request;

use Firebase;
use Log;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\ServiceAccount\Discoverer;
use App\Singletons\FirebaseServiceAccountFactory;

trait GetsDataFromFirebase {

	protected function getFirebaseList(Request $request,$resource,$withStartAndEnd=true) {

		$firebase = $this->getFirebaseInstance();

		$database = $firebase->getDatabase();
		$reference = $database->getReference($resource);

		if($withStartAndEnd) {

			$start = time() - 3600;
			$end = time();

			if($request->has('start')) {
				$start = $request->input('start');
			}

			if($request->has('end')) {
				$end = $request->input('end');
			}

			$snapshot = $reference->orderByChild('time')->startAt($start)->endAt($end)->getSnapshot();

		} else {

			$snapshot = $reference->getSnapshot();

		}

		$data = $snapshot->getValue();

		$list = array();

		if($data) {

			foreach($data as $item) {
				$list[] = $item;
			}

		}

		return $list;

	}

	protected function getFirebaseObject($resource) {

		$firebase = $this->getFirebaseInstance();

		$database = $firebase->getDatabase();
		$reference = $database->getReference($resource);

		$snapshot = $reference->getSnapshot();
		$object = $snapshot->getValue();

		return $object;

	}

	protected function filterListByAccessType($list,$accessType) {

		$data = array();

		for($i = 0; $i < count($list); $i++) {

			if($list[$i]['payload']['access_type'] == $accessType) {
				array_push($data,$list[$i]);
			}

		}

		return $data;

	}

	private function getFirebaseInstance() {

		$discoverer = new Discoverer([
			function() {
				$fbs = FirebaseServiceAccountFactory::Instance();
				$serviceAccount = ServiceAccount::fromJson($fbs->getServiceAccountConfig());
				return $serviceAccount;
			}
		]);

		$firebase = (new Factory())
			->withServiceAccountDiscoverer($discoverer)
			->create();

		return $firebase;

	}

}
