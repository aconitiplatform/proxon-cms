<?php

namespace App\Console\Traits;

use App\Models\Billable;

trait AsksForBillingDetails {

    protected function createBillingDetails($tenantId, $billableObjectType, $billableObjectId, $easybillCustomerId = null) {

        $billingDetailsCorrect = false;

        while(!$billingDetailsCorrect) {

            $billable = $this->askForBillingDetails($tenantId, $billableObjectType, $billableObjectId, $easybillCustomerId);
            $billingDetailsCorrect = $this->confirm('Is the displayed information correct?');

        }

        $billable->save();
        $this->info('Billable saved.');

    }

    protected function askForBillingDetails($tenantId, $billableObjectType, $billableObjectId, $easybillCustomerId) {

        $billable = Billable::where('tenant_id',$tenantId)->first();

        if($billable != null) {
            $this->info('A billable object for this tenant already exists and will be updated with the new information.');
        } else {
            $this->info('Creating a new billable object for this tenant.');
            $billable = new Billable();
        }

        $billable->billable_object_type = $billableObjectType;
        $billable->billable_object_id = $billableObjectId;
        $billable->tenant_id = $tenantId;

        if($easybillCustomerId != null) {
            $billable->easybill_customer_id = $easybillCustomerId;
            $this->info('Billable Easybill Customer ID => '.$billable->easybill_customer_id);
        }

        $billable->price_per_beacon = $this->ask("What is the price per beacon per month in cents for this billable?");
        $billable->discount = $this->ask("What is the discount in percent? E.g. a reseller would get 25%.");

        $this->info('Billable Object => '.$billable->billable_object_type.' '.$billable->billable_object_id);
        $this->info('Billable Pricing => '.$billable->price_per_beacon.' with a '.$billable->discount.'% discount');

        return $billable;

    }

}
