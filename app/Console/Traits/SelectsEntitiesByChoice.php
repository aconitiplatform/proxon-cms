<?php

namespace App\Console\Traits;

use App\Models\User;
use App\Models\Tenant;

use App\Http\Services\Apis\EasybillService;

trait SelectsEntitiesByChoice {

    protected function selectCustomer() {

        $response = EasybillService::getCustomers();

        if(count($response) > 0) {

            foreach($response->items as $customer) {

                if($customer->company_name == null) {
                    $choices[] = $customer->id.' -- '.$customer->last_name;
                } else {
                    $choices[] = $customer->id.' -- '.$customer->company_name;
                }

            }

            $customer = $this->choice('Customer',$choices);
            return $this->getEntityId($customer);

        } else {
            $this->error('No customers.');
            exit;
        }


    }

    protected function selectReseller() {

        $resellers = User::where('role','reseller')->get();

        if($resellers->isEmpty()) {
            $this->error('No resellers.');
            exit;
        }

        $choices = [];

        foreach($resellers as $reseller) {
            $choices[] = $reseller->id.' -- '.$reseller->lastname;
        }

        $reseller = $this->choice('Reseller',$choices);

        $this->info('Selected reseller: '.$reseller);

        return $this->getEntityId($reseller);

    }

    protected function selectTenant($superadmin, $onTrial = false) {

        $tenants = Tenant::where('id','!=',$superadmin->tenant_id);

        if($onTrial) {
            $tenants->where('trial',true);
        }

        $tenants = $tenants->get();

        if($tenants->isEmpty()) {
            $this->error('No tenants.');
            exit;
        }

        $choices = [];

        foreach($tenants as $tenant) {
            $choices[] = $tenant->id.' -- '.$tenant->name;
        }

        $tenant = $this->choice('Tenant',$choices);

        $this->info('Selected tenant: '.$tenant);

        return $this->getEntityId($tenant);

    }

    private function getEntityId($entityChoice) {

        $exploder = ' -- ';
        $parts = explode($exploder,$entityChoice);

        return $parts[0];

    }

}
