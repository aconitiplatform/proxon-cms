<?php

namespace App\Console\Traits;

use App;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Billable;
use App\Models\Invoice;

use App\Http\Services\Service;
use App\Http\Services\Apis\EasybillService;

use App\Jobs\CreateInvoiceJob;

use App\Traits\InvalidatesCache;

trait CreatesInvoices {

    use InvalidatesCache;

    protected function createMonthlyResellerInvoice($resellerId,$async = false,$delay = null) {

        if($async && $delay) {
            $this->dispatchJob((new CreateInvoiceJob('monthly_reseller',['reseller_id'=>$resellerId]))->delay($delay),'invoices');
        } else if ($async && !$delay) {
            $this->dispatchJob(new CreateInvoiceJob('monthly_reseller',['reseller_id'=>$resellerId]),'invoices');
        } else {

            $reseller = User::findOrFail($resellerId);

            $billables = Billable::where('billable_object_type','reseller')->where('billable_object_id',$reseller->id)->get();

            if(!$billables->isEmpty()) {

                $items = [];
                $easybillCustomerId = '';

                foreach($billables as $billable) {

                    if($tenant = $this->isNotOnTrial($billable->tenant_id)) {

                        $easybillCustomerId = $billable->easybill_customer_id;

                        $items[] = [

                            'description' => $this->createItemDescription($tenant,'monthly','reseller'),
                            'quantity' => $tenant->allowed_beacons,
                            'type' => 'POSITION',
                            'position' => 1,
                            'single_price_net' => round($billable->price_per_beacon / 100 * (100 - $billable->discount)),
                            'vat_percent' => 19

                        ];

                    }

                }

                if(count($items) != 0) {
                    $invoice = $this->createInvoice($easybillCustomerId,$items);
                    $invoice = $this->storeInvoice($reseller->tenant_id,$invoice,$easybillCustomerId,'reseller',$reseller->id,$items,'monthly');
                }

            }

        }


    }

    protected function createSingleResellerInvoice($resellerId,$tenantId,$amountOfNewBeacons,$async = false,$delay = null) {

        if($async && $delay) {
            $this->dispatchJob((new CreateInvoiceJob('single_reseller',['reseller_id'=>$resellerId, 'tenant_id'=>$tenantId, 'amount_of_new_beacons'=>$amountOfNewBeacons]))->delay($delay),'invoices');
        } else if ($async && !$delay) {
            $this->dispatchJob(new CreateInvoiceJob('single_reseller',['reseller_id'=>$resellerId, 'tenant_id'=>$tenantId, 'amount_of_new_beacons'=>$amountOfNewBeacons]),'invoices');
        } else {

            $tenant = Tenant::findOrFail($tenantId);

            $reseller = User::findOrFail($resellerId);

            $billable = Billable::where('billable_object_type','reseller')->where('billable_object_id',$reseller->id)->where('tenant_id',$tenant->id)->first();

            if($billable != null) {

                $items = [];
                $easybillCustomerId = $billable->easybill_customer_id;

                if(!$tenant->trial) {

                    $items[] = [

                        'description' => $this->createItemDescription($tenant,'single','reseller'),
                        'quantity' => $amountOfNewBeacons,
                        'type' => 'POSITION',
                        'position' => 1,
                        'single_price_net' => round($this->calculatePartialPrice($billable->price_per_beacon / 100 * (100 - $billable->discount))),
                        'vat_percent' => 19

                    ];

					$invoice = $this->createInvoice($easybillCustomerId,$items);
					if ($invoice != null) {
						$invoice = $this->storeInvoice($reseller->tenant_id,$invoice,$easybillCustomerId,'reseller',$reseller->id,$items,'single');
					} else {
						return false;
					}

                }

            }

		}

		return true;

    }

    protected function createMonthlyTenantInvoice($tenantId,$async = false,$delay = null) {

        if($async && $delay) {
            $this->dispatchJob((new CreateInvoiceJob('monthly_tenant',['tenant_id'=>$tenantId]))->delay($delay),'invoices');
        } else if ($async && !$delay) {
            $this->dispatchJob(new CreateInvoiceJob('monthly_tenant',['tenant_id'=>$tenantId]),'invoices');
        } else {

            $tenant = Tenant::findOrFail($tenantId);

            $billable = Billable::where('billable_object_type','tenant')->where('billable_object_id',$tenant->id)->first();

            if($billable != null) {

                $items = [];
                $easybillCustomerId = $billable->easybill_customer_id;

                if(!$tenant->trial) {

                    $items[] = [

                        'description' => $this->createItemDescription($tenant,'monthly','tenant'),
                        'quantity' => $tenant->allowed_beacons,
                        'type' => 'POSITION',
                        'position' => 1,
                        'single_price_net' => round($billable->price_per_beacon / 100 * (100 - $billable->discount)),
                        'vat_percent' => 19

                    ];

                    $invoice = $this->createInvoice($easybillCustomerId,$items);
                    $invoice = $this->storeInvoice($tenantId,$invoice,$easybillCustomerId,'tenant',$tenant->id,$items,'monthly');

                }

            }

        }

    }

    protected function createSingleTenantInvoice($tenantId,$amountOfNewBeacons,$async = false,$delay = null) {

        if($async && $delay) {
            $this->dispatchJob((new CreateInvoiceJob('single_tenant',['tenant_id'=>$tenantId, 'amount_of_new_beacons'=>$amountOfNewBeacons]))->delay($delay),'invoices');
        } else if ($async && !$delay) {
            $this->dispatchJob(new CreateInvoiceJob('single_tenant',['tenant_id'=>$tenantId, 'amount_of_new_beacons'=>$amountOfNewBeacons]),'invoices');
        } else {

            $tenant = Tenant::findOrFail($tenantId);

            $billable = Billable::where('billable_object_type','tenant')->where('billable_object_id',$tenant->id)->first();

            if($billable != null) {

                $items = [];
                $easybillCustomerId = $billable->easybill_customer_id;

                if(!$tenant->trial) {

                    $items[] = [

                        'description' => $this->createItemDescription($tenant,'single','tenant'),
                        'quantity' => $amountOfNewBeacons,
                        'type' => 'POSITION',
                        'position' => 1,
                        'single_price_net' => round($this->calculatePartialPrice($billable->price_per_beacon / 100 * (100 - $billable->discount))),
                        'vat_percent' => 19

                    ];

                    $invoice = $this->createInvoice($easybillCustomerId,$items);
                    if($invoice != null) {
                        $invoice = $this->storeInvoice($tenantId,$invoice,$easybillCustomerId,'tenant',$tenant->id,$items,'single');
                    } else {
                        return false;
                    }

                }

            }

        }

        return true;

    }

    protected function createInvoice($easybillCustomerId,$items) {

        $invoice = [
            'currency' => 'EUR',
            'customer_id' => $easybillCustomerId,
            'items' => $items,
            'status' => 'ACCEPT',
            'type' => 'INVOICE',
            'use_shipping_address' => false
        ];

        if(!App::environment('production')) {
            $invoice['number'] = 'Test';
        }

		$invoice = EasybillService::createDocument($invoice);
		if ($invoice != null) {
			$invoice = EasybillService::setDocumentDone($invoice->id);
			EasybillService::sendDocumentPerEmail($invoice->id);
		}

        return $invoice;

    }

    protected function storeInvoice($tenantId,$easybillInvoice,$easybillCustomerId,$billableObjectType,$billableObjectId,$items,$type = 'single') {

        $dt = Carbon::now();

        $invoice = new Invoice();
        $invoice->easybill_invoice_id = $easybillInvoice->id;
        $invoice->easybill_customer_id = $easybillCustomerId;
        $invoice->billable_object_type = $billableObjectType;
        $invoice->billable_object_id = $billableObjectId;
        $invoice->easybill_invoice_number = $easybillInvoice->number;
        $invoice->easybill_created_at = $easybillInvoice->created_at;
        $invoice->items = $items;
        $invoice->type = $type;
        $invoice->month = $dt->month;
        $invoice->year = $dt->year;
        $invoice->times_generated = 1;
        $invoice->amount = $easybillInvoice->amount;
        $invoice->amount_net = $easybillInvoice->amount_net;
        $invoice->save();

        $this->invalidate($invoice->id,$tenantId,'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx','invoices');
        Service::updateLastModified($tenantId,new Invoice());

        return $invoice;

    }

    private function isNotOnTrial($tenantId) {

        $tenant = Tenant::findOrFail($tenantId);
        return ($tenant->trial) ? false : $tenant;

    }

    private function calculatePartialPrice($price) {

        $dt = Carbon::now();

        $daysToPay = $dt->daysInMonth - $dt->day;

        $percentToBePaidOfMonth = round($daysToPay/$dt->daysInMonth, 2);

        return $price * $percentToBePaidOfMonth;

    }

    private function createItemDescription($tenant,$type,$billable) {

        $dt = Carbon::now();

        switch ($type) {
            case 'monthly':
                $timestring = $dt->day(1)->format('d.m.Y')." - ".$dt->modify('last day of this month')->format('d.m.Y');
                break;
            case 'single':
                $timestring = $dt->format('d.m.Y')." - ".$dt->modify('last day of this month')->format('d.m.Y');
                break;
        }

        switch ($billable) {
            case 'reseller':
                $description = "<b>Substance</b><br>Kunde: ".$tenant->name."<br>Zeitraum: ".$timestring;
                break;
            case 'tenant':
                $description = "<b>Substance</b><br>Zeitraum: ".$timestring;
                break;
        }

        return $description;

    }

}
