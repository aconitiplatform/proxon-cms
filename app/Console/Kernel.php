<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App;

class Kernel extends ConsoleKernel {

    protected $commands = [
        Commands\CreateNewTenant::class,
        Commands\SyncBeaconsFromKontaktIo::class,
        Commands\PlannedMaintenance::class,
        Commands\CreateNewBeacons::class,
        Commands\RestoreEdwardStone::class,
        Commands\ApplyChanges::class,
        Commands\CreateAnalyticsEvents::class,
        Commands\CreateSuperAdmin::class,
        Commands\SetTenantActivity::class,
        Commands\CreateReseller::class,
        Commands\AddTenantToReseller::class,
        Commands\EndTrial::class,
        Commands\CreateResellerInvoice::class,
        Commands\CreateTenantInvoice::class,
        Commands\CreateMonthlyInvoices::class,
        Commands\ServerIp::class,
        Commands\Alive::class,
        Commands\RemoveExpiredGroups::class,
        Commands\RedisAnalytics::class,
        Commands\ShipCart::class,
        Commands\CartsToShip::class,
        Commands\SendEmailConfirmationReminders::class,
    ];

    protected function schedule(Schedule $schedule) {

        if(App::environment('development')) {
            // $schedule->command('invoices:monthly')->monthlyOn(28,'15:45');
            $schedule->command('pois:expired-groups')->daily();
            $schedule->command('inform:email-confirmation')->daily()->at('15:30');
        }

        if(App::environment('staging')) {
            $schedule->command('invoices:monthly')->monthlyOn(29,'15:45');
            $schedule->command('pois:expired-groups')->hourly();
            $schedule->command('inform:email-confirmation')->daily()->at('15:30');
        }

        if(App::environment('production') && config('substance.whitelabel.active') == false) {
            $schedule->command('pois:expired-groups')->hourly();
            $schedule->command('substance:alive')->everyThirtyMinutes()->thenPing('http://beats.envoyer.io/heartbeat/PVLJyNXkPyWPeZO');
            $schedule->command('invoices:monthly')->monthlyOn(1,'13:00')->thenPing('http://beats.envoyer.io/heartbeat/e2ijZnT7tGnVDvU');
            $schedule->command('backup:clean')->daily()->at('01:30');
            $schedule->command('backup:run')->twiceDaily(2,14)->thenPing('http://beats.envoyer.io/heartbeat/q9QT8uc2VWV27Yx');
            $schedule->command('inform:email-confirmation')->daily()->at('15:30');
        }

        if(App::environment('production') && config('substance.whitelabel.active') == true) {
            $schedule->command('pois:expired-groups')->hourly();
            if(config('substance.whitelabel.bill_manually') == false) {
                $schedule->command('invoices:monthly')->monthlyOn(1,'10:00');
            }
            $schedule->command('backup:clean')->daily()->at('01:30');
            $schedule->command('backup:run')->twiceDaily(2,14);
        }

    }
}
