<?php

namespace App\Console\Commands;

use Faker;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Features\Feature;
use App\Models\Features\Module;

use App\Http\Controllers\DesignController;

use App\Http\Services\DesignService;

use App\Jobs\DeliverTenantToDistributionJob;

use App\Traits\AffectsEdwardStone;
use App\Traits\WorksWithQueue;

class CreateSuperAdmin extends BaseCommand {

    use AffectsEdwardStone;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:superadmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a superadmin and its tenant to login as other user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $superadmin = User::where('role','superadmin')->first();

        if($superadmin != null) {

            $this->info('Superadmin already exists. Email and password will be set to the latest version in .env file.');

            $superadmin->email = config('substance.super_admin.email');
            $superadmin->password = bcrypt(config('substance.super_admin.password'));
            $superadmin->firstname = 'Proxon';
            $superadmin->lastname = 'Administrator';

            $superadmin->save();

        } else {

            $faker = Faker\Factory::create();

            $tenant = new Tenant();
            $cn = 'Proxon Super Admin';
            $tenant->name = $cn;
            $tenant->fallback_language = 'de';
            $tenant->locale = 'de_DE';
            $tenant->allowed_beacons = 0;
            $tenant->slug = str_slug($cn);
            $tenant->logo = '';
            $tenant->url = 'https://platform.proxon.io';
            $tenant->imprint = '';
            $tenant->email = $faker->safeEmail;
            $tenant->logo_settings = [
                'type' => 'none',
            ];
            $tenant->save();

            $designController = new DesignController();
            $design = $designController->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',$tenant->id);

            $tenant->tenant_design_id = $design->id;
            $tenant->save();

            $user = new User();
            $user->firstname = 'Proxon';
            $user->lastname = 'Administrator';
            $user->email = config('substance.super_admin.email');
            $user->password = bcrypt(config('substance.super_admin.password'));
            $user->language = 'de';
            $user->locale = 'de_DE';
            $user->role = 'superadmin';
            $user->tenant_id = $tenant->id;
            $user->active = true;
            $user->save();

            $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));

            $features = Feature::all();

            if(!$features->isEmpty()) {

                foreach($features as $feature) {
                    $featureIds[] = $feature->id;
                }

                $tenant->features()->sync($featureIds);

            }

            $modules = Module::all();

            if(!$modules->isEmpty()) {

                foreach($modules as $module) {
                    $moduleIds[] = $module->id;
                }

                $tenant->modules()->sync($moduleIds);

            }


            $this->info('Superadmin created.');

        }

    }

}
