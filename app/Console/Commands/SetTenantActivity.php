<?php

namespace App\Console\Commands;

use Cache;

use App\Models\Tenant;

use App\Http\Services\Service;

use App\Events\TenantHasChanged;

use App\Traits\AffectsEdwardStone;
use App\Traits\GeneratesCacheKeys;

class SetTenantActivity extends BaseCommand {

    use AffectsEdwardStone, GeneratesCacheKeys;

    protected $signature = 'tenants:set {tenantId} {--inactive}';

    protected $description = 'Activate or deactivate a tenant.';

    private $tenantId;

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $this->tenantId = $this->argument('tenantId');

        try {

            $tenant = Tenant::findOrFail($this->tenantId);

            if($this->option('inactive')) {
                $this->deactivate($tenant);
            } else {
                $this->activate($tenant);
            }

        } catch(\Exception $e) {
            $this->info("Problem occurred: ".$e->getTraceAsString());
        }

    }

    private function activate($tenant) {

        $tenant->active = true;
        $this->info("Tenant {$tenant->name} activated.\n");
        $this->save($tenant);

    }

    private function deactivate($tenant) {

        $tenant->active = false;
        $this->info("Tenant {$tenant->name} deactivated.\n");
        $this->save($tenant);

    }

    private function save($tenant) {

        $tenant->save();
        $this->info("Tenant {$tenant->name} saved.\n");

        if(config('broadcasting.should_broadcast')) event(new TenantHasChanged($tenant));

        Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
        $this->info("Tenant {$tenant->name} cached.\n");

        Service::updateLastModified($tenant->id,new Tenant());
        $this->info("Last modified for tenant {$tenant->name} updated.\n");

        $this->publishTenantToEdwardStone($tenant->id,true);
        $this->info("Tenant {$tenant->name} published to Edward Stone.\n");

    }

}
