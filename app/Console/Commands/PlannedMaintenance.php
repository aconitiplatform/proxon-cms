<?php

namespace App\Console\Commands;

use App\Models\User;

use App\Jobs\MaintenanceEmailJob;

class PlannedMaintenance extends BaseCommand {

    protected $signature = 'inform:maintenance {when} {howLong}';

    protected $description = 'Inform all Substance users that a planned maintenance, e.g. a new deploy, will take place.';

    public function handle() {

        $this->info("Retrieving users.");

        $users = User::all();

        if(!$users->isEmpty()) {

            foreach($users as $user) {

                $this->dispatchJob(new MaintenanceEmailJob($user,$this->argument('when'),$this->argument('howLong')),'emails');
                $this->info("Dispatched email: ".$user->email);

            }

        } else {
            $this->info("No users found.");
        }

        $this->info("Completed.");

    }

}
