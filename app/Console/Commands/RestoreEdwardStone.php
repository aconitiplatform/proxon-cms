<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Cache;

use App\Models\Tenant;
use App\Models\Poi;
use App\Models\Beacon;
use App\Models\MicroPayment;

use App\Traits\AffectsEdwardStone;

class RestoreEdwardStone extends BaseCommand {

    use AffectsEdwardStone;

    protected $signature = 'edward:restore';

    protected $description = 'Restore Edward Stone on Redis failure.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $this->info("Starting restore process\n");
        $this->info("Flushing cache\n");

        Cache::flush();

        $this->info("Restoring tenants");

        $tenants = Tenant::all();

        $this->info("Found ".$tenants->count()." tenants");

        if(!$tenants->isEmpty()) {

            foreach($tenants as $tenant) {

                $this->info("Restoring tenant ".$tenant->name);
                $this->publishTenantToEdwardStone($tenant->id,true);

            }

        }

        $this->info("\nRestoring POIs");

        Poi::chunk(50, function($pois) {

            foreach($pois as $poi) {

                $this->info("Restoring POI ".$poi->name);
                $this->publishPoiToEdwardStone($poi->id,$poi->active);

            }

        });

        $this->info("\nRestoring Slugs and URLs");

        Beacon::where('poi_id','!=',null)->chunk(50, function($beacons) {

            foreach($beacons as $beacon) {

                $this->info("Restoring Slug and URL for beacon ".$beacon->name);
                $this->publishPoiUrlConnection($beacon->poi_id,$beacon->url,$beacon->tenant_id,true);

            }

        });

        $this->info("\nRestoring Micropayments");

        $payments = MicroPayment::all();

        $this->info("Found ".$payments->count()." payments");

        if(!$payments->isEmpty()) {

            foreach($payments as $payment) {

                $this->info("Restoring payment ".$payment->name);
                $this->publishMicroPaymentToEdwardStone($payment->id,true);

            }

        }

        $this->info("\nSuccessfully dispatched all jobs to restore Edward Stone.");

    }

}
