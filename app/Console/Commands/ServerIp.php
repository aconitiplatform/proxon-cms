<?php

namespace App\Console\Commands;

use App;
use App\Http\Services\ServerService;

class ServerIp extends BaseCommand {

    protected $signature = 'serverip {--commit=default} {--server=qmui}';

    protected $description = 'Create an app server configuration.';

    public function handle() {

        $server = $this->getServerType($this->option('server'));
        $commit = $this->getCommit($this->option('commit'));
        $ip = $this->getIp();

        $this->makeAppServerConfig($server,$commit,$ip);

    }

    private function makeAppServerConfig($server,$commit,$ip) {

        $appServerConfig = fopen(__DIR__.'/../../../config/app-server.php','w') or $this->error("Could not open or create app-server.php config file.");
        fwrite($appServerConfig,"<?php return ['ip'=>'".$ip."','commit'=>'".$commit."','server'=>'".$server."'];");
        fclose($appServerConfig);

        $this->info("Created or updated app-server.php");

    }

    private function getIp() {

        if(App::environment('local')) {
            $ip = exec("ifconfig enp0s3 | grep 'inet addr' | awk -F':' {'print $2'} | awk -F' ' {'print $1'}");
        } else {
            $ip = exec("ifconfig eth0 | grep 'inet addr' | awk -F':' {'print $2'} | awk -F' ' {'print $1'}");
        }

        if($ip == "") {
            $ip = "unknown IP";
        }

        $this->info("IP is: ".$ip);

        return $ip;

    }

    private function getServerType($serverOption) {

        if(!in_array($serverOption,ServerService::getServerOptions())) {
            $this->info("Unknown server option {$serverOption}. Defaulting to complete server.");
        }

        switch ($serverOption) {
            case 'q':
                $this->info("Server is a queue server");
                return $serverOption;
            case 'qm':
                $this->info("Server is a queue and migration server");
                return $serverOption;
            case 'ui':
                $this->info("Server is a ui server");
                return $serverOption;
            default:
                $this->info("Server is a complete (qmui) server");
                return 'qmui';
        }

    }

    private function getCommit($commitOption) {

        if($commitOption == 'default') {
            $commit = trim(exec('git log --pretty="%H" -n1 HEAD'));
        } else {
            $commit = $commitOption;
        }

        $this->info("Commit is: ".$commit);

        return $commit;

    }

}
