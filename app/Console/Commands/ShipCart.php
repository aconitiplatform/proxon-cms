<?php

namespace App\Console\Commands;

use Log;

use App\Models\Cart;

class ShipCart extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cart:shipped {cartId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set a cart to shipped.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $cart = Cart::findOrFail($this->argument('cartId'));

        if($cart->shipped) {
            $this->info("Cart {$cart->id} already shipped.");
        } else {

            $cart->shipped = true;
            $cart->save();
            $this->info("Cart {$cart->id} set to shipped.");

        }

    }
}
