<?php

namespace App\Console\Commands;

use Faker;

use App\Models\Reseller;
use App\Models\ResellerSetting;
use App\Models\User;
use App\Models\Tenant;

use App\Http\Controllers\Auth\PasswordResetController;

use App\Traits\SyncsFeaturesAndModules;
use App\Console\Traits\SelectsEntitiesByChoice;

class CreateReseller extends BaseCommand {

    use SyncsFeaturesAndModules, SelectsEntitiesByChoice;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:reseller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a reseller user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $superadmin = User::where('role','superadmin')->first();

        if($superadmin == null) {
            $this->error('Please create a superadmin first by calling php artisan make:superadmin');
        } else {

            $tenant = Tenant::findOrFail($superadmin->tenant_id);

            $resellerDetailsCorrect = false;

            while(!$resellerDetailsCorrect) {

                $reseller = $this->askForResellerDetails($tenant);
                $resellerDetailsCorrect = $this->confirm('Is the displayed information correct?');

            }

            $reseller->save();

            $settingsCorrect = false;

            while(!$settingsCorrect) {

                $settings = $this->askForSettings($reseller);
                $settingsCorrect = $this->confirm('Is the displayed information correct?');

            }

            $settings->save();

            $this->syncDefaultsForReseller($reseller);

            $passwordResetController = new PasswordResetController();
            $passwordResetController->triggerInternalSendResetLinkEmail($reseller->email,true);

        }

    }

    private function askForResellerDetails($tenant) {

        $reseller = new Reseller();
        $reseller->firstname = 'Reseller';
        $reseller->lastname = $this->ask("What is the reseller's name?");
        $reseller->email = $this->ask("What is the reseller's email address?");
        $reseller->password = bcrypt(config('app.key').time());
        $reseller->language = $tenant->fallback_language;
        $reseller->locale = $tenant->locale;
        $reseller->role = 'reseller';
        $reseller->tenant_id = $tenant->id;
        $reseller->active = true;

        $this->info('Reseller Name => '.$reseller->firstname.' '.$reseller->lastname);
        $this->info('Reseller Email => '.$reseller->email);

        return $reseller;

    }

    private function askForSettings($reseller) {

        $settings = new ResellerSetting();

        $settings->easybill_customer_id = $this->selectCustomer();

        if($this->confirm('Does the reseller have an own Kontakt.io API key?')) {
            $settings->kontakt_io_api_key = $this->ask("What is the reseller's Kontakt.io API key?");
        }

        if($this->confirm('Does the reseller have an own URL for Substance?')) {

            $settings->substance_url = $this->ask("What is the reseller's Substance URL?");

            if($this->confirm('Does the reseller have an own sidebar product logo (url)?')) {
                $settings->sidebar_product_logo = $this->ask("What is the reseller's sidebar product logo (url)?");
            }

            if($this->confirm('Does the reseller have an own login screen company logo (url)?')) {
                $settings->login_screen_company_logo = $this->ask("What is the reseller's login screen company logo (url)?");
            }

            if($this->confirm('Does the reseller have an own login screen product logo (url)?')) {
                $settings->login_screen_product_logo = $this->ask("What is the reseller's login screen product logo (url)?");
            }

            if($this->confirm('Does the reseller have an own login screen image (url)?')) {
                $settings->login_screen_image = $this->ask("What is the reseller's login screen image (url)?");
            }

        }

        if($this->confirm('Does the reseller have an own URL for Edward Stone?')) {
            $settings->edward_stone_url = $this->ask("What is the reseller's Edward Stone URL?");
        }

        if($this->confirm('Does the reseller have an own product name?')) {
            $settings->product_name = $this->ask("What is the reseller's product name?");
        }

        if($this->confirm('Does the reseller have an own company name?')) {
            $settings->company_name = $this->ask("What is the reseller's company name?");
        }

        $this->info("easybill_customer_id => ".$settings->easybill_customer_id);
        $this->info("kontakt_io_api_key => ".$settings->kontakt_io_api_key);
        $this->info("substance_url => ".$settings->substance_url);
        $this->info("edward_stone_url => ".$settings->edward_stone_url);
        $this->info("sidebar_product_logo => ".$settings->sidebar_product_logo);
        $this->info("login_screen_company_logo => ".$settings->login_screen_company_logo);
        $this->info("login_screen_product_logo => ".$settings->login_screen_product_logo);
        $this->info("login_screen_image => ".$settings->login_screen_image);
        $this->info("product_name => ".$settings->product_name);
        $this->info("company_name => ".$settings->company_name);

        $settings->reseller_id = $reseller->id;

        return $settings;

    }

}
