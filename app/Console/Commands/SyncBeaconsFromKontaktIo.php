<?php

namespace App\Console\Commands;

use App\Models\Tenant;

use App\Traits\SyncsTenantBeacons;

class SyncBeaconsFromKontaktIo extends BaseCommand {

    use SyncsTenantBeacons;

    protected $signature = 'tenants:syncbeacons {tenant}';

    protected $description = 'Sync assigned beacons from a tenant\'s Kontakt.IO venue to Substance.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $tenantId = $this->argument('tenant');
        $tenant = Tenant::find($tenantId);

        $this->syncBeacons($tenant,true);

    }

}
