<?php

namespace App\Console\Commands;

use App\Models\User;

use App\Console\Traits\SelectsEntitiesByChoice;
use App\Console\Traits\CreatesInvoices;

class CreateResellerInvoice extends BaseCommand {

    use SelectsEntitiesByChoice, CreatesInvoices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resellers:invoice {--single}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a reseller invoice.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $resellerId = $this->selectReseller();

        if($this->option('single')) {

            $superadmin = User::where('role','superadmin')->first();
            $tenantId = $this->selectTenant($superadmin);
            $amountOfNewBeacons = $this->ask('How many beacons will this invoice contain?');
            $this->createSingleResellerInvoice($resellerId,$tenantId,$amountOfNewBeacons);

        } else {
            $this->createMonthlyResellerInvoice($resellerId);
        }



    }

}
