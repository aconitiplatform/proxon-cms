<?php

namespace App\Console\Commands;

use Log;

use App\Models\Cart;

class CartsToShip extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cart:to-ship';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find carts that need to be shipped.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $carts = Cart::where('shipped',false)->get();

        if(!$carts->isEmpty()) {

            foreach($carts as $cart) {

                $this->info("ID: {$cart->id}");
                $this->info("Amount of items: ".count($cart->items));
                $this->info("\n");

            }

        }

    }
}
