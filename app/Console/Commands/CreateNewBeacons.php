<?php

namespace App\Console\Commands;

use Auth;

use App\Models\Tenant;
use App\Models\User;
use App\Models\Beacon;

use App\Http\Controllers\Auth\PasswordResetController;

use App\Http\Services\BeaconService;
use App\Http\Services\Apis\KontaktIoService;

use App\Jobs\DeliverTenantToDistributionJob;

use App\Traits\InvalidatesCache;
use App\Traits\GeneratesBeaconUrl;

class CreateNewBeacons extends BaseCommand {

    use InvalidatesCache, GeneratesBeaconUrl;

    protected $signature = 'beacons:new {tenantId} {amount} {own} {--names=}';

    protected $description = 'Create new beacons for a tenant through the command line.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $tenant = Tenant::find($this->argument('tenantId'));

        if($tenant != null) {

            $tenant->allowed_beacons = $tenant->allowed_beacons + $this->argument('amount');
            $tenant->save();
            $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
            $nameList = $this->createNameList($tenant);
            $this->createTenantBeacons($tenant,$nameList);

        } else {
            $this->error('Tenant does not exist.');
        }

    }

    private function createNameList($tenant) {

        if($this->option('names') != null) {
            $nameList = explode(',',$this->option('names'));
        } else {

            for($i = 0; $i < $this->argument('amount'); $i++) {

                $serial = str_pad($i+1, strlen($tenant->allowed_beacons), "0", STR_PAD_LEFT);
                $name = BeaconService::generateRandomString(config('substance.beacon_name_length'),true,"R7");
                $nameList[] = $name;

            }

        }

        return $nameList;

    }

    private function createTenantBeacons($tenant,$nameList) {

        $beaconInfos = [];

        $createdBeacons = 0;

        while($this->argument('amount') > $createdBeacons) {

            $beacon = new Beacon();
            $beacon->name = $nameList[$createdBeacons];
            $beacon->url = $this->newUniqueUrl();
            $beacon->url_hash = md5($beacon->url);
            $beacon->tenant_id = $tenant->id;

            if($this->argument('own') == 'true') {
                $beacon->substance_beacon = false;
            } else {
                $beacon->substance_beacon = true;
            }

            $beacon->save();

            $this->info('Name: '.$beacon->name.' URL: '.config('substance.edward_stone_url').'/'.$beacon->url);

            $createdBeacons++;

            BeaconService::addTenantUsersAndAdminsToBeacon($beacon->id,$beacon->tenant_id);

            $this->invalidate($beacon->id,$tenant->id,'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx','beacons');

        }

        //Set a user to generate cache keys
        $user = User::where('tenant_id',$tenant->id)->where('role','admin')->first();
        Auth::setUser($user);
        BeaconService::updateLastModified($tenant->id,new Beacon());

    }

}
