<?php

namespace App\Console\Commands;


use App\Models\Tenant;

use App\Jobs\PrepareKpiDataJob;

/**
 * @deprecated
 */
class KpiDispatcher extends BaseCommand {

    protected $signature = 'kpidispatcher';

    protected $description = 'Dispatch jobs to generate KPI data for tenants.';

    public function handle() {

        $tenants = Tenant::all();

        if(!$tenants->isEmpty()) {
            foreach($tenants as $tenant) {
                $job = (new PrepareKpiDataJob($tenant->id))->onQueue(config('substance.queue_prefix').'firebase');
                dispatch($job);
            }

        }

    }
}
