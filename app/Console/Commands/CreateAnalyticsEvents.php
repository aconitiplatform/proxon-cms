<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Faker;

use App\Models\UrlAccess;
use App\Models\Beacon;
use App\Models\Poi;
use App\Models\Tenant;

class CreateAnalyticsEvents extends BaseCommand {

    protected $signature = 'analytics:fake {tenantId} {amount}';

    protected $description = 'Create new analytics events for the last 2 months for testing purposes.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $tenant = Tenant::find($this->argument('tenantId'));

        if($tenant != null) {

            $bar = $this->output->createProgressBar($this->argument('amount'));

            for($i = 0;$i < $this->argument('amount'); $i++) {

                $this->createEvent($tenant);
                $bar->advance();

            }

            $bar->finish();

        } else {
            $this->error('Tenant does not exist.');
        }

    }

    public function createEvent($tenant) {

        $faker = Faker\Factory::create();

        $event = new UrlAccess();
        $event->tenant_id = $tenant->id;
        $event->access_type = $this->randomAccessType();
        $event->url = $this->randomUrl($event->access_type,$tenant);
        $t = time() + rand(-5184000,86400);
        $c = Carbon::createFromTimestamp($t);
        $event->created_at = $c->toDateTimeString();
        $event->updated_at = $c->toDateTimeString();
        $event->token = md5(time().rand(1,10000));
        $event->poi_id = $this->randomPoiId($tenant);
        $event->user_agent = $faker->userAgent;
        $event->save();

    }

    private function randomAccessType() {

        $i = rand(1,6);

        switch ($i) {
            case 1:
            case 4:
            case 5:
                return 'beacon';
                break;
            case 2:
            case 6:
                return 'web';
                break;
            case 3:
                return 'channel';
                break;
        }

    }

    private function randomUrl($type,$tenant) {

        if($type=='beacon' || $type=='web') {

            $beacon = Beacon::where('tenant_id',$tenant->id)->inRandomOrder()->first();

            return $beacon->url;

        } else {

            return $this->randomPoiId($tenant);

        }

    }

    private function randomPoiId($tenant) {

        $poi = Poi::where('tenant_id',$tenant->id)->inRandomOrder()->first();

        return $poi->id;

    }

}
