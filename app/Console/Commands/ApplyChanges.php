<?php

namespace App\Console\Commands;

use Cache;
use DB;
use App;

use App\Models\Tenant;
use App\Models\Design;
use App\Models\Poi;
use App\Models\User;
use App\Models\Billable;
use App\Models\Reseller;
use App\Models\Media;
use App\Models\ResellerSetting;

use App\Http\Controllers\DesignController;

use App\Http\Services\DesignService;
use App\Http\Services\TenantService;
use App\Http\Services\UserService;

use App\Jobs\ResizeImageJob;
use App\Jobs\Changes\MigrateOldAnalyticsJob;
use App\Jobs\Changes\NewTimestampForFiredAtJob;

use App\Traits\AffectsEdwardStone;
use App\Traits\GeneratesCacheKeys;
use App\Traits\SyncsFeaturesAndModules;

class ApplyChanges extends BaseCommand {

    use AffectsEdwardStone, GeneratesCacheKeys, SyncsFeaturesAndModules;

    protected $signature = 'changes:apply';

    protected $description = 'Apply changes to existing tenants when being deployed.';

    public function handle() {

        $this->getSettings();

        if(!App::environment('production')) {
            $this->call('db:seed');
        }
        $this->addLocaleToTenant();
        $this->addDesignsToTenants();
        $this->removeNullValuesFromTenant();
        $this->addLogoSettingsToTenant();
        $this->adjustDesignsToNewModel();
        $this->addDesignsToPois();
        $this->refactorEntityRelations();
        $this->modifyDesignSettingsHeaderFooter();
        $this->removeDefaultHeightsForHeaderFooter();
        $this->addEmptyArrayToFonts();
        $this->addDesignToEdwardApp();
        $this->removeNullFromEdwardButton();
        $this->migrateOldAnalytics();
        $this->firedAtTimestampsForAnalytics();
        $this->modulesAndFeaturesForExistingTenants();
        $this->modulesAndFeaturesForExistingResellers();
        $this->addResellerIdsToApplicableTenants();
        $this->migrateDesignFromBodyToEdwardApp();
        $this->createNewMediaThumbnails();
        $this->configureInitialKontaktIoSync();
        $this->createResellerSettingForExistingResellers();
        $this->confirmExistingEmailAddresses();
        $this->call('edward:restore');

    }

    private function getSettings() {

        $this->settings = DB::table('changes')->select('method','ran_once')->get();

    }

    private function confirmExistingEmailAddresses() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'confirmExistingEmailAddresses' && $setting->ran_once == true) {
                $run = false;
                $this->info("All email addresses of Substance users confirmed.");
            }

        }

        if($run) {

            $users = User::where('email_confirmed',false)->get();

            if(!$users->isEmpty()) {

                foreach($users as $user) {
                    $user->email_confirmed = true;
                    $user->save();
                }

            } else {
                $this->info("No unconfirmed users.");
            }

            DB::table('changes')->insert([
                'method' => 'confirmExistingEmailAddresses',
                'ran_once' => true
            ]);

        }

    }

    private function createResellerSettingForExistingResellers() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'createResellerSettingForExistingResellers' && $setting->ran_once == true) {
                $run = false;
                $this->info("Reseller settings already created.");
            }

        }

        if($run) {

            $resellers = Reseller::where('role','reseller')->get();

            if(!$resellers->isEmpty()) {

                foreach($resellers as $reseller) {

                    if($reseller->settings == null) {

                        $settings = new ResellerSetting();
                        $settings->reseller_id = $reseller->id;

                        $billable = Billable::where('billable_object_type','reseller')->where('billable_object_id',$reseller->id)->first();

                        if($billable != null) {

                            $settings->easybill_customer_id = $billable->easybill_customer_id;
                            $settings->save();
                            $this->info("Created settings for reseller {$reseller->firstname} {$reseller->lastname}.");

                        } else {
                            $this->info("Could not create settings for reseller {$reseller->firstname} {$reseller->lastname}.");
                        }

                    } else {
                        $this->info("Reseller {$reseller->firstname} {$reseller->lastname} already has settings.");
                    }

                }

            }

            DB::table('changes')->insert([
                'method' => 'createResellerSettingForExistingResellers',
                'ran_once' => true
            ]);

        }

    }

    private function configureInitialKontaktIoSync() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'configureInitialKontaktIoSync' && $setting->ran_once == true) {
                $run = false;
                $this->info("Existing tenants are in sync with kontakt.io.");
            }

        }

        if($run) {

            $tenants = Tenant::whereNotNull('kontakt_io_venue_id')->get();

            if(!$tenants->isEmpty()) {

                foreach($tenants as $tenant) {
                    $tenant->initial_kontakt_io_sync = true;
                    $tenant->save();
                }

            }

            DB::table('changes')->insert([
                'method' => 'configureInitialKontaktIoSync',
                'ran_once' => true
            ]);

        }

    }

    private function createNewMediaThumbnails() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'createNewMediaThumbnails' && $setting->ran_once == true) {
                $run = false;
                $this->info("New thumbnails already created.");
            }

        }

        if($run) {

            Media::chunk(100, function($mediaObjects) {

                $mediaObjects->each(function($item,$key) {

                    if($item->simple_type == 'image') {
                        $this->dispatchJob(new ResizeImageJob($item));
                    }

                });

            });

            DB::table('changes')->insert([
                'method' => 'createNewMediaThumbnails',
                'ran_once' => true
            ]);

        }

    }

    private function addResellerIdsToApplicableTenants() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'addResellerIdsToApplicableTenants' && $setting->ran_once == true) {
                $run = false;
                $this->info("Existing tenants have resellers if applicable.");
            }

        }

        if($run) {

            $billables = Billable::where('billable_object_type','reseller')->get();

            if(!$billables->isEmpty()) {

                foreach($billables as $billable) {

                    $tenant = Tenant::find($billable->tenant_id);
                    $tenant->reseller_id = $billable->billable_object_id;
                    $tenant->save();
                    $this->info("Updated reseller_id for {$tenant->name}");

                }

            } else {
                $this->info("No tenants with resellers.");
            }

            DB::table('changes')->insert([
                'method' => 'addResellerIdsToApplicableTenants',
                'ran_once' => true
            ]);

        }

    }

    private function migrateDesignFromBodyToEdwardApp() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'migrateDesignFromBodyToEdwardApp' && $setting->ran_once == true) {
                $run = false;
                $this->info("Design from body to edward_app already migrated.");
            }

        }

        if($run) {

            $designs = Design::all();

            if(!$designs->isEmpty()) {

                $this->info("Migrating design: from body to edward_app");

                foreach($designs as $design) {

                    $body = $design->body;
                    $edwardApp = $design->edward_app;

                    if(isset($body['padding-left'])) {
                        $edwardApp['padding-left'] = $body['padding-left'];
                        unset($body['padding-left']);
                    }

                    if(isset($body['padding-right'])) {
                        $edwardApp['padding-right'] = $body['padding-right'];
                        unset($body['padding-right']);
                    }

                    if(isset($body['background'])) {
                        $edwardApp['background'] = $body['background'];
                        unset($body['background']);
                    }

                    if(isset($body['font-size'])) {
                        unset($body['font-size']);
                    }

                    if(isset($body['background-color'])) {
                        unset($body['background-color']);
                    }

                    if(!isset($body['color'])) {
                        $body['color'] = "#021A24";
                    }

                    if(!isset($body['font-family'])) {
                        $body['font-family'] = "";
                    }

                    $design->body = $body;
                    $design->edward_app = $edwardApp;
                    $design->save();

                }

            } else {
                $this->info("No designs found.");
            }


            DB::table('changes')->insert([
                'method' => 'migrateDesignFromBodyToEdwardApp',
                'ran_once' => true
            ]);

        }

    }

    private function modulesAndFeaturesForExistingTenants() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'modulesAndFeaturesForExistingTenants' && $setting->ran_once == true) {
                $run = false;
                $this->info("Existing tenants have been synced with default features and modules from backend.");
            }

        }

        if($run) {

            $tenants = Tenant::all();

            if(!$tenants->isEmpty()) {

                foreach($tenants as $tenant) {
                    $this->syncDefaults($tenant);
                    $this->info("Features and modules for {$tenant->name}");
                }

            } else {
                $this->info("No tenants found.");
            }

            DB::table('changes')->insert([
                'method' => 'modulesAndFeaturesForExistingTenants',
                'ran_once' => true
            ]);

        }

    }

    private function modulesAndFeaturesForExistingResellers() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'modulesAndFeaturesForExistingResellers' && $setting->ran_once == true) {
                $run = false;
                $this->info("Existing resellers have been synced with default features and modules from backend.");
            }

        }

        if($run) {

            $resellers = Reseller::where('role','reseller')->get();

            if(!$resellers->isEmpty()) {

                foreach($resellers as $reseller) {
                    $this->syncDefaultsForReseller($reseller);
                    $this->info("Features and modules for {$reseller->firstname}");
                }

            } else {
                $this->info("No resellers found.");
            }

            DB::table('changes')->insert([
                'method' => 'modulesAndFeaturesForExistingResellers',
                'ran_once' => true
            ]);

        }

    }

    private function firedAtTimestampsForAnalytics() {

        $this->dispatchJob((new NewTimestampForFiredAtJob('App\Models\Analytics\AnalyticsEvent'))->delay(60*8),'analytics');
        $this->dispatchJob((new NewTimestampForFiredAtJob('App\Models\Analytics\AnalyticsScan'))->delay(60*8),'analytics');
        $this->info("Updating analytics fired_at_timestamp");

    }

    private function migrateOldAnalytics() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'migrateOldAnalytics' && $setting->ran_once == true) {
                $run = false;
                $this->info("Old analytics already migrated.");
            }

        }

        if($run) {

            $tenants = Tenant::all();

            if(!$tenants->isEmpty()) {

                foreach($tenants as $tenant) {
                    $this->dispatchJob((new MigrateOldAnalyticsJob($tenant->id))->delay(60*8),'analytics');
                    $this->info("Migrating analytics for {$tenant->name}");
                }

            } else {
                $this->info("No tenants found.");
            }

            DB::table('changes')->insert([
                'method' => 'migrateOldAnalytics',
                'ran_once' => true
            ]);

        }

    }

    private function removeNullFromEdwardButton() {

        $designs = Design::where('edward_button',null)->orWhere('edward_button','')->get();
        $designs->each(function($item,$key) {
            $item->edward_button = [
                'color' => '',
                'font-size' => '',
                'font-weight' => '',
                'text-transform' => '',
                'background-color' => '',
                'border-color' => '',
                'border-width' => '',
                'border-radius' => '',
                'padding-top' => '',
                'padding-right' => '',
                'padding-bottom' => '',
                'padding-left' => '',
            ];
            $item->save();
        });

    }

    private function addEmptyArrayToFonts() {

        $designs = Design::where('fonts','')->orWhere('fonts',null)->get();

        if(!$designs->isEmpty()) {

            foreach($designs as $design) {
                $design->fonts = [];
                $design->save();
            }

            $this->info('Updated empty font strings to arrays.');

        }

    }

    private function addDesignToEdwardApp() {

        $designs = Design::where('edward_app','')->orWhere('edward_app',null)->get();

        if(!$designs->isEmpty()) {

            foreach($designs as $design) {
                $design->edward_app = [
                	'background' => '',
                	'padding-left' => '',
                	'padding-right' => ''
                ];
                $design->save();
            }

            $this->info('Updated empty edward_app strings to arrays.');

        }

    }

    private function removeDefaultHeightsForHeaderFooter() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'removeDefaultHeightsForHeaderFooter' && $setting->ran_once == true) {
                $run = false;
                $this->info("Header and footer logo height defaults already removed.");
            }

        }

        if($run) {

            $designs = Design::where('header_logo_height','default')->get();

            if(!$designs->isEmpty()) {

                foreach($designs as $design) {

                    $design->header_logo_height = null;
                    $design->save();

                }

            }

            $designs = Design::where('footer_logo_height','default')->get();

            if(!$designs->isEmpty()) {

                foreach($designs as $design) {

                    $design->footer_logo_height = null;
                    $design->save();

                }

            }

            DB::table('changes')->insert([
                'method' => 'removeDefaultHeightsForHeaderFooter',
                'ran_once' => true
            ]);

        }

    }

    private function modifyDesignSettingsHeaderFooter() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'modifyDesignSettingsHeaderFooter' && $setting->ran_once == true) {
                $run = false;
                $this->info("Header and footer design already modified.");
            }

        }

        if($run) {

            $tenants = Tenant::where('tenant_design_id','!=',null)->get();

            if(!$tenants->isEmpty()) {

                foreach($tenants as $tenant) {

                    $tenant->tenantDesign->footer_format = 'all_center';
                    $tenant->tenantDesign->header_logo_settings = [
                        'type' => 'none'
                    ];
                    $tenant->tenantDesign->footer_logo_settings = [
                        'type' => 'none'
                    ];
                    $tenant->tenantDesign->footer_logo = 'visible';
                    $tenant->tenantDesign->footer_logo_height = '50px';
                    $tenant->tenantDesign->header_logo_height = '50px';
                    $tenant->tenantDesign->save();

                    $td = Design::find($tenant->tenantDesign->id);

                    $h = $td->header;
                    $h['height'] = '70px';
                    $h['padding-top'] = '10px';
                    $h['padding-left'] = '10px';
                    $h['padding-right'] = '10px';
                    $h['padding-bottom'] = '10px';
                    $td->header = $h;

                    $f = $td->footer;

                    $f['height'] = '70px';
                    $f['font-size'] = '11px';
                    $f['padding-top'] = '10px';
                    $f['padding-left'] = '10px';
                    $f['padding-right'] = '10px';
                    $f['padding-bottom'] = '10px';

                    $td->footer = $f;

                    $td->save();


                    DesignService::updateLastModified($tenant->id,new Tenant());

                    $this->info('Adjusted design for '.$tenant->name);

                }

            }

            $pois = Poi::where('poi_design_id','!=',null)->get();

            if(!$pois->isEmpty()) {

                foreach($pois as $poi) {

                    $poi->poiDesign->footer_format = 'default';
                    $poi->poiDesign->header_logo_settings = [
                        'type' => 'default'
                    ];
                    $poi->poiDesign->footer_logo_settings = [
                        'type' => 'default'
                    ];
                    $poi->poiDesign->footer_logo = 'default';
                    $poi->poiDesign->footer_logo_height = '50px';
                    $poi->poiDesign->header_logo_height = '50px';
                    $poi->poiDesign->save();

                    $pd = Design::find($poi->poiDesign->id);

                    $h = $pd->header;
                    $h['height'] = '70px';
                    $h['padding-top'] = '10px';
                    $h['padding-left'] = '10px';
                    $h['padding-right'] = '10px';
                    $h['padding-bottom'] = '10px';
                    $pd->header = $h;

                    $f = $pd->footer;

                    $f['height'] = '70px';
                    $f['font-size'] = '11px';
                    $f['padding-top'] = '10px';
                    $f['padding-left'] = '10px';
                    $f['padding-right'] = '10px';
                    $f['padding-bottom'] = '10px';

                    $pd->footer = $f;

                    $pd->save();

                    DesignService::updateLastModified($poi->tenant_id,new Poi());

                    $this->info('Adjusted design for '.$poi->name);

                }

            }

            DB::table('changes')->insert([
                'method' => 'modifyDesignSettingsHeaderFooter',
                'ran_once' => true
            ]);

        }

    }

    private function refactorEntityRelations() {

        $run = true;

        foreach($this->settings as $setting) {

            if($setting->method == 'refactorEntityRelations' && $setting->ran_once == true) {
                $run = false;
                $this->info("Entities already refactored.");
            }

        }

        if($run) {

            $users = User::all();

            if(!$users->isEmpty()) {

                foreach($users as $user) {

                    $this->info("Refactoring entities for ".$user->firstname." ".$user->lastname);
                    UserService::removeAccessFromAllEntities($user);
                    UserService::giveAccessToAllEntities($user);

                }

            }

            DB::table('changes')->insert([
                'method' => 'refactorEntityRelations',
                'ran_once' => true
            ]);

        }

    }

    private function addLocaleToTenant() {

        $tenants = Tenant::where('locale',null)->get();

        if(!$tenants->isEmpty()) {

            foreach($tenants as $tenant) {

                if($tenant->fallback_language == 'en') {
                    $tenant->locale = $tenant->fallback_language.'_US';
                } else {
                    $tenant->locale = $tenant->fallback_language.'_'.strtoupper($tenant->fallback_language);
                }

                $tenant->save();
                Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
                TenantService::updateLastModified($tenant->id,new Tenant());
                $this->publishTenantToEdwardStone($tenant->id,true);
                $this->info('Created locale for '.$tenant->name);

            }

        }

    }

    private function addDesignsToPois() {

        $pois = Poi::where('poi_design_id',null)->get();

        if(!$pois->isEmpty()) {

            foreach($pois as $poi) {

                $designController = new DesignController();
                $design = $designController->store(DesignService::getPoiDesignBlueprint(), 'App\Models\PoiDesign',$poi->tenant_id);
                $poi->poi_design_id = $design->id;
                $poi->save();

                DesignService::updateLastModified($poi->tenant_id,new Poi());

                $this->info('Created design for '.$poi->name);

            }

        }

    }

    private function adjustDesignsToNewModel() {

        $designs = Design::where('header_fixed',1)->orWhere('header_fixed',0)->get();

        if(!$designs->isEmpty()) {

            foreach($designs as $design) {

                if($design->header_fixed == '1') {
                    $design->header_fixed = 'fixed';
                } else if ($design->header_fixed == '0') {
                    $design->header_fixed = 'scrollable';
                } else {
                    $design->header_fixed = 'default';
                }

                if($design->header_logo == '1') {
                    $design->header_logo = 'visible';
                } else if ($design->header_logo == '0') {
                    $design->header_logo = 'invisible';
                } else {
                    $design->header_logo = 'default';
                }

                if($design->header_title == '1') {
                    $design->header_title = 'visible';
                } else if ($design->header_title == '0') {
                    $design->header_title = 'invisible';
                } else {
                    $design->header_title = 'default';
                }

                if($design->header_visible == '1') {
                    $design->header_visible = 'visible';
                } else if ($design->header_visible == '0') {
                    $design->header_visible = 'invisible';
                } else {
                    $design->header_visible = 'default';
                }

                $design->save();
                $this->info('Updated design to new data model.');

            }

        }

    }

    private function addLogoSettingsToTenant() {

        $tenants = Tenant::where('logo_settings',null)->get();

        if(!$tenants->isEmpty()) {

            foreach($tenants as $tenant) {

                if($tenant->logo != null || $tenant->logo != '') {

                    $tenant->logo_settings = [
                        'type' => 'url',
                        'url' => $tenant->logo
                    ];

                } else {

                    $tenant->logo_settings = [
                        'type' => 'none',
                    ];

                }

                $tenant->save();
                Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
                TenantService::updateLastModified($tenant->id,new Tenant());
                $this->publishTenantToEdwardStone($tenant->id,true);
                $this->info('Created logo settings for '.$tenant->name);

            }

        }

    }

    private function removeNullValuesFromTenant() {

        $nullableOptions = [
            'url',
            'css',
            'logo',
            'imprint',
            'email',
            'ga_tracking_id',
            'ga_utm_source',
        ];

        foreach($nullableOptions as $nullableOption) {

            $tenants = Tenant::where($nullableOption,null)->get();

            if(!$tenants->isEmpty()) {

                foreach($tenants as $tenant) {

                    $tenant->$nullableOption = '';
                    $tenant->save();
                    Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
                    DesignService::updateLastModified($tenant->id,new Tenant());
                    $this->publishTenantToEdwardStone($tenant->id,true);
                    $this->info('Replaced nullable fields with empty strings fields for '.$tenant->name);

                }

            }

        }

    }

    private function addDesignsToTenants() {

        $tenants = Tenant::where('tenant_design_id',null)->get();

        if(!$tenants->isEmpty()) {

            foreach($tenants as $tenant) {

                $designController = new DesignController();
                $design = $designController->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',$tenant->id);
                $tenant->tenant_design_id = $design->id;
                $tenant->save();

                $cacheKey = $this->getTenantRootCacheKey('tenant-design',$tenant->id);
                Cache::forever($cacheKey,$design);
                Cache::forever($this->getTenantRootCacheKey('tenant',$tenant->id),$tenant);
                DesignService::updateLastModified($tenant->id,new Tenant());
                $this->publishTenantToEdwardStone($tenant->id,true);

                $this->info('Created design for '.$tenant->name);

            }

        }

    }

}
