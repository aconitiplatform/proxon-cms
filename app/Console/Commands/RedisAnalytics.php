<?php

namespace App\Console\Commands;

use Log;
use App;

use App\Models\Analytics\AnalyticsScan;
use App\Models\Analytics\AnalyticsEvent;

use App\Jobs\Analytics\Traits\CachesCounts;

class RedisAnalytics extends BaseCommand {

    protected $signature = 'redis-analytics';

    protected $description = 'Poke my shoulder to see if I am still alive...';

    protected $localUuids = [
        '004c574e-ee90-421d-941c-bb8f7a5df332',
        '1bc0622e-d07f-4f67-99cc-74a278b85003',
        'd3e088cc-d5c0-4537-a992-3a186d2aa168',
        'cf4d28c5-93a0-4843-b817-2ac487248a37',
        '77aae48b-2179-4c32-a895-359203550c5e',
        '8938ff90-e050-44cb-904c-4099da8de2ad',
        '3987cbc0-14e1-43c9-b3a2-5bc117992eb8',
        'c239827d-2cc5-4b95-8f31-22ef4949feeb',
        '79fac91c-b274-4dfe-9e0e-2ca40845bb7d',
        '9b2c0921-92fb-467d-9f1f-a9f3b4c73235',
    ];

    protected $chunk = 500;

    public function handle() {

        AnalyticsEvent::chunk($this->chunk, function($events) {

            $this->info("caching {$this->chunk} events (poi/channel)...\n");

            $bar = $this->output->createProgressBar($events->count());

            foreach($events as $event) {

                $journeyId = (App::environment('local')) ? $this->localUuids[rand(0,9)] : $event->journey_id;

                CachesCounts::increaseCounts($event->tenant_id,$event->fired_at,$event->poi_id,$journeyId,$event->url,$event->accessed,$event->from,$event->event);
                $bar->advance();

            }

            $bar->finish();

            $this->info("\ncached {$this->chunk} events.\n");

        });

        AnalyticsScan::chunk($this->chunk, function($events) {

            $this->info("caching {$this->chunk} scans...\n");

            $bar = $this->output->createProgressBar($events->count());

            foreach($events as $event) {

                CachesCounts::increaseCounts($event->tenant_id,$event->fired_at,$event->poi_id,$event->journey_id,$event->url);
                $bar->advance();

            }

            $bar->finish();

            $this->info("\ncached {$this->chunk} scans.\n");

        });

    }

}
