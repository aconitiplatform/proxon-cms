<?php

namespace App\Console\Commands;

use App\Models\Tenant;

use App\Jobs\PrepareChartDataJob;
/**
 * @deprecated
 */
class ChartDispatcher extends BaseCommand {

    protected $signature = 'chartdispatcher';

    protected $description = 'Dispatch jobs to generate chart data for tenants.';

    public function handle() {

        $tenants = Tenant::all();

        if(!$tenants->isEmpty()) {
            foreach($tenants as $tenant) {
                $job = (new PrepareChartDataJob($tenant->id))->onQueue(config('substance.queue_prefix').'firebase');
                dispatch($job);
            }

        }

    }
}
