<?php

namespace App\Console\Commands;

use App\Models\User;

use App\Console\Traits\SelectsEntitiesByChoice;
use App\Console\Traits\CreatesInvoices;

class CreateTenantInvoice extends BaseCommand {

    use SelectsEntitiesByChoice, CreatesInvoices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenants:invoice {--single}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a tenant invoice.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $superadmin = User::where('role','superadmin')->first();
        $tenantId = $this->selectTenant($superadmin);

        if($this->option('single')) {

            $amountOfNewBeacons = $this->ask('How many beacons will this invoice contain?');
            $this->createSingleTenantInvoice($tenantId,$amountOfNewBeacons);

        } else {
            $this->createMonthlyTenantInvoice($tenantId);
        }


    }

}
