<?php

namespace App\Console\Commands;


use App\Models\Tenant;
use App\Models\User;
use App\Models\Beacon;

use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Controllers\DesignController;

use App\Http\Services\BeaconService;
use App\Http\Services\Apis\KontaktIoService;
use App\Http\Services\DesignService;

use App\Jobs\DeliverTenantToDistributionJob;
use App\Jobs\SendTenantDataToAdminJob;

use App\Traits\SyncsFeaturesAndModules;
use App\Traits\CreatesTenantBeacons;
use App\Console\Traits\SelectsEntitiesByChoice;

class CreateNewTenant extends BaseCommand {

    use SyncsFeaturesAndModules, CreatesTenantBeacons, SelectsEntitiesByChoice;

    protected $signature = 'tenants:new';

    protected $description = 'Create a new tenant with an admin user and beacons through the command line.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $tenantDetailsCorrect = false;
        $userDetailsCorrect = false;
        $withKontaktIo = $this->confirm('Do you want to create a venue at Kontakt.IO for this tenant?');

        while(!$tenantDetailsCorrect) {

            $tenant = $this->askForTenantDetails($withKontaktIo);
            if(!$tenant) {
                $this->info('A slug may only consist of letters (a-z), numbers, and dashes.');
            } else {
                $tenantDetailsCorrect = $this->confirm('Is the displayed information correct?');
            }

        }

        $tenant->save();

        $designController = new DesignController();
        $design = $designController->store(DesignService::getTenantDesignBlueprint(), 'App\Models\TenantDesign',$tenant->id);

        $tenant->tenant_design_id = $design->id;
        $tenant->save();

        if($withKontaktIo) {

            $venue = KontaktIoService::createVenue($tenant->name,$tenant->id." (Substance Tenant ID)");

            if($venue == null) {
                $this->error('Could not create venue at Kontakt IO');
            } else {
                $tenant->kontakt_io_venue_id = $venue->id;
                $tenant->save();
                $this->info('Venue with name '.$tenant->name.' and description '.$tenant->id.' created.');
            }

        }

        if(!$withKontaktIo) {
            //move to beacon sync
            $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));
        }

        while(!$userDetailsCorrect) {

            $user = $this->askForUserDetails($tenant);
            $userDetailsCorrect = $this->confirm('Is the displayed information correct?');

        }

        $user->save();

        $beaconInfos = $this->createTenantBeacons($tenant,true);

        if(!$withKontaktIo) {
            //move to beacon sync
            $passwordResetController = new PasswordResetController();
            $passwordResetController->triggerInternalSendResetLinkEmail($user->email,true);
            $this->dispatchJob(new SendTenantDataToAdminJob($tenant,$user,$beaconInfos));

        }

        $this->syncDefaults($tenant);

        $this->info('Tenant '.$tenant->name.' has been created.');

    }

    private function askForTenantDetails($withKontaktIo) {

        $tenant = new Tenant();
        $tenant->name = $this->ask("What is the tenant's name?");

        $tenant->slug = str_slug($tenant->name);
        $tenant->email = $this->ask("What is the tenants's email address?");
        $tenant->fallback_language = $this->anticipate("What is the tenant's fallback language?", ['de', 'en']);
        $tenant->locale = $this->ask("What is the tenant's standard locale (Format: xx_YY)?");
        $tenant->allowed_beacons = $this->ask("How many beacons can the tenant use?");
        $tenant->active = !$withKontaktIo;
        $tenant->url = '';
        $tenant->css = '';
        $tenant->logo = '';
        $tenant->imprint = '';
        $tenant->privacy = '';
        $tenant->logo_settings = [
            'type' => 'none',
        ];
        $tenant->copyright_label = '';
        $tenant->first_login = true;

        if($this->confirm('Does the tenant have a reseller?')) {
            $tenant->reseller_id = $this->selectReseller();
        }

        if($this->confirm('Does the tenant have an own URL for Substance?')) {
            $tenant->substance_url = $this->ask("What is the tenants's Substance URL?");
        }

        if($this->confirm('Does the tenant have an own URL for Edward Stone?')) {
            $tenant->edward_stone_url = $this->ask("What is the tenants's Edward Stone URL?");
        }

        $this->info('Tenant Name => '.$tenant->name);
        $this->info('Tenant Slug => '.$tenant->slug);
        $this->info('Tenant Email => '.$tenant->email);
        $this->info('Tenant Language => '.$tenant->fallback_language);
        $this->info('Tenant Locale => '.$tenant->locale);
        $this->info('Tenant Allowed Beacons => '.$tenant->allowed_beacons);
        $this->info('Tenant Reseller => '.$tenant->reseller_id);

        return $tenant;

    }

    private function askForUserDetails($tenant) {

        $user = new User();
        $user->firstname = $this->ask("What is the user's first name?");
        $user->lastname = $this->ask("What is the user's last name?");
        $user->email = $this->ask("What is the user's email address?");
        if($this->confirm("Do you want to set a password now?")) {
            $password = $this->ask("What is the user's password?");
            $user->password = bcrypt($password);
        } else {
            $user->password = bcrypt(config('app.key').time());
        }
        $user->language = $tenant->fallback_language;
        $user->locale = $tenant->locale;
        $user->role = 'admin';
        $user->tenant_id = $tenant->id;
        $user->active = true;
        $user->email_confirmed = true;

        $this->info('User Name => '.$user->firstname.' '.$user->lastname);
        $this->info('User Email => '.$user->email);
        $this->info('User Password => '.$user->password);

        return $user;

    }

}
