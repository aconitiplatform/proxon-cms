<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Tenant;

use App\Console\Traits\CreatesInvoices;

class CreateMonthlyInvoices extends BaseCommand {

    use CreatesInvoices;

    protected $signature = 'invoices:monthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all monthly invoices.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $tenants = Tenant::where('trial',false)->get();

        if(!$tenants->isEmpty()) {

            foreach($tenants as $tenant) {
                $this->createMonthlyTenantInvoice($tenant->id,true);
            }

        }

        $resellers = User::where('role','reseller')->get();

        if(!$resellers->isEmpty()) {

            foreach($resellers as $reseller) {
                $this->createMonthlyResellerInvoice($reseller->id,true);
            }

        }

    }

}
