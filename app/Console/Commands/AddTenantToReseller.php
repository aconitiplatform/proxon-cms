<?php

namespace App\Console\Commands;

use Faker;

use App\Models\User;
use App\Models\Tenant;
use App\Models\Billable;

use App\Jobs\DeliverTenantToDistributionJob;

use App\Console\Traits\SelectsEntitiesByChoice;
use App\Console\Traits\AsksForBillingDetails;

class AddTenantToReseller extends BaseCommand {

    use SelectsEntitiesByChoice, AsksForBillingDetails;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenants:add-reseller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a reseller to a tenant.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $superadmin = User::where('role','superadmin')->first();

        if($superadmin == null) {
            $this->error('Please create a superadmin first by calling php artisan make:superadmin');
        } else {

            try {

                if($this->confirm('Does the reseller already have a customer id at easybill?')) {
                    $easybillId = $this->selectCustomer();
                } else {
                    $easybillId = null;
                }

                $resellerId = $this->selectReseller();
                $tenantId = $this->selectTenant($superadmin);

                $this->createBillingDetails($tenantId,'reseller',$resellerId,$easybillId);
                $this->info('Tenant updated with new reseller.');

            } catch(\Exception $e) {
                $this->error('Problem with reseller or tenant.');
            }

        }

    }

}
