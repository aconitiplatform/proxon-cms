<?php

namespace App\Console\Commands;

use Faker;

use App\Models\User;
use App\Models\Tenant;

use App\Jobs\DeliverTenantToDistributionJob;

use App\Console\Traits\AsksForBillingDetails;
use App\Console\Traits\SelectsEntitiesByChoice;
use App\Console\Traits\CreatesInvoices;

class EndTrial extends BaseCommand {

    use AsksForBillingDetails, SelectsEntitiesByChoice, CreatesInvoices;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenants:end-trial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'End a trial period.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $superadmin = User::where('role','superadmin')->first();

        if($superadmin == null) {
            $this->error('Please create a superadmin first by calling php artisan make:superadmin');
        } else {

            try {

                $tenantId = $this->selectTenant($superadmin,true);

                $tenant = Tenant::findOrFail($tenantId);
                $tenant->trial = false;
                $tenant->save();
                $this->info('Tenant not on trial anymore.');

                $this->dispatchJob(new DeliverTenantToDistributionJob($tenant->id));

                if($this->confirm('Does the reseller or tenant already have a customer id at easybill?')) {
                    $easybillId = $this->selectCustomer();
                } else {
                    $easybillId = null;
                }

                if($this->confirm('Does the tenant have a reseller?')) {
                    $resellerId = $this->selectReseller();
                    $this->createBillingDetails($tenant->id,'reseller',$resellerId,$easybillId);

                    if($this->confirm('Should the reseller be billed for the rest of the month?')) {
                        $this->createSingleResellerInvoice($resellerId,$tenant->id,$tenant->allowed_beacons);
                    }

                } else {
                    $this->createBillingDetails($tenant->id,'tenant',$tenant->id,$easybillId);

                    if($this->confirm('Should the tenant be billed for the rest of the month?')) {
                        $this->createSingleTenantInvoice($tenant->id,$tenant->allowed_beacons);
                    }

                }

            } catch(\Exception $e) {
                $this->error('Problem with tenant.');
            }

        }

    }



}
