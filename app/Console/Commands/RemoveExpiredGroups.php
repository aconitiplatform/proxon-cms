<?php

namespace App\Console\Commands;

use Faker;

use App\Models\Poi;

use App\Jobs\RemoveExpiredGroupsJob;


class RemoveExpiredGroups extends BaseCommand {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pois:expired-groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for expired groups and remove them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        Poi::chunk(50, function($pois) {

            foreach($pois as $poi) {

                $this->dispatchJob(new RemoveExpiredGroupsJob($poi),'pois');

            }

        });

    }



}
