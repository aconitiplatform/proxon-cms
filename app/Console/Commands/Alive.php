<?php

namespace App\Console\Commands;

use Log;

class Alive extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'substance:alive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Poke my shoulder to see if I am still alive...';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('I feel so alive, for the very first time. I can\'t deny you. I feel so alive.');
    }
}
