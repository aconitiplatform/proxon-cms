<?php

namespace App\Console\Commands;

use Carbon\Carbon;

use App\Models\User;
use App\Models\EmailConfirmation;

use App\Events\UserEmailHasChanged;

use App\Jobs\ConfirmEmailJob;

class SendEmailConfirmationReminders extends BaseCommand {

    protected $signature = 'inform:email-confirmation';

    protected $description = 'Send a reminder to all users with unconfirmed emails.';

    public function handle() {

        $users = User::where('active',true)->where('email_confirmed',false)->get();

        if(!$users->isEmpty()) {

            foreach($users as $user) {

                $now = Carbon::now();
                $confirmationSentAt = Carbon::parse($user->email_confirmation_sent_at);

                if($confirmationSentAt->diffInDays($now) <= config('substance.email.confirmation_period')) {

                    $confirmation = EmailConfirmation::where('user_id',$user->id)->first();

                    if($confirmation != null) {
                        $this->dispatchJob(new ConfirmEmailJob($user,$confirmation),'emails');
                    } else {
                        event(new UserEmailHasChanged($user));
                    }

                }

            }

        }

    }

}
