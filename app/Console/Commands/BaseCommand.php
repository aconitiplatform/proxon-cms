<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Traits\WorksWithQueue;

class BaseCommand extends Command {

    use WorksWithQueue;

}
