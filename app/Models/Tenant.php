<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Beacon;
use App\Models\BeaconBatch;
use App\Models\Billable as SubstanceBillable;

use App\Traits\HasUuidForKey;

class Tenant extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'tenants';

    protected $fillable = [
        'name',
        'url',
        'css',
        'logo',
        'imprint',
        'email',
        'fallback_language',
        'locale',
        'ga_tracking_id',
        'ga_utm_source',
        'substance_url',
        'edward_stone_url',
        'logo_settings',
        'privacy',
        'copyright_label',
    ];

    protected $guarded = [
        'id',
        'allowed_beacons',
        'active',
        'created_at',
        'updated_at',
        'slug',
        'tenant_billing_info_id',
        'tenant_design_id',
        'trial',
        'request_limit',
        'reseller_id',
        'initial_kontakt_io_sync',
        'stripe_customer_id',
        'stripe_token_id',
        'stripe_plan',
        'stripe_subscription',
        'canceled_at',
        'card_brand',
        'card_last_four',
        'registered',
        'first_login',
    ];

    protected $hidden = [
        'tenant_billing_info_id',
        'tenant_design_id',
        'reseller_id',
        'stripe_customer_id',
        'registered',
    ];

    protected $casts = [
        'active' => 'boolean',
        'logo_settings' => 'array',
        'trial' => 'boolean',
        'initial_kontakt_io_sync' => 'boolean',
        'first_login' => 'boolean',
    ];

    protected $appends = [
        'used_beacons',
        'dash_locale',
        'billable',
        'syncable_batches',
    ];

    public function getDashLocaleAttribute() {

        return str_replace('_','-',$this->locale);

    }

    public function getUsedBeaconsAttribute() {

        $count = Beacon::where('tenant_id',$this->id)->whereNotNull('poi_id')->count();
        return $count;

    }

    public function getBillableAttribute() {

        if(!$this->trial && !$this->registered) {

            $billable = SubstanceBillable::where('tenant_id',$this->id)->first();
            if($billable != null) {
                return $billable->billable_object_type;
            }

        } else if ($this->registered) {
            return 'stripe';
        }

        return 'trial';

    }

    public function getSyncableBatchesAttribute() {

        $batchesCount = BeaconBatch::where('tenant_id',$this->id)->where('synced',false)->count();

        if($batchesCount == 0) {
            return false;
        }
        return true;

    }

    public function tenantDesign() {
        return $this->hasOne('App\Models\TenantDesign','id','tenant_design_id');
    }

    public function users() {
        return $this->hasMany('App\Models\User', 'tenant_id');
    }

    public function reseller() {
        return $this->hasOne('App\Models\Reseller','id','reseller_id');
    }

    public function pois() {
        return $this->hasMany('App\Models\Poi', 'tenant_id');
    }

    public function beacons() {
        return $this->hasMany('App\Models\Beacon', 'tenant_id');
    }

    public function imprints() {
        return $this->hasMany('App\Models\Imprint', 'tenant_id');
    }

    public function carts() {
        return $this->hasMany('App\Models\Cart', 'tenant_id');
    }

    public function beaconBatches() {
        return $this->hasMany('App\Models\BeaconBatch', 'tenant_id');
    }

    public function analyticsEvents() {
        return $this->hasMany('App\Models\Analytics\AnalyticsEvent', 'tenant_id');
    }

    public function analyticsScans() {
        return $this->hasMany('App\Models\Analytics\AnalyticsScan', 'tenant_id');
    }

    public function analyticsJourneys() {
        return $this->hasMany('App\Models\Analytics\AnalyticsJourney', 'tenant_id');
    }

    public function poiLocks() {
        return $this->hasMany('App\Models\PoiLock','tenant_id');
    }

    public function features() {
        return $this->belongsToMany('App\Models\Features\Feature', 'feature_tenant', 'tenant_id', 'feature_id');
    }

    public function modules() {
        return $this->belongsToMany('App\Models\Features\Module', 'module_tenant', 'tenant_id', 'module_id');
    }

}
