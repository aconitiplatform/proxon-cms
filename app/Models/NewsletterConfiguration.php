<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class NewsletterConfiguration extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'newsletter_configurations';

    protected $fillable = [
        'name', 'tenant_id', 'type', 'configuration', 'user_id',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'user_id', 'tenant_id',
    ];

    protected $casts = [
        'configuration' => 'array',
    ];

    public function users() {
        return $this->belongsToMany('App\Models\User', 'newsletter_configuration_user', 'newsletter_configuration_id', 'user_id');
    }

}
