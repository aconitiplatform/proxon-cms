<?php

namespace App\Models\Features;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Module extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'modules';

    protected $fillable = [
        'name',
        'slug',
        'label',
        'type',
        'group',
        'data',
        'icon',
        'tags',
        'default',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
    ];

    protected $casts = [
        'data' => 'object',
        'icon' => 'object',
        'hidden' => 'boolean',
        'default' => 'boolean',
    ];

    protected $appends = [
        'hidden'
    ];

    public function getLabelAttribute($value) {
        return trans($value);
    }

    public function getGroupAttribute($value) {
        return trans($value);
    }

    public function getHiddenAttribute() {
        //temporary
        if($this->type == "vag" || $this->type == "micro_payment") {
            return true;
        }
        return false;

    }

    public function features() {
        return $this->belongsToMany('App\Models\Features\Feature', 'feature_module', 'module_id', 'feature_id');
    }

    public function tenants() {
        return $this->belongsToMany('App\Models\Tenant', 'module_tenant', 'module_id', 'tenant_id');
    }

}
