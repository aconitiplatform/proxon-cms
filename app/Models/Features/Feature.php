<?php

namespace App\Models\Features;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Feature extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'features';

    protected $fillable = [
        'name',
        'slug',
        'default',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    protected $appends = [
    ];

    public function modules() {
        return $this->belongsToMany('App\Models\Features\Module', 'feature_module', 'feature_id', 'module_id');
    }

    public function tenants() {
        return $this->belongsToMany('App\Models\Tenant', 'feature_tenant', 'feature_id', 'tenant_id');
    }

}
