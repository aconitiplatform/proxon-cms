<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Models\MicroPaymentTransaction;

use App\Traits\HasUuidForKey;

class MicroPayment extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'micro_payments';

    protected $fillable = [
        'name',
        'description',
        'tenant_id',
        'amount',
        'type',
        'currency',
        'allowed_payment_types',
        'fields',
        'emails_payee',
        'emails_payer',
        'email',
        'callback_url',
        'heartbeat_url',
        'user_id',
    ];

    protected $hidden = [
        'pivot', 'stripe_product_id',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'stripe_product_id',
    ];

    protected $appends = [
        'checks_heartbeat'
    ];

    protected $casts = [
        'allowed_payment_types' => 'array',
        'fields' => 'array',
        'emails_payee' => 'boolean',
        'emails_payer' => 'boolean'
    ];

    public function getChecksHeartbeatAttribute() {

        if($this->heartbeat_url != null) {
            return true;
        } else {
            return false;
        }

    }

    public function transactions() {
        return $this->hasMany('App\Models\MicroPaymentTransaction','micro_payment_id');
    }

}
