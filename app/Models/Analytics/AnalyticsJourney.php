<?php

namespace App\Models\Analytics;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class AnalyticsJourney extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'analytics_journeys';

    protected $fillable = [
        'tenant_id', 'journey_id',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function events() {
        return $this->hasMany('App\Models\Analytics\AnalyticsEvent','journey_id');
    }

    public function scans() {
        return $this->hasMany('App\Models\Analytics\AnalyticsScan','journey_id');
    }

}
