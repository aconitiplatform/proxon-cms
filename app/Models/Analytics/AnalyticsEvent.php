<?php

namespace App\Models\Analytics;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class AnalyticsEvent extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'analytics_events';

    protected $fillable = [
        'tenant_id',
        'poi_id',
        'journey_id',
        'accessed',
        'url',
        'user_agent',
        'from',
        'event',
        'os',
        'fired_at',
        'fired_at_timestamp',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function journey() {
        return $this->belongsTo('App\Models\Analytics\AnalyticsJourney');
    }

}
