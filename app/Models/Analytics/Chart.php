<?php

namespace App\Models\Analytics;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Chart extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'charts';

    protected $fillable = [
        'key', 'value',
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'value' => 'array',
    ];

}
