<?php

namespace App\Models\Analytics;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class AnalyticsScan extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'analytics_scans';

    protected $fillable = [
        'tenant_id',
        'poi_id',
        'journey_id',
        'url',
        'user_agent',
        'fired_at',
        'fired_at_timestamp',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function journey() {
        return $this->belongsTo('App\Models\Analytics\AnalyticsJourney');
    }

}
