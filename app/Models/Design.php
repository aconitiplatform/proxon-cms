<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class Design extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'designs';

    protected $fillable = [
        'header',
        'header_fixed',
        'header_logo',
        'header_title',
        'header_format',
        'header_visible',
        'h1',
        'h2',
        'h3',
        'body',
        'footer',
        'tenant_id',
        'header_logo_settings',
        'footer_logo_settings',
        'footer_format',
        'footer_logo',
        'header_logo_height',
        'footer_logo_height',
        'fonts',
        'edward_button',
        'edward_app'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at', 'tenant_id',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'header' => 'array',
        'h1' => 'array',
        'h2' => 'array',
        'h3' => 'array',
        'body' => 'array',
        'footer' => 'array',
        'header_logo_settings' => 'array',
        'footer_logo_settings' => 'array',
        'fonts' => 'array',
        'edward_button' => 'array',
        'edward_app' => 'array'
    ];

    // public function setFontsAttribute($value) {
    //
    //     if(!is_array($value)) {
    //         return [];
    //     }
    //
    // }

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant','tenant_id','id');
    }

}
