<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class User extends Authenticatable {

    use HasUuidForKey, BelongsToTenant;

    protected $table = 'users';

    public $incrementing = false;

    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'language', 'locale', 'active', 'role', 'email_confirmed', 'email_confirmation_sent_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'active' => 'boolean',
        'email_confirmed' => 'boolean',
    ];

    protected $appends = [
        'is_super_admin',
        'is_tenant_admin',
        'dash_locale',
    ];

    public function getDashLocaleAttribute() {

        return $this->locale == null ? null : str_replace('_','-',$this->locale);

    }

    public function getIsSuperAdminAttribute() {
        return ($this->role == 'superadmin');
    }

    public function getIsTenantAdminAttribute() {
        return ($this->role == 'admin' || $this->role == 'superadmin');
    }

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant');
    }

    public function pois() {
        return $this->belongsToMany('App\Models\Poi', 'poi_user', 'user_id', 'poi_id');
    }

    public function beacons() {
        return $this->belongsToMany('App\Models\Beacon', 'beacon_user', 'user_id', 'beacon_id');
    }

    public function microPayments() {
        return $this->belongsToMany('App\Models\MicroPayment', 'micro_payment_user', 'user_id', 'micro_payment_id');
    }

    public function newsletterConfigurations() {
        return $this->belongsToMany('App\Models\NewsletterConfiguration', 'newsletter_configuration_user', 'user_id', 'nlc_id');
    }

    public function surveys() {
        return $this->belongsToMany('App\Models\Survey', 'survey_user', 'user_id', 'survey_id');
    }

    public function media() {
        return $this->belongsToMany('App\Models\Media', 'media_user', 'user_id', 'media_id');
    }

    public function poiLocks() {
        return $this->hasMany('App\Models\PoiLock','user_id');
    }

}
