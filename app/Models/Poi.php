<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class Poi extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $fillable = [
        'name',
        'tenant_id',
        'description',
        'meta',
        'has_channel',
        'active',
        'fallback_language',
        'has_sweepstake',
        'has_feedback',
        'has_contact',
        'has_micro_payment',
        'show',
        'user_id',
		'overwrite_imprint',
		'imprint',
		'overwrite_privacy',
		'privacy',
		'overwrite_copyright',
		'copyright',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'poi_design_id',
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'meta' => 'array',
        'has_sweepstake' => 'boolean',
        'has_feedback' => 'boolean',
        'has_contact' => 'boolean',
        'has_micro_payment' => 'boolean',
        'active' => 'boolean',
        'has_channel' => 'boolean',
		'overwrite_imprint' => 'boolean',
		'overwrite_privacy' => 'boolean',
		'overwrite_copyright' => 'boolean',
    ];

    public function tenant() {
        return $this->hasOne('App\Models\Tenant','id','tenant_id');
    }

    public function poiDesign() {
        return $this->hasOne('App\Models\PoiDesign','id','poi_design_id');
    }

    public function poiContents() {
        return $this->hasMany('App\Models\PoiContent','poi_id');
    }

    public function poiLocks() {
        return $this->hasMany('App\Models\PoiLock','poi_id');
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'poi_user', 'poi_id', 'user_id');
    }

    public function beacons() {
        return $this->hasMany('App\Models\Beacon','poi_id');
    }

    public function poiEvents() {
        return $this->hasMany('App\Models\PoiEvent','poi_id');
    }

    public function analyticsScans() {
        return $this->hasMany('App\Models\Analytics\AnalyticsScan','poi_id');
    }

    public function analyticsEvents() {
        return $this->hasMany('App\Models\Analytics\AnalyticsEvent','poi_id');
    }

    public function media() {
        return $this->belongsToMany('App\Models\Media', 'media_poi', 'poi_id', 'media_id');
    }

}
