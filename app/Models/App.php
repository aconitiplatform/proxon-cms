<?php

namespace App\Models;

class App extends User {

    protected $fillable = [
        'firstname', 'language', 'locale', 'active',
    ];

    protected $hidden = [
        'firstname', 'password', 'remember_token', 'lastname', 'email', 'role',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'password', 'remember_token', 'lastname', 'email', 'role',
    ];

    protected $appends = [
        'key','secret','appname'
    ];

    public function getAppnameAttribute() {
        return $this->firstname;
    }

    public function getKeyAttribute() {
        return hash('sha1',$this->email);;
    }

    public function getSecretAttribute() {
        return hash('sha1',$this->password);;
    }

}
