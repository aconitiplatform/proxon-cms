<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class BeaconBatch extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'beacon_batches';

    protected $fillable = [
        'tenant_id', 'kontakt_io_venue_id', 'kontakt_io_venue_name', 'synced', 'number_of_beacons',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'synced' => 'boolean',
    ];

    protected $appends = [

    ];

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant');
    }

}
