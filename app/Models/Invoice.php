<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Invoice extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'invoices';

    protected $fillable = [
        'billable_id',
        'easybill_invoice_id',
        'easybill_customer_id',
        'type',
        'items',
        'times_generated',
        'month',
        'year',
        'amount',
        'amount_net',
        'easybill_invoice_number',
        'easybill_created_at',
    ];

    protected $guarded = [
        'id',
        'times_generated',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'easybill_customer_id',
    ];

    protected $casts = [
        'items' => 'array',
    ];

    protected $appends = [
    ];

}
