<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class PoiLock extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'poi_locks';

    protected $fillable = [
        'poi_id', 'user_id', 'tenant_id',
    ];

    protected $hidden = [
        'updated_at',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [

    ];

    public function poi() {
        return $this->belongsTo('App\Models\Poi','poi_id','id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant','tenant_id','id');
    }

}
