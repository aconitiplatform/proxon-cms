<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Billable extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'billables';

    protected $fillable = [
        'billable_object_type',
        'billable_object_id',
        'easybill_customer_id',
        'price_per_beacon',
        'discount',
        'tenant_id',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    protected $appends = [
    ];

}
