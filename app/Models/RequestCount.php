<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class RequestCount extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'request_counts';

    protected $fillable = [
        'tenant_id', 'month', 'year', 'count', 'notified_limit', 'notified_approach',
    ];

    protected $guarded = [
        'id', 'limit', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'notified_approach' => 'boolean',
        'notified_limit' => 'boolean',
    ];

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant');
    }

}
