<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class EventLog extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'logs';

    protected $fillable = [
        'tenant_id',
        'user_id',
        'event',
        'ip_hash',
        'resource',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'resource' => 'array',
    ];

    public function tenant() {
        return $this->hasOne('App\Models\Tenant','id','tenant_id');
    }

}
