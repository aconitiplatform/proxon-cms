<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailConfirmation extends Model {

    protected $table = 'email_confirmations';

    protected $fillable = ['user_id', 'token'];

    public function setUpdatedAtAttribute($value) {
        // to disable updated_at
    }

}
