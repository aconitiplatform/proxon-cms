<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Imprint extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $fillable = [
        'text', 'language', 'tenant_id',
    ];

    protected $hidden = [

    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [

    ];

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant','tenant_id','id');
    }

}
