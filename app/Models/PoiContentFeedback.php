<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class PoiContentFeedback extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $fillable = [
        'tenant_id', 'poi_content_id', 'poi_id', 'language', 'positive', 'negative',
    ];

    protected $hidden = [
        'id','created_at',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    public function poiContent() {
        return $this->belongsTo('App\Models\Poi','poi_content_id','id');
    }

    public function poi() {
        return $this->belongsTo('App\Models\Poi','poi_id','id');
    }

}
