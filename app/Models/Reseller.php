<?php

namespace App\Models;

class Reseller extends User {

    public function features() {
        return $this->belongsToMany('App\Models\Features\Feature', 'feature_reseller', 'reseller_id', 'feature_id');
    }

    public function modules() {
        return $this->belongsToMany('App\Models\Features\Module', 'module_reseller', 'reseller_id', 'module_id');
    }

    public function tenants() {
        return $this->hasMany('App\Models\Tenant', 'reseller_id');
    }

    public function settings() {
        return $this->hasOne('App\Models\ResellerSetting','reseller_id','id');
    }

}
