<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use Storage;

use App\Http\Services\TenantService;

use App\Traits\HasUuidForKey;

class Media extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'media';

    protected $fillable = [
        'type', 'url', 'name','tenant_id','user_id','storage_path','language','locale', 'used_for',
    ];

    protected $hidden = [
        'storage_path'
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $appends = [
        'simple_type'
    ];

    public function getSimpleTypeAttribute() {

        if(strpos($this->type, 'image') !== false) {
            return 'image';
        }

        if(strpos($this->type, 'video') !== false) {
            return 'video';
        }

        if(strpos($this->type, 'audio') !== false) {
            return 'audio';
        }

        return 'unknown';

    }

    public function getUrlAttribute($value) {
        return TenantService::getSubstanceAppUrl($this->tenant_id).'/'.config('api.prefix').'/'.config('substance.endpoint_media').'/'.$this->id.'/file';
    }

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant','tenant_id','id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function pois() {
        return $this->belongsToMany('App\Models\Poi', 'media_poi', 'media_id', 'poi_id');
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'media_user', 'media_id', 'user_id');
    }

}
