<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class Cart extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'carts';

    protected $fillable = [
        'items',
        'stripe_token_id',
        'address_name',
        'address_optional',
        'address_street',
        'address_zip',
        'address_city',
        'address_country',
        'shipped',
        'tenant_id',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
    ];

    protected $casts = [
        'items' => 'array',
        'shipped' => 'boolean',
    ];

    protected $appends = [
    ];

    public function tenant() {
        return $this->hasOne('App\Models\Tenant','id','tenant_id');
    }

}
