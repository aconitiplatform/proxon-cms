<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class Beacon extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $fillable = [
        'name', 'tenant_id', 'poi_id', 'config', 'description', 'alias',
    ];

    protected $guarded = [
        'id', 'url', 'url_hash', 'substance_beacon', 'beacon_batch_id',
    ];

    protected $hidden = [
        'url_hash',
    ];

    protected $casts = [
        'config' => 'array',
        'substance_beacon' => 'boolean',
    ];

    protected $appends = [
        'in_use'
    ];

    public function tenant() {
        return $this->belongsTo('App\Models\Tenant');
    }

    public function getInUseAttribute() {
        return $this->attributes['poi_id'] != null;
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'beacon_user', 'beacon_id', 'user_id');
    }

    public function beaconLocation() {
        return $this->hasOne('App\Models\BeaconLocation','beacon_id');
    }

    public function poi() {
        return $this->hasOne('App\Models\Poi','id','poi_id');
    }

}
