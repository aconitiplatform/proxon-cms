<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class PoiEvent extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    public $table = 'poi_log';

    protected $fillable = [
        'user_id',
        'tenant_id',
        'poi_id',
        'type',
        'text',
        'by_superadmin',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
        'user_id',
        'tenant_id',
        'by_superadmin',
        'updated_at',
    ];

    protected $casts = [

    ];

    public function tenant() {
        return $this->hasOne('App\Models\Tenant','id','tenant_id');
    }

    public function poi() {
        return $this->hasOne('App\Models\Poi','id','poi_id');
    }

    public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function superadmin() {
        return $this->hasOne('App\Models\User','id','by_superadmin');
    }

}
