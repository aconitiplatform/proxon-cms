<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class PoiContent extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $fillable = [
        'poi_id', 'language', 'data', 'active', 'url', 'has_url', 'meta', 'has_sweepstake', 'has_feedback', 'has_contact', 'has_micro_payment',
    ];

    protected $hidden = [
        'has_sweepstake', 'has_feedback', 'has_contact', 'has_micro_payment',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'data' => 'array',
        'meta' => 'array',
        'has_sweepstake' => 'boolean',
        'has_feedback' => 'boolean',
        'has_contact' => 'boolean',
        'has_micro_payment' => 'boolean',
    ];

    public function getHasUrlAttribute($value) {
        if($value == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getActiveAttribute($value) {
        if($value == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function poi() {
        return $this->belongsTo('App\Models\Poi','poi_id','id');
    }

}
