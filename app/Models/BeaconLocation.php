<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class BeaconLocation extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $fillable = [
        'beacon_id', 'note', 'latitude', 'longitude', 'indoor', 'level',
    ];

    protected $hidden = [

    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'indoor' => 'boolean'
    ];

    public function beacon() {
        return $this->belongsTo('App\Models\Beacon','beacon_id','id');
    }

}
