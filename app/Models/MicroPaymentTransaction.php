<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class MicroPaymentTransaction extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'micro_payment_transactions';

    protected $fillable = [
        'tenant_id', 'micro_payment_id', 'amount', 'currency', 'stripe_id',
    ];

    protected $hidden = [

    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

}
