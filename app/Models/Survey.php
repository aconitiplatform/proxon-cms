<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenant;

use App\Traits\HasUuidForKey;

class Survey extends Model {

    use HasUuidForKey, BelongsToTenant;

    public $incrementing = false;

    protected $table = 'surveys';

    protected $fillable = [
        'name', 'fallback_language', 'status', 'tenant_id', 'user_id', 'description', 'survey_questions',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [
        'survey_questions' => 'array',
    ];

    public function users() {
        return $this->belongsToMany('App\Models\User', 'survey_user', 'survey_id', 'user_id');
    }

}
