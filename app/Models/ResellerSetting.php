<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUuidForKey;

class ResellerSetting extends Model {

    use HasUuidForKey;

    public $incrementing = false;

    protected $table = 'reseller_settings';

    protected $fillable = [
        'kontakt_io_api_key', 'substance_url', 'edward_stone_url', 'sidebar_product_logo', 'login_screen_company_logo', 'login_screen_product_logo', 'product_name', 'company_name', 'easybill_customer_id', 'reseller_id',
    ];

    protected $hidden = [
        'kontakt_io_api_key', 'easybill_customer_id', 'reseller_id', 'id', 'created_at', 'updated_at',
    ];

    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    protected $casts = [

    ];

    public function reseller() {
        return $this->hasOne('App\Models\Reseller','id','reseller_id');
    }

}
