<?php

    return [
        "TEXT" => "Text",
        "IMAGE" => "Image",
        "VIDEO" => "Video",
        "BUTTON" => "Button",
        "AUDIO" => "Audio",
        "NEWSLETTER" => "Newsletter",
        "DIVIDER" => "Divider",
        "HEADING" => "Heading",
        "SOCIAL_ICONS" => "Social Icons",
        "FEEDBACK" => "Feedback",
        "MICRO_PAYMENT" => "Micro Payment",
        "CONTACT_FORM" => "Contact Form",
        "SWEEPSTAKE" => "Sweepstake",
        "ICON" => "Icon",
        "SPOTIFY" => "Spotify",
        "IMAGE_GALLERY" => "Image Gallery",
        "VOUCHER" => "Voucher",
        "VAG" => "undefined",
    ];
