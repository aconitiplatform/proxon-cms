<?php

    return [
        'PAYEE_SUBJECT' => 'Connect: New Payment :CHARGE received (:PAYMENTNAME).',
        'PAYEE_BODY' => 'The amount :AMOUNT :CURRENCY has been paid.',
        'PAYEE_HINT' => 'Transactions can be viewed in our platform within the corresponding micro payment.',
        'PAYEE_SUCCESS' => 'The amount of the transaction has been paid.',
        'PAYEE_FAILED' => 'The amount of the transaction has not been paid.',
        'PAYER_SUBJECT' => ':TENANTNAME - You have been charged for :PAYMENTNAME',
        'PAYER_CHARGE_THANKS' => 'Thank you for your payment!',
        'PAYER_CHARGE_BODY' => 'Your payment method will be charged accordingly. You will receive a separate receipt from our payment provider.',
        'PAYER_CHARGE_AMOUNT' => 'Amount',
    ];
