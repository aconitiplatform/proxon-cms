<?php

    return [
        'startrampe' => 'Launch Pad',
        'troposphere' => 'Troposphere',
        'stratosphere' => 'Stratosphere',
        'mesosphere' => 'Mesosphere',
        'thermosphere' => 'Thermosphere',
        'plan_description' => 'Contains :amount touchpoint URLs.',
    ];
