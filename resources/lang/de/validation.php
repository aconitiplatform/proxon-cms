<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Das Attribut :attribute muss akzeptiert sein.',
    'active_url'           => 'Das Attribut :attribute ist keine valide URL.',
    'after'                => 'Das Attribut :attribute muss nach dem Datum :date sein.',
    'alpha'                => 'Das Attribut :attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => 'Das Attribut :attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => 'Das Attribut :attribute darf nur Buchstaben und Zahlen enthalten.',
    'array'                => 'Das Attribut :attribute muss eine Liste sein.',
    'before'               => 'Das Attribut :attribute muss vor dem Datum :date sein.',
    'between'              => [
        'numeric' => 'Das Attribut :attribute muss zwischen :min und :max liegen.',
        'file'    => 'Das Attribut :attribute muss zwischen :min und :max Kilobyte groß sein.',
        'string'  => 'Das Attribut :attribute muss zwischen :min und :max Zeichen lang sein.',
        'array'   => 'Das Attribut :attribute muss zwischen :min und :max Einträge enthalten.',
    ],
    'boolean'              => 'Das Attribut :attribute muss true oder false sein.',
    'confirmed'            => 'Das Attribut :attribute confirmation stimmt nicht nicht überein.',
    'date'                 => 'Das Attribut :attribute ist kein valides Datum.',
    'date_format'          => 'Das Attribut :attribute stimmt nicht mit dem Format :format überein.',
    'different'            => 'Das Attribut :attribute und das Attribut :other müssen verschieden sein.',
    'digits'               => 'Das Attribut :attribute muss :digits Ziffern enthalten.',
    'digits_between'       => 'Das Attribut :attribute muss zwischen :min und :max Ziffern beinhalten.',
    'email'                => 'Das Attribut :attribute muss eine valide Email-Adresse sein.',
    'exists'               => 'Das ausgewählte Attribute :attribute ist ungültig.',
    'filled'               => 'Das Attribut :attribute wird benötigt.',
    'image'                => 'Das Attribut :attribute muss ein Bild sein.',
    'in'                   => 'Das ausgewählte Attribute :attribute ist ungültig.',
    'integer'              => 'Das Attribut :attribute muss ein Integer sein.',
    'ip'                   => 'Das Attribut :attribute muss eine gültige IP Adresse sein.',
    'json'                 => 'Das Attribut :attribute muss ein gültiger JSON String sein.',
    'max'                  => [
        'numeric' => 'Das Attribut :attribute darf nicht größer sein als :max.',
        'file'    => 'Das Attribut :attribute darf nicht größer sein als :max Kilobyte.',
        'string'  => 'Das Attribut :attribute darf nicht größer sein als :max characters.',
        'array'   => 'Das Attribut :attribute darf nicht mehr als :max Einträge enthalten.',
    ],
    'mimes'                => 'Das Attribut :attribute muss eine Datei vom Typ type: :values sein.',
    'min'                  => [
        'numeric' => 'Das Attribut :attribute muss mindestens :min groß sein.',
        'file'    => 'Das Attribut :attribute muss mindestens :min Kilobytes groß sein.',
        'string'  => 'Das Attribut :attribute muss mindestens :min Zeichen lang sein.',
        'array'   => 'Das Attribut :attribute muss mindestens :min Einträge enthalten.',
    ],
    'not_in'               => 'Das ausgewählte Attribute :attribute ist ungültig.',
    'numeric'              => 'Das Attribut :attribute muss eine Zahl sein.',
    'regex'                => 'Das Attribut :attribute Format ist ungültig.',
    'required'             => 'Das Attribut :attribute wird benötigt.',
    'required_if'          => 'Das Attribut :attribute wird benötigt, wenn das Attribut :other den Wert :value hat.',
    'required_unless'      => 'Das Attribut :attribute wird benötigt, außer das Attribut :other hat die Werte :values.',
    'required_with'        => 'Das Attribut :attribute wird benötigt, wenn die Werte :values vorhanden sind.',
    'required_with_all'    => 'Das Attribut :attribute wird benötigt, wenn die Werte :values vorhanden sind.',
    'required_without'     => 'Das Attribut :attribute wird benötigt, wenn die Werte :values nicht vorhanden sind.',
    'required_without_all' => 'Das Attribut :attribute wird benötigt, wenn keiner der Werte :values vorhanden ist.',
    'same'                 => 'Das Attribut :attribute und das Attribut :other müssen übereinstimmen.',
    'size'                 => [
        'numeric' => 'Das Attribut :attribute muss :size Ziffern lang sein.',
        'file'    => 'Das Attribut :attribute muss :size Kilobyte groß sein.',
        'string'  => 'Das Attribut :attribute muss :size Zeichen lang sein.',
        'array'   => 'Das Attribut :attribute muss :size Einträge enthalten.',
    ],
    'string'               => 'Das Attribut :attribute muss ein String sein.',
    'timezone'             => 'Das Attribut :attribute muss eine valide Zeitzone sein.',
    'unique'               => 'Das Attribut :attribute ist bereits in Verwendung.',
    'url'                  => 'Das Attribut :attribute ist keine valide URL.',
    'image_gallery'        => 'Es gibt ein Problem mit den angegebenen Bildern.',
    'font_list'            => 'Eine Fontliste benötigt Objekte mit den Attributen family, import, standard. Standard darf nur ein oder keinmal wahr sein.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
