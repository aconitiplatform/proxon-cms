<?php

    return [
        'startrampe' => 'Startrampe',
        'troposphere' => 'Troposphäre',
        'stratosphere' => 'Stratosphäre',
        'mesosphere' => 'Mesosphäre',
        'thermosphere' => 'Thermosphäre',
        'plan_description' => 'Beinhaltet :amount Touchpoint-URLs.',
    ];
