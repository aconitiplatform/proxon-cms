<?php

    return [
        'PAYEE_SUBJECT' => 'Connect: Neue Bezahlung :CHARGE erhalten (:PAYMENTNAME).',
        'PAYEE_BODY' => 'Der Betrag :AMOUNT :CURRENCY wurde bezahlt.',
        'PAYEE_HINT' => 'Transaktionen können in der Plattform unter dem entsprechenden Micro-Payment eingesehen werden.',
        'PAYEE_SUCCESS' => 'Der Betrag der Transaktion wurde bezahlt.',
        'PAYEE_FAILED' => 'Der Betrag der Transaktion wurde nicht bezahlt.',
        'PAYER_SUBJECT' => ':TENANTNAME - Bezahlung für :PAYMENTNAME',
        'PAYER_CHARGE_THANKS' => 'Vielen Dank für deine Bezahlung!',
        'PAYER_CHARGE_BODY' => 'Der Betrag wird dir in Rechnung gestellt und dein angegebenes Zahlungsmittel entsprechend belastet. Du erhältst eine separate Rechnung von unserem Zahlungsanbieter.',
        'PAYER_CHARGE_AMOUNT' => 'Betrag',
    ];
