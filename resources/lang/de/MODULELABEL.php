<?php

    return [
        "TEXT" => "Text",
        "IMAGE" => "Bild",
        "VIDEO" => "Video",
        "BUTTON" => "Button",
        "AUDIO" => "Audio",
        "NEWSLETTER" => "Newsletter",
        "DIVIDER" => "Trenner",
        "HEADING" => "Überschrift",
        "SOCIAL_ICONS" => "Social Icons",
        "FEEDBACK" => "Feedback",
        "MICRO_PAYMENT" => "Micro Payment",
        "CONTACT_FORM" => "Kontaktformular",
        "SWEEPSTAKE" => "Gewinnspiel",
        "ICON" => "Symbol",
        "SPOTIFY" => "Spotify",
        "IMAGE_GALLERY" => "Bildergalerie",
        "VOUCHER" => "Gutschein",
        "VAG" => "undefined",
    ];
