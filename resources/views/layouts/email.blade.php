<!DOCTYPE html>
<html>
    <head>

        <style>

            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 300;
                font-size: 14px;
            }

            img {
                width: 100px;
            }

            .container {
                text-align: left;
            }

            .content {
                margin: 20px;
                text-align: left;
                display: inline-block;
            }

            h1 {
                font-weight: 400;
                font-size: 20px;
            }

            h2 {
                font-weight: 400;
                font-size: 18px;
            }

            h3 {
                font-weight: 300;
                font-size: 16px;
            }

            .title {
                font-size: 20px;
                padding: 10px 0px 10px 0px;
                text-align: center;
            }

            .charge {
                width: 70%;
                margin: 20px auto;
            }

            table {
                margin: 20px 0px;
                width: 100%;
            }

            .r {
                text-align: right;
            }

            .thanks {
                font-weight: 500;
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">

                @yield('content')

            </div>
        </div>
    </body>
</html>
