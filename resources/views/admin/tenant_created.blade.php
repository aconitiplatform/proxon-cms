@extends('layouts.email')

@section('content')
<h3>Tenant</h3>
<div>
    <p>
        @if(isset($reseller))
            Reseller: {{$reseller['name']}}<br>
        @endif
        Tenant Name: {{$tenant['name']}}<br>
        User Name: {{$user['firstname']}} {{$user['lastname']}}<br>
        User Email: {{$user['email']}}<br>
    </p>
</div>
<h3>Beacons ({{$tenant['allowed_beacons']}} allowed)</h3>
<div>
    name;url<br>
    @forelse ($beaconInfos as $beaconInfo)
        {{ $beaconInfo[0] }};{{ $beaconInfo[1] }}<br>
    @empty
        <p>No beacons</p>
    @endforelse
</div>
<div>
    <p>Mit freundlichen Grüßen,</p>
    @if(isset($reseller['settings']) && isset($reseller['settings']['product_name']))
        <h3>{{$reseller['settings']['product_name']}}</h3>
    @else
        <h3>Substance</h3>
    @endif
    @if(isset($reseller['settings']) && isset($reseller['settings']['substance_url']))
        <span>{{$reseller['settings']['substance_url']}}</span>
    @else
        <span>{{config('substance.url')}}</span>
    @endif
</div>
@endsection
