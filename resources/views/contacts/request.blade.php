@extends('layouts.email')

@section('content')
<div>From: {{$name}}</div>
<div>Email: {{$email}}</div>
<div>Text: {{$text}}</div>
@endsection
