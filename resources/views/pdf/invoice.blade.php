<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <body style="font-family:sans-serif;">
        <header>
            <h3>Invoice / Rechnung {{$id}}</h3>
            <p>Invoice date / Rechnungsdatum {{$date}}</p>
        </header>

        <div>
            <small>Issued by / Ausgestellt von</small><br>
            {{ config('substance.billing.from') }}
            <br>
            <br>
        </div>

        <div>
            <small>Recipient / Empfänger</small><br>
            <b>{{ $tenant }}</b><br>
            <small>Customer No. / Kunden-Nr. {{$customer}}</small><br>
            {{ $information }}
        </div>

        <div>
            <p>
                @if ($paid)
                    The invoice has already been paid via our payment provider Stripe. / Die Rechnung wurde bereits über unseren Zahlungsanbieter Stripe beglichen.
                @else
                    The invoice has not been paid yet. / Die Rechnung ist noch nicht beglichen.
                @endif
            </p>
            <table style="width:100%;">
                <tr>
                    <td style="width:25%;text-align:left;"><b>CMS</b></td>
                    <td style="width:35%;text-align:left;"><small>Subscription / Abonnement<small></td>
                    <td style="width:40%;text-align:right;"><small>Price / Preis</small></td>
                </tr>
                <tr>
                    <td></td>
                    <td><small>{{ $period_start }} - {{ $period_end }}</small></td>
                    <td style="width:40%;text-align:right;"><small>Subtotal / Netto: {{ number_format($subtotal / 100,2) }}€</small></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width:40%;text-align:right;"><small>VAT / Mehrwertsteuer ({{$tax_percent}}%): {{ number_format($tax / 100,2) }}€</small></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width:40%;text-align:right;"><b>Total / Brutto: {{ number_format($total / 100,2) }}€</b></td>
                </tr>
            </table>
        </div>

        <footer>
            <p><small>Tax ID / Steuernummer {{ config('substance.billing.tax') }} | {{ config('substance.billing.hrb') }}</small></p>
        </footer>
    </body>
</html>
