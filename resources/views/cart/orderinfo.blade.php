@extends('layouts.email')

@section('content')
<h3>Account</h3>
<div>
    <p>
        {{$tenant['name']}}, ordered by {{$user['firstname']}} {{$user['lastname']}}
    </p>
</div>

<h3>Your order will be shipped to</h3>
<div>
    {{$cart['address_name']}}<br>
    {{$cart['address_optional']}}<br>
    {{$cart['address_street']}}<br>
    {{$cart['address_zip']}}<br>
    {{$cart['address_city']}}<br>
    {{$cart['address_country']}}<br>
</div>

<h3>Order</h3>
<div>
    <table>
        <tr>
            <th><b>Name</b></th>
            <th>Price</th>
            <th>Hardware</th>
            <th>URL</th>
        </tr>
        @forelse ($cart['items'] as $item)
            <tr>
                <td><b>{{ $item['name'] }}</b></td>
                <td>{{ $item['price'] / 100 }}€</td>
                <td>{{ $item['hardware'] }}</td>
                <td>{{ $item['url'] }}</td>
            </tr>
        @empty
            <tr>
                <td>No items</td>
            </tr>
        @endforelse
    </table>
</div>

<div>
    <p>Your order will be processed immediately.</p>
    <h3>{{config('substance.name')}}</h3>
    <span>{{config('substance.url')}}</span>
</div>
@endsection
