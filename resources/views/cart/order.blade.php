@extends('layouts.email')

@section('content')
<h3>Tenant</h3>
<div>
    <p>
        Tenant Name: {{$tenant['name']}}<br>
    </p>
</div>

<h3>Ship to</h3>
<div>
    {{$cart['address_name']}}<br>
    {{$cart['address_optional']}}<br>
    {{$cart['address_street']}}<br>
    {{$cart['address_zip']}}<br>
    {{$cart['address_city']}}<br>
    {{$cart['address_country']}}<br>
</div>

<h3>Cart {{$cart['id']}}</h3>
<div>
    <table>
        <tr>
            <th><b>Name</b></th>
            <th>Price</th>
            <th>Hardware</th>
            <th>URL</th>
        </tr>
        @forelse ($cart['items'] as $item)
            <tr>
                <td><b>{{ $item['name'] }}</b></td>
                <td>{{ $item['price'] / 100 }}€</td>
                <td>{{ $item['hardware'] }}</td>
                <td>{{ $item['url'] }}</td>
            </tr>
        @empty
            <tr>
                <td>No items</td>
            </tr>
        @endforelse
    </table>
</div>

<div>
    <p>Mit freundlichen Grüßen,</p>
    <h3>Substance</h3>
    <span>{{config('substance.url')}}</span>
</div>
@endsection
