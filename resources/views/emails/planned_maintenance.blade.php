@extends('layouts.email')

@section('content')
<h3>
    Hi {{$name}},
</h3>
<p>
    <b>@lang('emails.planned_maintenance_instruction', ['when'=>$when, 'howlong'=>$howLong])</b>
</p>
<p>
    @lang('emails.planned_maintenance_infos')
</p>
@endsection
