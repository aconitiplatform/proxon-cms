@extends('layouts.email')

@section('content')
<h3>
    @lang('emails.monthly_billing_heading')
</h3>

@if(isset($billables))
    @foreach ($billables as $billable)

        <div>
            <h4>
                @lang('emails.monthly_billing_list_tenant_name') {{$billable['tenant']['name']}}
            </h4>
            <div>
                @lang('emails.monthly_billing_list_tenant_beacons') {{$billable['tenant']['allowed_beacons']}}
            </div>
            <div>
                <div>@lang('emails.monthly_billing_list_billing_info')</div>
                @foreach ($billable['tenant']['tenant_billing_info'] as $k => $v)
                    @if($k == 'price_per_beacon')
                        <div>{{$k}}: {{number_format($v/100,2,',','.')}}€</div>
                    @elseif($k == 'discount')
                        <div>{{$k}}: {{$v}}%</div>
                    @else
                        <div>{{$k}}: {{$v}}</div>
                    @endif
                @endforeach
            </div>

            <h4>
                @lang('emails.monthly_billing_list_billing_price')
            </h4>
            <div>
                @if($billable['meta']['full_month'])
                    @lang('emails.monthly_billing_complete_month')
                @else
                    @lang('emails.monthly_billing_part_month') {{$billable['meta']['percent_to_be_paid_of_month']}}%
                @endif
            </div>
            <div>
                <div>@lang('emails.monthly_billing_list_billing_net_actual'): {{ $billable['payment']['payment_net_actual'] }}€</div>
                <div>@lang('emails.monthly_billing_list_billing_net_discount'): {{ $billable['payment']['payment_net_discount'] }}€</div>
                <div>@lang('emails.monthly_billing_list_billing_gross_actual'): {{ $billable['payment']['payment_gross_acutal'] }}€</div>
                <div>@lang('emails.monthly_billing_list_billing_gross_discount'): {{ $billable['payment']['payment_gross_discount'] }}€</div>
            </div>

        </div>

        <hr>

    @endforeach
@else
    <div>
        @lang('emails.monthly_billing_list_empty')
    </div>
@endif

@if(isset($nonBillables))
    <h4>
        @lang('emails.monthly_billing_list_non_billables')
    </h4>
    <ul>
        @foreach ($nonBillables as $nonBillable)
            <li>
                @lang('emails.monthly_billing_list_tenant_name') {{$nonBillable['name']}} {{$nonBillable['id']}}
                <br><code>php artisan tenants:create-billing {{$nonBillable['id']}}</code><hr>
            </li>
        @endforeach
    </ul>
@endif

<div>
    <h3>Substance ({{config('substance.url')}})</h3>
</div>
@endsection
