@extends('layouts.email')

@section('content')
<h1>@lang('emails.registered_heading',['substance' => config('substance.name')])</h1>

<p>@lang('emails.registered_instruction',['substance' => config('substance.name')])</p>

<p>
    <a href="{{config('substance.url')}}">
    @lang('emails.registered_goto',['substance' => config('substance.name')])
    </a>
</p>

<p>
    <small>@lang('emails.registered_warning')</small>
</p>
@endsection
