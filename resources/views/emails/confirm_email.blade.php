@extends('layouts.email')

@section('content')
<h1>@lang('emails.confirm_email_heading',['substance' => config('substance.name')])</h1>

<p>@lang('emails.confirm_email_instruction',['substance' => config('substance.name')])</p>

<p>
    <a href="{{$substanceUrl}}{{config('substance.confirm_email')}}{{$token}}">
    {{$substanceUrl}}{{config('substance.confirm_email')}}{{$token}}
    </a>
</p>

<p>
    <small>@lang('emails.confirm_email_warning',['substance' => config('substance.name')])</small>
</p>
@endsection
