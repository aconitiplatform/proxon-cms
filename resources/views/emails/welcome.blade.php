@extends('layouts.email')

@section('content')
<h1>@lang('emails.welcome_heading',['substance' => config('substance.name')])</h1>

<p>@lang('emails.welcome_instruction',['substance' => config('substance.name')])</p>

<p>
    <a href="{{$substanceUrl}}{{config('substance.auth_password_set')}}{{$email}}/{{$token}}">
    {{$substanceUrl}}{{config('substance.auth_password_set')}}{{$email}}/{{$token}}
    </a>
</p>

<p>
    <small>@lang('emails.welcome_warning',['substance' => config('substance.name')])</small>
</p>
@endsection
