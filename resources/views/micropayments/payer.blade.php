@extends('layouts.email')

@section('content')
<div class="title">
    <a href="https://rakete7.com" target="_blank"><img src="{{config('substance.url')}}/img/R7.png"></a><br>
    {{$tenant['name']}}
</div>
<div class="charge">
    <p class="thanks">{{ trans('micropayments.PAYER_CHARGE_THANKS') }}</p>
    <p>{{ trans('micropayments.PAYER_CHARGE_BODY') }}</p>
    <hr>
    <table>
        <tr>
            <td>{{$microPayment['name']}}</td>
            <td class="r">{{trans('micropayments.PAYER_CHARGE_AMOUNT')}}</td>
        </tr>
        <tr>
            <td></td>
            <td class="r">
                @if($microPayment['currency'] == 'eur')
                    {{($microPayment['amount'] / 100)}}€
                @elseif($microPayment['currency'] == 'usd')
                    ${{($microPayment['amount'] / 100)}}
                @endif
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="r"></td>
        </tr>
    </table>
</div>
@endsection
