@extends('layouts.email')

@section('content')
<div class="title">
    {{ trans('micropayments.PAYEE_SUBJECT', ['CHARGE'=>$microPayment['charge'],'PAYMENTNAME'=>$microPayment['name']]) }}
</div>
<div>
    <p>
        {{ trans('micropayments.PAYEE_BODY', ['AMOUNT'=>$microPayment['amount']/100,'CURRENCY'=>$microPayment['currency']]) }}
    </p>
    <p>
        {{ trans('micropayments.PAYEE_HINT') }}
    </p>
    @if($microPayment['paid']==1)
        {{date('Y-m-d H:i:s',$microPayment['created_at'])}} {{ trans('micropayments.PAYEE_SUCCESS') }}
    @else
        {{date('Y-m-d H:i:s',$microPayment['created_at'])}} {{ trans('micropayments.PAYEE_FAILED') }}
    @endif
</div>
@endsection
