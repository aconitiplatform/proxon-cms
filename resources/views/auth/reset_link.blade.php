@extends('layouts.email')

@section('content')
<h1>@lang('emails.password_reset_heading',['substance' => config('substance.name')])</h1>

<p>@lang('emails.password_reset_instruction',['substance' => config('substance.name')])</p>

<p>
    <a href="{{$substanceUrl}}{{config('substance.auth_password_reset')}}{{$email}}/{{$token}}">
    {{$substanceUrl}}{{config('substance.auth_password_reset')}}{{$email}}/{{$token}}
    </a>
</p>

<p>
    <small>@lang('emails.password_reset_warning',['substance' => config('substance.name')])</small>
</p>
@endsection
