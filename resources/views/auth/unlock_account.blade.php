@extends('layouts.email')

@section('content')
<h1>@lang('emails.unlock_account_heading',['substance' => config('substance.name')])</h1>

<p>@lang('emails.unlock_account_instruction',['substance' => config('substance.name')])</p>

<p>
    <a href="{{$substanceUrl}}{{config('substance.unlock_account')}}{{$email}}/{{$token}}">
    {{$substanceUrl}}{{config('substance.unlock_account')}}{{$email}}/{{$token}}
    </a>
</p>

<p>
    <small>@lang('emails.unlock_account_warning',['substance' => config('substance.name')])</small>
</p>
@endsection
