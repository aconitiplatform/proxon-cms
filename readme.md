# Proxon

## Substance

Substance is a content management system for the Physical Web.

## Setup & Installation

### Software

We recommend running the development environment [Laravel Homestead](https://laravel.com/docs/5.4/homestead).
The following software is required:

- Nginx (or Apache)
- PHP >= 7.0
- MariaDB (or MySQL)
- Redis
- Composer
- Node with NPM, Bower, Gulp

The following sofware is recommended but does not come with Homestead:

- [Redis Commander](https://github.com/joeferner/redis-commander) or any Redis UI
- [Sequel Pro](https://www.sequelpro.com/)

### Setup

Create a new site in Homestead and provision it. It must point to substance/public.
Create a .env file from the .env.example file and fill it with the data provided by Proxon.
Create a new database and add its credentials to the environment file (to connect from the host machine you have to specify the port 33060).
Once the .env is complete, go to your terminal, SSH into your Homestead machine, change to Substance root directory, and run:
```
$ composer install
$ php artisan migrate
$ php artisan serverip
$ npm install
$ bower install
$ gulp
$ php artisan tenants:new
```
This will install all necessary dependencies, create the database structure, and build your Javascript.

### Queues

The tenants:new command makes also use of the queue to make data available in Edward Stone. To make sure everything it in place, run
```
$ php artisan queue:listen
```
This will work the default queue via Redis. To run different queues, check the config/queue.php file to see which queues are available.

### Commands
```
$ php artisan serverip {--commit=default} {--server=qmui}
```
Creates a git-ignored app-server.php config file containing the IP of the current machine, the current git hash, and the type of server. This IP is used for sophisticated logging with Monolog.
Server types are:
- q - a queue worker
- qm - a queue worker that runs migrations, apply:changes, etc.
- ui - a UI server
- qmui (default) - a complete server, such as homestead or our dev machine

```
$ php artisan tenants:new
```
This command will guide you through a console based wizzard to create a new tenant. Make sure queues run in the background to send password reset emails and make the tenant available to Edward Stone.

```
$ php artisan tenants:syncbeacons {tenantId (uuid)}
```
If your new tenant has a Kontakt.io venue and the beacons have been assigned to the venue, use this command to sync the beacon configuration. Afterwards the beacons can be bulk-configured with the Kontakt.io app.

```
$ php artisan tenants:create-billing {tenantId}
```
Creates billing infos for a tenant. If the info already exists, you can update it with this command.

```
$ php artisan tenants:billing {date}
```
Get information on how much to bill each tenant for a certain month. The date parameter must be Y-m, e.g. 2017-03.

```
$ php artisan inform:maintenance {when (string, e.g. "28.02.2017 12:00")} {howLong (int, minutes)}
```
Inform all Substance users that a planned maintenance, e.g. a new deploy, will take place.

```
$ php artisan edward:restore
```
Restore all Redis entries for Edward Stone. This will flush all cached items.

```
$ php artisan beacons:new {tenantId} {amount} {own (boolean)} {--names=comma,separated,list,of,names}
```
Create new beacons for a tenant through the command line. If own is true, the beacon names will be editable. The amount of new beacons must match the amount of (optional) names given.

```
$ php artisan changes:apply
```
Add this command to your deploy script after migrations, etc. If something needs to be adjusted in data models, put the code for adjustment there and make sure it is tested accurately for a safe live-deploy. This command calls edward:restore afterwards.

```
$ php artisan analytics:fake {tenantId} {amount}
```
This command creates analytics events with the Faker library to test chart and kpi card behavior.


```
$ php artisan make:superadmin
```
Create a superadmin. This command will only work once. The credentials in the .env file are the one used to login the superadmin.


```
$ php artisan tenants:set {tenantId} {--inactive}
```
Activate or deactivate a tenant.


```
$ php artisan make:reseller
```
Create a reseller user. This user can bill tenants.


```
$ php artisan tenants:add-reseller
```
Add a reseller to a tenant. The reseller should already have an ID in easybill.


```
$ php artisan tenants:end-trial
```
End the trial period of a tenant and make it billable (either by adding a reseller or by having billing information created for us to bill). The tenant/reseller should already have an ID in easybill. **THE TENANT OR RESELLER WILL BE BILLED IMMEDIATELY FOR THE REST OF THE MONTH, IF CONFIRMED!**


```
$ php artisan tenants:invoice {--single}
```
Creates a monthly invoice for a tenant (without single option). With single option a one time invoice for a new amount of beacons will be created.


```
$ php artisan resellers:invoice {--single}
```
Creates a monthly invoice for a reseller including all their tenants (without single option). With single option a one time invoice for a new amount of beacons of a tenant will be created.

```
$ php artisan pois:expired-groups
```
Search all POIs for expired groups that need to be deleted.

```
$ php artisan cart:shipped {cartId}
```
Set a cart to shipped.

```
$ php artisan cart:to-ship
```
Find carts that need to be shipped.

```
$ php artisan inform:email-confirmation
```
Send confirmation emails to unconfirmed users.
