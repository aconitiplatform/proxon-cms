import {AriaConfig} from './config/aria.config';
import {DateLocaleConfig} from './config/date-locale.config';
import {RoutesConfig} from './config/routes.config';
import {LoadingBarConfig} from './config/loading_bar.config';
import {ThemeConfig} from './config/theme.config';
import {SatellizerConfig} from './config/satellizer.config';
import {TranslateConfig} from './config/translate.config';
import {ChartJsConfig} from './config/chart-js.config';
import {AnalyticsConfig} from './config/analytics.config';
import {MarkdownConfig} from './config/markdown.config';
import {NumeralJsConfig} from './config/numeraljs.config';

angular.module('app.config')
	.config(AriaConfig)
	.config(DateLocaleConfig)
	.config(RoutesConfig)
	.config(LoadingBarConfig)
	.config(ThemeConfig)
	.config(TranslateConfig)
	.config(SatellizerConfig)
	.config(ChartJsConfig)
	.config(AnalyticsConfig)
	.config(MarkdownConfig)
	.config(NumeralJsConfig);
