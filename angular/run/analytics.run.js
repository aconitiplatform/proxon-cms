export function AnalyticsRun(Analytics, $transitions) {

	'ngInject';

	$transitions.onEnter({ }, function(transition, state) {

		if(!state.abstract){

			if(state.trackPage){

				Analytics.trackPage(state.trackPage);

			}else{

				Analytics.trackPage(state.url.pattern);

			}

		}

	});

}
