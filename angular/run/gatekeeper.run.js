export function GatekeeperRun($transitions, $localStorage, $state, TenantService, UserService, $rootScope) {

	'ngInject';

	let featuresObject = null;

	let loadFeatures = () => {

		if (UserService.isLoggedIn()) {

			TenantService.loadAvailableFeatures().then((features) => {
				featuresObject = {};
				for (let feature of features) {
					featuresObject[feature.slug] = true;
				}
			}, (error) => {
				console.log('Failed to get available features for current tenant!', error);
			});

		}

	}

	$rootScope.$on('SuperadminScopeChange', () => {
		loadFeatures();
	});

	let checkFeature = (featureName) => {
		let ret;
		if (!featuresObject) {
			// we weren't logged in before, so this is a special case: when not logged in, we permit everything
			ret = true;
		} else {
			// check if featuresObject contains a key with (i. e.) "profile"
			ret = !!featuresObject[featureName];
		}
		return ret;
	}

	$transitions.onStart({ }, function(transition) {

		let transitionOkay = true;

		if(transition.to().restrictedTo){

			if(_.includes(transition.to().restrictedTo, $localStorage.user.role) < 0){

				transitionOkay = false;

			}

		}

		if (transitionOkay && transition.to().feature) {

			transitionOkay = checkFeature(transition.to().feature);

		}

		if (!featuresObject) {

			loadFeatures();

		}

		if (!transitionOkay) {
			return transition.router.stateService.target('public.notfound');
		}

		return transitionOkay;

	});

	loadFeatures();

}