export function LocationChangesRun($rootScope, LocationChangeService, $state, Analytics, $trace, $transitions) {

	'ngInject';

	$rootScope.$on('$locationChangeSuccess', (event, currentLocation, lastLocation) => {

		if((currentLocation.indexOf('notfound') !== -1)){

			LocationChangeService.lastLocation = lastLocation;

		}

	});

}