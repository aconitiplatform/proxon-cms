export function RoutesRun($state, $transitions, $auth, $localStorage, CommonService) {

	'ngInject';

	let requiresAuthCriteria = {
		to: ($state) => $state.data && $state.data.auth
	};

	let redirectToLogin = () => {

		'ngInject';

		if(!$auth.isAuthenticated()){

			$localStorage.$reset();

			return $state.target('public.login', undefined, {location: true});

		}

	};

	$transitions.onBefore(requiresAuthCriteria, redirectToLogin, {priority:10});

}
