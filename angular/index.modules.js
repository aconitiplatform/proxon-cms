angular.module('app', [
	'app.run',
	'app.constants',
	'app.filters',
	'app.services',
	'app.components',
	'app.directives',
	'app.routes',
	'app.config',
	'app.partials'
]);

angular.module('app.run', []);
angular.module('app.constants', []);
angular.module('app.routes', []);
angular.module('app.filters', []);
angular.module('app.services', [
	'ngStorage',
	'restangular'
]);
angular.module('app.config', []);
angular.module('app.directives', []);
angular.module('app.components', [
	'ui.router',
	'ngMaterial',
	'angular-loading-bar',
	'restangular',
	'ngStorage',
	'satellizer',
	'bsLoadingOverlay',
	'pascalprecht.translate',
	'ngCookies',
	'angularMoment',
	'dndLists',
	'ngSanitize',
	'com.2fdevs.videogular',
	'com.2fdevs.videogular.plugins.controls',
	'com.2fdevs.videogular.plugins.overlayplay',
	'com.2fdevs.videogular.plugins.poster',
	'ngMessages',
	'ui.bootstrap',
	'ngFileUpload',
	'angular.filter',
	'chart.js',
	'ngCookies',
	'mdColorPicker',
	'textAngular',
	'pusher-angular',
	'ng.deviceDetector',
	'ngYoutubeEmbed',
	'angular-google-analytics',
	'ngVimeo',
	'ui.timepicker',
	'hc.marked',
	'ng.deviceDetector',
	'angular-inview',
	'ngNumeraljs',
	'ngFileSaver',
	'vcRecaptcha'
]);
