import {RegisterService}				from './services/register.service';
import {StripeService}					from './services/stripe.service';
import {CartService}					from './services/cart.service';
import {ProductConfigService}			from './services/product-config.service';
import {ListService}					from './services/list.service';
import {PoiActivityService}				from './services/poi-activity.service';
import {UpdateService}					from './services/update.service';
import {InvoicesService}				from './services/invoices.service';
import {DesignService}					from './services/design.service'
import {SuperadminService}				from './services/superadmin.service';
import {IconsService}					from './services/icons.service';
import {PusherService}					from './services/pusher.service';
import {SweepstakeService}				from './services/sweepstake.service';
import {FeedbackService}				from './services/feedback.service';
import {MicroPaymentService}			from './services/micro-payment.service';
import {NewsletterConfigurationService}	from './services/newsletter-configuration.service';
import {ChartsService}					from './services/charts.service';
import {KpiCardService}					from './services/kpi-card.service';
import {AuthService}					from './services/auth.service';
import {MediaService}					from './services/media.service';
import {LogoutService}					from './services/logout.service';
import {TenantService}					from './services/tenant.service';
import {TouchpointService}				from './services/touchpoint.service';
import {ModuleEditService}				from './services/module-edit.service';
import {PoiValidatorService}			from './services/poi-validator.service';
import {CommonService}					from './services/common.service';
import {UsersService}					from './services/users.service';
import {UserService}					from './services/user.service';
import {AppsService}					from './services/apps.service';
import {AppService}						from './services/app.service';
import {LanguageNamesService}			from './services/language-names.service';
import {LocationChangeService}			from './services/location-change.service';
import {PoiService}						from './services/poi.service';
import {LanguageLoaderService}			from './services/language-loader.service';
import {APIService}						from './services/API.service';
import {DialogService}					from './services/dialog.service';
import {ToastService}					from './services/toast.service';
import {EventUnsubscribeService}		from './services/event-unsubscribe.service';
import {MarkdownService}				from './services/markdown.service';
import {TimepickerConfigService}		from './services/timepicker-config.service';
import {ModuleValidationService}		from './services/module-validation.service';
import {PaginationService}				from './services/pagination.service';
import {FontService}					from './services/font.service';
import {PoiTimingService}				from './services/poi-timing.service';
import {GenerateNotificationService}	from './services/generate-notification.service';
import {PromiseHelperService}			from './services/promise-helper.service';
import {SubscriptionService}			from './services/subscription.service';
import {TranslatedToastService}			from './services/translated-toast.service';
import {LoginService}					from './services/login.service';


angular.module('app.services')
	.service('RegisterService', RegisterService)
	.service('StripeService', StripeService)
	.service('CartService', CartService)
	.service('ProductConfigService', ProductConfigService)
	.service('ListService', ListService)
	.service('PoiActivityService', PoiActivityService)
	.service('UpdateService', UpdateService)
	.service('InvoicesService', InvoicesService)
	.service('DesignService', DesignService)
	.service('SuperadminService', SuperadminService)
	.service('IconsService', IconsService)
	.service('PusherService', PusherService)
	.service('SweepstakeService', SweepstakeService)
	.service('FeedbackService', FeedbackService)
	.service('MicroPaymentService', MicroPaymentService)
	.service('NewsletterConfigurationService', NewsletterConfigurationService)
	.service('ChartsService', ChartsService)
	.service('KpiCardService', KpiCardService)
	.service('AuthService', AuthService)
	.service('MediaService', MediaService)
	.service('LogoutService', LogoutService)
	.service('TenantService', TenantService)
	.service('TouchpointService', TouchpointService)
	.service('ModuleEditService', ModuleEditService)
	.service('PoiValidatorService', PoiValidatorService)
	.service('CommonService', CommonService)
	.service('UsersService', UsersService)
	.service('UserService', UserService)
	.service('AppsService', AppsService)
	.service('AppService', AppService)
	.service('LanguageNamesService', LanguageNamesService)
	.service('LocationChangeService', LocationChangeService)
	.service('PoiService', PoiService)
	.service('LanguageLoaderService', LanguageLoaderService)
	.service('API', APIService)
	.service('DialogService', DialogService)
	.service('ToastService', ToastService)
	.service('EventUnsubscribeService', EventUnsubscribeService)
	.service('MarkdownService', MarkdownService)
	.service('TimepickerConfigService', TimepickerConfigService)
	.service('ModuleValidationService', ModuleValidationService)
	.service('PaginationService', PaginationService)
	.service('FontService', FontService)
	.service('PoiTimingService', PoiTimingService)
	.service('GenerateNotificationService', GenerateNotificationService)
	.service('PromiseHelperService', PromiseHelperService)
	.service('SubscriptionService', SubscriptionService)
	.service('TranslatedToastService', TranslatedToastService)
	.service('LoginService', LoginService)
