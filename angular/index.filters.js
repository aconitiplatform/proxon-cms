import {CurrencyEuroFilter} from './filters/currency-euro.filter';
import {LocalizedDateFilter} from './filters/localized-date.filter';
import {DateHoursMinutesFilter} from './filters/date-hours-minutes.filter';
import {StartfromFilter} from './filters/startfrom.filter';
import {CapitalizeFilter} from './filters/capitalize.filter';
import {HumanReadableFilter} from './filters/human_readable.filter';
import {TruncatCharactersFilter} from './filters/truncate_characters.filter';
import {TruncateWordsFilter} from './filters/truncate_words.filter';
import {TrustHtmlFilter} from './filters/trust_html.filter';
import {UcFirstFilter} from './filters/ucfirst.filter';
import {CurrencyDisplayFilter} from './filters/currency-display.filter';

angular.module('app.filters')
	.filter('currencyEuro', CurrencyEuroFilter)
	.filter('localizedDateFilter', LocalizedDateFilter)
	.filter('dateHoursMinutes', DateHoursMinutesFilter)
	.filter('startfrom', StartfromFilter)
	.filter('capitalize', CapitalizeFilter)
	.filter('humanReadable', HumanReadableFilter)
	.filter('truncateCharacters', TruncatCharactersFilter)
	.filter('truncateWords', TruncateWordsFilter)
	.filter('trustHtml', TrustHtmlFilter)
	.filter('ucfirst', UcFirstFilter)
	.filter('currencyDisplay', CurrencyDisplayFilter);
