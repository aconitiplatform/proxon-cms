import {SaveOnceDirective} from './directives/save-once/save-once.directive';
import {OpenCartDirective} from './directives/open-cart/open-cart.directive';
import {AddToCartDirective} from './directives/add-to-cart/add-to-cart.directive';
import {OpenQrDialogDirective} from './directives/open-qr-dialog/open-qr-dialog.directive';
import {ClearInputDirective} from './directives/clear-input/clear-input.directive';
import {UnitsInputDirective} from './directives/units-input/units-input.directive';
import {HiddenOnLiveDirective} from './directives/hidden-on-live/hidden-on-live.directive';
import {HttpInputDirective} from './directives/http-input/http-input.directive';

angular.module('app.directives')
	.directive('saveOnce', SaveOnceDirective)
	.directive('openCart', OpenCartDirective)
	.directive('addToCart', AddToCartDirective)
	.directive('openQrDialog', OpenQrDialogDirective)
	.directive('clearInput', ClearInputDirective)
	.directive('unitsInput', UnitsInputDirective)
	.directive('hiddenOnLive', HiddenOnLiveDirective)
	.directive('httpInput', HttpInputDirective);
