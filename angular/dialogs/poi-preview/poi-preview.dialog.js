export class PoiPreviewController{

	constructor(DialogService, $localStorage, PoiService, $sce, language, $stateParams, $q, $translate, LanguageNamesService, PoiTimingService){

		'ngInject';

		this.DialogService = DialogService;
		this.$localStorage = $localStorage;
		this.PoiService = PoiService;
		this.$sce = $sce;
		this.language = language;
		this.$stateParams = $stateParams;
		this.$q = $q;
		this.$translate = $translate;
		this.LanguageNamesService = LanguageNamesService;
		this.PoiTimingService = PoiTimingService;

		this.poiId = this.$stateParams.id;
		this.poi = this.$localStorage.poi[this.poiId].changes;

		this.showUrlModeHint = false;
		this.showTimeHint = false;

		// toggle visibility of hidden elements
		this.hiddenElements = false;
		this.hiddenElementsVisible = false;

		this.showContentSelection = this.poi.poi_contents.length > 1;

		this.devices = {
			"iPhone 7": {
				width: '375px',
				height: '667px'
			},
			"iPhone 7 plus": {
				width: '414px',
				height: '736px'
			},
			"iPhone SE": {
				width: '320px',
				height: '568px'
			},
			"Samsung Galaxy S3 - S7": {
				width: '360px',
				height: '640px'
			},
			"Samsung Galaxy S, S2, S3 mini": {
				width: '320px',
				height: '533px'
			},

		}

		if(!this.$localStorage.selectedPreviewDevice){

			this.$localStorage.selectedPreviewDevice = 'iPhone 7';

		}

		this.updateContent();

		this.previewTime = 'now';

	}

	hiddenElementsHere() {

		let hidden = false;

		let currentLanguageContent = this.poi.poi_contents.filter(content => {
			return content.language === this.language;
		});

		currentLanguageContent[0].data.groups.forEach(group => {

			if (_isHidden(group)) {
				hidden = true;
			}

			group.modules.forEach(module => {
				if (_isHidden(module)) {
					hidden = true;
				}

			});

		});

		return hidden;

		function _isHidden(obj) {
			return obj.hasOwnProperty('hidden') ? true : false;
		}

	}

	updateContent() {
		this.showUrlModeHint = false;
		this.showTimeHint = false;
		for (let content of this.poi.poi_contents) {
			if (content.language == this.language) {
				if (content.has_url) {
					this.showUrlModeHint = true;
					this.forwardUrl = content.url;
				}
				if (content.data.groups) {
					for (let group of content.data.groups) {
						if (group.ignoreInPOIChange && group.ignoreInPOIChange.showTimeHint) {
							this.showTimeHint = true;
						}
					}
				}
				break;
			}
		}

		this.hiddenElements = this.hiddenElementsHere();
		this.updateHints();
		this.updatePreview();
	}

	updateHints() {

		if (this.showUrlModeHint) {

			this.$q.resolve(

				this.$translate('POIS.PREVIEW_HINT', {url: this.forwardUrl})

			).then((translation) => {

				this.urlModeHint = translation;

			});

		}

		if (this.showTimeHint) {

			this.updateTimes();

		}

	}

	updatePreview(){

		this.PoiService.preview(this.poiId, this.language).then(response => {

			this.previewUrl = response.url;

			// add time paramter?
			if (this.previewTime != 'now') {
				this.previewUrl += '?time=' + this.previewTime;
			}

			// add showHidden paramter?
			if (this.hiddenElementsVisible) {
				let getParamterSymbol = this.previewUrl.includes('?') ? '&' : '?';
				this.previewUrl += getParamterSymbol + 'showHidden=true';
			}

			this.previewUrl = this.$sce.trustAsResourceUrl(this.previewUrl);

		});

	}


	updateTimes() {
		let times = [];
		for (let content of this.poi.poi_contents) {
			if (content.language == this.language) {
				if (content.data.groups) {
					for (let group of content.data.groups) {
						if (group.ignoreInPOIChange && group.ignoreInPOIChange.showTimeHint) {
							let time = this.PoiTimingService.getTimeWhereGroupIsVisible(group);
							if (time) {
								times.push({
									time: time.unix(),
									displayTime: time.format('llll'),
									group: group
								});
							}

						}
					}
				}
				break;
			}
		}
		this.times = times;
	}

	onLanguageChange() {

		this.updateContent();

	}

	onTimeChange() {

		this.updatePreview();

	}

	save(){

		this.DialogService.hide();

	}

	cancel(){

		this.DialogService.cancel();

	}

}
