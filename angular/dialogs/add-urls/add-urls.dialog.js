export class AddUrlsDialogController {

	constructor($translate, DialogService, TenantService, TranslatedToastService, tenant) {

		'ngInject';

		this.$translate = $translate;
		this.DialogService = DialogService;
		this.TenantService = TenantService;
		this.TranslatedToastService = TranslatedToastService;
		this.addUrlsFormData = {
			'tenant_id': tenant.id,
			'amount': 1,
			'kontakt_io': false
		};
		this.tenant = tenant;

	}

	save() {

		this.TenantService.addUrlsToTenant(this.addUrlsFormData).then(success => {
			this.TranslatedToastService.show('TOASTS.ADD_URLS_SUCCESS', {amount: this.addUrlsFormData.amount, name: this.tenant.name });
			console.log("add urls dialog success!",success);
			this.DialogService.hide(success.synced);
		}, error => {
			this.TranslatedToastService.error('TOASTS.ADD_URLS_ERROR');
		});

	}

	cancel() {

		this.DialogService.cancel();

	}

}
