export class ShowTransactionsController{

	constructor(DialogService, API, MicroPaymentService, microPaymentId){

		'ngInject';

		this.DialogService = DialogService;
		this.API = API;
		this.MicroPaymentService = MicroPaymentService;
		this.microPaymentId = microPaymentId;

		this.micropaymentTransactionsLoaded = false;
		this.skip = 0;
		this.microPaymentTransactions = [];

		this.loadMicroPaymentTransactions();

	}

	loadMicroPaymentTransactions() {

		this.micropaymentTransactionsLoaded = false;

		this.MicroPaymentService.getMicroPaymentTransactions(this.microPaymentId,this.skip).then((microPaymentTransactions) => {

			this.microPaymentTransactions = microPaymentTransactions;
			this.micropaymentTransactionsLoaded = true;

		}, (notMicroPaymentTransactions) => {


		});

	}

	loadMoreMicroPaymentTransactions(type) {

		if(type == 'before') {
			this.skip = this.skip - 10;
		} else if(type == 'after') {
			this.skip = this.skip + 10;
		}

		this.loadMicroPaymentTransactions();

	}

	cancel(){

		this.DialogService.cancel();

	}

}
