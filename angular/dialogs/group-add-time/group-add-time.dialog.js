export class GroupAddTimeController{

	constructor(DialogService, day, from, to, times, TimepickerConfigService){

		'ngInject';

		this.DialogService = DialogService;
		this.day = day;
		this.TimepickerConfigService = TimepickerConfigService;

		this.isValid = true;
		this.originalFrom = from;
		this.originalTo = to;
		this.originalTimes = angular.copy(times);

		if (from) {
			this.newTimeFrom = this.toMoment(from);
		} else {
			this.newTimeFrom = new moment(0);
		}

		if (to) {
			this.newTimeTo = this.toMoment(to);
		} else {
			this.newTimeTo = new moment(0);
		}

		if (this.originalTimes) {
			this.parseTimes(from, to, this.originalTimes);
		}

		this.checkValidity();

	}

	toMoment(timeString) {
		let m = new moment(0);
		let temp = new moment(timeString, 'HH:mm');
		m.hour(temp.hour()).minutes(temp.minutes());
		return m;
	}

	getDayName() {

		// we use a 2nd moment to preserve the users locale
		let d = moment().locale('en').day(this.day).isoWeekday();
		let m = moment();
		m.isoWeekday(d);

		return m.format('dddd');

	}

	checkValidity() {

		// time selector returns date-objects isntead of moment-objects
		this.newTimeFrom = new moment(this.newTimeFrom);
		this.newTimeTo = new moment(this.newTimeTo);

		let isValid = this.newTimeFrom.isBefore(this.newTimeTo);
		this.isValid = isValid;

		if (this.isValid) {
			this.checkOverlapping();
		} else {
			this.overlapping = false;
			this.combined = false;
		}

	}

	parseTimes(from, to, times) {

		let momentTimes = [];

		for(let i = 0; i < times.length; i++) {

			let time = times[i];

			if (time.time_from == from && time.time_to == to) {
				// skip
			} else {

				momentTimes.push({
					time_from: this.toMoment(time.time_from),
					time_to: this.toMoment(time.time_to),
					originalTime: time
				});

			}

		}

		this.times = momentTimes;

	}

	checkOverlapping() {

		this.notOverlapping = [];

		if (!this.times || this.times.length == 0) {
			this.overlapping = false;
		}

		let ret = [];

		for (let time of this.times) {

			if (time.time_from.isSameOrBefore(this.newTimeTo) && time.time_to.isSameOrAfter(this.newTimeFrom)) {

				ret.push(time);

			} else {

				this.notOverlapping.push(time);

			}

		}

		if (ret.length > 0) {
			this.overlapping = ret;
			this.combine(this.overlapping);
		} else {
			this.overlapping = false;
			this.combined = false;
		}

	}

	combine(times) {

		let start = this.newTimeFrom;
		let end = this.newTimeTo;

		for (let time of times) {
			if (time.time_from.isBefore(start)) {
				start = time.time_from;
			}
			if (time.time_to.isAfter(end)) {
				end = time.time_to;
			}
		}

		this.combined = {
			time_from: start,
			time_to: end
		}

	}

	save(){

		let newTimes = undefined;
		if (this.combined) {

			newTimes = [];
			newTimes.push({
				time_from: this.combined.time_from.format('HH:mm'),
				time_to: this.combined.time_to.format('HH:mm')
			});
			for (let time of this.notOverlapping) {
				newTimes.push(time.originalTime);
			}

		}

		var ret = {
			from: this.newTimeFrom,
			to: this.newTimeTo,
			newTimes: newTimes
		}

		this.DialogService.hide(ret);

	}

	cancel(){

		this.DialogService.cancel();

	}

}

