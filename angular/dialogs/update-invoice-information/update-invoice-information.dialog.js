
export class UpdateInvoiceInformationController{

	constructor(DialogService, $localStorage, SubscriptionService, TranslatedToastService){

		'ngInject';

		this.DialogService = DialogService;
		this.$localStorage = $localStorage;
		this.SubscriptionService = SubscriptionService;
		this.TranslatedToastService = TranslatedToastService;

		this.invoiceInformation = this.$localStorage.tenant.invoice_information;
		if (!this.invoiceInformation) {
			this.invoiceInformation = '';
		}

	}

	save(){

		this.loading = true;
		this.SubscriptionService.updateInvoiceInformation(this.invoiceInformation).then((result) => {

			this.TranslatedToastService.show('COMMON.UPDATE_SUCCESSFULL');
			this.DialogService.hide();

		}, (error) => {

			this.TranslatedToastService.error('COMMON.FAILED_TO_UPDATED');
			this.loading = false;

		});


	}

	cancel(){

		this.DialogService.cancel();

	}

}

