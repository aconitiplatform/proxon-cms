export class FontSelectController{

	constructor(selected, forbidden, DialogService, FontService, $localStorage, $timeout){

		'ngInject';

		this.selected = selected;
		this.forbidden = forbidden;
		this.DialogService = DialogService;
		this.FontService = FontService;
		this.$localStorage = $localStorage;
		this.$timeout = $timeout;

		this.pageSize = 10;
		this.testString = "Lorem ipsum dolor";

		this.use = 'all';

		this.all = {
			pageFrom: 0,
			pageTo: 0,
			pageItems: [],
			totalPages: 0,
			fonts: []
		}

		this.working = selected;

		this.FontService.loadFonts().then(() => {

			this.all.fonts = this.FontService.getFonts();
			this.all.totalPages = Math.floor(this.all.fonts.length / this.pageSize);

			this.selectPage(this.getPageForSelected());

			this.$timeout(100).then(() => {
				if (this.selected) {
					let selectedNode = angular.element( document.querySelector( '#' + this.toId(this.selected) ) );
					if (selectedNode && selectedNode[0]) {
						selectedNode[0].scrollIntoView();
					}
				}
			});

		}, (reject) => {
		});

	}

	$onInit() {

	}

	getPageForSelected() {

		let page = 0;

		if (this.selected) {

			let index = -1;

			for (let i = 0; i < this[this.use].fonts.length; i++) {

				let font = this[this.use].fonts[i];

				if (font && font.family) {

					if (this.selected == font.family) {
						index = i;
						break;
					}

				}

			}

			if (index > -1) {
				page = Math.floor(index / this.pageSize);
			}

		}

		return page;

	}

	loadMoreTop() {

		this.appendPage(-1);

	}

	loadMoreBottom() {

		this.appendPage(1);

	}

	appendFont(family) {
		this.FontService.appendFontByName(family);
	}

	selectPage(index) {

		this[this.use].pageFrom = index;
		this[this.use].pageTo = index;
		this.appendPage(1);

	}

	appendPage(direction) {

		let start, end;

		if (direction == 1) {
			if (this[this.use].pageTo + 1 > this[this.use].totalPages) {
				return;
			}
			start = this[this.use].pageTo;
			this[this.use].pageTo += 1;
			end = this[this.use].pageTo;
		} else if (direction == -1) {
			if (this[this.use].pageFrom - 1 < 0) {
				return;
			}
			end = this[this.use].pageFrom;
			this[this.use].pageFrom -= 1;
			start = this[this.use].pageFrom;
		}

		start *= this.pageSize;
		end *= this.pageSize;

		if (end > this[this.use].fonts.length) {
			end = this[this.use].fonts.length;
		}

		let target;
		if (direction == -1) {
			// adjust scroll top
			let list = angular.element( document.querySelector( '#font_select_list' ) );
			if (list) {
				target = list[0].children[1];
			}
		}

		let toAppend = this[this.use].fonts.slice(start, end);
		if (direction == 1) {
			this[this.use].pageItems = this[this.use].pageItems.concat(toAppend);
		} else if (direction == -1) {
			this[this.use].pageItems = toAppend.concat(this[this.use].pageItems);
		}

		for (let i = start; i < end; i++) {
			this.appendFont(this[this.use].fonts[i].family);
		}

		if (target) {
			target.scrollIntoView();
		}

	}

	toId(fontName) {
		return fontName.replace(/ /g, '_');
	}

	isSelected(font) {

		return this.selected == font;

	}

	isForbidden(font) {
		return this.forbidden && Array.isArray(this.forbidden) && this.forbidden.indexOf(font) > -1;
	}

	searchFonts() {

		if (!this.searchString) {
			this.searchString = '';
		}

		if (this.searchString == '') {
			this.use = 'all';
		} else {

			// https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
			let escapeRegex = /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g;

			let cleanSearchString = this.searchString.replace(escapeRegex, '\\$&"');

			let regex = new RegExp('.*' + cleanSearchString + '.*', 'gi')

			let searchFonts = [];

			for (let i = 0; i < this.all.fonts.length; i++) {
				let font = this.all.fonts[i];
				if (font && font.family) {
					if (regex.test(font.family)) {
						searchFonts.push(font);
					}
				}
			}

			this.search = {
				page: 0,
				fonts: searchFonts,
				totalPages: Math.floor(searchFonts.length / this.pageSize),
				pageItems: []
			};

			this.use = 'search';
			this.selectPage(this.getPageForSelected());

		}

	}

	selectFont(font) {

		if (this.isForbidden(font)) {
			return;
		}

		this.working = font;
		this.submit();

	}

	submit() {

		this.selected = this.working;

		this.DialogService.hide(this.selected);

	}

	cancel(){

		this.DialogService.cancel();

	}

}

