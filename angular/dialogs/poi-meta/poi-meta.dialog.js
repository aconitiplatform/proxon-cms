
export class PoiMetaController{

	constructor(DialogService, $localStorage, CommonService, $scope, poiId, contentIndex, GenerateNotificationService){

		'ngInject';

		this.DialogService = DialogService;
		this.$localStorage = $localStorage;
		this.CommonService = CommonService;
		this.$scope = $scope;
		this.poiId = poiId;
		this.contentIndex = contentIndex;
		this.GenerateNotificationService = GenerateNotificationService;

		this.poiContent = angular.copy($localStorage.poi[this.poiId].changes.poi_contents[this.contentIndex]);
		this.meta = this.poiContent.meta;
		this.loading = false;

		this.replaceTitle = true;
		this.replaceDescription = true;
		this.replaceImage = true;

		if(!this.meta.image_type){

			this.meta.image_type = 'id';

		}

	}

	idSet() {

		this.meta.image_type = 'id';

	}

	generateNotification() {

		this.loading = true;

		this.GenerateNotificationService.changeMeta(
			this.poiContent.url,
			this.meta,
			this.replaceTitle,
			this.replaceDescription,
			this.replaceImage
		).then((response) => {

			this.loading = false;

		}, (error) => {

			this.loading = false;

		});

	}

	save(){

		this.$localStorage.poi[this.poiId].changes.poi_contents[this.contentIndex].meta = angular.copy(this.meta);

		this.DialogService.hide();

	}

	cancel(){

		this.DialogService.cancel();

	}

}

