export class ModuleSelectionController{

	constructor(DialogService, GlobalConstants, $localStorage, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.GlobalConstants = GlobalConstants;
		this.$localStorage = $localStorage;
		this.TenantService = TenantService;

		this.loading = true;
		this.TenantService.getAvailableModules().then((modules) => {
			this.modules = modules;
			this.loading = false;
		}, (error) => {
			console.log('Failed to load modules', error);
			this.loading = false;
		});

	}

	selectModule(selectedType){

		this.selectedType = selectedType;

		this.save();

	}

	save(){

		this.DialogService.hide(this.selectedType);

	}

	cancel(){

		this.DialogService.cancel();

	}

	isHidden(module){

		if(module.hidden){

			if(this.GlobalConstants.appEnv == 'production'){

				// return true;

				// workaround to demo vag module
				if(module.type == 'vag' || module.type == 'payment'){

					let rakete7 = '0f2a33d6-8da6-4f78-aeee-38316fef1734';
					let vag = 'e65832f1-128f-4064-a87d-cbf474dac474';
					let zollhof = "1979940e-6ac1-492a-acad-e39f2c325c65";

					let allowedTenants = []

					if(module.type == 'vag'){
						allowedTenants.push(rakete7, vag, zollhof);
					} else if (module.type == 'payment'){
						allowedTenants.push(rakete7);
					}

					if(allowedTenants.indexOf(this.$localStorage.user.tenant_id) != -1){
						return false;
					}else{
						return true;
					}

				}else{

					return true;

				}

			}else{

				return false;

			}

		}else{

			return false;

		}

	}

}

