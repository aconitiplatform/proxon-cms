export class KontaktIoSyncController {

	constructor(tenant, $state, TenantService, DialogService, TranslatedToastService) {

		'ngInject';


		this.$state = $state;
		this.TenantService = TenantService;
		this.DialogService = DialogService;
		this.TranslatedToastService = TranslatedToastService;

		this.tenant = tenant;
		this.syncBtnDisabled = false;
		console.log("tenant", tenant);

		if (tenant.syncable_batches) {
			this.getSyncBatches();
		}

	}

	cancel() {

		this.DialogService.cancel();

	}

	checkIfInitialized() {

		this.syncBtnDisabled = true;

		if (this.tenant.kontakt_io_venue_id && this.tenant.initial_kontakt_io_sync == false) {

			this.TenantService.initializedAtKontaktio(this.tenant.id).then(result => {

				console.log("initializedAtKontaktio", result);
				this.TranslatedToastService.show('TOASTS.KONTAKTIO_SYNC_SUCCESS');
				this.cancel();

			}, error => {
				this.syncBtnDisabled = false;
			});

		}

		if (this.batches.length) {

			this.TenantService.syncUrlBatchesWithKontaktio(this.tenant.id).then(result => {

				console.log("syncUrlBatchesWithKontaktio", result);

				let allBatchesSynced = true;
				Object.keys(result).forEach(batch_id => {

					if (result[batch_id].batch.synced) {
						this.batchesMap.get(batch_id).sync_successful = true;
					} else {
						allBatchesSynced = false;
						this.batchesMap.get(batch_id).sync_successful = false;
					}

				});

				if (allBatchesSynced) {
					this.tenant.syncable_batches = false;
					this.TranslatedToastService.show('TOASTS.KONTAKTIO_SYNC_SUCCESS');
					this.cancel();
				}

				this.syncBtnDisabled = false;

			}, error => {
				this.syncBtnDisabled = false;
			});

		}

	}

	getSyncBatches() {

		this.TenantService.getSyncBatches(this.tenant.id).then(batches => {
			console.log("batches", batches);

			this.batches = batches;
			this.batchesMap = new Map();

			this.batches.forEach((batch, index) => {
				this.batchesMap.set(batch.id, this.batches[index]);
			});

		});

	}

}

