export class CartDialogController {

	constructor($translate, DialogService, CartService, GlobalConstants, StripeService, TranslatedToastService) {

		'ngInject';

		this.$translate = $translate;
		this.DialogService = DialogService;
		this.CartService = CartService;
		this.GlobalConstants = GlobalConstants;
		this.StripeService = StripeService;
		this.TranslatedToastService = TranslatedToastService;

		this.StripeService.setupStripe();

		this.cart = this.CartService.getCart();
		this.totalPrice = this.CartService.getTotalPrice();
		this.currentNavItem = 'cart';
		this.address = {};
		this.productNames = {
			'nfc_tag': 'NFC-Tag',
			'beacon': 'Beacon',
			'beacon_pro': 'Beacon Pro'
		};

	}

	removeFromCart(index) {

		this.cart = this.CartService.removeFromCart(index);
		this.totalPrice = this.CartService.getTotalPrice();

		console.log('this.cart is ', this.cart);

		if (!this.cart.length) {
			this.cancel();
		}

	}

	buy() {

		this.StripeService.openStripeCheckout(this.totalPrice).then((tokenID) => {

			this.CartService.placeOrder(this.cart, this.address, tokenID).then((success) => {
				this.TranslatedToastService.show('TOASTS.ORDER_SUCCESS');
				this.emptyCart();
			}, (error) => {
				this.TranslatedToastService.error('TOASTS.ORDER_ERROR');
				this.cancel();
			});

		}, (noTokenID) => {
			this.cancel();
		});

	}

	emptyCart() {

		this.CartService.emptyCart();
		this.cancel();

	}

	cancel() {

		this.DialogService.cancel();

	}

}
