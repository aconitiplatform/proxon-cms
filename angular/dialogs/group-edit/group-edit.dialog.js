import {GroupAddTimeController} from '../group-add-time/group-add-time.dialog';

export class GroupEditController{

	constructor(DialogService, group, poiIsLockedByAnotherUser, moment, $q, $translate, TimepickerConfigService, $timeout){

		'ngInject';

		this.DialogService = DialogService;
		this.group = group;
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.moment = moment;
		this.$q = $q;
		this.$translate = $translate;
		this.TimepickerConfigService = TimepickerConfigService;
		this.$timeout = $timeout;

		this.currentNavItem = 'timing';

		this.label = this.group.label;
		this.date_from = new Date(this.group.time_from*1000);
		this.date_from_test = this.group.time_from;
		this.time_from = new Date(this.group.time_from*1000);

		if(this.group.time_to){

			this.hasTimeTo = true;

			this.date_to = new Date(this.group.time_to*1000);
			this.time_to = new Date(this.group.time_to*1000);

		}else{
			this.date_to = new Date();
			this.time_to = new Date();
		}

		if(!this.group.show) {
			this.group.show = 'always';
		}

		this.hasWeekdays = this.group.weekdays_active;
		this.weekdays = this.group.weekdays;
		this.delete_when_expired = this.group.delete_when_expired;

	}

	addTime(day){

		this.DialogService.fromTemplate('', 'group-add-time', {
			controller: GroupAddTimeController,
			controllerAs: 'vm',
			clickOutsideToClose:false,
			fullscreen: true,
			skipHide: true,
			locals: {
				day: day,
				from: null,
				to: null,
				times: this.weekdays[day].times
			}
		}).then((ret) => {

			if (ret.newTimes) {

				this.weekdays[day].times = ret.newTimes;

			} else {

				this.weekdays[day].times.push({
					'time_from':ret.from.format('HH:mm'),
					'time_to': ret.to.format('HH:mm'),
				});

			}

		});

	}

	editTime(day, index) {

		let times = this.weekdays[day].times[index];

		this.DialogService.fromTemplate('', 'group-add-time', {
			controller: GroupAddTimeController,
			controllerAs: 'vm',
			clickOutsideToClose:false,
			fullscreen: true,
			skipHide: true,
			locals: {
				day: day,
				from: times.time_from,
				to: times.time_to,
				times: this.weekdays[day].times
			}
		}).then((ret) => {

			if (ret.newTimes) {

				this.weekdays[day].times = ret.newTimes;

			} else {

				this.weekdays[day].times[index] = {
					'time_from': ret.from.format('HH:mm'),
					'time_to': ret.to.format('HH:mm'),
				};

			}

		});

	}

	removeTime(day, index){

		this.weekdays[day].times.splice(index, 1);

	}

	save(){

		var time_from = moment(moment(this.date_from).format('YYYY-MM-DD') + ' ' + moment(this.time_from).format('HH:mm'), 'YYYY-MM-DD HH:mm').unix();

		this.group.label = this.label;
		this.group.time_from = time_from;
		this.group.weekdays_active = this.hasWeekdays;
		this.group.weekdays = this.weekdays;

		if(this.hasTimeTo){

			var time_to = moment(moment(this.date_to).format('YYYY-MM-DD') + ' ' + moment(this.time_to).format('HH:mm'), 'YYYY-MM-DD HH:mm').unix();

			this.group.time_to = time_to;

		}else{

			delete this.group.time_to;

		}

		this.group.delete_when_expired = this.delete_when_expired;

		this.DialogService.hide(this.group);

	}

	getDayName(day) {
		// we use a 2nd moment to preserve the users locale
		let d = moment().locale('en').day(day).isoWeekday();

		let m = moment();
		m.isoWeekday(d);
		return m.format('ddd');
	}

	dateRevalidate() {

		// fix for https://github.com/angular/material/issues/7896

		let start = this.date_from;
		let end = this.date_to;
		this.date_from = null;
		this.date_to = null;
		this.$timeout(() => {
			this.date_from = start;
			this.date_to = end;
		});

	}

	delete(){

		this.DialogService.hide(false);

	}

	cancel(){

		this.DialogService.cancel();

	}

	submit() {

		let formScope = angular.element(document.getElementById('group_dialog_form')).scope();
		let form = formScope.group_dialog_form;

	  	if(form.group_label.$invalid){
			this.currentNavItem = 'timing';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
