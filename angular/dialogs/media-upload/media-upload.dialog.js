export class MediaUploadController{

	constructor(DialogService, $scope, $rootScope, type, customMimeType, allowMultipleSelections, disabledFiles, EventUnsubscribeService){

		'ngInject';

		this.DialogService = DialogService;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.type = type;
		this.customMimeType = customMimeType;
		this.allowMultipleSelections = allowMultipleSelections;
		this.disabledFiles = disabledFiles;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.$scope.$on('contentFromDialogSelected', (event, data) => {

			this.save(data);

		});

		this.unsubscribe = this.$rootScope.$on('fileSelected', (event, data) => {

			if (data.id) {
				this.selectedFile = data.id;
			} else {
				this.selectedFile = data.files;
			}

		});

	}

	save(){

		this.DialogService.hide(this.selectedFile);

	}

	cancel(){

		this.DialogService.cancel();

	}

}

