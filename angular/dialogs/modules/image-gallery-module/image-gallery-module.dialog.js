import {MediaUploadController} from '../../media-upload/media-upload.dialog';

export class ImageGalleryModuleController {

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, $translate, CommonService, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.$translate = $translate;
		this.CommonService = CommonService;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.ensureDefaults();

	}

	selectImages(event){

		var disabledFiles = [];

		if(this.module.data.images && Array.isArray(this.module.data.images)){

			_.each(this.module.data.images, (image) => {
				disabledFiles.push(image.id);
			});

		}

		this.DialogService.fromTemplate('', 'media-upload', {
			controller: MediaUploadController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				type: 'image',
				allowMultipleSelections: true,
				disabledFiles: disabledFiles,
				customMimeType: false,
				isDialog: true
			}
		}).then((images) => {

			if(typeof this.module.data.images == 'undefined'){

				this.module.data.images = [];

			}

			_.each(images, (id) => {

				this.module.data.images.push({
					id: id,
					caption: '',
					alt: ''
				});

			});

		});

	}

	removeImage(image){

		_.pull(this.module.data.images, image);

	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	updateClassicAlign() {

		this.module.data.css.classic.inner.textAlign = this.align;

		if (this.align == 'left') {
			this.module.data.css.classic.inner.marginLeft = '0';
			this.module.data.css.classic.inner.marginRight = 'auto';
		} else if (this.align == 'center') {
			this.module.data.css.classic.inner.marginLeft = 'auto';
			this.module.data.css.classic.inner.marginRight = 'auto';
		} else if (this.align == 'right') {
			this.module.data.css.classic.inner.marginLeft = 'auto';
			this.module.data.css.classic.inner.marginRight = '0';
		}

	}

	ensureDefaults() {

		if (!this.module.data.css.classic) {
			this.module.data.css.classic = {};
		}

		if (!this.module.data.css.classic.caption) {
			this.module.data.css.classic.caption = {};
		}

		if (!this.module.data.css.classic.inner) {
			this.module.data.css.classic.inner = {};
		}

		if (!this.module.data.css.classic.inner.textAlign) {
			this.module.data.css.classic.inner.textAlign = 'center';
		}
		this.align = this.module.data.css.classic.inner.textAlign;

		if (!this.module.data.css.swiper) {
			this.module.data.css.swiper = {};
		}

		if (!this.module.data.css.swiper.pagination) {
			this.module.data.css.swiper.pagination = {};
		}

		if (!this.module.data.css.swiper.pagination.display) {
			this.module.data.css.swiper.pagination.display = 'inherit';
		}

		if (!this.module.data.css.swiper.buttons) {
			this.module.data.css.swiper.buttons = {};
		}

		if (!this.module.data.css.swiper.buttons.display) {
			this.module.data.css.swiper.buttons.display = 'inherit';
		}

		if (this.module.data.show_caption !== false) {
			this.module.data.show_caption = true;
		}

		if (!this.module.data.css.grid) {
			this.module.data.css.grid = {};
		}

		if (!this.module.data.css.grid.element) {
			this.module.data.css.grid.element = {};
		}

	}

	submit() {

		let formScope = angular.element(document.getElementById('image_gallery_module_dialog_form')).scope();
		let form = formScope.image_gallery_module_dialog_form;

	  	if(form.module_label.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_spacing', 'design_item_max_width', 'design_wrapper_height', 'design_wrapper_width', 'design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}

