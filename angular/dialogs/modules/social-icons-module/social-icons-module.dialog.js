export class SocialIconsModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, $translate, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.$translate = $translate;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.valid = {
			facebook: true,
			instagram: true,
			twitter: true,
			youtube: true,
			xing: true,
			linkedin: true,
			google_plus: true,
			pinterest: true,
			tumblr: true,
			reddit: true
		}

		for (let field in this.valid) {
			this.checkValid(field);
		}

	}

	checkValid(field) {

		let ret = true;

		if (this.module.data[field]) {

			let domain = field;

			if (field == 'google_plus') {
				domain = 'plus.google';
			}

			// checks for DOMAIN.TLD (more or less)
			let regex = new RegExp('.*' + domain + '\\.[a-z]{2,7}/.*');

			ret = regex.test(this.module.data[field]);

		}

		this.valid[field] = ret;

	}

	save(){

		console.info(this.module);

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('social_icons_module_dialog_form')).scope();
		let form = formScope.social_icons_module_dialog_form;

	  	if(form.module_label.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
