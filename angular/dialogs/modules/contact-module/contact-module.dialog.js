export class ContactModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, $translate, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.$translate = $translate;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

	}

	save(){

		console.info(this.module);

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('contact_module_dialog_form')).scope();
		let form = formScope.contact_module_dialog_form;

	  	if(form.module_label.$invalid || form.data_recipient.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_bottom', 'design_margin_top', 'design_margin_bottom'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}

