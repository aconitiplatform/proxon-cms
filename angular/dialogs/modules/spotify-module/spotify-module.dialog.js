
export class SpotifyModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, CommonService, $q, $translate, $sce, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.CommonService = CommonService;
		this.$q = $q;
		this.$translate = $translate;
		this.$sce = $sce;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';
	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	getIFrameSrc() {
		let uri = encodeURI(this.module.data.uri);
		let src = 'https://embed.spotify.com/?uri=' + uri + '&theme=' + this.module.data.theme;
		src = this.$sce.trustAsResourceUrl(src);
		return src;
	}

	submit() {

		let formScope = angular.element(document.getElementById('spotify_module_dialog_form')).scope();
		let form = formScope.spotify_module_dialog_form;

	  	if(form.module_label.$invalid || form.data_uri.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
