import {MediaUploadController} from '../../media-upload/media-upload.dialog';

export class AudioModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $scope, CommonService, $q, $translate, MediaService, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$scope = $scope;
		this.CommonService = CommonService;
		this.$q = $q;
		this.$translate = $translate;
		this.MediaService = MediaService;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		if (this.module.data.id) {
			this.setAudioName(this.module.data.id)
		}

	}

	selectAudio() {

		this.DialogService.fromTemplate('', 'media-upload', {
			controller: MediaUploadController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				type: 'audio',
				allowMultipleSelections: false,
				disabledFiles: [],
				customMimeType: false,
				isDialog: true
			}
		}).then((id) => {

			this.module.data.id = id;
			this.setAudioName(id);

		});

	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	setAudioName(id) {

		this.MediaService.getMediaDetails(id).then((details) => {

			if (!details.errors) {

				this.audioName =  details.name;

			}

		});

	}

	submit() {

		let formScope = angular.element(document.getElementById('audio_module_dialog_form')).scope();
		let form = formScope.audio_module_dialog_form;

	  	if(form.module_label.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
