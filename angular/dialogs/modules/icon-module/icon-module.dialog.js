export class IconModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, $translate, IconsService, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.$translate = $translate;
		this.IconsService = IconsService;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.iconsets = {};

		// this.iconsets = this.IconsService.getAllIconSets();


		this.getIconSets();


	}

	getIconSets(){

		this.iconsets = this.IconsService.getAllIconSets();

	}

	selectIcon(iconset, icon){

		this.module.data.icon_set = iconset;
		this.module.data.icon = icon;

	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('icon_module_dialog_form')).scope();
		let form = formScope.icon_module_dialog_form;

	  	if(form.module_label.$invalid){
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_font_size', 'design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}

