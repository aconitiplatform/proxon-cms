import {MediaUploadController} from '../../media-upload/media-upload.dialog';

export class VideoModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, CommonService, $q, $translate, MediaService, $sce, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.CommonService = CommonService;
		this.$q = $q;
		this.$translate = $translate;
		this.MediaService = MediaService;
		this.$sce = $sce;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.vimeoConfig = {
			videoId: ''
		}

		// manually call "onChange" to udpate vimeo preview
		this.onChange();

		this.invalidVimeoURL = false;

		if (this.module.data.mp4_id) {
			this.setVideoName(this.module.data.mp4_id, 'mp4');
		}

		if (this.module.data.webm_id) {
			this.setVideoName(this.module.data.webm_id, 'webm');
		}

	}

	onChange(){

		let fallback = true;

		if (this.module.data.url) {

			let regex = /^https:\/\/vimeo\.com\/(\d+).*/g
			let match = regex.exec(this.module.data.url);

			if (match) {

				let id = match[1];
				this.vimeoConfig.videoId = id;
				fallback = false;
				this.invalidVimeoURL = false;

			}

		}

		if (fallback) {

		this.invalidVimeoURL = true;
		this.vimeoConfig.videoId = '';

		}

	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	getMediaUrlById(id) {

		let ret = this.CommonService.getMediaURL(id);
		return ret;

	}

	getMediaUrlByUrl(url) {

		let ret = this.$sce.trustAsResourceUrl(url);
		return ret;

	}

	selectVideo(source) {

		let mimeType = 'video/' + source;

		this.DialogService.fromTemplate('', 'media-upload', {
			controller: MediaUploadController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				type: 'video',
				allowMultipleSelections: false,
				disabledFiles: [],
				customMimeType: mimeType,
				isDialog: true
			}
		}).then((id) => {

			if (source == 'mp4'){
				this.module.data.mp4_id = id;
			} else if (source == 'webm'){
				this.module.data.webm_id = id;
			}

			this.setVideoName(id, source);

		});

	}

	setVideoName(id, source) {

		this.MediaService.getMediaDetails(id).then((details) => {

			if (!details.errors) {

				if (source == 'mp4'){
					this.videoNameMP4 =  details.name;
				} else if (source == 'webm'){
					this.videoNameWEBM =  details.name;
				}

			}

		});

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('video_module_dialog_form')).scope();
		let form = formScope.video_module_dialog_form;

		let fields = ['module_label', 'data_youtube_url', 'data_vimeo_url', 'data_webm_url', 'data_mp4_url'];

		for (let field of fields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'general';
				break;
			}

		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
