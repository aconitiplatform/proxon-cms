import {MediaUploadController} from '../../media-upload/media-upload.dialog';

export class ImageModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $scope, CommonService, $q, $translate, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$scope = $scope;
		this.CommonService = CommonService;
		this.$q = $q;
		this.$translate = $translate;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.imageId = this.module.data.id;
		this.customUrl = this.module.data.url;
		this.type = this.module.data.type;

		if (!this.type) {

			this.type = 'id';

		}

		this.updatePreview();

	}

	updatePreview(){

		if(this.type == 'url' && this.customUrl){

			this.previewUrl = this.customUrl;

		}

	}

	save(){

		this.module.data.id = this.imageId;
		this.module.data.url = this.customUrl;
		this.module.data.type = this.type;

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('image_module_dialog_form')).scope();
		let form = formScope.image_module_dialog_form;

	  	if(form.module_label.$invalid){	
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_image_width', 'design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
