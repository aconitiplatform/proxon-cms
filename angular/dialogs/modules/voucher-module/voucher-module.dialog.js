import {MediaUploadController} from '../../media-upload/media-upload.dialog';

export class VoucherModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, MediaService, $timeout, TranslatedToastService, Upload, $scope, $auth, CommonService, $window, $http, $rootScope, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.CommonService = CommonService;
		this.MediaService = MediaService;
		this.$timeout = $timeout;
		this.Upload = Upload;
		this.$scope = $scope;
		this.$auth = $auth;
		this.$window = $window;
		this.$http = $http;
		this.$rootScope = $rootScope;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.bcContentType = 'text';

		this.disableSave = true;

		if (this.module.data.image_id) {
			this.updateImageId(this.module.data.image_id, true);
		}
		this.updateBarcodeContentType(false);

	}

	save(){

		this.uploadCanvasImage();

		this.disableSave = true;

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	updateBarcodeContentType(suppressUpdateCanvas) {

		if (this.module.data.bc_type=='code39') {
			this.bcContentType = 'number';
			// remove everything that is not a number, because code39 only supports numbers
			if (this.module.data.bc_content && typeof this.module.data.bc_content === 'string') {
				this.module.data.bc_content = this.module.data.bc_content.replace(/\D/g,'');
			}
		} else {
			this.bcContentType = 'text';
		}

		if (this.module.data.bc_type && this.module.data.bc_content) {

			this.updateBarcodeImageSrc().then((src) => {
				if (!suppressUpdateCanvas) {
					this.updateVoucherCanvas();
				}
			});

		} else {

			if (!suppressUpdateCanvas) {
				this.updateVoucherCanvas();
			}

		}


	}

	updateBarcodeImageSrc() {

		let ret = this.$q((resolve,reject) => {

			if (this.module.data.bc_type && this.module.data.bc_content) {

				let data = {
					bc_type: this.module.data.bc_type,
					bc_content: this.module.data.bc_content
				}

				this.$http({
					method: 'POST',
					url: this.CommonService.getVoucherProxyURL(),
					data: data,
					headers: {
						'Content-Type':'application/json',
						'Accept':'application/json'
					}
				}).then((response) => {
					let src = response.data;
					this.barcodeImageSrc = src;
					resolve(src);
				}, (e) => {
					console.error(e);
					reject(e);
				});

			} else {
				reject();
			}

		});

		return ret;

	}

	selectImage(event){

		this.DialogService.fromTemplate('', 'media-upload', {
			controller: MediaUploadController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				type: 'image',
				allowMultipleSelections: false,
				disabledFiles: [],
				customMimeType: false,
				isDialog: true
			}
		}).then((id) => {

			this.updateImageId(id);

		});

	}

	updateImageId(id, suppressUpdateCanvas) {

		this.module.data.image_id = id;
		this.imageUrl = this.CommonService.getVoucherImageURL(id);
		if (!suppressUpdateCanvas) {
			this.updateVoucherCanvas();
		}

	}

	prepareVoucherText(textState, state) {
		if (!textState.fontFamily) {
			textState.fontFamily = 'Arial';
		}
		if (!textState.lineHeight) {
			textState.lineHeight = 18;
		}
		let color = this.module.data.voucher_font_color;
		if (!color) {
			color = '#000000'
		}
		if (!textState.style) {
			textState.style = color;
		}
		textState.font = textState.lineHeight + 'px ' + textState.fontFamily;
		textState.startY = state.y;

		let lines = [];

		if (!textState.adjustFontSize) {
			let paras = textState.text.split(/\r?\n/g);
			state.context.font = textState.font;
			for (let i = 0; i < paras.length; i++) {
				let words = paras[i].split(' ');
				let line = '';
				for(let n = 0; n < words.length; n++) {
					let testLine = line + words[n];
					let metrics = state.context.measureText(testLine);
					let testWidth = metrics.width;
					if (testWidth > state.maxWidth) {
						if (n > 0) {
							lines.push(line);
							line = words[n] + ' ';
						}
					} else {
						line = testLine + ' ';
					}
				}
				lines.push(line);
			}
		} else {
			let line = textState.text;
			let maxWidth = state.maxWidth;
			if (textState.frame) {
				maxWidth -= 2 * textState.frame.padding;
			}
			for (let i = textState.lineHeight; i > 8; i--) {
				let tempFont = i + 'px ' + textState.fontFamily;
				state.context.font = tempFont;
				let metrics = state.context.measureText(line);
				let testWidth = metrics.width;
				if (testWidth <= maxWidth) {
					textState.font = tempFont;
					textState.lineHeight = i;
					break;
				}
			}
			lines.push(line);
		}

		if (textState.frame) {
			state.y += 2 * textState.frame.padding;
			textState.startY += textState.frame.padding;
			textState.x = state.padding + textState.frame.padding;
		}

		state.y += lines.length * textState.lineHeight;
		state.y += state.padding;
		textState.lines = lines;

		state.childStates.push(textState);
	}

	prepareVoucherImage(imageState, state) {

		imageState.startY = state.y;

		if (!imageState.imgWidth) {
			imageState.imgWidth = state.maxWidth;
		}

		if (!imageState.imgHeight) {
			imageState.imgHeight = imageState.imgWidth;
		}

		state.y = state.y + imageState.imgHeight + state.padding;

		state.childStates.push(imageState);

	}

	prepareVoucher(state) {

		if (this.imageUrl) {
			let scaledHeight = (state.maxWidth / state.previewImage.width) * state.previewImage.height;
			this.prepareVoucherImage({
				img: state.previewImage,
				imgHeight: scaledHeight
			}, state);
		}

		if (this.module.data.description) {
			this.prepareVoucherText({
				text: this.module.data.description,
				lineHeight: 20
			}, state);
		}

		if (this.module.data.type == 'barcode') {
			this.prepareVoucherImage({
				img: state.barcodeImage,
				imgHeight: 200,
				imgWidth: 200
			}, state);
		}

		if (this.module.data.type == 'text' && this.module.data.text_content) {
			let color = this.module.data.voucher_code_background_color;
			if (!color) {
				color = '#e0e0e0';
			}
			this.prepareVoucherText({
				text: this.module.data.text_content,
				lineHeight: 50,
				adjustFontSize: true,
				frame: {
					background: color,
					padding: 20
				}
			}, state);
		}

		if (this.module.data.terms) {
			this.prepareVoucherText({
				text: this.module.data.terms,
				lineHeight: 12
			}, state);
		}

		if (this.module.data.duration) {
			// duration is set by the server, so just prepare space
			this.prepareVoucherText({
				text: "",
				lineHeight: 12
			}, state);
		}

	}

	writeVoucherContent(state) {
		state.canvas.width = state.maxWidth + 2 * state.padding;
		state.canvas.height = state.y;

		let color = this.module.data.voucher_background_color;
		if (!color) {
			color = '#ffffff';
		}
		state.context.fillStyle = color;
		state.context.fillRect(0, 0, state.canvas.width, state.canvas.height);

		for (let i = 0; i < state.childStates.length; i++) {
			let childState = state.childStates[i];
			if (!childState.img) {

				let x = state.padding;
				if (childState.x) {
					x = childState.x;
				}
				let y = childState.startY;

				if (childState.frame) {
					state.context.fillStyle = childState.frame.background;
					let frameX = x - childState.frame.padding;
					let frameY = y - childState.frame.padding;
					let frameWidth = state.maxWidth;
					let frameHeight = childState.lineHeight + 2 * childState.frame.padding;
					state.context.fillRect(frameX, frameY, frameWidth, frameHeight);
				}

				state.context.fillStyle = childState.style;
				state.context.font = childState.font;
				state.context.textBaseline = 'top';

				for (let i = 0; i < childState.lines.length; i++) {
					state.context.fillText(childState.lines[i], x, y);
					y += childState.lineHeight;
				}
			} else {
				let x = state.padding + state.maxWidth / 2 - childState.imgWidth / 2;
				try {
					state.context.drawImage(childState.img, x, childState.startY, childState.imgWidth, childState.imgHeight);
				} catch (e) {
					console.error('failed to draw image on canvas', e);
				}
			}
		}
	}

	updateVoucherCanvas() {

		let canvas = document.createElement('canvas');

		let state = {
			y: 20,
			childStates: [],
			canvas: canvas,
			maxWidth: 400,
			context: canvas.getContext('2d'),
			padding: 20,
			previewImage: new Image,
			barcodeImage: new Image,
			barcodeImageLoaded: true,
			previewImageLoaded: true
		};

		if (this.imageUrl) {
			state.previewImageLoaded = false;
		}

		if (this.module.data.type == 'barcode' && this.barcodeImageSrc) {
			state.barcodeImageLoaded = false;
		}

		let execute = () => {
			this.prepareVoucher(state);
			this.writeVoucherContent(state);
			let parent = document.getElementById('voucher_canvas');
			if (parent) {
				parent.innerHTML = '';
				parent.appendChild(state.canvas);
				this.canvas = state.canvas;
			}
			this.disableSave = false;
		};
		execute = execute.bind(this);

		let checkLoaded = () => {
			if (state.barcodeImageLoaded && state.previewImageLoaded) {
				this.$timeout(execute);
			}
		}
		checkLoaded = checkLoaded.bind(this);

		//fallback for no logo and no barcode image
		checkLoaded();

		if (!state.barcodeImageLoaded) {
			state.barcodeImage.onload = () => {
				state.barcodeImageLoaded = true;
				checkLoaded();
			};
			state.barcodeImage.src = this.barcodeImageSrc;
		}

		if (!state.previewImageLoaded) {
			state.previewImage.onload = () => {
				state.previewImageLoaded = true;
				checkLoaded();
			};
			state.previewImage.src = this.imageUrl;
		}

	}

	uploadCanvasImage() {

		let dataURI = this.canvas.toDataURL();
		let byteString;

		if (dataURI.split(',')[0].indexOf('base64') >= 0) {
			byteString = atob(dataURI.split(',')[1]);
		} else {
			byteString = unescape(dataURI.split(',')[1]);
		}

		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
		// write the bytes of the string to a typed array
		var ia = new Uint8Array(byteString.length);

		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		let blob = new Blob([ia], {type:mimeString});

		this.Upload.upload({
			url: this.CommonService.getMediaUploadURL(),
			data: {
				name: 'Voucher.png',
				'used_for': 'voucher',
				file: blob
			},
			headers: {
				Authorization: 'Bearer ' + this.$window.localStorage.satellizer_token,
				Accept: 'application/x.laravel.v1+json'
			}
		}).then((response) => {

			this.module.data.voucher_image_id = response.data.data.id;

			this.DialogService.hide(this.module);

		}, (response) => {

			this.$rootScope.$emit('uploadFailed', {
				toast: 'Upload failed.'
			});

		}, (evt) => {

		});

	}

	submit() {

		let formScope = angular.element(document.getElementById('voucher_module_dialog_form')).scope();
		let form = formScope.voucher_module_dialog_form;

		let fields = ['module_label', 'data_youtube_url', 'data_vimeo_url', 'data_webm_url', 'data_mp4_url'];

		for (let field of fields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'general';
				break;
			}
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_bottom'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
