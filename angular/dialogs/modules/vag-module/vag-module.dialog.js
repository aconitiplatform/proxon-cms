export class VagModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, $q, $translate, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.$q = $q;
		this.$translate = $translate;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.stops = [];

		if(this.module.data && this.module.data.stop_name) {
			this.search = this.module.data.stop_name;
			this.loadStopData();
		}

	}

	loadStopData() {

		if (this.search && this.search.length > 1) {

			let url = 'https://start.vag.de/dm/api/haltestellen.json/vgn?name=' + this.search;
			$.ajax({
				url: url
			}).done((data) => {

				let temp = data.Haltestellen
				if (data && temp) {
					this.stops = temp;
				} else {
					this.stops = [];
				}

				if (this.stops.length > 0) {
					this.module.data.stop_id = this.stops[0].VGNKennung;
				}

			});

		}

	}

	onChange() {

		if (this.module.data.stop_id) {

			for (let stop of this.stops) {
				if (this.module.data.stop_id == stop.VGNKennung) {
					this.module.data.stop_name = stop.Haltestellenname;
					break;
				}
			}

		}

	}

	save(){

		console.info(this.module);

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

	}

}
