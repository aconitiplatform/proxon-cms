export class NewsletterModuleController{

	constructor(DialogService, module, poiIsLockedByAnotherUser, NewsletterConfigurationService, $q, $translate, TenantService){

		'ngInject';

		this.DialogService = DialogService;
		this.module = angular.copy(module);
		this.poiIsLockedByAnotherUser = poiIsLockedByAnotherUser;
		this.NewsletterConfigurationService = NewsletterConfigurationService;
		this.$q = $q;
		this.$translate = $translate;
		this.TenantService = TenantService;

		this.currentNavItem = 'general';

		this.getNewsletterConfigurations();

	}

	getNewsletterConfigurations(){

		this.isLoading = true;

		this.NewsletterConfigurationService.getNewsletterConfigurations().then((newsletterConfigurations) => {

			this.isLoading = false;

			this.newsletterConfigurations = newsletterConfigurations;

		});

	}

	save(){

		this.DialogService.hide(this.module);

	}

	cancel(){

		this.DialogService.cancel();

	}

	delete(){

		this.DialogService.hide(false);

	}

	submit() {

		let formScope = angular.element(document.getElementById('newsletter_module_dialog_form')).scope();
		let form = formScope.newsletter_module_dialog_form;

	  	if(form.module_label.$invalid){	
			this.currentNavItem = 'general';
			return;
		}

		let designFields = ['design_padding_top', 'design_padding_right', 'design_padding_bottom', 'design_padding_left', 'design_margin_top', 'design_margin_right', 'design_margin_bottom', 'design_margin_left'];

		for (let field of designFields) {

			if (form[field] && form[field].$invalid) {
				this.currentNavItem = 'design';
				return;
			}

		}

	}

}
