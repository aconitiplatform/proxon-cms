export class QrCodeDialogController {

	constructor($q, $localStorage, API, GlobalConstants, DialogService, url) {

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.GlobalConstants = GlobalConstants;
		this.DialogService = DialogService;
		this.url = url;
		this.loading = true;
		this.getQRCode().then($url => {
			const qrContainer = document.getElementById('qr-code');
			this.qrCodeImg = document.createElement('img');
			this.qrCodeImg.src = $url;
			qrContainer.appendChild(this.qrCodeImg);
			this.loading = false;
			this.qrDataUri = $url;
		});

	}

	downloadQRCode() {
		console.log('download');
		var link = document.createElement('a');
		link.download = 'qr-code.png';
		link.href = this.qrCodeImg.src;
		link.click();
		link.remove();
	}

	printQRCode() {
		console.log('print');
		let popup = window.open('');
		console.dir(this.qrCodeImg);
		popup.document.write(this.qrCodeImg.outerHTML)
		popup.focus(); //required for IE
		popup.print();
	}

	getQRCode() {

		return this.$q((resolve, reject) => {

			let data = {
				bc_type: 'qrcode',
				bc_content: this.url
			}

			this.API
				.one('/vouchers/proxy')
				.customPOST(data)
				.then(url => {
					resolve(url);
				}, error => {
					console.error(error);
					reject(error);
				});

		});

	}

	cancel(){

		this.DialogService.cancel();

	}

}
