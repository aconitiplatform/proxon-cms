export class DisplayFaqController{

	constructor(faqId, DialogService, MarkdownService){

		'ngInject';

		this.faqId = faqId;
		this.DialogService = DialogService;
		this.MarkdownService = MarkdownService;

	}

	$onInit() {

		this.MarkdownService.loadFaqs();

	}

	cancel(){

		this.DialogService.cancel();

	}

}

