window.GlobalConstants = {
	appEnv: 'APP_ENV',
	appUrl: 'APP_URL',
	appEdwardStoneUrl: 'APP_EDWARD_STONE_URL',
	appVersion: 'APP_VERSION',
	pusherKey: 'PUSHER_KEY',
	substanceGaTrackingId: 'SUBSTANCE_GA_TRACKING_ID',
	googleApiKey: 'GOOGLE_API_KEY',
	commitId: 'COMMIT_ID',
	commitIdLong: 'COMMIT_LONG',
	resellerCanSetUrlsForTenants: RESELLER_CAN_SET_URLS_FOR_TENANTS,
	superadminCanSetUrlsForTenants: SUPERADMIN_CAN_SET_URLS_FOR_TENANTS,
	themePrimary: 'THEME_PRIMARY',
	themeAccent: 'THEME_ACCENT',
	themeWarn: 'THEME_WARN',
	stripePublicKey: 'STRIPE_PUBLIC',
	recaptchaPublicKey: 'RECAPTCHA_PUBLIC',
	appEdwardStoneQrUrl: 'APP_EDWARD_STONE_QR_URL',
	appEdwardStoneNfcUrl: 'APP_EDWARD_STONE_NFC_URL',
	appEdwardStoneNearbyUrl: 'APP_EDWARD_STONE_NEARBY_URL',
	shippingCost: SHIPPING_COST,
	nfcTagCost: NFC_TAG_COST,
	beaconCost: BEACON_COST,
	beaconProCost: BEACON_PRO_COST
}

angular.module('app.constants')
	.constant('GlobalConstants', window.GlobalConstants);
