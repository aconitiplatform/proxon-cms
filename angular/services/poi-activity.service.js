export class PoiActivityService {

	constructor($q, API, CommonService) {

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	createNote(poiId, text) {

		let note = {
			'type': 'note',
			'text': text
		};

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getPoisResource(), poiId)
				.customPOST(note, 'log')
				.then(response => {
					resolve();
				}, error => {
					throw new Error(error);
				});

		});

	}

}
