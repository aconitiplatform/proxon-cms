export class AuthService{

	constructor(API, $auth, $localStorage){

		'ngInject';

		this.API = API;
		this.$auth = $auth;
		this.$localStorage = $localStorage;

	}

	refresh(){

		this.API.one('auth').one('refresh').customPOST().then((response) => {

			console.info(response.headers('Authorization'));

			// this.$auth.setToken(response.data);

			// this.$localStorage.user = response.data.data.user;
			// this.$localStorage.tenant = response.data.data.tenant;

			// let d = new Date();
			// let time = d.getTime();

			// this.$localStorage.tenantUpdated = time;
			// this.$localStorage.userUpdated = time;

		});

	}

}

