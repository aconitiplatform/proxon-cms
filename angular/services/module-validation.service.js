export class ModuleValidationService{

	constructor($q, CommonService, API){

		'ngInject';

		this.$q = $q;
		this.CommonService = CommonService;
		this.API = API;

	}

	validate(module) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getModuleValidationResource()).customPOST(module).then((response) => {

				resolve(response.data);

			}, () => {

				reject({});

			});

		});

	}

}

