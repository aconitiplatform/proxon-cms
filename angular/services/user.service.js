export class UserService{

	constructor($q, $localStorage, API, CommonService, TranslatedToastService, $translate){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;
		this.TranslatedToastService = TranslatedToastService;
		this.$translate = $translate;

		if(!this.$localStorage.user) {
			this.$localStorage.user = {};
		}

	}

	getUser(id) {

		return this.$q((resolve,reject) => {

			this.API.one(this.CommonService.getUsersResource(),id).get().then((result) => {

				let user = result.data;
				resolve(this.transformToFrontendModel(user));

			}, (error) => {

				reject(error);

			});

		});

	}

	addUser(user) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getUsersResource()).customPOST(user).then((result) => {

				resolve(this.transformToFrontendModel(result.data));

			}, () => {

				reject({});

			});

		});

	}


	isSelf(user) {

		let ret = user.id === this.getLoggedInUser().id;

		return ret;

	}

	updateUser(user) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getUsersResource(),user.id).patch(user).then((result) => {

				if ( this.isSelf(user) ) {
					this.$localStorage.user = result.data;
					this.$translate.use(this.$localStorage.user.language);
					moment.locale(this.$localStorage.user.language);
				}

				resolve(this.transformToFrontendModel(result.data));

			}, () => {

				reject({});

			});

		});

	}

	deleteUser(user) {

		let userName = user.fullname;

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getUsersResource(),user.id).remove().then(() => {

				resolve(userName);

			}, () => {

				reject(false);

			});

		});

	}

	resetPassword(user) {

		let email = '';

		if(!!user.oldEmail) {
			email = user.oldEmail;
		} else {
			email = user.email;
		}

		this.API.all('auth/password/email').post({
			email: email
		}).then(() => {

			this.TranslatedToastService.show('TOASTS.USER_EMAIL_INSTRUCTIONS');

		});

	}

	transformToFrontendModel(user) {

		let u = user;

		u.active = !!u.active;
		u.oldEmail = u.email;
		u.fullname = u.firstname + " " + u.lastname;
		u.savedRole = u.role;

		return u;

	}

	getLoggedInUser() {

		return this.transformToFrontendModel(this.$localStorage.user);

	}

	getloggedInUserIsAdmin() {

		return (this.$localStorage.user.role == 'admin' || this.$localStorage.user.role == 'superadmin');

	}

	isLoggedIn() {
		return (this.$localStorage.user && this.$localStorage.user.id);
	}

}
