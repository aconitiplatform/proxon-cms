export class PromiseHelperService {

	constructor(){

		'ngInject';

		this.waiting = [];
		this.loading = [];

	}

	resolveAll(key, data) {

		if (this.waiting[key]) {
			for (let wait of this.waiting[key]) {
				wait.resolve(data);
			}
			this.waiting[key] = [];
		}

	}

	rejectAll(key, data) {

		if (this.waiting[key]) {
			for (let wait of this.waiting[key]) {
				wait.rejectAll(data);
			}
			this.waiting[key] = [];
		}

	}

	isLoading(key) {

		let ret = !!this.loading[key];
		return ret;

	}

	setLoading(key, loading) {

		this.loading[key] = loading;

	}

	enqueue(key, p) {

		if (!this.waiting[key]) {
			this.waiting[key] = [];
		}
		this.waiting[key].push(p);

	}

}
