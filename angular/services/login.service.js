export class LoginService{

	constructor($q, $auth, $localStorage, $state, $translate, bsLoadingOverlayService, ProductConfigService, TenantService) {

		'ngInject';

		this.$q = $q;
		this.$auth = $auth;
		this.$localStorage = $localStorage;
		this.$state = $state;
		this.$translate = $translate;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.ProductConfigService = ProductConfigService;
		this.TenantService = TenantService;

	}

	login(user) {

		this.bsLoadingOverlayService.start();

		return this.$q((resolve, reject) => {

			this.$auth.login(user).then((response) => {

				this.$auth.setToken(response.data);

				this.$localStorage.user = response.data.data.user;
				this.$localStorage.tenant = response.data.data.tenant;

				let d = new Date();
				let time = d.getTime();

				this.$localStorage.tenantUpdated = time;
				this.$localStorage.userUpdated = time;

				this.$translate.use(this.$localStorage.user.language);

				moment.locale(this.$localStorage.user.language);

				this.ProductConfigService.saveConfigToLocalStorage();
				this.TenantService.getAvailableFeatures().then((features) => {
					// ignore: we don't care, we just want to load them ahead of time
				}, (error) => {
					// ignore: we don't care, we just want to load them ahead of time
				});

				let role = this.$localStorage.user.role;
				if (role == 'superadmin') {
					this.$localStorage.superAdminToken = response.data.data.token;
					this.$localStorage.superAdminUser = response.data.data.user;
				}
				this.transitLoggedInUser();

				resolve(response);

			}, (error) => {

				this.bsLoadingOverlayService.stop();
				reject(error);

			});

		});

	}

	handleIfAlreadyAuthenticated(){

		if(this.$auth.isAuthenticated()){

			console.log('handle already authenticated - already authenticated!')

			this.transitLoggedInUser();

		} else {

			console.log('handle already authenticated - not authenticated!')

		}

	}

	transitLoggedInUser() {

		let role = this.$localStorage.user.role;

		if (role == 'superadmin') {
			this.$state.transitionTo('authorized.superadmin-login');
		} else if (role == 'reseller') {
			this.$state.transitionTo('authorized.reseller');
		} else if (this.$localStorage.tenant.first_login) {
			this.$state.transitionTo('account-setup');
		} else {
			this.$state.transitionTo('authorized.pois');
		}
	}

	isFirstLogin() {
		return (this.$localStorage.tenant && this.$localStorage.tenant.first_login);
	}

}
