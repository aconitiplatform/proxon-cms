export class IconsService{

	constructor($http, $q){

		'ngInject';

		this.$http = $http;
		this.$q = $q;

	}

	loadAllIconSets(){

		return this.$q((resolve,reject) => {

			this.$q.all([
				this.$http.get('/icons/fa.json')
			]).then((iconsets) => {

				var sets = {
					fa: iconsets[0].data.icons
				}

				this.iconsets = sets;

				resolve(sets);

			}, () => {

				console.info("JSON missing");
				console.info(reject);
				reject({});

			});

		});

	}

	getAllIconSets(){

		return this.iconsets;

	}

}

