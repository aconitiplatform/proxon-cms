export class PaginationService {

	constructor($q, $localStorage, API, PromiseHelperService) {

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.PromiseHelperService = PromiseHelperService;

	}

	resetLoading(type) {

		let listName = type + 'List';

		if (!this.$localStorage[listName]) {
			this.$localStorage[listName] = {};
		}

		this.$localStorage[listName].since = 0;
		this.$localStorage[listName].skip = 0;
		this.$localStorage[listName].complete = false;
		this.$localStorage[listName].loading = false;
		this.$localStorage[listName].refreshed = false;
		this.$localStorage[listName].data = [];

		if (!this.$localStorage[listName].use) {
			this.$localStorage[listName].use = 'data';
		}

	}

	getList(type) {
		let listName = type + 'List';

		if (this.PromiseHelperService.isLoading(type)) {
			let p = this.$q((resolve, reject) => {
				this.PromiseHelperService.enqueue(type, {resolve: resolve, reject: reject});
			});
			return p;
		}

		if (!this.$localStorage[listName]) {
			this.resetLoading(type);
		}

		this.$localStorage[listName].loading = true;
		this.PromiseHelperService.setLoading(type, true);

		return this.$q((resolve, reject) => {

			this.getLastModified(type).then((lastModified) => {

				if (!this.$localStorage[listName].since) {
					this.$localStorage[listName].since = 0;
				}

				if (lastModified <= this.$localStorage[listName].since) {
					// keep local storage
					this.$localStorage[listName].refreshed = true;
				} else {
					// load new
					this.resetLoading(type);
				}

				if (!this.$localStorage[listName].complete || !this.$localStorage[listName].refreshed) {

					this.actualLoad(type).then((data) => {

						data = angular.copy(data);
						resolve(data);
						this.PromiseHelperService.resolveAll(type, data);
						this.PromiseHelperService.setLoading(type, false);
						this.$localStorage[listName].loading = false;
						this.$localStorage[listName].refreshed = true;

					}, (error) => {

						reject(error);
						this.PromiseHelperService.rejectAll(type, error);
						this.PromiseHelperService.setLoading(type, false);
						this.$localStorage[listName].loading = false;

					});

				} else {

					let data = angular.copy(this.$localStorage[listName].data);
					resolve(data);
					this.PromiseHelperService.resolveAll(type, data);
					this.PromiseHelperService.setLoading(type, false);
					this.$localStorage[listName].loading = false;

				}

			});
		});

	}

	getLastModified(type) {

		return this.$q((resolve, reject) => {

			this.API.one(type + '/last-modified').get().then((result) => {
				resolve(result.data['updated_at']);
			}, (error) => {
				reject(error);
			});

		});

	}

	actualLoad (type) {

		let listName = type + 'List';

		return this.$q((resolve,reject) => {

			//this.API.one(type + '?skip=' + skip + '&since=' + this.$localStorage[listName].since + '&take=5').get().then((result) => {
			this.API.one(type + '?skip=' + this.$localStorage[listName].skip + '&since=' + this.$localStorage[listName].since).get().then((result) => {

				this.$localStorage[listName].since = result.meta.last_modified;

				if (result.meta.next_skip) {
					this.$localStorage[listName].skip = result.meta.next_skip;
				} else {
					this.$localStorage[listName].complete = true;
				}

				if (!result.data) {
					result.data = [];
				}

				if(this.$localStorage[listName].skip > 0 && result.meta.is_delta){

					if (result.data.length > 0) {

						this.$localStorage[listName].data = this.$localStorage[listName].data.concat(result.data);

					}

				}else{

					this.$localStorage[listName].data = result.data;

				}

				resolve(this.$localStorage[listName].data);

			}, (error) => {

				reject([]);

			});

		});

	};

	search(type, query) {

		let listName = type + 'List';

		this.$localStorage[listName].loading = true;

		return this.$q((resolve,reject) => {

			this.API.one(type).one('search?q=' + encodeURIComponent(query)).get().then((result) => {

				this.$localStorage[listName].searchData = result.data;

				this.$localStorage[listName].loading = false;

				resolve(result.data);

			}, (error) => {

				reject([]);

			});

		});

	}

	checkLastModified(type, onNextRequest) {

		let listName = type + 'List';
		if (!this.$localStorage[listName]) {
			this.resetLoading(type);
		}
		this.$localStorage[listName].refreshed = false;
		if (!onNextRequest) {
			this.getList(type);
		}

	}

}