export class FeedbackService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;


	}

	getFeedbacks(poiId) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getPoisResource(),poiId).one(this.CommonService.getFeedbacksResource()).get().then((result) => {

					resolve(result.data);

				}, (error) => {

					reject(error);

				});

		});

	}

}
