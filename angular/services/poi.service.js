export class PoiService {

	constructor($q, $localStorage, API, CommonService, Restangular) {

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;
		this.Restangular = Restangular;

	}

	getPois() {

		return this.$q((resolve, reject) => {

			this.CommonService.getLastModified(this.CommonService.getPoisResource()).then((t) => {

				if (!!this.$localStorage.pois && this.$localStorage.poisUpdated >= t) {

					resolve(this.$localStorage.pois);

				} else {

					this.$localStorage.pois = [];

					this.API.one(this.CommonService.getPoisResource()).get().then((result) => {

						let pois = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.poisUpdated = time;

						this.$localStorage.pois = pois;

						resolve(pois);

					}, () => {

						reject([]);

					});

				}

			}, () => {

				reject([]);

			});

		});

	}

	getPoisWithEvents() {

		return this.$q((resolve, reject) => {

			let d = new Date();
			let t = d.getTime();

			if (!!this.$localStorage.poisWithEvents && this.$localStorage.poisWithEventsUpdated >= t - 3600000) {

				resolve(this.$localStorage.poisWithEvents);

			}

			this.$localStorage.poisWithEvents = [];

			this.API.one(this.CommonService.getPoisWithEventsResource()).get().then((result) => {

				let poisWithEvents = result.data;

				this.$localStorage.poisWithEventsUpdated = t;

				this.$localStorage.poisWithEvents = poisWithEvents;

				resolve(poisWithEvents);

			}, () => {

				reject([]);

			});

		});

	}

	checkForChanges(id) {

		return this.$q((resolve) => {

			let saved = angular.toJson(this.$localStorage.poi[id].loaded);
			let current = angular.toJson(this.$localStorage.poi[id].changes);

			// remove properties that are artifically made, like group timing stuff
			let regex = /,?"?ignoreInPOIChange"?:\{(?:(?:\{[^\}]*\})|[^\}]*?)*\}/g;

			current = current.replace(regex, '');

			if (saved != current) {

				resolve(true);

			} else {

				resolve(false);

			}

		});

	}

	getPoi(id) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPoisResource()).one(id).get().then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	updatePoi(id) {

		return this.$q((resolve, reject) => {

			var poi = angular.copy(this.$localStorage.poi[id].changes);

			poi.beacons = this.$localStorage.poiBeaconChanges;

			this.API.one(this.CommonService.getFacadesResource()).one(this.CommonService.getPoisResource()).one(this.$localStorage.poi[id].changes.id).patch(poi).then((response) => {

				this.$localStorage.poiBeaconChanges = {};

				resolve(response.data);

			}, () => {

				reject({});

			});

		});

	}

	addPoi(poi) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPoisResource()).customPOST(poi).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	removeContent(poiId, contentId) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPoisResource()).one(poiId).one(this.CommonService.getPoiContentsResource()).one(contentId).remove().then(() => {

				resolve(true);

			}, () => {

				reject({});

			});

		});

	}

	deletePoi(id) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPoisResource()).one(id).remove().then(() => {

				this.$localStorage.beaconUpdated = null;

				resolve(true);

			}, () => {

				reject({});

			});

		});

	}

	preview(id, language) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPoisResource()).one('previews').one(language).customPOST(this.$localStorage.poi[id].changes).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

}