import {TextModuleController} from '../dialogs/modules/text-module/text-module.dialog';
import {ImageModuleController} from '../dialogs/modules/image-module/image-module.dialog';
import {VideoModuleController} from '../dialogs/modules/video-module/video-module.dialog';
import {ButtonModuleController} from '../dialogs/modules/button-module/button-module.dialog';
import {AudioModuleController} from '../dialogs/modules/audio-module/audio-module.dialog';
import {NewsletterModuleController} from '../dialogs/modules/newsletter-module/newsletter-module.dialog';
import {DividerModuleController} from '../dialogs/modules/divider-module/divider-module.dialog';
import {HeadingModuleController} from '../dialogs/modules/heading-module/heading-module.dialog';
import {SocialIconsModuleController} from '../dialogs/modules/social-icons-module/social-icons-module.dialog';
import {FeedbackModuleController} from '../dialogs/modules/feedback-module/feedback-module.dialog';
import {MicroPaymentModuleController} from '../dialogs/modules/micro-payment-module/micro-payment-module.dialog';
import {ContactModuleController} from '../dialogs/modules/contact-module/contact-module.dialog';
import {SweepstakeModuleController} from '../dialogs/modules/sweepstake-module/sweepstake-module.dialog';
import {IconModuleController} from '../dialogs/modules/icon-module/icon-module.dialog';
import {SpotifyModuleController} from '../dialogs/modules/spotify-module/spotify-module.dialog';
import {ImageGalleryModuleController} from '../dialogs/modules/image-gallery-module/image-gallery-module.dialog';
import {VoucherModuleController} from '../dialogs/modules/voucher-module/voucher-module.dialog';
import {VagModuleController} from '../dialogs/modules/vag-module/vag-module.dialog';


export class ModuleEditService{

	constructor(DialogService, $q){

		'ngInject';

		this.DialogService = DialogService;
		this.$q = $q;

		this.modules = {
			"text": {
				"template": "text-module",
				"controller": TextModuleController
			},
			"image": {
				"template": "image-module",
				"controller": ImageModuleController
			},
			"video": {
				"template": "video-module",
				"controller": VideoModuleController
			},
			"button": {
				"template": "button-module",
				"controller": ButtonModuleController
			},
			"audio": {
				"template": "audio-module",
				"controller": AudioModuleController
			},
			"newsletter": {
				"template": "newsletter-module",
				"controller": NewsletterModuleController
			},
			"divider": {
				"template": "divider-module",
				"controller": DividerModuleController
			},
			"heading": {
				"template": "heading-module",
				"controller": HeadingModuleController
			},
			"social_icons": {
				"template": "social-icons-module",
				"controller": SocialIconsModuleController
			},
			"feedback": {
				"template": "feedback-module",
				"controller": FeedbackModuleController
			},
			"micro_payment": {
				"template": "micro-payment-module",
				"controller": MicroPaymentModuleController
			},
			"contact": {
				"template": "contact-module",
				"controller": ContactModuleController
			},
			"sweepstake": {
				"template": "sweepstake-module",
				"controller": SweepstakeModuleController
			},
			"icon": {
				"template": "icon-module",
				"controller": IconModuleController
			},
			"spotify": {
				"template": "spotify-module",
				"controller": SpotifyModuleController
			},
			"image_gallery": {
				"template": "image-gallery-module",
				"controller": ImageGalleryModuleController
			},
			"voucher": {
				"template": "voucher-module",
				"controller": VoucherModuleController
			},
			"vag": {
				"template": "vag-module",
				"controller": VagModuleController
			}
		}

	}

	openDialog(module, poiIsLockedByAnotherUser){

		return this.$q((resolve) => {

			this.DialogService.fromTemplate('/modules', this.modules[module.type].template, {
				controller: this.modules[module.type].controller,
				controllerAs: 'vm',
				clickOutsideToClose:false,
				fullscreen: true,
				skipHide: true,
				locals: {
					module: module,
					poiIsLockedByAnotherUser: poiIsLockedByAnotherUser
				}
			}).then((test) => {

				resolve(test);

			});

		});

	}

}
