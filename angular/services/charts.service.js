export class ChartsService {

	constructor($q, $localStorage, API, CommonService, $translate) {

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;
		this.$translate = $translate;

	}

	getChartDataByType(time,type,urlParams) {

		switch (type) {
			case 'seven-day-delivered-content':
				return this.getSevenDayDeliveredContentFacade(time,urlParams);
				break;
			case 'four-weeks-delivered-content':
				return this.getFourWeeksDeliveredContentFacade(time,urlParams);
				break;
			case 'four-weeks-used-channels':
				return this.getFourWeeksUsedChannelsFacade(time,urlParams);
				break;
			case 'four-weeks-scanned-beacons':
				return this.getFourWeeksScannedTouchpointsFacade(time,urlParams);
				break;
			case 'seven-day-url-access':
				return this.getSevenDayUrlAccessFacade(time,urlParams);
				break;
			case 'four-weeks-url-access':
				return this.getFourWeeksUrlAccessFacade(time,urlParams);
				break;
			case 'four-weeks-poi-access':
				return this.getFourWeeksPoiAccessFacade(time,urlParams);
				break;
			case 'four-weeks-poi-scans':
				return this.getFourWeeksPoiScansFacade(time,urlParams);
				break;
			case 'four-weeks-poi-channels':
				return this.getFourWeeksPoiChannelsFacade(time,urlParams);
				break;
			default:

		}

	}

	getTranslationPromises(type,number,values = {}) {

		switch (type) {
			case 'seven-day-delivered-content':
				return [
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_TITLE', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-delivered-content':
				return [
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_TITLE', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-scanned-beacons':
				return [
					this.$translate('CHARTS.DAYS_SCANNED_TOUCHPOINTS_TITLE', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-used-channels':
				return [
					this.$translate('CHARTS.DAYS_VIEWED_CHANNELS_TITLE', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_DELIVERED_CONTENT_LABEL_SECOND', { days: number })
				];
				break;
			case 'seven-day-url-access':
				return [
					this.$translate('CHARTS.DAYS_URL_ACCESS_TITLE', { days: number, name: values.name }),
					this.$translate('CHARTS.DAYS_URL_ACCESS_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_URL_ACCESS_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-url-access':
				return [
					this.$translate('CHARTS.DAYS_URL_ACCESS_TITLE', { days: number, name: values.name }),
					this.$translate('CHARTS.DAYS_URL_ACCESS_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_URL_ACCESS_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-poi-access':
				return [
					this.$translate('CHARTS.DAYS_POI_ACCESS_TITLE', { days: number, name: values.name }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-poi-scans':
				return [
					this.$translate('CHARTS.DAYS_POI_SCANS_TITLE', { days: number, name: values.name }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_SECOND', { days: number })
				];
				break;
			case 'four-weeks-poi-channels':
				return [
					this.$translate('CHARTS.DAYS_POI_CHANNELS_TITLE', { days: number, name: values.name }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_FIRST', { days: number }),
					this.$translate('CHARTS.DAYS_POI_ACCESS_LABEL_SECOND', { days: number })
				];
				break;
			default:

		}

	}

	getSevenDayDeliveredContentFacade(time,urlParams) {

		let range = this.getSevenDaysResource();
		let type = this.getDeliveredContentResource();

		return this.getSeries(range,type,time);

	}

	getFourWeeksDeliveredContentFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getDeliveredContentResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksUsedChannelsFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getUsedChannelsResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksScannedTouchpointsFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getScannedTouchpointsResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getSevenDayUrlAccessFacade(time,urlParams) {

		let range = this.getSevenDaysResource();
		let type = this.getUrlAccessResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksUrlAccessFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getUrlAccessResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksPoiAccessFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getDeliveredContentResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksPoiScansFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getScannedTouchpointsResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getFourWeeksPoiChannelsFacade(time,urlParams) {

		let range = this.getFourWeeksResource();
		let type = this.getUsedChannelsResource();

		return this.getSeries(range,type,time,urlParams);

	}

	getSeries(range,type,time,urlParams) {

		return this.$q((resolve,reject) => {

			// time = 1479980583;

			this.API.one(this.CommonService.getChartsResource()).one(range).one(type,time).get(urlParams).then((result) => {

				resolve(result.data);

			}, (error) => {
				reject([]);

			});

		});

	}

	getSevenDaysResource() {
		return "seven-days";
	}

	getFourWeeksResource() {
		return "four-weeks";
	}

	getDeliveredContentResource() {
		return "delivered-content";
	}

	getScannedTouchpointsResource() {
		return "scanned-beacons";
	}

	getUsedChannelsResource() {
		return "viewed-channels";
	}

	getUrlAccessResource() {
		return "url-access";
	}

	getChartConfiguration(title) {
		return {
			title: {
				text: title,
				display: true,
				fontSize: 25,
				padding: 15,
				fontStyle: 'normal'
			},
			layout: {
				padding: 40
			},
			tooltips: {
				position: 'nearest',
				backgroundColor: 'rgba(0,0,0,0.6)',
				titleMarginBottom: 15,
				bodySpacing: 15,
				xPadding: 15,
				yPadding: 15,
				caretSize: 10,
				cornerRadius: 10,
				displayColors: false
			},
			scales: {
				xAxes: [
					{
						gridLines: {
							display:true
						}
					}
				],
				yAxes: [
					{
						gridLines: {
							display:false
						}
					}
				]
			}
		}
	}

}
