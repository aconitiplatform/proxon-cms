export class DialogService {
	constructor($mdDialog) {
		'ngInject';

		this.$mdDialog = $mdDialog
	}

	fromTemplate(dir, template, options) {
		if (!template) {
			return false;
		}

		if (!options) {
			options = {};
		}

		options.templateUrl = './views/dialogs' + dir + '/' + template + '/' + template + '.dialog.html'

		return this.$mdDialog.show(options);
	}

	hide(params) {
		return this.$mdDialog.hide(params);
	}

	cancel(){
		return this.$mdDialog.cancel();
	}

	alert(title, content, params, ok = 'Ok') {
		let alert = this.$mdDialog.alert(params)
			.title(title)
			.content(content)
			.ariaLabel(content)
			.ok(ok);

		this.$mdDialog.show(alert);
	}

	confirmOriginal(title, content, params, ok = 'Ok', cancel = 'Cancel') {
		let confirm = this.$mdDialog.confirm(params)
			.title(title)
			.content(content)
			.ariaLabel(content)
			.ok(ok)
			.cancel(cancel);

		return this.$mdDialog.show(confirm);
	}

	confirm(title, content, params, ok = 'Ok', cancel = 'Cancel') {

		let template = '<md-dialog class="max-width-dialog" ng-class="dialog.css"><md-dialog-content class="md-dialog-content" role="document" tabIndex="-1"><h2 class="md-title">' + title + '</h2><div class="md-dialog-content-body"><p>' + content + '</p></div></md-dialog-content><md-dialog-actions><md-button ng-click="dialog.abort()" class="md-primary md-cancel-button">{{ dialog.cancel }}</md-button><md-button ng-click="dialog.hide()" class="md-primary md-confirm-button">{{ dialog.ok }}</md-button></md-dialog-actions></md-dialog>';

		let abort = () => {
			this.$mdDialog.cancel();
		}

		let hide = () => {
			this.$mdDialog.hide();
		}

		let controller = class DialogController {

			constructor() {

				'ngInject';

				this.ok = ok;
				this.cancel = cancel;
				this.abort = abort;
				this.hide = hide;

			}

		}

		let options = {
			template: template,
			controller: controller,
			controllerAs: 'dialog',
			bindToController: true
		};

		return this.$mdDialog.show(options);

	}

	prompt(title, content, placeholder, params, ok = 'Ok', cancel = 'Cancel') {
		let prompt = this.$mdDialog.prompt(params)
			.title(title)
			.textContent(content)
			.placeholder(placeholder)
			.ariaLabel(placeholder)
			.ok(ok)
			.cancel(cancel);

		return this.$mdDialog.show(prompt);
	}
}
