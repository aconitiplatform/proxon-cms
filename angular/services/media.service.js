export class MediaService{

	constructor($q, API, CommonService, $localStorage){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;
		this.$localStorage = $localStorage;

	}

	getMedia(skip) {

		var since = moment().unix()*1000;

		if(this.$localStorage.lastTimeMediaGotRefreshed){

			since = this.$localStorage.lastTimeMediaGotRefreshed;

		}

		return this.$q((resolve,reject) => {

			this.API.one(this.CommonService.getMediaResource() + '?skip=' + skip + '&since=' + since).get().then((result) => {

				var media = result.data;

				for(let m in media) {

					media[m].previewURL = this.CommonService.getMediaPreviewURL(media[m].id);

				}

				this.$localStorage.lastTimeMediaGotRefreshed = moment().unix()*1000;

				if(skip > 0 && result.meta.is_delta){

					var a = {
						media: media,
						type: 'more'
					}

				}else{

					var a = {
						media: media,
						type: 'new'
					}

				}

				resolve(a);

			}, (error) => {

				reject([]);

			});


		});

	}

	getMediaDetails(id){

		return this.$q((resolve, reject) => {


			this.API.one(this.CommonService.getMediaResource(), id).get().then((result) => {

				console.info(result);

				resolve(result.data);

			}, (error) => {

				reject([]);

			});


		});

	}

	deleteFile(id) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getMediaResource(), id).remove().then((result) => {

				resolve('removed');

			}, (error)=> {

				reject('notRemoved');

			});

		});

	}

}

