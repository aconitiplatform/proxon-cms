export class CartService {

	constructor($q, $rootScope, API, CommonService, GlobalConstants, $localStorage) {

		'ngInject';

		this.$q = $q;
		this.$rootScope = $rootScope;
		this.API = API;
		this.CommonService = CommonService;
		this.GlobalConstants = GlobalConstants;
		this.$localStorage = $localStorage;

		this.prices = {
			'nfc_tag': this.GlobalConstants.nfcTagCost,
			'beacon': this.GlobalConstants.beaconCost,
			'beacon_pro': this.GlobalConstants.beaconProCost
		}

	}

	ensureState() {

		if (!this.$localStorage.cart) {
			this.emptyCart();
		}

	}

	placeOrder(cart, address, stripeTokenId) {

		let order = {
			'stripe_token_id': stripeTokenId,
			'cart': cart,
			'address_name': address.name,
			'address_optional': address.optional || '',
			'address_street': address.street,
			'address_zip': address.zip,
			'address_city': address.city,
			'address_country': address.country
		}

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getCartsResource())
				.customPOST(order)
				.then(result => {
					resolve(result);
				}, error => {
					reject(error);
				});

		})

	}

	emitCartSize() {

		console.log('emitting cart size.....');

		this.$rootScope.$emit('cartSize', this.getCartSize());

	}

	getCartSize() {

		this.ensureState();
		return this.$localStorage.cart.length -1; // 1 item is shipping...

	}

	getCart() {


		this.ensureState();

		this.emitCartSize();

		return this.$localStorage.cart;

	}

	getTotalPrice() {

		this.ensureState();

		return this.$localStorage.cart.reduce((totalPrice, product) => {
			return totalPrice += product.price;
		}, 0);

	}

	createProduct(touchpoint, hardware) {

		let product = {
			// 'id': touchpoint.id,
			'name': touchpoint.name,
			'url': touchpoint.url,
			'hardware': hardware,
			'price': this.prices[hardware]
		};

		return product;

	}

	createShippingProduct() {

		let product = {
			'name': '-',
			'url':'-',
			'hardware': 'shipping_cost',
			'price': this.GlobalConstants.shippingCost
		};

		return product;

	}

	addToCart(product) {

		this.ensureState();

		this.$localStorage.cart.push(product);
		return this.getCart();

	}

	removeFromCart(index) {

		this.ensureState();

		if (index == 0) {
			// never remove shipping cost please
			return this.getCart();
		}

		this.$localStorage.cart.splice(index, 1);
		console.log(`removed product with index ${index} from cart`);
		return this.getCart();

	}

	emptyCart() {

		this.$localStorage.cart = [];
		this.addToCart(this.createShippingProduct());
		return this.getCart();

	}

	reset() {

		this.$localStorage.cart = null;

	}

}
