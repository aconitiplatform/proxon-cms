
export class PoiTimingService {

	constructor () {

		'ngInject';

	}

	/**
	 * @param timeString like 'HH:mm'
	 * @param time (optional) time on which to set the hour and minute (default: now)
	 */
	timeStringToMoment(timeString, time) {
		if (!time){
			time = moment(0);
		} else {
			time = this.timeToMoment(time);
		}
		let temp = new moment(timeString, 'HH:mm');
		time.hour(temp.hour()).minutes(temp.minutes());
		return time;
	}

	/**
	 * @param time (optional) turns unix timestamp or date into moment (default: now)
	 */
	timeToMoment(time) {
		if (time) {
			if (!moment.isMoment(time)) {
				time = moment(time * 1000);
			}
		} else {
			time = moment();
		}
		return time;
	}

	/**
	 * @param group group to test
	 * @param time (optional) time to compare group-start to (default: now)
	 */
	isGroupStarted(group, time) {
		let start = this.timeToMoment(group.time_from);
		time = this.timeToMoment(time);
		return time.isAfter(start)
	}

	/**
	 * @param group group to test
	 * @param time (optional) time to compare group-end to (default: now)
	 */
	isGroupEnded(group, time) {
		let end;
		if (group.time_to) {
			end = this.timeToMoment(group.time_to);
		} else {
			return false;
		}
		time = this.timeToMoment(time);
		return end.isBefore(time);
	}

	/**
	 * @param time time to check
	 * @param from timeString like 'HH:mm'
	 * @param to timeString like 'HH:mm'
	 */
	isTimeBetween(time, from, to) {
		time = this.timeToMoment(time);
		from = this.timeStringToMoment(from, time.clone());
		to = this.timeStringToMoment(to, time.clone());

		return time.isAfter(from) && to.isAfter(time);
	}

	/**
	 * @param group group to test
	 * @param time (optional) check if a group is visible at the given time (default: now)
	 */
	isGroupVisible(group, time) {

		time = this.timeToMoment(time);

		if (!this.isGroupStarted(group, time)) {
			return false;
		}
		if (this.isGroupEnded(group, time)) {
			return false;
		}

		if (group.weekdays_active) {
			let keys = Object.keys(group.weekdays);
			let timeWeekday = time.isoWeekday(); // 1-7, mo-su
			let weekday = group.weekdays[keys[timeWeekday-1]];
			if (weekday.active){
				if (weekday.times && weekday.times.length > 0) {
					for (let timing of weekday.times) {
						if (this.isTimeBetween(time, timing.time_from, timing.time_to)) {
							return true;
						}
					}
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * returns a momentjs object of a time, where the group should be visible, or undefined if no such moment can be found
	 * @param group group to test
	 */
	getTimeWhereGroupIsVisible(group) {
		let time;

		time = this.timeToMoment(group.time_from);
		time.add(1, 'minutes');

		if (group.weekdays_active) {
			let index = 0;
			for (let weekdayName in group.weekdays) {
				index ++;
				let weekday = group.weekdays[weekdayName];
				let toTest;
				if (weekday.active) {
					let timeInDay = this.getNextIsoWeekday(time, index);
					if (weekday.times && weekday.times.length > 0) {
						// get a point in that weekday time
						for (let timing of weekday.times) {
							let timeFrom = this.timeStringToMoment(timing.time_from, timeInDay.clone());
							toTest = timeFrom.add(1, 'minutes');

							if (this.isTimeBetween(toTest, timing.time_from, timing.time_to)) {
								return toTest;
							}
						}
					} else {
						// get any time in that weekday
						if (this.isGroupVisible(group, timeInDay)) {
							return timeInDay;
						}
					}
				}
			}
		} else {
			if (this.isGroupVisible(group, time)) {
				return time;
			}
		}
	}

	getNextIsoWeekday(time, isoWeekday) {
		// https://stackoverflow.com/a/39615119
		if (time.isoWeekday() <= isoWeekday) {
			return time.clone().isoWeekday(isoWeekday);
		} else {
			return time.clone().add(1, 'weeks').isoWeekday(isoWeekday);
		}
	}
}
