export class MicroPaymentService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;

	}

	getLastModifiedHasBeenChecked() {
		return this.CommonService.lastModifiedHasBeenChecked;
	}

	getMicroPayments() {

		this.CommonService.lastModifiedHasBeenChecked = false;

		return this.$q((resolve,reject) => {

			this.CommonService.getLastModified(this.CommonService.getMicroPaymentsResource()).then((t) => {

				if(!!this.$localStorage.microPayments && this.$localStorage.microPaymentsUpdated >= t) {

					resolve(this.$localStorage.microPayments);

				} else {

					this.API.one(this.CommonService.getMicroPaymentsResource()).get().then((result) => {

						console.info(result);

						let microPayments = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.microPayments = microPayments;

						this.$localStorage.microPaymentsUpdated = time;

						if(microPayments.length == this.$localStorage.microPayments.length) {
							resolve(this.$localStorage.microPayments);
						}

					}, (error) => {

						reject([]);

					});

				}

			},(reject) => {
				reject([]);
			});

		});

	}

	getMicroPayment(id) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getMicroPaymentsResource(),id).get().then((result) => {

					resolve(result.data);

				}, (error) => {

					reject(error);

				});

		});

	}

	getMicroPaymentTransactions(id,skip) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getMicroPaymentsResource(),id).one(this.CommonService.getMicroPaymentTransactionsResource()).get({skip: skip}).then((result) => {

					resolve(result.data);

				}, (error) => {

					reject(error);

				});

		});

	}

	addMicroPayment(microPayment) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getMicroPaymentsResource()).customPOST(microPayment).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	updateMicroPayment(microPayment) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getMicroPaymentsResource(),microPayment.id).patch(microPayment).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	deleteMicroPayment(microPayment) {

		let name = microPayment.name;

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getMicroPaymentsResource(),microPayment.id).remove().then(() => {

				resolve(name);

			}, () => {

				reject(false);

			});

		});

	}

}
