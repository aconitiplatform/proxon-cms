export class ToastService {
	constructor($mdToast, $timeout, $q) {
		'ngInject';

		this.$mdToast = $mdToast;
		this.$timeout = $timeout;
		this.$q = $q;

		this.delay = 6000;
		this.position = 'bottom right';
		this.action = 'OK';

		this.toastQueue = [];
		this.showingToast = false;
	}

	show(content) {
		if (!content) {
			return;
		}

		let	toastConfig = this.$mdToast.simple()
			.content(content)
			.position(this.position)
			.action(this.action)
			.hideDelay(this.delay);

		this.toastQueue.push(toastConfig);
		this.handleToast();

	}

	error(content) {
		if (!content) {
			return;
		}

		let	toastConfig = this.$mdToast.simple()
			.content(content)
			.position(this.position)
			.theme('warn')
			.action(this.action)
			.hideDelay(this.delay);

		this.toastQueue.push(toastConfig);
		this.handleToast();

	}

	handleToast() {

		if (this.isRunning || this.toastQueue.length == 0) {
			return;
		} else {
			this.isRunning = true;
		}

		let content = this.toastQueue.splice(0, 1)[0];

		this.$timeout().then(() => {
			this.$mdToast.show(
				content
			).then((resolve) => {
				this.isRunning = false;
				this.handleToast();
			}, (reject) => {
				this.isRunning = false;
				this.handleToast();
			});
		});

	}
}
