export class SubscriptionService{

	constructor($q, API, CommonService, $localStorage){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;
		this.$localStorage = $localStorage;

	}

	getSubscriptionPlans() {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPlansResource()).get().then((result) => {
				resolve(result.data);
			}, (error) => {
				reject(error);
			});

		});

	}

	subscribeToPlan(stripeToken, planId) {

		return this.$q((resolve, reject) => {

			this.API
			.one(this.CommonService.getPlansResource())
			.one(this.CommonService.getSubscribeResource())
			.customPOST({
				'stripe_token_id': stripeToken,
				'plan': planId
			})
			.then((result) => {
				this.$localStorage.tenant = result.data.tenant;
				resolve(result.data);
			}, (error) => {
				reject(error);
			});

		});

	}

	unsubscribe() {

		return this.$q((resolve, reject) => {

			this.API
			.one(this.CommonService.getPlansResource(), this.CommonService.getUnsubscribeResource())
			.customPOST()
			.then((result) => {
				this.$localStorage.tenant = result.data;
				resolve(result.data);
			}, (error) => {
				reject(error);
			});

		});

	}

	getSubscriptionInvoices() {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getPlansResource()).one(this.CommonService.getInvoicesResource()).get().then((result) => {
				resolve(result.data);
			}, (error) => {
				reject(error);
			});

		});

	}

	updateCard(stripeToken) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getUpdateCardResource())
				.customPOST({
					'stripe_token_id': stripeToken
				})
				.then((result) => {
					this.$localStorage.tenant.card_brand = result.data.card_brand;
					this.$localStorage.tenant.card_last_four = result.data.card_last_four;
					resolve(result.data);
				}, (error) => {
					reject(error);
				});

		});

	}

	getSubscriptionInvoicePdf(id) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getPlansResource())
				.one(this.CommonService.getInvoicesResource())
				.one(id).withHttpConfig({responseType: 'blob'}).get().then((result) => {

					resolve(result);

				}, (error) => {

					reject(error);

				});

		});

	}

	updateInvoiceInformation(invoiceInformation) {

		let payload = {
			'invoice_information': invoiceInformation
		};

		return this.$q((resolve,reject) => {

			this.API.one(this.CommonService.getPlansResource())
			.one(this.CommonService.getInvoicesResource()).patch(payload).then((result) => {

				console.log('updated invoice information', result);
				this.$localStorage.tenant = result.data;
				resolve(result);

			}, (error) => {

				reject(error);

			});

		});

	}

}
