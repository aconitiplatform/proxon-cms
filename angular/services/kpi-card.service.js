export class KpiCardService{
	constructor($q,API,CommonService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	getKpis() {

		return this.$q((resolve, reject) => {


			this.API.one(this.CommonService.getKpisResource()).get().then((result) => {

				resolve(result.data);

			}, (error) => {

				reject([]);

			});


		});

	}

	getKpiCard(type){

		return this.$q((resolve, reject) => {


			this.API.one(this.CommonService.getKpisResource() + "/" + type).get().then((result) => {

				resolve(result.data);

			}, (error) => {

				reject([]);

			});


		});

	}

}
