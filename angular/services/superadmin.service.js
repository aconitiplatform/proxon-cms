export class SuperadminService {

	constructor($q, API, CommonService, $localStorage, $auth, bsLoadingOverlayService, $translate, $state, $rootScope, ProductConfigService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;
		this.$localStorage = $localStorage;
		this.$auth = $auth;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.$translate = $translate;
		this.$state = $state;
		this.$rootScope = $rootScope;
		this.ProductConfigService = ProductConfigService;

	}

	getAllTenants() {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getSuperadminTenantsResource()).get().then((result) => {

				resolve(result.data);

			}, () => {

				reject([]);

			});

		});

	}

	login(user_id) {

		let user = {
			user_id: user_id
		};

		this.bsLoadingOverlayService.start();

		this.API.one(this.CommonService.getSuperadminLoginResource()).customPOST(user).then((response) => {

			let clipBoard = this.$localStorage.clipBoard;
			let superAdminToken = this.$localStorage.superAdminToken;
			let superAdminUser = this.$localStorage.superAdminUser;

			this.$localStorage.$reset();

			this.$localStorage.clipBoard = clipBoard;
			this.$localStorage.superAdminToken = superAdminToken;
			this.$localStorage.superAdminUser = superAdminUser;

			this.$auth.setToken(response);

			this.$localStorage.user = response.data.user;
			this.$localStorage.tenant = response.data.tenant;

			let d = new Date();
			let time = d.getTime();

			this.$localStorage.tenantUpdated = time;
			this.$localStorage.userUpdated = time;

			this.$translate.use(this.$localStorage.user.language);

			moment.locale(this.$localStorage.user.language);

			this.ProductConfigService.getConfig().then(() => {
				this.ProductConfigService.saveConfigToLocalStorage();
			}, (error) => {
				console.log('Failed to get product config after superadmin relog');
			});

			this.$rootScope.$emit('SuperadminScopeChange');

			this.$state.transitionTo('authorized.pois', undefined, {reload: true});


		}, (error) => {

			console.log('Failed to log in as other user', error);

		});

	}

	backToSuperadmin() {

		this.bsLoadingOverlayService.start();

		let clipBoard = this.$localStorage.clipBoard;
		let superAdminToken = this.$localStorage.superAdminToken;
		let superAdminUser = this.$localStorage.superAdminUser;

		this.$localStorage.$reset();

		this.$localStorage.clipBoard = clipBoard;
		this.$localStorage.superAdminUser = superAdminUser;
		this.$localStorage.user = superAdminUser;
		this.$localStorage.superAdminToken = superAdminToken;

		this.ProductConfigService.getConfig().then(() => {
			this.ProductConfigService.saveConfigToLocalStorage();
		}, (error) => {
			console.log('Failed to get product config after superadmin relog');
		});

		this.$auth.setToken(superAdminToken);

		this.$rootScope.$emit('SuperadminScopeChange');

		this.$state.transitionTo('authorized.superadmin-login', undefined, {reload: true});

	}

}