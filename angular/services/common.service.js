export class CommonService{

	constructor($q, API, Restangular, $translate, GlobalConstants){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.Restangular = Restangular;
		this.$translate = $translate;
		this.GlobalConstants = GlobalConstants;

		this.lastModifiedHasBeenChecked = false;

	}

	resetService() {

		this.lastModifiedHasBeenChecked = false;

	}

	getLastModified(resource) {

		return this.$q((resolve,reject) => {

			this.API.one(resource + '/last-modified').get().then((result) => {

				this.lastModifiedHasBeenChecked = true;
				resolve(result.data.updated_at);

			}, (error) => {

				this.lastModifiedHasBeenChecked = true;
				reject(false);

			});

		});

	}

	getEdwardPreviewURL(id, language){

		if(language){

			return this.GlobalConstants.appEdwardStoneUrl + '/previews/' + id + '/' + language;

		}else{

			return this.GlobalConstants.appEdwardStoneUrl + '/pois/' + id;

		}

	}

	getCachedMediaPreviewURL(id) {

		return '/api/media/' + id + '/file?preview=true';

	}

	getMediaPreviewURL(id){

		return '/api/media/' + id + '/file?preview=true&shingshong=' + moment().unix();

	}

	getVoucherImageURL(id){

		return '/api/vouchers/' + id + '/file?shingshong=' + moment().unix();

	}

	getVoucherProxyURL() {

		return '/api/vouchers/proxy';

	}

	getMediaURL(id){
		return '/api/media/' + id + '/file';
	}

	getMediaUploadURL() {
		return '/api/media/';
	}

	getUpdateCheckResource() {
		return '/auth/sha/latest';
	}

	getProductConfigResource() {
		return '/auth/config';
	}

	getRegisterResource() {
		return '/auth/register';
	}

	getVerifyRecaptchaResource() {
		return '/auth/verify-recaptcha';
	}

	getUsersResource() {
		return 'users';
	}

	getAppsResource() {
		return 'apps';
	}

	getMediaResource() {
		return 'media';
	}

	getFacadesResource() {
		return 'facades';
	}

	getPermissionsResource() {
		return 'permissions';
	}

	getInitializeResource() {
		return 'initialize';
	}

	getPoiResource() {
		return 'poi';
	}

	getPoisResource() {
		return 'pois';
	}

	getLocksResource() {
		return 'locks';
	}

	getDesignResource() {
		return 'design';
	}

	getBlueprintResource() {
		return 'blueprints';
	}

	getDesignsResource() {
		return 'designs';
	}

	getPoisWithEventsResource() {
		return 'analytics/pois-with-events';
	}

	getFeedbacksResource() {
		return 'feedbacks';
	}

	getSweepstakesResource() {
		return 'sweepstakes';
	}

	getPoiContentsResource() {
		return 'contents';
	}

	getTenantResource() {
		return 'tenant';
	}

	getTenantsResource() {
		return 'tenants';
	}

	getBeaconsResource() {
		return 'beacons';
	}

	getAddBeaconsResource() {
		return 'add-beacons';
	}

	getKpisResource() {
		return 'kpis';
	}

	getChartsResource() {
		return 'charts';
	}

	getNewsletterConfigurationsResource() {
		return 'newsletter-configurations';
	}

	getMicroPaymentsResource() {
		return 'micro-payments';
	}

	getMicroPaymentTransactionsResource() {
		return 'transactions';
	}

	getSuperadminTenantsResource() {
		return 'superadmin/tenants';
	}

	getSuperadminLoginResource() {
		return 'superadmin/login';
	}

	getVouchersResource() {
		return 'vouchers';
	}

	getModuleValidationResource() {
		return 'modules/validate';
	}

	getInvoicesResource() {
		return 'invoices';
	}

	getInvoicePdfResource(id) {
		return 'invoices/' + id + '/pdf';
	}

	getLastModifiedResource() {
		return 'last-modified';
	}

	getFeaturesResource() {
		return 'features';
	}

	getModulesResource() {
		return 'modules';
	}

	getFeaturesResource() {
		return 'features';
	}

	getPlansResource() {
		return 'plans';
	}

	getUpdateCardResource() {
		return 'plans/update-card';
	}

	getSubscribeResource() {
		return 'subscribe';
	}

	getUnsubscribeResource() {
		return 'unsubscribe';
	}

	getCartsResource() {
		return 'carts';
	}

	getSyncBatchesResource() {
		return 'sync-batches';
	}

	getBatchesResource() {
		return 'batches';
	}

	getAuthResource() {
		return 'auth';
	}

	getResendEmailConfirmationResource() {
		return 'resend-email-confirmation';
	}

	getConfirmEmailResource() {
		return 'confirm-email';
	}

	getWeekDayMapping() {

		return this.$q((resolve,reject) => {

			this.$q.all([
				this.$translate('COMMON.SUNDAY'),
				this.$translate('COMMON.MONDAY'),
				this.$translate('COMMON.TUESDAY'),
				this.$translate('COMMON.WEDNESDAY'),
				this.$translate('COMMON.THURSDAY'),
				this.$translate('COMMON.FRIDAY'),
				this.$translate('COMMON.SATURDAY'),
				this.$translate('COMMON.YESTERDAY'),
				this.$translate('COMMON.DAY_BEFORE_YESTERDAY')
			]).then((i18n) => {

				resolve({
					0: i18n[0],
					1: i18n[1],
					2: i18n[2],
					3: i18n[3],
					4: i18n[4],
					5: i18n[5],
					6: i18n[6],
					y: i18n[7],
					dby: i18n[8]
				});

			}, (notI18n) => {

				console.info('Strings missing.');
				console.info(reject);
				reject({});

			});

		});



	}

}
