export class MarkdownService{

	constructor($q, $http, $localStorage, UserService){

		'ngInject';

		this.$q = $q;
		this.$http = $http;
		this.$localStorage = $localStorage;
		this.UserService = UserService;

		if(!this.$localStorage.faqs) {
			this.$localStorage.faqs = {};
		}

		if(!this.$localStorage.changelog) {
			this.$localStorage.changelog = {};
		}

	}

	getFaqUrl(id) {

		return this.getMarkdownUrl('faqs', id);

	}

	getChangelogUrl(id) {

		return this.getMarkdownUrl('changelog', id);

	}

	getMarkdownUrl(type, id) {

		let markdown = this.$localStorage[type][id];
		let userLang = this.UserService.getLoggedInUser().language;
		let lang = 'en';

		if (markdown.langs.indexOf(userLang) > -1) {
			lang = userLang;
		}

		if (markdown.langs.indexOf(lang) == -1) {
			lang = faq.langs[0];
		}

		let ret = 'markdown/' + type + '/' + lang + '/' + id + '.md';

		return ret;

	}

	loadChangelog() {

		return this.getChangelog().then((changelog) => {
		});

	}

	loadFaqs() {

		return this.getFaqs().then((faqs) => {
		});

	}

	getFaqs() {

		return this.getMarkdown('faqs');

	}

	getChangelog() {

		return this.getMarkdown('changelog');

	}

	getMarkdown(type) {

		return this.$q((resolve,reject) => {

			if(!this.$localStorage[type] || Object.keys(this.$localStorage[type]).length == 0) {

				this.$http.get('markdown/'+type+'/'+type+'.json').success((response) => {

					this.$localStorage[type] = response;
					resolve(response);

				}).error((error) => {

					reject(error);

				});

			} else {

				resolve(this.$localStorage[type]);

			}

		});

	}

}
