export class UsersService{

	constructor($q, $localStorage, API, UserService, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.UserService = UserService;
		this.CommonService = CommonService;

		if(!this.$localStorage.users) {
			this.$localStorage.users = [];
		}

	}

	getLastModifiedHasBeenChecked() {
		return this.CommonService.lastModifiedHasBeenChecked;
	}

	getUsers() {

		this.CommonService.lastModifiedHasBeenChecked = false;

		return this.$q((resolve,reject) => {

			this.CommonService.getLastModified(this.CommonService.getUsersResource()).then((t) => {

				if(!!this.$localStorage.users && this.$localStorage.usersUpdated >= t) {

					resolve(this.$localStorage.users);

				} else {

					this.$localStorage.users = [];

					this.API.one(this.CommonService.getUsersResource()).get().then((result) => {

						let users = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.users = users;

						this.$localStorage.usersUpdated = time;

						if(users.length == this.$localStorage.users.length) {
							resolve(this.$localStorage.users);
						}

					}, (error) => {

						reject([]);

					});

				}

			},(reject) => {
				reject([]);
			});

		});

	}

}
