export class TimepickerConfigService{

	constructor(UserService){

		'ngInject';

		this.UserService = UserService;

		let user = this.UserService.getLoggedInUser();

		let show24 = ['de', 'fr', 'es'];

		if (show24.indexOf(user.language) > -1) {

			this.config = {
				timeFormat: 'G:i',
				show2400: true
			};

		} else {

			this.config = {};

		}

	}

}

