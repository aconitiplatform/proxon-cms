export class LogoutService{

	constructor($localStorage, PoiService, LocationChangeService, LanguageNamesService, CommonService, $window, PusherService, CartService){

		'ngInject';

		this.$localStorage = $localStorage;
		this.PoiService = PoiService;
		this.LocationChangeService = LocationChangeService;
		this.LanguageNamesService = LanguageNamesService;
		this.CommonService = CommonService;
		this.$window = $window;
		this.PusherService = PusherService;
		this.CartService = CartService;

	}

	resetAllServices() {

		this.LocationChangeService.resetService();
		this.LanguageNamesService.resetService();
		this.CommonService.resetService();
		this.$localStorage.$reset();
		this.$window.localStorage.clear();
		this.CartService.reset();

	}

}
