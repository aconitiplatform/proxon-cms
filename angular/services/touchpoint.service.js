export class TouchpointService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;

		if(!this.$localStorage.beacons) {
			this.$localStorage.beacons = [];
		}

	}

	getTouchpoint(id) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getBeaconsResource(),id).get().then((result) => {

					let beacon = result.data;
					resolve(this.transformToFrontendModel(beacon));

				}, (error) => {

					reject(error);

				});

		});

	}

	addTouchpoint(beacon) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getBeaconsResource()).customPOST(beacon).then((result) => {

				resolve(this.transformToFrontendModel(result.data));

			}, () => {

				reject({});

			});

		});

	}

	updateTouchpoint() {

		return this.$q((resolve, reject) => {

			let beacon = this.$localStorage.beaconChanges;

			this.API.one(this.CommonService.getFacadesResource()).one(this.CommonService.getBeaconsResource(),beacon.id).patch(beacon).then((result) => {

				resolve(this.transformToFrontendModel(result.data));

			}, () => {

				reject({});

			});

		});

	}

	deleteTouchpoint(beacon) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getBeaconsResource(),beacon.id).remove().then(() => {

				resolve(true);

			}, () => {

				reject(false);

			});

		});

	}

	transformToFrontendModel(beacon) {

		let b = beacon;
		return b;

	}

	addTouchpointUser(beacon,user) {

		let payload = {
			id: user.id
		}

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getBeaconsResource(),beacon.id).one(this.CommonService.getUsersResource()).customPOST(payload).then((result) => {

				resolve(true);

			}, () => {

				reject(false);

			});

		});

	}

	deleteTouchpointUser(beacon,user) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getBeaconsResource(),beacon.id).one(this.CommonService.getUsersResource(),user.id).remove().then(() => {

				resolve(true);

			}, () => {

				reject(false);

			});

		});

	}

	getTouchpoints() {

		return this.$q((resolve,reject) => {

			this.CommonService.getLastModified(this.CommonService.getBeaconsResource()).then((t) => {

				if(!!this.$localStorage.beacons && this.$localStorage.beaconsUpdated >= t) {

					resolve(angular.copy(this.$localStorage.beacons));

				} else {

					this.$localStorage.beacons = [];

					this.API.one(this.CommonService.getBeaconsResource()).get().then((result) => {

						let beacons = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.beacons = beacons;

						this.$localStorage.beaconsUpdated = time;

						if(beacons.length == this.$localStorage.beacons.length) {
							resolve(angular.copy(this.$localStorage.beacons));
						}

					}, (error) => {

						reject([]);

					});

				}

			},(reject) => {

			});

		});

	}


	checkForChanges() {

		return this.$q((resolve) => {

			if (angular.toJson(this.$localStorage.beacon) != angular.toJson(this.$localStorage.beaconChanges)) {

				resolve(true);

			} else {

				resolve(false);

			}

		});

	}

}
