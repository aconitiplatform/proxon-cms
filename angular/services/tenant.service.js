export class TenantService{

	constructor($q, $localStorage, API, CommonService, PromiseHelperService, $rootScope){

		'ngInject';

		this.$localStorage = $localStorage;
		this.API = API;
		this.$q = $q;
		this.CommonService = CommonService;
		this.PromiseHelperService = PromiseHelperService;
		this.$rootScope = $rootScope;

	}

	ensureState() {

		if(!this.$localStorage.tenant) {
			this.$localStorage.tenant = {};
		}

		if (!this.$localStorage.tenantModulesChecked) {
			this.$localStorage.tenantModulesChecked = -1;
		}

		if (!this.$localStorage.tenantFeaturesChecked) {
			this.$localStorage.tenantFeaturesChecked = -1;
		}

		if (!this.$localStorage.tenantModules) {
			this.$localStorage.tenantModules = {};
		}

		if (!this.$localStorage.tenantFeatures) {
			this.$localStorage.tenantFeatures = {};
		}

	}

	notifySaveButton(controllerName, state) {

		this.$rootScope.$emit(`${controllerName}Loading`, state);
		// if state == true, there is no need to emit 'Changed'
		if (!state) {
			this.$rootScope.$emit(`${controllerName}Changed`, state);
		}

	}

	getTenant() {

		return this.$q((resolve,reject) => {


			if (!!this.$localStorage.tenant && this.$localStorage.tenantUpdated != -1) {

				this.CommonService.getLastModified(this.CommonService.getTenantsResource()).then((t) => {

					if(this.$localStorage.tenantUpdated >= t) {

						this.$localStorage.tenantChanges = angular.copy(this.$localStorage.tenant);
						resolve(angular.copy(this.$localStorage.tenant));

					} else {

						this.loadTenant().then((tenant) => {
							resolve(tenant);
						}, (error) => {
							reject(error);
						});

					}

				},(error) => {
					reject(error);
				});

			} else {

				this.loadTenant().then((tenant) => {
					resolve(tenant);
				}, (error) => {
					reject(error);
				});

			}

		});

	}

	loadTenant() {

		return this.$q((resolve, reject) => {
			this.API.one(this.CommonService.getTenantsResource()).one('current').get().then((result) => {

				let tenant = result.data;
				this.$localStorage.tenant = angular.copy(tenant);
				if (!this.$localStorage.tenantChanges) {
					this.$localStorage.tenantChanges = angular.copy(tenant);
				}
				this.$localStorage.tenantUpdated = tenant.updated_at;
				resolve(tenant);

			}, (error) => {

				reject(error);

			});
		});

	}

	getTenantPerId(tenantId) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource()).one(tenantId).get().then(result => {
				resolve(result.data);
			}, error => {
				reject(error);
			});

		});

	}

	updateTenant(tenant, controllerName) {

		this.notifySaveButton(controllerName, true);

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource(),tenant.id).patch(tenant).then((result) => {

				this.$localStorage.tenant = angular.copy(result.data);
				this.$localStorage.tenantChanges = angular.copy(result.data);
				this.notifySaveButton(controllerName, false);
				resolve(angular.copy(result.data));

			}, () => {

				reject({});

			});

		});

	}

	deleteTenant(tenant) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource(),tenant.id).remove().then(() => {

				resolve(true);

			}, () => {

				reject(false);

			});

		});

	}

	/**
	 * Loads modules from backend _only if required_
	 */
	getAvailableModules(tenant) {

		this.ensureState();
		let tenantId = tenant;
		if (!tenantId) {
			tenantId = 'self';
		};

		let queueKey = 'modules-' + tenantId;

		if (this.PromiseHelperService.isLoading(queueKey)) {
			let p = this.$q((resolve, reject) => {
				this.PromiseHelperService.enqueue(queueKey, {resolve: resolve, reject: reject});
			})
			return p;
		} else {
			this.PromiseHelperService.setLoading(queueKey, true);
		}

		let p;

		if (tenantId != 'self' || !this.$localStorage.tenantModules[tenantId] || this.$localStorage.tenantModules[tenantId].length == 0) {
			p = this.loadAvailableModules(tenantId);
		} else {
			p = this.$q((resolve, reject) => {

				this.checkIfModulesModified().then((isUpToDate) => {
					if (isUpToDate) {
						resolve(this.$localStorage.tenantModules[tenantId]);
					} else {
						this.loadAvailableModules(tenantId).then((response) => {
							resolve(response);
						}, (error) => {
							reject(error);
						});
					}
				});
			});
		}

		p.then((data) => {
			this.PromiseHelperService.setLoading(queueKey, false);
			this.PromiseHelperService.resolveAll(queueKey, data);
		}, (error) => {
			this.PromiseHelperService.rejectAll(queueKey, error);
		});

		return p;
	}

	/**
	 * Loads modules from the backend, without checks for changes
	 */
	loadAvailableModules(tenantId) {

		this.ensureState();
		let path = this.CommonService.getTenantsResource() + '/' + this.CommonService.getModulesResource();

		if (tenantId && tenantId != 'self') {
			path = path + '/' + tenantId;
		}

		return this.$q((resolve, reject) => {

			this.API.one(path).get().then((response) => {

				if (!tenantId) {
					tenantId = 'self';
				}
				this.$localStorage.tenantModules[tenantId] = response.data;

				if (tenantId == 'self') {
					this.prepareModuleIcons(response.data);
				}
				resolve(response.data);

			}, () => {

				reject('Failed to get available modules for tenant!');

			});

		});

	}

	checkIfModulesModified() {

		this.ensureState();
		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource())
			.one(this.CommonService.getModulesResource())
			.one(this.CommonService.getLastModifiedResource()).get().then((response) => {

				let updatedAt = response.data.updated_at;

				if (!updatedAt) {
					updatedAt = 0;
				}

				let isUpToDate = this.$localStorage.tenantModulesChecked >= updatedAt;

				this.$localStorage.tenantModulesChecked = updatedAt;

				resolve(isUpToDate);

			}, () => {

				reject('Failed to get available modules for tenant!');

			});

		});
	}

	/**
	 * Loads features from backend _only if required_
	 */
	getAvailableFeatures(tenant) {

		this.ensureState();
		let tenantId = tenant;
		if (!tenantId) {
			tenantId = 'self';
		};

		let queueKey = 'features-' + tenantId;

		if (this.PromiseHelperService.isLoading(queueKey)) {
			let p = this.$q((resolve, reject) => {
				this.PromiseHelperService.enqueue(queueKey, {resolve: resolve, reject: reject});
			})
			return p;
		} else {
			this.PromiseHelperService.setLoading(queueKey, true);
		}

		let p;

		if (tenantId != 'self' || !this.$localStorage.tenantFeatures[tenantId] || this.$localStorage.tenantFeatures[tenantId].length == 0) {
			p = this.loadAvailableFeatures(tenantId);
		} else {
			p = this.$q((resolve, reject) => {

				this.checkIfFeaturesModified().then((isUpToDate) => {
					if (isUpToDate) {
						resolve(this.$localStorage.tenantFeatures[tenantId]);
					} else {
						this.loadAvailableFeatures(tenantId).then((response) => {
							resolve(response);
						}, (error) => {
							reject(error);
						});
					}
				});
			});
		}

		p.then((data) => {
			this.PromiseHelperService.setLoading(queueKey, false);
			this.PromiseHelperService.resolveAll(queueKey, data);
		}, (error) => {
			this.PromiseHelperService.rejectAll(queueKey, error);
		});

		return p;
	}

	/**
	 * Loads features from the backend
	 */
	loadAvailableFeatures(tenantId) {

		this.ensureState();
		let path = this.CommonService.getTenantsResource() + '/' + this.CommonService.getFeaturesResource();

		if (tenantId && tenantId != 'self') {
			path = path + '/' + tenantId;
		}

		return this.$q((resolve, reject) => {

			this.API.one(path).get().then((response) => {

				if (!tenantId) {
					tenantId = 'self';
				};
				this.$localStorage.tenantFeatures[tenantId] = response.data;

				resolve(response.data);

			}, () => {

				reject('Failed to get available features for tenant!');

			});

		});

	}

	checkIfFeaturesModified() {

		this.ensureState();
		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource())
			.one(this.CommonService.getFeaturesResource())
			.one(this.CommonService.getLastModifiedResource()).get().then((response) => {

				let updatedAt = response.data.updated_at;

				if (!updatedAt) {
					updatedAt = 0;
				}

				let isUpToDate = this.$localStorage.tenantFeaturesChecked >= updatedAt;

				this.$localStorage.tenantFeaturesChecked = updatedAt;

				resolve(isUpToDate);

			}, () => {

				reject('Failed to get available features for tenant!');

			});

		});
	}

	prepareModuleIcons(modules) {
		if (!this.moduleIconsByName) {
			this.moduleIconsByName = {};
		}
		for (module of modules) {
			this.moduleIconsByName[module.slug] = module.icon;
		}
	}

	getModuleIcon(moduleSlug) {
		if (!this.moduleIconsByName) {
			this.moduleIconsByName = {};
		}
		return this.moduleIconsByName[moduleSlug];
	}

	createTenant(tenant) {

		// info: backend also creates a user

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.customPOST(tenant)
				.then(result => {
					resolve(result.data.tenant);
				}, error => {
					reject();
				});

		});

	}

	initializedAtKontaktio(tenantId) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getInitializeResource())
				.customPOST({
					tenant_id: tenantId
				})
				.then(result => {

					let tenantsList = this.$localStorage.tenantsList;
					let initializedTenant = tenantsList.data.filter(tenant => tenant.id === tenantId)[0];
					if (initializedTenant) {
						initializedTenant.active = true;
						initializedTenant.initial_kontakt_io_sync = true;
					}

					resolve(result);

				}, error => {
					reject(error);
				});

		});

	}

	getFeaturesAndModulesForTenant(tenantId) {
		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getFacadesResource())
				.one(this.CommonService.getPermissionsResource())
				.one(tenantId)
				.get()
				.then(result => {
					resolve(result.data);
				}, error => {
					reject();
				});

		});
	}

	updateTenantModules(tenantId, modulesSlugArray, controllerName) {

		this.notifySaveButton(controllerName, true);

		return this.$q((resolve, reject) => {

			let request = {
				tenant_id: tenantId,
				modules: modulesSlugArray
			}

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getModulesResource())
				.customPOST(request)
				.then(result => {
					this.notifySaveButton(controllerName, false);
					resolve(result);
				}, error => {
					reject(error);
				});

		});

	}

	updateTenantFeatures(tenantId, featuresSlugArray, controllerName) {

		this.notifySaveButton(controllerName, true);

		return this.$q((resolve, reject) => {

			let request = {
				tenant_id: tenantId,
				features: featuresSlugArray
			}

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getFeaturesResource())
				.customPOST(request)
				.then(result => {
					this.notifySaveButton(controllerName, false);
					resolve(result);
				}, error => {
					reject(error);
				});

		});
	}

	toggleTenantLock(tenantId) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one('activity')
				.patch({
					tenant_id: tenantId
				})
				.then(result => {

					let tenantsList = this.$localStorage.tenantsList;
					let toggledTenant = tenantsList.data.filter(tenant => tenant.id === tenantId)[0];
					if (toggledTenant) {
						toggledTenant.active = result.data.active;
					}

					resolve(result.data);

				}, error => {
					reject();
				});

		});

	}

	/*
		tenantTrialObj = {
			"tenant_id":"{{tenantId}}",
			"easybill_customer_id":"130065893", //only required if superadmin ends a trial of their customers (for now)
			"price_per_touchpoint":900,
			"discount":0,
			"bill_rest_of_month":true
		}
	 */
	endTenantTrial(tenantTrialObj) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one('end-trial')
				.customPOST(tenantTrialObj)
				.then(result => {
					resolve(result.data);
				}, error => {
					reject(error.message);
				});

		});

	}

	/*
		addUrlsFormData = {
			'tenant_id': tenant.id,
			'amount': 1,
			'kontakt_io': false
		};
	 */
	addUrlsToTenant(addUrlsFormData) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getAddBeaconsResource())
				.customPOST(addUrlsFormData)
				.then(response => {
					resolve(response.data);
				}, error => {
					reject(error.message);
				});

		});

	}

	syncUrlBatchesWithKontaktio(tenantId) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getSyncBatchesResource())
				.customPOST({
					tenant_id: tenantId
				}).then(response => {
					resolve(response.data);
				}, error => {
					reject(error.message);
				});

		});

	}

	getSyncBatches(tenantId) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getTenantsResource())
				.one(this.CommonService.getBatchesResource())
				.one(tenantId)
				.get().then(response => {
					resolve(response.data);
				}, error => {
					reject(error.message);
				});

		});

	}

}
