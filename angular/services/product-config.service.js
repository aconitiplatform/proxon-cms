export class ProductConfigService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;

	}

	getConfig(){

		return this.$q((resolve,reject) => {

			this.API.one(this.CommonService.getProductConfigResource()).get().then((result) => {

				this.productConfig = result.data;

				resolve(result.data);

			}, (error) => {

				reject(error);

			});

		});

	}

	saveConfigToLocalStorage(){

		this.$localStorage.productConfig = this.productConfig;

	}

}

