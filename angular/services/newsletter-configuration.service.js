export class NewsletterConfigurationService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;

		// if(!this.$localStorage.newsletterConfigurations) {
		//     this.$localStorage.newsletterConfigurations = [];
		// }

	}

	getLastModifiedHasBeenChecked() {
		return this.CommonService.lastModifiedHasBeenChecked;
	}

	getNewsletterConfigurations() {

		this.CommonService.lastModifiedHasBeenChecked = false;

		return this.$q((resolve,reject) => {

			this.CommonService.getLastModified(this.CommonService.getNewsletterConfigurationsResource()).then((t) => {

				if(!!this.$localStorage.newsletterConfigurations && this.$localStorage.newsletterConfigurationsUpdated >= t) {

					resolve(this.$localStorage.newsletterConfigurations);

				} else {

					this.API.one(this.CommonService.getNewsletterConfigurationsResource()).get().then((result) => {

						console.info(result);

						let newsletterConfigurations = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.newsletterConfigurations = newsletterConfigurations;

						this.$localStorage.newsletterConfigurationsUpdated = time;

						if(newsletterConfigurations.length == this.$localStorage.newsletterConfigurations.length) {
							resolve(this.$localStorage.newsletterConfigurations);
						}

					}, (error) => {

						reject([]);

					});

				}

			},(reject) => {
				reject([]);
			});

		});

	}

	getNewsletterConfiguration(id) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getNewsletterConfigurationsResource(),id).get().then((result) => {

					resolve(result.data);

				}, (error) => {

					reject(error);

				});

		});

	}

	addNewsletterConfiguration(newsletterConfiguration) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getNewsletterConfigurationsResource()).customPOST(newsletterConfiguration).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	updateNewsletterConfiguration(newsletterConfiguration) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getNewsletterConfigurationsResource(),newsletterConfiguration.id).patch(newsletterConfiguration).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	deleteNewsletterConfiguration(newsletterConfiguration) {

		let name = newsletterConfiguration.name;

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getNewsletterConfigurationsResource(),newsletterConfiguration.id).remove().then(() => {

				resolve(name);

			}, () => {

				reject(false);

			});

		});

	}

}
