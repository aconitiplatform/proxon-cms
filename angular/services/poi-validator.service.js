export class PoiValidatorService{

	constructor($q, LanguageNamesService, $translate){

		'ngInject';

		this.$q = $q;
		this.LanguageNamesService = LanguageNamesService;
		this.$translate = $translate;

	}

	validate(poi){

		var errors = [];

		return this.$q((resolve, reject) => {

			for (let content of poi.poi_contents) {

				if(content.action == 'delete'){

					continue;

				}

				if(content.data.groups.length > 0){

					for (let group of content.data.groups) {

						if(group.modules.length < 1){

							// we need to memorize the variable fields and translate all errors at once
							errors.push({
								type: 'empty_group',
								groupLabel: group.label,
								language: this.LanguageNamesService.getLanguage(content.language).nativeName
							});

						}

					}

				}

				if(content.has_url){

					if(content.url){

						var regex = new RegExp(/(http(s)?:\/\/.)(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);

						if(!content.url.match(regex)){

							errors.push({
								type: 'url',
								language: this.LanguageNamesService.getLanguage(content.language).nativeName,
								url: content.url
							});

						}

					}else{

						errors.push({
							type: 'url_missing',
							language: this.LanguageNamesService.getLanguage(content.language).nativeName
						});

					}

				}

			}

			if(errors.length > 0){

				var promisses = [];
				var translatedErrors = [];

				for(var error of errors){

					if(error.type == 'empty_group'){

						promisses.push(this.$q.resolve(

							this.$translate('POIS.INVALID_POI_ERROR.EMPTY_GROUP', {groupLabel: error.groupLabel, language: error.language})

						).then((translation) => {

							translatedErrors.push({
								'text': translation,
								'type': 'error'
							});

						}));

					}

					if(error.type == 'url'){

						promisses.push(this.$q.resolve(

							this.$translate('POIS.INVALID_POI_ERROR.URL', {url: error.url, language: error.language})

						).then((translation) => {

							translatedErrors.push({
								'text': translation,
								'type': 'error'
							});

						}));

					}

					if(error.type == 'url_missing'){

						promisses.push(this.$q.resolve(

							this.$translate('POIS.INVALID_POI_ERROR.URL_MISSING', {language: error.language})

						).then((translation) => {

							translatedErrors.push({
								'text': translation,
								'type': 'error'
							});

						}));

					}

				}

				// after all errors are translated, we can reject the promise
				return Promise.all(promisses).then(() => {

					reject(translatedErrors);

				});

			}else{

				resolve();

			}

		});

	}

}

