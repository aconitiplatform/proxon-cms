export class ListService {

	constructor($q, $rootScope, API) {

		'ngInject';

		this.$q = $q;
		this.$rootScope = $rootScope;
		this.API = API;

		this.list = {};

	}

	initList(type, id, urlSuffix) {

		if (!this.getList(type, id, urlSuffix)) {

			if (id && urlSuffix) {

				_.set(this.list, [type + '-' + urlSuffix, id], {
					skip: 0,
					since: 0,
					data: [],
					loading: false,
					complete: false,
					orderProperty: [],
					orderReverse: false
				});

			} else {

				this.list[type] = {
					skip: 0,
					since: 0,
					data: [],
					loading: false,
					complete: false,
					orderProperty: [],
					orderReverse: false
				}

			}

		}

	}

	getList(type, id, urlSuffix) {

		if (id && urlSuffix) {
			return _.get(this.list, [type + '-' + urlSuffix, id]);
		} else {
			return this.list[type];
		}

	}

	setList(type, id, urlSuffix, newValuesObj) {

		_.extend(this.getList(type, id, urlSuffix), newValuesObj);
		this.$rootScope.$emit(`${type + urlSuffix + id}ListChanged`, this.getList(type, id, urlSuffix));

	}

	sortList(type, id, urlSuffix, property) {

		this.setList(type, id, urlSuffix, {
			orderReverse: (this.getList(type, id, urlSuffix).orderProperty == property) ? !this.getList(type, id, urlSuffix).orderReverse : false,
			orderProperty: property,
		});

	}

	reset() {
		this.list = {};
	}

	/**
	 * Get List from API or local
	 *
	 * @param {string} type
	 * @param {number} id
	 * @param {string} urlPrefix
	 * @param {string} urlSuffix
	 */
	loadList(type, id, urlPrefix, urlSuffix) {

		id = id || '';
		urlPrefix = urlPrefix || '';
		urlSuffix = urlSuffix || '';

		this.setList(type, id, urlSuffix, {loading: true});

		this.API
			.one(urlPrefix).one(type, id).one(urlSuffix).one('last-modified')
			.get()
			.then(result => result.data['updated_at'])
			.then(lastModified => {

				if (lastModified > this.getList(type, id, urlSuffix).since) {

					this.setList(type, id, urlSuffix, {
						since: 0,
						skip: 0,
						complete: false
					});

					return this.fetchDataFromAPI(type, id, urlPrefix, urlSuffix);

				} else {

					if (this.getList(type, id, urlSuffix).data)
						return this.getList(type, id, urlSuffix).data;
					else
						return this.fetchDataFromAPI(type, id, urlPrefix, urlSuffix);

				}

			})
			.then(listData => {

				this.setList(type, id, urlSuffix, {
					data: listData,
					loading: false
				});

			})
			.catch(error => {
				throw new Error(error);
			});



	}

	loadMore(type, id, urlPrefix, urlSuffix) {

		id = id || '';
		urlPrefix = urlPrefix || '';
		urlSuffix = urlSuffix || '';

		if (!this.getList(type, id, urlSuffix).complete) {

			this.setList(type, id, urlSuffix, {loading: true});
			this.fetchDataFromAPI(type, id, urlPrefix, urlSuffix).then(results => {

				this.setList(type, id, urlSuffix, {
					data: this.getList(type, id, urlSuffix).data.concat(results),
					loading: false
				});

			}, error => {
				throw new Error(error);
			});

		}

	}

	fetchDataFromAPI(type, id, urlPrefix, urlSuffix) {

		return this.$q((resolve, reject) => {

			this.API
				.one(urlPrefix).one(type, id).one(urlSuffix)
				.get({
					'skip': this.getList(type, id, urlSuffix).skip,
					'since': this.getList(type, id, urlSuffix).since
				})
				.then(result => {

					this.setList(type, id, urlSuffix, {since: result.meta.last_modified});

					if (result.meta.next_skip === null) {

						this.setList(type, id, urlSuffix, {
							skip: 0,
							complete: true
						});

					} else {
						this.setList(type, id, urlSuffix, {skip: result.meta.next_skip});
					}

					resolve(result.data);

				})
				.catch(error => {
					reject(error.data.message);
				});

		});

	}

}
