export class RegisterService {

	constructor($q, API, CommonService) {

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	registerUser($user, $tenant) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getRegisterResource())
				.customPOST({
					tenant: $tenant,
					user: $user
				}).then((response) => {
					resolve(response.data);
				}, error => {
					reject(error);
				});

		});

	}

	resendConfirmationEmail(email) {

		let request = {
			'email': email
		};

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getAuthResource())
				.one(this.CommonService.getResendEmailConfirmationResource())
				.customPOST(request).then((response) => {
					resolve(response.data);
				}, error => {
					reject(error);
				});

		});

	}

	confirmEmailAddress(token) {

		let request = {
			'token': token
		};

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getAuthResource())
				.one(this.CommonService.getConfirmEmailResource())
				.customPOST(request).then((response) => {
					resolve(response.data);
				}, error => {
					reject(error);
				});

		});

	}

	// https://developers.google.com/recaptcha/docs/verify
	reCaptchaVerifyUserResponse($userResponse) {

		return this.$q((resolve, reject) => {

			this.API
				.one(this.CommonService.getVerifyRecaptchaResource())
				.customPOST({
					user_response: $userResponse
				}).then(response => {
					resolve(response.data.success);
				}, error => {
					reject(error);
				});

		});

	}

}
