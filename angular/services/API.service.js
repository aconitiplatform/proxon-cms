export class APIService {

	constructor(Restangular, ToastService, $window, $auth, $localStorage) {

		'ngInject';

		//content negotiation
		let headers = {
			'Content-Type': 'application/json',
			'Accept': 'application/x.laravel.v1+json'
		};

		return Restangular.withConfig(function (RestangularConfigurer) {
			RestangularConfigurer
				.setBaseUrl('/api/')
				.setDefaultHeaders(headers)
				.setErrorInterceptor(function (response) {

					if (response.status === 422 || response.status === 401) {

						for (let error in response.data.errors) {

							ToastService.error(response.data.errors[error][0]);

							return true;

						}

					}

					if (response.status === 500) {

						ToastService.error(response.statusText);

						return true;

					}

					if (response.status === 503) {

						ToastService.error(response.data.data);

						return true;

					}

				})
				.addFullRequestInterceptor(function (element, operation, what, url, headers) {

					let token = $window.localStorage.satellizer_token;
					if (token) {

						var payload = $auth.getPayload();

						if (payload.exp - moment().unix() <= 1200) {

							headers['X-JWT-Token'] = 'refresh';
							headers.Authorization = 'Bearer ' + token;

						} else {

							headers.Authorization = 'Bearer ' + token;

						}

					}

					let superAdmin = $localStorage['superAdminUser'];
					if (superAdmin) {
						headers['X-Superadmin'] = superAdmin.id;
					}

				})
				.addResponseInterceptor(function (data, operation, what, url, response, deferred) {

					var newAuthToken = response.headers('X-JWT-Token');

					if (newAuthToken) {

						$auth.setToken(newAuthToken);

					}

					deferred.resolve(data);

				});
		});

	}

}
