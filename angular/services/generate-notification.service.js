
export class GenerateNotificationService {

	constructor($q, API) {

		'ngInject';

		this.$q = $q;
		this.API = API;
	}

	getData(testUrl) {

		return this.$q((resolve, reject) => {

			let encodedUrl = encodeURIComponent(testUrl);
			let queryUrl = 'url-parser?url='+ encodedUrl + '&language=de'

			this.API.one(queryUrl).get().then((response) => {
				resolve(response);
			}, (error) => {
				console.error('Failed to get data!', error);
				reject(error);

			});

		});

	}

	changeMeta(url, poiMeta, changeTitle, changeDescription, changeImage) {

		return this.$q((resolve, reject) => {

			this.getData(url).then((response) => {
				if (response && response.data) {
					let data = response.data;
					if (changeTitle !== false && data.title) {
						poiMeta.title = data.title;
					}
					if (changeDescription !== false && data.description) {
						poiMeta.description = data.description;
					}
					if (changeImage !== false && data.url) {
						poiMeta.image_type = 'url';
						poiMeta.image_url = data.url;
					}
					resolve();
				} else {
					reject('Failed to parse open graph data');
				}
			}, (error) => {
				reject(error);
			});

		});

	}

}