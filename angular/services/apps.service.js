export class AppsService{

	constructor($q, $localStorage, API, AppService, CommonService){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.AppService = AppService;
		this.CommonService = CommonService;

		if(!this.$localStorage.apps) {
			this.$localStorage.apps = [];
		}

	}

	getLastModifiedHasBeenChecked() {
		return this.CommonService.lastModifiedHasBeenChecked;
	}

	getApps() {

		this.CommonService.lastModifiedHasBeenChecked = false;

		return this.$q((resolve,reject) => {

			this.CommonService.getLastModified(this.CommonService.getAppsResource()).then((t) => {

				if(!!this.$localStorage.apps && this.$localStorage.appsUpdated >= t) {

					resolve(this.$localStorage.apps);

				} else {

					this.$localStorage.apps = [];

					this.API.one(this.CommonService.getAppsResource()).get().then((result) => {

						let apps = result.data;

						let d = new Date();
						let time = d.getTime();

						this.$localStorage.apps = apps;

						this.$localStorage.appsUpdated = time;

						if(apps.length == this.$localStorage.apps.length) {
							resolve(this.$localStorage.apps);
						}

					}, (error) => {

						reject([]);

					});

				}

			},(reject) => {
				reject([]);
			});

		});

	}

}
