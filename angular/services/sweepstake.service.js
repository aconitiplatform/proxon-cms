export class SweepstakeService{

	constructor($q, $localStorage, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	getSweepstakes(poiId) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getPoisResource(),poiId).one(this.CommonService.getSweepstakesResource()).get().then((result) => {

					resolve(result);

				}, (error) => {

					reject(error);

				});

		});

	}

}
