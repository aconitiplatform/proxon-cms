
export class EventUnsubscribeService {

	constructor () {

		'ngInject';

	}

	registerUnsubscribe(component) {

		let unsubcribeFunction = () => {

			// unsubscribe from $rootScope events
			if (component.unsubscribe) {

				if (typeof component.unsubscribe === 'function') {
					component.unsubscribe();
				} else if (Array.isArray(component.unsubscribe)) {

					for (let subFunction of component.unsubscribe) {
						subFunction();
					}

				}

			}

		}

		// ensure that we don't overwrite an existing $onDestory
		let originalOnDestroy = component['$onDestroy'];
		let newOnDestroy = unsubcribeFunction;

		if (originalOnDestroy) {
			originalOnDestroy = originalOnDestroy.bind(component);
			newOnDestroy = () => {
				originalOnDestroy();
				unsubcribeFunction();
			}
			newOnDestroy = newOnDestroy.bind(component);
		}

		component['$onDestroy'] = newOnDestroy;

	}

}
