
export class FontService {

	constructor ($http, $localStorage, $q, GlobalConstants) {

		'ngInject';

		this.$http = $http;
		this.$localStorage = $localStorage;
		this.$q = $q;
		this.GlobalConstants = GlobalConstants;

		this.added = {};

	}


	loadFonts() {

		return this.$q((resolve, reject) => {

			if (!this.$localStorage.fonts) {

				let url = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=alpha&key=' + this.GlobalConstants.googleApiKey;



			} else {

				resolve(this.$localStorage.fonts);

			}

		});

	}

	getFonts() {

		return this.$localStorage.fonts;

	}

	getFont(index) {

		return this.$localStorage.fonts[index];

	}

	appendFontByIndex(index) {

		let fontName = this.getFont(index).family;
		this.appendFontByName(fontName);

	}

	appendFontByName(fontName) {

		let uriName = encodeURIComponent(fontName);

		let link = '//fonts.googleapis.com/css?family=' + uriName;

		if (!this.added[fontName]) {
			$('link:last').after('<link href="' + link + '" rel="stylesheet" type="text/css">');
			this.added[fontName] = true;
		}

	}

}