export class InvoicesService{

	constructor($q, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	getInvoices() {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getInvoicesResource()).get().then((result) => {

					resolve(result.data);

				}, (error) => {

					reject(error);

				});

		});

	}

	getInvoicePdf(id){

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getInvoicePdfResource(id)).withHttpConfig({responseType: 'blob'}).get().then((result) => {

					resolve(result);

				}, (error) => {

					reject(error);

				});

		});

	}

}

