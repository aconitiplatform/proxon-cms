export class StripeService {

	constructor($q, $localStorage, $translate, GlobalConstants) {

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.$translate = $translate;
		this.GlobalConstants = GlobalConstants;

		this.stripeHandler;
		this.stripeLoaded = false;

	}

	setupStripe() {

		if (this.stripeLoaded) return;

		let s = document.createElement('script');
		document.head.appendChild(s);
		s.type = 'text/javascript';
		s.src = 'https://checkout.stripe.com/checkout.js';
		s.onload = () => {

			console.log('checkout stripe loaded!');
			this.stripeLoaded = true;

			this.stripeHandler = StripeCheckout.configure({
				key: this.GlobalConstants['stripePublicKey']
			});

		};

	}

	openStripeCheckout(totalPrice, options) {

		options = options || {};

		options['description'] = options['description'] || null;
		options['email']       = options['email']       || null;
		options['panel-label'] = options['panel-label'] || null;

		return this.$q((resolve,reject) => {

			this.$q.all([
				this.$translate(options['description']),
				this.$translate(options['panel-label'])
			]).then((translations) => {
				options['description'] = translations[0];
				options['panel-label'] = translations[1];
			}).catch((error) => {}).then(() => {

				let transactionToken = null;

				this.stripeHandler.open({
					name: 'Connect',
					panelLabel: options['panel-label'],
					description: options['description'],
					email: options['email'],
					zipCode: true,
					amount: totalPrice,
					currency: 'EUR',
					locale: 'auto',
					token: (token) => {
						console.log('received stripe token!', token);
						transactionToken = token;
					},
					closed: () => {
						// the token callback will be called first, according to API
						console.log('stripe checkout closed');
						if (!!transactionToken) {
							resolve(transactionToken.id);
						} else {
							reject('stripe checkout aborted');
						}
					}
				});

			});

		});

	}

	closeStripeCheckout() {

		this.stripeHandler.close();

	}

}
