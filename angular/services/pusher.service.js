export class PusherService {

	constructor($rootScope, $pusher, $q, $window, GlobalConstants, CommonService, API) {

		'ngInject';

		this.$rootScope = $rootScope;
		this.$pusher = $pusher;
		this.$q = $q;
		this.$window = $window;
		this.GlobalConstants = GlobalConstants;
		this.CommonService = CommonService;
		this.API = API;

		this.pusherClient = new Pusher(GlobalConstants.pusherKey, {
			authEndpoint: '/api/pusher/authenticate-presence',
			auth: {
				headers: {
					'Authorization': 'Bearer ' + $window.localStorage.satellizer_token
				}
			},
			cluster: 'eu'
		});
		this.pusher = $pusher(this.pusherClient);
		this.subscribedChannels = {};
		this.subscribedEvents = {};
		this.eventData = {};

	}

	getEventData(eventName) {

		console.log('getting event data for', eventName, '...');
		if (this.eventData[eventName] === undefined) {
			console.log('getting event data - error');
			return false;
		} else {
			console.log('getting event data - success', this.eventData[eventName]);
			return this.eventData[eventName];
		}

	}

	poiLockedByAnotherUser(poiId, userId) {

		let lockedPois = this.getEventData('locked-pois').pois || {},
			islocked;

		if ( lockedPois.hasOwnProperty(poiId) ) {

			console.log('current POI IS locked');
			let editingUserId = lockedPois[poiId][0].id;

			if (userId == editingUserId) {
				console.log('current user IS the editing user');
				islocked = false;
			} else {
				console.log('current user is NOT the editing user');
				islocked = true;
			}

		} else {
			console.log('current POI is NOT locked');
			islocked = false;
		}

		// notify poi-tabs and poi-content
		this.$rootScope.$emit('poiIsLockedByAnotherUser', islocked);
		return islocked;

	}

	setEventData(eventName, eventData) {

		this.eventData[eventName] = eventData;
		this.$rootScope.$emit(eventName + '-event', eventData);
		console.log('settings event data for', eventName, 'data:', eventData);

	}

	subscribe(channel) {

		if ( !this.subscribedChannels[channel] ) {
			this.subscribedChannels[channel] = this.pusher.subscribe(channel);
			console.log('subscribed to channel', channel);
		} else {
			console.log('couldn\'t subscribe to channel',channel,' reason: channel allready exists.');
		}

	}

	unsubscribe(channel) {

		this.subscribedChannels[channel].unbind();
		this.subscribedChannels[channel] = null;
		this.pusher.unsubscribe(channel);
		console.log('unsubscribed from channel', channel);

	}

	bind(event, channel, callback) {

		if ( !this.subscribedChannels[channel][event] ) {
			console.log('bound', event, 'to', channel);
			this.subscribedChannels[channel][event] = this.subscribedChannels[channel].bind(event, data => {
				console.warn('new event! executing callback', event, data);
				this.eventData[event] = data;
				callback(data);
			});

		}

	}

}

