export class AppService{

	constructor($q, API, CommonService){

		'ngInject';

		this.$q = $q;
		this.API = API;
		this.CommonService = CommonService;

	}

	getApp(id) {

		return this.$q((resolve,reject) => {

				this.API.one(this.CommonService.getAppsResource(),id).get().then((result) => {

					let app = result.data;
					resolve(app);

				}, (error) => {

					reject(error);

				});

		});

	}

	addApp(app) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getAppsResource()).customPOST(app).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	updateApp(app) {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getAppsResource(),app.id).patch(app).then((result) => {

				resolve(result.data);

			}, () => {

				reject({});

			});

		});

	}

	deleteApp(app) {

		let appName = app.appname;

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getAppsResource(),app.id).remove().then(() => {

				resolve(appName);

			}, () => {

				reject(false);

			});

		});

	}

}
