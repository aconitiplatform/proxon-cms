export class UpdateService{

	constructor($q, $localStorage, API, CommonService, GlobalConstants){

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;
		this.GlobalConstants = GlobalConstants;

	}

	checkForUpdate(){

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getUpdateCheckResource()).get().then((result) => {

				if(result.data.sha == this.GlobalConstants.commitIdLong){

					resolve(false);

				}else{

					resolve(true);

				}

			}, () => {

				reject([]);

			});

		});

	}

}

