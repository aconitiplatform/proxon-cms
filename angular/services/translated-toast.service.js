export class TranslatedToastService {
	constructor(ToastService, $translate, $q) {
		'ngInject';

		this.ToastService = ToastService;
		this.$translate = $translate;
		this.$q = $q;
	}

	show(translationTag, translationOptions) {
		this.$q.resolve(
			this.$translate(translationTag, translationOptions)
		).then((translation) => {
			this.ToastService.show(translation);
		}, (error) => {
			this.ToastService.show(translationTag);
		});
	}

	error(translationTag, translationOptions) {
		this.$q.resolve(
			this.$translate(translationTag, translationOptions)
		).then((translation) => {
			this.ToastService.error(translation);
		}, (error) => {
			this.ToastService.error(translationTag);
		});
	}

}
