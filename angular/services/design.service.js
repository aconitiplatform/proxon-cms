export class DesignService {

	constructor($q, $localStorage, API, CommonService, Restangular) {

		'ngInject';

		this.$q = $q;
		this.$localStorage = $localStorage;
		this.API = API;
		this.CommonService = CommonService;
		this.Restangular = Restangular;

	}

	getDesign() {

		return this.$q((resolve, reject) => {

			this.API.one(this.CommonService.getTenantsResource()).one(this.CommonService.getDesignResource()).get().then((response) => {

				this.$localStorage.design = response.data;
				this.$localStorage.designChanges = angular.copy(response.data);

				resolve(angular.copy(response));

			});

		});

	}

	updateDesign() {

		return this.$q((resolve) => {

			var design = angular.copy(this.$localStorage.designChanges);

			this.API.one(this.CommonService.getTenantsResource()).one(this.CommonService.getDesignResource()).patch(design).then((response) => {

				this.$localStorage.design = response.data;

				resolve(angular.copy(response.data));

			});

		});

	}

	getTenantDesignBlueprint() {

		return this.$q((resolve) => {

			let cs = this.CommonService;

			this.API.one(cs.getBlueprintResource()).one(cs.getDesignsResource()).one(cs.getTenantResource()).get().then((response) => {

				resolve(response.data);

			});

		});

	}

	getPoiDesignBlueprint() {

		return this.$q((resolve) => {

			let cs = this.CommonService;

			this.API.one(cs.getBlueprintResource()).one(cs.getDesignsResource()).one(cs.getPoiResource()).get().then((response) => {

				resolve(response.data);

			});

		});

	}

}
