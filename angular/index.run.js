import {RoutesRun} from './run/routes.run';
import {OverlayRun} from './run/overlay.run';
import {LocationChangesRun} from './run/location-changes.run';
import {ConfigLoaderRun} from './run/config-loader.run';
import {AnalyticsRun} from './run/analytics.run';
import {GatekeeperRun} from './run/gatekeeper.run';

angular.module('app.run')
	.run(RoutesRun)
	.run(OverlayRun)
	.run(LocationChangesRun)
	.run(ConfigLoaderRun)
	.run(AnalyticsRun)
	.run(GatekeeperRun);
