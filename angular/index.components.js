import {MapComponent}									from './app/components/map/map.component';
import {TenantEditComponent}							from './app/components/tenant-edit/tenant-edit.component';
import {TenantsFormComponent}							from './app/components/superadmin/tenants-form/tenants-form.component';
import {LoginScreenComponent}							from './app/components/app/public/login-screen/login-screen.component';
import {PoiActivityComponent}							from './app/components/pois/poi-activity/poi-activity.component';
import {InvoicesComponent}								from './app/components/admin/invoices/invoices.component';
import {SuperadminSearchComponent}						from './app/components/superadmin/superadmin-search/superadmin-search.component';
import {ModuleErrorsComponent}							from './app/components/pois/module-errors/module-errors.component';
import {PoiDesignDefaultValueComponent}					from './app/components/pois/poi-design-default-value/poi-design-default-value.component';
import {PoiDesignComponent}								from './app/components/pois/poi-design/poi-design.component';
import {SettingsButtonComponent}						from './app/components/admin/settings-button/settings-button.component';
import {SettingsComponent}								from './app/components/admin/settings/settings.component';
import {SuperadminLoginComponent}						from './app/components/superadmin/superadmin-login/superadmin-login.component';
import {TenantFeaturesComponent}						from './app/components/superadmin/tenant-features/tenant-features.component';
import {TenantListComponent}							from './app/components/superadmin/tenant-list/tenant-list.component';
import {DesignComponent}								from './app/components/admin/design/design.component';
import {DesignButtonComponent}							from './app/components/admin/design-button/design-button.component';
import {PoiAccessComponent}								from './app/components/analytics/poi-access/poi-access.component';
import {PoiInteractionComponent}						from './app/components/pois/poi-interaction/poi-interaction.component';
import {ModuleIconComponent}							from './app/components/pois/module-icon/module-icon.component';
import {DeliveredContentComponent}						from './app/components/analytics/delivered-content/delivered-content.component';
import {UrlAccessComponent}								from './app/components/analytics/url-access/url-access.component';
import {MicroPaymentDetailComponent}					from './app/components/micro-payment/micro-payment-detail/micro-payment-detail.component';
import {MicroPaymentFormComponent}						from './app/components/micro-payment/micro-payment-form/micro-payment-form.component';
import {MicroPaymentListComponent}						from './app/components/micro-payment/micro-payment-list/micro-payment-list.component';
import {NewsletterConfigurationDetailComponent}			from './app/components/newsletter-configuration/newsletter-configuration-detail/newsletter-configuration-detail.component';
import {NewsletterConfigurationFormComponent}			from './app/components/newsletter-configuration/newsletter-configuration-form/newsletter-configuration-form.component';
import {NewsletterConfigurationListComponent}			from './app/components/newsletter-configuration/newsletter-configuration-list/newsletter-configuration-list.component';
import {LineChartComponent}								from './app/components/charts/line-chart/line-chart.component';
import {PoiTouchpointsComponent}						from './app/components/pois/poi-touchpoints/poi-touchpoints.component';
import {KpiCardComponent}								from './app/components/dashboard/demo/kpi-card/kpi-card.component';
import {PoiContentComponent}							from './app/components/pois/poi-content/poi-content.component';
import {MediaUploadComponent}							from './app/components/media/media-upload/media-upload.component';
import {SpinnerComponent}								from './app/components/app/spinner/spinner.component';
import {MediaDetailComponent}							from './app/components/media/media-detail/media-detail.component';
import {MediaComponent}									from './app/components/media/media/media.component';
import {TouchpointComponent}							from './app/components/touchpoints/touchpoint/touchpoint.component';
import {TouchpointButtonComponent}						from './app/components/touchpoints/touchpoint-button/touchpoint-button.component';
import {TouchpointListComponent}						from './app/components/touchpoints/touchpoint-list/touchpoint-list.component'
import {TouchpointSelectComponent}						from './app/components/touchpoints/touchpoint-select/touchpoint-select.component';
import {PoiMessagesComponent}							from './app/components/pois/poi-messages/poi-messages.component';
import {UsersDetailComponent}							from './app/components/admin/users/users-detail/users-detail.component';
import {UsersFormComponent}								from './app/components/admin/users/users-form/users-form.component';
import {UsersListComponent}								from './app/components/admin/users/users-list/users-list.component';
import {AppsDetailComponent}							from './app/components/admin/apps/apps-detail/apps-detail.component';
import {AppsFormComponent}								from './app/components/admin/apps/apps-form/apps-form.component';
import {AppsListComponent}								from './app/components/admin/apps/apps-list/apps-list.component';
import {PoiSettingsComponent}							from './app/components/pois/poi-settings/poi-settings.component';
import {PoiTabsComponent}								from './app/components/pois/poi-tabs/poi-tabs.component';
import {AddPoiFormComponent}							from './app/components/pois/add-poi-form/add-poi-form.component';
import {PoiAddButtonComponent}							from './app/components/pois/poi-add-button/poi-add-button.component';
import {PoisComponent}									from './app/components/pois/pois/pois.component';
import {NotFoundComponent}								from './app/components/app/not-found/not-found.component';
import {PoiComponent}									from './app/components/pois/poi/poi.component';
import {SideMenuComponent}								from './app/components/app/side-menu/side-menu.component';
import {OverlayComponent}								from './app/components/app/overlay/overlay.component';
import {LoadingIndicatorComponent}						from './app/components/app/loading-indicator/loading-indicator.component';
import {AppHeaderComponent}								from './app/components/app/app-header/app-header.component';
import {AppViewComponent}								from './app/components/app/app-view/app-view.component';
import {AppShellComponent}								from './app/components/app/app-shell/app-shell.component';
import {ResetPasswordComponent}							from './app/components/app/public/reset-password/reset-password.component';
import {ForgotPasswordComponent}						from './app/components/app/public/forgot-password/forgot-password.component';
import {ResendUnlockAccountComponent}					from './app/components/app/public/resend-unlock-account/resend-unlock-account.component';
import {LoginFormComponent}								from './app/components/app/public/login-form/login-form.component';
import {UnlockAccountComponent}							from './app/components/app/public/unlock-account/unlock-account.component';
import {RegisterFormComponent}							from './app/components/app/public/register-form/register-form.component';
import {AnalyticsOverviewComponent}						from './app/components/analytics/analytics-overview/analytics-overview.component';
import {DesignFormComponent}							from './app/components/admin/design-form/design-form.component';
import {ButtonDesignComponent}							from './app/components/admin/button-design/button-design.component';
import {BackgroundDesignComponent}						from './app/components/admin/background-design/background-design.component';
import {FaqLinkComponent}								from './app/components/help/faq-link/faq-link.component';
import {MarkdownListComponent}							from './app/components/help/markdown-list/markdown-list.component';
import {MarkdownSearchComponent}						from './app/components/help/markdown-search/markdown-search.component';
import {FontSelectComponent}							from './app/components/admin/font-select/font-select.component';
import {GenericSearchComponent}							from './app/components/generic-search/generic-search.component';
import {InfiniteScrollingComponent}						from './app/components/infinite-scrolling/infinite-scrolling.component';
import {ImagePreviewComponent}							from './app/components/media/image-preview/image-preview.component';
import {ImageSelectionComponent}						from './app/components/media/image-selection/image-selection.component';
import {SaveButtonComponent}							from './app/components/save-button/save-button.component';
import {SubscriptionComponent}							from './app/components/admin/subscription/subscription.component';
import {AccountSetupComponent}							from './app/components/account-setup/account-setup.component';
import {ConfirmEmailComponent}							from './app/components/app/public/confirm-email/confirm-email.component';

angular.module('app.components')
	.component('map', MapComponent)
	.component('tenantEdit', TenantEditComponent)
	.component('tenantsForm', TenantsFormComponent)
	.component('loginScreen', LoginScreenComponent)
	.component('poiActivity', PoiActivityComponent)
	.component('invoices', InvoicesComponent)
	.component('superadminSearch', SuperadminSearchComponent)
	.component('moduleErrors', ModuleErrorsComponent)
	.component('poiDesignDefaultValue', PoiDesignDefaultValueComponent)
	.component('poiDesign', PoiDesignComponent)
	.component('settingsButton', SettingsButtonComponent)
	.component('settings', SettingsComponent)
	.component('superadminLogin', SuperadminLoginComponent)
	.component('tenantFeatures', TenantFeaturesComponent)
	.component('tenantList', TenantListComponent)
	.component('design', DesignComponent)
	.component('designButton', DesignButtonComponent)
	.component('poiAccess', PoiAccessComponent)
	.component('poiInteraction', PoiInteractionComponent)
	.component('moduleIcon', ModuleIconComponent)
	.component('deliveredContent', DeliveredContentComponent)
	.component('urlAccess', UrlAccessComponent)
	.component('microPaymentDetail', MicroPaymentDetailComponent)
	.component('microPaymentForm', MicroPaymentFormComponent)
	.component('microPaymentList', MicroPaymentListComponent)
	.component('newsletterConfigurationDetail', NewsletterConfigurationDetailComponent)
	.component('newsletterConfigurationForm', NewsletterConfigurationFormComponent)
	.component('newsletterConfigurationList', NewsletterConfigurationListComponent)
	.component('lineChart', LineChartComponent)
	.component('poiTouchpoints', PoiTouchpointsComponent)
	.component('kpiCard', KpiCardComponent)
	.component('poiContent', PoiContentComponent)
	.component('mediaUpload', MediaUploadComponent)
	.component('spinner', SpinnerComponent)
	.component('mediaDetail', MediaDetailComponent)
	.component('media', MediaComponent)
	.component('touchpoint', TouchpointComponent)
	.component('touchpointButton', TouchpointButtonComponent)
	.component('touchpointList', TouchpointListComponent)
	.component('touchpointSelect', TouchpointSelectComponent)
	.component('poiMessages', PoiMessagesComponent)
	.component('usersDetail', UsersDetailComponent)
	.component('usersForm', UsersFormComponent)
	.component('usersList', UsersListComponent)
	.component('appsDetail', AppsDetailComponent)
	.component('appsForm', AppsFormComponent)
	.component('appsList', AppsListComponent)
	.component('poiSettings', PoiSettingsComponent)
	.component('poiTabs', PoiTabsComponent)
	.component('addPoiForm', AddPoiFormComponent)
	.component('poiAddButton', PoiAddButtonComponent)
	.component('pois', PoisComponent)
	.component('notFound', NotFoundComponent)
	.component('poi', PoiComponent)
	.component('sideMenu', SideMenuComponent)
	.component('overlay', OverlayComponent)
	.component('loadingIndicator', LoadingIndicatorComponent)
	.component('appHeader', AppHeaderComponent)
	.component('appView', AppViewComponent)
	.component('appShell', AppShellComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('resendUnlockAccount', ResendUnlockAccountComponent)
	.component('loginForm', LoginFormComponent)
	.component('unlockAccount', UnlockAccountComponent)
	.component('registerForm', RegisterFormComponent)
	.component('analyticsOverview', AnalyticsOverviewComponent)
	.component('designForm', DesignFormComponent)
	.component('buttonDesign', ButtonDesignComponent)
	.component('backgroundDesign', BackgroundDesignComponent)
	.component('faqLink', FaqLinkComponent)
	.component('fontSelect', FontSelectComponent)
	.component('markdownList', MarkdownListComponent)
	.component('markdownSearch', MarkdownSearchComponent)
	.component('genericSearch', GenericSearchComponent)
	.component('infiniteScrolling', InfiniteScrollingComponent)
	.component('imagePreview', ImagePreviewComponent)
	.component('imageSelection', ImageSelectionComponent)
	.component('saveButton', SaveButtonComponent)
	.component('subscription', SubscriptionComponent)
	.component('accountSetup', AccountSetupComponent)
	.component('confirmEmail', ConfirmEmailComponent);
