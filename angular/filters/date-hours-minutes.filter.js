export function DateHoursMinutesFilter(){

	'ngInject';

	return function( input ){

		return moment.utc(input, 'HH:mm').local().format('HH:mm');

	}

}
