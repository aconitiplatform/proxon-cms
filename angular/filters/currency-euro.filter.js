export function CurrencyEuroFilter() {
	return function(input) {
		return (input/100).toFixed(2) + '€'
	};
}
