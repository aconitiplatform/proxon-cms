export function CurrencyDisplayFilter() {
	return function(input) {

		let temp = Math.floor(input/100);
		let temp2 = input - temp * 100;
		if (temp2 < 10) {
			temp2 = '0' + temp2;
		}
		let newValue = '' + temp + ',' + temp2 + ' €';

		return newValue;
	};
}
