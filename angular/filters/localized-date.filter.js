export function LocalizedDateFilter(){

	'ngInject';

	return function(input, format = 'L'){

		// moment.locale('en_GB');

		return moment(input).format(format);

	}

}