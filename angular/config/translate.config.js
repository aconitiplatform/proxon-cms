export function TranslateConfig($translateProvider) {

	'ngInject';

	$translateProvider.useLoader('LanguageLoaderService', {
		prefix: 'i18n/locale-',
		suffix: '.json'
	});

	$translateProvider.useCookieStorage();
	$translateProvider.preferredLanguage('en');

	$translateProvider.useSanitizeValueStrategy('escape');

}
