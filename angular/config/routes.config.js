export function RoutesConfig($stateProvider, $urlRouterProvider) {

	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	// $urlRouterProvider.when('', '/pois');

	$urlRouterProvider.when('', ['$match', '$stateParams', function () {

		var storage = window.localStorage;

		var user = JSON.parse(storage.getItem('ngStorage-user'));

		if(user){

			switch (user.role) {

				case 'superadmin':
					return '/superadmin-login';
				case 'reseller':
					return '/reseller';
				default:
					return '/pois';

			}

		}else{

			return '/login';

		}

	}]);

	$urlRouterProvider.otherwise('/notfound');

	$stateProvider

		.state('public', {
			abstract: true,
			data: {},
			views: {
				public: {}
			}
		})
		.state('public.login', {
			url: '/login',
			views: {
				'public@': {
					templateUrl: getView('login')
				}
			}
		})
		.state('public.register', {
			url: '/register',
			views: {
				'public@': {
					templateUrl: getView('register')
				}
			}
		})
		.state('public.forgotpassword', {
			url: '/forgot-password',
			views: {
				'public@': {
					templateUrl: getView('forgot-password')
				}
			}
		})
		.state('public.resendunlockaccount', {
			url: '/resend-unlock-account/:email',
			views: {
				'public@': {
					templateUrl: getView('resend-unlock-account')
				}
			}
		})
		.state('public.resetpassword', {
			url: '/reset-password/:email/:token',
			views: {
				'public@': {
					templateUrl: getView('reset-password')
				}
			}
		})
		.state('public.setpassword', {
			url: '/set-password/:email/:token',
			views: {
				'public@': {
					templateUrl: getView('set-password')
				}
			}
		})
		.state('public.unlockaccount', {
			url: '/unlock-account/:email/:token',
			views: {
				'public@': {
					templateUrl: getView('unlock-account')
				}
			}
		})
		.state('public.confirmemail', {
			url: '/confirm-email/:token',
			views: {
				'public@': {
					templateUrl: getView('confirm-email')
				}
			}
		})
		.state('public.notfound', {
			url: '/notfound',
			views: {
				'public@': {
					templateUrl: getView('not-found')
				}
			}
		})

		.state('account-setup', {
			url: '/account-setup',
			data: {
				auth: true
			},
			views: {
				'account-setup': {
					templateUrl: getView('account-setup')
				}
			}
		})

		.state('authorized', {
			abstract: true,
			data: {
				auth: true
			},
			views: {
				'authorized@': {
					templateUrl: getView('wrapper')
				}
			}
		})

		.state('authorized.dashboard', {
			url: '/dashboard',
			views: {
				'main': {
					templateUrl: getView('dashboard')
				}
			},
			activeMenuItem: 'dashboard',
			title: 'COMMON.DASHBOARD'
		})
		.state('authorized.profile', {
			url: '/profile',
			views: {
				'main': {
					templateUrl: getView('profile')
				}
			},
			activeMenuItem: 'profile',
			feature: 'profile'
		})
		.state('authorized.analytics', {
			url: '/analytics',
			views: {
				'main': {
					templateUrl: getView('analytics')
				}
			},
			activeMenuItem: 'analytics',
			title: 'COMMON.ANALYTICS',
			feature: 'analytics'
		})
		.state('authorized.touchpoints', {
			url: '/touchpoints',
			views: {
				'main': {
					templateUrl: getView('touchpoints')
				}
			},
			activeMenuItem: 'touchpoints',
			title: 'COMMON.TOUCHPOINTS',
			feature: 'beacon'
		})
		.state('authorized.touchpoint', {
			url: '/touchpoints/:id',
			views: {
				'main': {
					templateUrl: getView('touchpoint')
				}
			},
			activeMenuItem: 'touchpoints',
			base: {
				title: 'COMMON.TOUCHPOINTS',
				url: '/touchpoints'
			},
			trackPage: '/touchpoints/touchpoint',
			feature: 'beacon'
		})
		.state('authorized.pois', {
			url: '/pois',
			views: {
				'main': {
					templateUrl: getView('pois')
				}
			},
			activeMenuItem: 'pois',
			title: 'COMMON.POIS',
			feature: 'poi'
		})
		.state('authorized.poiadd', {
			url: '/pois/add',
			views: {
				'main': {
					templateUrl: getView('poi-add')
				}
			},
			activeMenuItem: 'pois',
			base: {
				title: 'POIs',
				url: '/pois'
			},
			title: 'POIS.NEW_POI',
			feature: 'poi'
		})
		.state('authorized.poi', {
			url: '/pois/:id',
			views: {
				'main': {
					templateUrl: getView('poi')
				}
			},
			activeMenuItem: 'pois',
			base: {
				title: 'COMMON.POIS',
				url: '/pois'
			},
			trackPage: '/pois/poi',
			feature: 'poi'
		})

		// .state('authorized.surveys', {
		//     url: '/surveys',
		//     views: {
		// 		'main': {
		// 			templateUrl: getView('surveys')
		// 		}
		//     },
		// 	activeMenuItem: 'surveys',
		// 	title: 'COMMON.SURVEYS'
		// })
		// .state('authorized.surveyadd', {
		//     url: '/surveys/add',
		//     views: {
		// 		'main': {
		// 			templateUrl: getView('survey-add')
		// 		}
		//     },
		// 	activeMenuItem: 'surveys',
		// 	base: {
		// 		title: 'COMMON.SURVEYS',
		// 		url: '/surveys'
		// 	},
		// 	title: 'SURVEYS.NEW_SURVEY'
		// })
		// .state('authorized.survey', {
		//     url: '/surveys/:id',
		//     views: {
		// 		'main': {
		// 			templateUrl: getView('survey')
		// 		}
		//     },
		// 	activeMenuItem: 'surveys',
		// 	base: {
		// 		title: 'COMMON.SURVEYS',
		// 		url: '/surveys'
		// 	}
		// })

		.state('authorized.micro-payments', {
			url: '/micro-payments',
			views: {
				'main': {
					templateUrl: getView('micro-payments')
				}
			},
			activeMenuItem: 'micro-payments',
			title: 'COMMON.MICRO_PAYMENTS',
			feature: 'payment'
		})
		.state('authorized.micropaymentadd', {
			url: '/micro-payments/add',
			views: {
				'main': {
					templateUrl: getView('micro-payment-add')
				}
			},
			activeMenuItem: 'micro-payments',
			base: {
				title: 'COMMON.MICRO_PAYMENTS',
				url: '/micro-payments'
			},
			title: 'MICRO_PAYMENTS.NEW_MICRO_PAYMENT',
			feature: 'payment'
		})
		.state('authorized.micropayment', {
			url: '/micro-payments/:id',
			views: {
				'main': {
					templateUrl: getView('micro-payment')
				}
			},
			activeMenuItem: 'micro-payments',
			base: {
				title: 'COMMON.MICRO_PAYMENTS',
				url: '/micro-payments'
			},
			trackPage: '/micro-payments/payment',
			feature: 'payment'
		})

		.state('authorized.newsletter-configurations', {
			url: '/newsletter-configurations',
			views: {
				'main': {
					templateUrl: getView('newsletter-configurations')
				}
			},
			activeMenuItem: 'newsletter-configurations',
			title: 'COMMON.NEWSLETTER_CONFIGURATIONS',
			feature: 'newsletter'
		})
		.state('authorized.newsletterconfigurationadd', {
			url: '/newsletter-configurations/add',
			views: {
				'main': {
					templateUrl: getView('newsletter-configuration-add')
				}
			},
			activeMenuItem: 'newsletter-configurations',
			base: {
				title: 'COMMON.NEWSLETTER_CONFIGURATIONS',
				url: '/newsletter-configurations'
			},
			title: 'NEWSLETTER_CONFIGURATIONS.NEW_NEWSLETTER_CONFIGURATION',
			feature: 'newsletter'
		})
		.state('authorized.newsletterconfiguration', {
			url: '/newsletter-configurations/:id',
			views: {
				'main': {
					templateUrl: getView('newsletter-configuration')
				}
			},
			activeMenuItem: 'newsletter-configurations',
			base: {
				title: 'COMMON.NEWSLETTER_CONFIGURATIONS',
				url: '/newsletter-configurations'
			},
			trackPage: '/newsletter-configurations/configuration',
			feature: 'newsletter'
		})

		.state('authorized.media', {
			url: '/media',
			views: {
				'main': {
					templateUrl: getView('media')
				}
			},
			activeMenuItem: 'media',
			title: 'COMMON.MEDIA',
			feature: 'media'
		})
		.state('authorized.mediaDetail', {
			url: '/media/:id',
			views: {
				'main': {
					templateUrl: getView('media-detail')
				}
			},
			activeMenuItem: 'media',
			base: {
				title: 'COMMON.MEDIA',
				url: '/media'
			},
			trackPage: '/media/media',
			feature: 'media'
		})

		.state('authorized.admin', {
			url: '/admin',
			views: {
				'main': {
					templateUrl: getView('admin')
				}
			},
			activeMenuItem: 'admin',
			title: 'COMMON.ADMIN',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.settings', {
			url: '/admin/settings',
			views: {
				'main': {
					templateUrl: getView('settings')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'settings',
			title: 'COMMON.SETTINGS',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.design', {
			url: '/admin/design',
			views: {
				'main': {
					templateUrl: getView('design')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'design',
			title: 'COMMON.DESIGN',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.apps', {
			url: '/admin/apps',
			views: {
				'main': {
					templateUrl: getView('apps')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'apps',
			title: 'COMMON.APPS',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.appadd', {
			url: '/admin/apps/add',
			views: {
				'main': {
					templateUrl: getView('app-add')
				}
			},
			activeMenuItem: 'admin',
			base: {
				title: 'COMMON.APPS',
				url: '/admin/apps'
			},
			title: 'ADMIN.NEW_APP',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.appDetail', {
			url: '/admin/apps/:id',
			views: {
				'main': {
					templateUrl: getView('app-detail')
				}
			},
			activeMenuItem: 'admin',
			base: {
				title: 'COMMON.APPS',
				url: '/admin/apps'
			},
			trackPage: '/admin/apps/app',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.users', {
			url: '/admin/users',
			views: {
				'main': {
					templateUrl: getView('users')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'user',
			title: 'COMMON.USER',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.useradd', {
			url: '/admin/users/add',
			views: {
				'main': {
					templateUrl: getView('user-add')
				}
			},
			activeMenuItem: 'admin',
			base: {
				title: 'COMMON.USERS',
				url: '/admin/users'
			},
			title: 'ADMIN.NEW_USER',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.userDetail', {
			url: '/admin/users/:id',
			views: {
				'main': {
					templateUrl: getView('user-detail')
				}
			},
			activeMenuItem: 'admin',
			base: {
				title: 'COMMON.USERS',
				url: '/admin/users'
			},
			trackPage: '/admin/users/user',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.billing', {
			url: '/admin/billing',
			views: {
				'main': {
					templateUrl: getView('billing')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'billing',
			title: 'COMMON.BILLING',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})
		.state('authorized.subscription', {
			url: '/admin/subscription',
			views: {
				'main': {
					templateUrl: getView('subscription')
				}
			},
			activeMenuItem: 'admin',
			activeSubMenuItem: 'subscription',
			title: 'SUBSCRIPTION.SUBSCRIPTION',
			restrictedTo: ['admin', 'superadmin'],
			feature: 'admin'
		})

		.state('authorized.help', {
			url: '/help',
			views: {
				'main': {
					templateUrl: getView('help')
				}
			},
			activeMenuItem: 'help',
			title: 'FAQ.HEADLINE',
			feature: 'help'
		})

		.state('authorized.changelog', {
			url: '/changelog',
			views: {
				'main': {
					templateUrl: getView('changelog')
				}
			},
			activeMenuItem: 'changelog',
			title: 'SIDE_MENU.CHANGELOG'
		})

		.state('authorized.superadmin-login', {
			url: '/superadmin-login',
			views: {
				'main': {
					templateUrl: getView('superadmin-login')
				}
			},
			activeMenuItem: 'superadmin-login',
			title: 'SUPERADMIN.LOGIN_PAGE_TITLE',
			restrictedTo: ['superadmin']
		})

		.state('authorized.tenants', {
			url: '/tenants',
			views: {
				'main': {
					templateUrl: getView('tenants')
				}
			},
			activeMenuItem: 'tenants',
			title: 'SUPERADMIN.TENANTS_PAGE_TITLE',
			restrictedTo: ['superadmin', 'reseller']
		})

		.state('authorized.tenantsadd', {
			url: '/tenants/add',
			views: {
				'main': {
					templateUrl: getView('tenant-add')
				}
			},
			activeMenuItem: 'tenants',
			base: {
				title: 'SUPERADMIN.TENANTS_PAGE_TITLE',
				url: '/tenants'
			},
			title: 'TENANTS.NEW_TENANT',
			restrictedTo: ['superadmin', 'reseller']
		})

		.state('authorized.tenant-edit', {
			url: '/tenant/:id',
			views: {
				'main': {
					templateUrl: getView('tenant-edit')
				}
			},
			activeMenuItem: 'tenants',
			base: {
				title: 'SUPERADMIN.TENANTS_PAGE_TITLE',
				url: '/tenants'
			},
			restrictedTo: ['superadmin', 'reseller']
		})

		.state('authorized.reseller', {
			url: '/reseller',
			views: {
				'main': {
					templateUrl: getView('reseller')
				}
			},
			activeMenuItem: 'reseller',
			title: 'RESELLER.PAGE_TITLE'
		});

}
