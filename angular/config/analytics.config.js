export function AnalyticsConfig(AnalyticsProvider, GlobalConstants) {

	'ngInject';

	AnalyticsProvider.setAccount(GlobalConstants.substanceGaTrackingId);

	AnalyticsProvider.logAllCalls(true);

	AnalyticsProvider.ignoreFirstPageLoad(true);

	AnalyticsProvider.setDomainName(GlobalConstants.substanceGaTrackingId.appUrl);

	AnalyticsProvider.setPageEvent('$locationChangeSuccess');

	AnalyticsProvider.trackPages(true);

	AnalyticsProvider.readFromRoute(true);

}
