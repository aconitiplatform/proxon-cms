export function MarkdownConfig(markedProvider){

	'ngInject';

	markedProvider.setOptions({
		gfm: true,
		tables: true,
		highlight: function (code, lang) {
			if (lang) {
				return hljs.highlight(lang, code, true).value;
			} else {
				return hljs.highlightAuto(code).value;
			}
		}
	});

}
