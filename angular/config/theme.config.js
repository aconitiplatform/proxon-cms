export function ThemeConfig($mdThemingProvider) {
	'ngInject';

	/* For more info, visit https://material.angularjs.org/#/Theming/01_introduction */

	// Create a new palette for aconiti and use it
	$mdThemingProvider.definePalette('aconiti', {
		'50': '33FFF8',
		'100': '1FFFF8',
		'200': '0AFFF7',
		'300': '00F5ED',
		'400': '00E0D9',
		'500': '00CCC5',
		'600': '00B8B1',
		'700': '00A39E',
		'800': '008F8A',
		'900': '007874',
		'A100': '006663',
		'A200': '00524F',
		'A400': '003D3B',
		'A700': '002927',
		'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
											// on this palette should be dark or light
	
		'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
		 '200', '300', '400', 'A100'],
		'contrastLightColors': undefined    // could also specify this if default was 'dark'
	  });

	// $mdThemingProvider.definePalette('aconiti')
	$mdThemingProvider.theme('default')
		// .primaryPalette('aconitiPalette',{
		// 	default: '900'
		// })
		.primaryPalette(window.GlobalConstants.themePrimary, {
			default: '600'
		})
		.accentPalette(window.GlobalConstants.themeAccent)
		.warnPalette(window.GlobalConstants.themeWarn);

	$mdThemingProvider.theme('warn')
		.primaryPalette(window.GlobalConstants.themeWarn);

}
