export function DateLocaleConfig($mdDateLocaleProvider){

	'ngInject';

	// Example of a French localization.
	$mdDateLocaleProvider.months = ['janvier', 'février', 'mars', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
	$mdDateLocaleProvider.shortMonths = ['janv', 'févr', 'mars', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
	$mdDateLocaleProvider.days = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
	$mdDateLocaleProvider.shortDays = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];

	// Can change week display to start on Monday.
	$mdDateLocaleProvider.firstDayOfWeek = 1;

	// Optional.
	// $mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, ...];

	// Example uses moment.js to parse and format dates.
	$mdDateLocaleProvider.parseDate = function(dateString) {
	  var m = moment(dateString, 'L', true);
	  return m.isValid() ? m.toDate() : new Date(NaN);
	};

	$mdDateLocaleProvider.formatDate = function(date) {
	  var m = moment(date);
	  return m.isValid() ? m.format('L') : '';
	};

	$mdDateLocaleProvider.monthHeaderFormatter = function(date) {
	  return $mdDateLocaleProvider.shortMonths[date.getMonth()] + ' ' + date.getFullYear();
	};

	// In addition to date display, date components also need localized messages
	// for aria-labels for screen-reader users.

	$mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
	  return 'Woche ' + weekNumber;
	};

	$mdDateLocaleProvider.msgCalendar = 'Calendrier';
	$mdDateLocaleProvider.msgOpenCalendar = 'Ouvrir le calendrier';

	// You can also set when your calendar begins and ends.
	$mdDateLocaleProvider.firstRenderableDate = new Date(1776, 6, 4);
	// $mdDateLocaleProvider.lastRenderableDate = new Date(2012, 11, 21);

}
