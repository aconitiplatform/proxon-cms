export function NumeralJsConfig($numeraljsConfigProvider) {

	'ngInject';

	$numeraljsConfigProvider.register('locale', 'de', {
		delimiters: {
			thousands: '.',
			decimal: ','
		},
		abbreviations: {
			thousand: 'k',
			million: 'm',
			billion: 'b',
			trillion: 't'
		},
		ordinal: (number) => {
			return '.';
		},
		currency: {
			symbol: '€'
		}
	});

	$numeraljsConfigProvider.locale('de');

}