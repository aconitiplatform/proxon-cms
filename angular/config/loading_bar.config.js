export function LoadingBarConfig(cfpLoadingBarProvider) {
	'ngInject';
	cfpLoadingBarProvider.includeSpinner = false;
	cfpLoadingBarProvider.includeBar = false;
}
