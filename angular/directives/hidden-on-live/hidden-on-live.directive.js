class HiddenOnLiveController{

	constructor($q, Restangular, CommonService, GlobalConstants){

		'ngInject';

		this.GlobalConstants = GlobalConstants;

	}

}

export function HiddenOnLiveDirective(){
	return {
		controller: HiddenOnLiveController,
		link: function(scope, element, attrs, controllers){

			if(controllers.GlobalConstants.appEnv == 'production' || controllers.GlobalConstants.appEnv == 'staging'){

				element.remove();

			}

		}
	}
}
