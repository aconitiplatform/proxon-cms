export function SaveOnceDirective() {

	'ngInject';

	return {
		restrict: 'A',
		replace: false,
		link: function($scope, $element, $attrs, $ctrl) {

			$element.on('click', function() {
				this.disabled = true;
				this.insertAdjacentHTML('afterbegin', '<md-icon class="spin material-icons">autorenew</md-icon>');
			});

		}

	};

}
