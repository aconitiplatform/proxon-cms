class UnitsInputController{

	constructor(){

		'ngInject';

	}

}

export function UnitsInputDirective($compile, $q, $translate){

	'ngInject';

	return {
		controller: UnitsInputController,
		require: 'ngModel',
		scope: true,
		link: function(scope, element, attrs, controller){

			if(typeof attrs.allowed != 'undefined'){

				attrs.allowed = attrs.allowed.replace(/\s+/g, '');
				attrs.allowed = attrs.allowed.split(',');

			}else{

				attrs.allowed = ['px'];

			}

			if(typeof attrs.default == 'undefined'){

				attrs.default = attrs.allowed[0];

			}

			var inputValue = '';

			scope.$watch(attrs.ngModel, function(value) {

				inputValue = value;

			});

			let inputScope, canDisplayMessage = false;

			if (element && element.length && element.length > 0 && element[0].name && element[0].form && element[0].form.name) {
				canDisplayMessage = true;

				let html = '<div ng-messages="'+element[0].form.name+'.'+element[0].name+'.$error">';
				html += '<div ng-message="noValidInput">{{'+element[0].form.name+'.'+element[0].name+'.noValidInputMessage}}</div>';
				html += '</div>';

				let content = angular.element(html);
				content.insertAfter(element);

				inputScope = angular.element(element[0].form).scope();

				$compile(content)(inputScope);

			}

			element.bind('blur', (event) => {

				let value = inputValue, setValid = true;

				if (!value || value == '') {

					// we don't care for empty values in this directive
					setValid = true;

				} else {

					// strip whitespace
					value = inputValue.replace(/\s+/g, '');

					// lé regex magic
					let regex = /^(\-?\d+)([^;\d]*);?$/i

					let match = regex.exec(value);

					let number, unit;

					if (match){

						number = match[1];

						if(match.length == 3) {
							unit = match[2];
							if (unit == null || '' === unit) {
								unit = attrs.default;
							}
						}

						let unitAllowed = false;

						for(let au of attrs.allowed){
							if (au == unit){
								unitAllowed = true;
								break;
							}
						}

						if (unitAllowed){

							var newValue = number + unit;
							setValid = true;
							controller.$setViewValue(newValue);

						}else{

							// no valid unit
							setValid = false;

						}

					} else {

						setValid = false;

					}

				}

				controller.$setValidity('noValidInput', setValid);

				if (!setValid && canDisplayMessage) {
					$q.resolve(
						$translate('COMMON.ALLOWED_UNITS')
					).then((translation) => {
						let msg = '', first = true;

						for(let au of attrs.allowed){
							if (!first) {
								msg += ', ';
							}
							msg += au;
							first = false;
						}
						msg = translation + ' ' + msg;
						inputScope[element[0].form.name][element[0].name].noValidInputMessage = msg;
					});
				}

				controller.$render();

			});

			let clearInput = angular.element('<md-button class="md-icon-button input-clear"><md-icon class="material-icons">close</md-icon></md-button>');
			clearInput.insertAfter(element);
			$compile(clearInput)(scope);

			clearInput.bind('click', () => {
				// actual reset
				controller.$setViewValue('');
				controller.$render();
				element[0].focus();
				element[0].blur();
			});

		}
	}

}
