export function AddToCartDirective(CartService) {

	'ngInject';

	return {
		restrict: 'A',
		replace: false,
		scope: {},
		controllerAs: 'vm',
		link: function($scope, $element, $attrs, $ctrl) {

			$element.on('click', () => {

				let hardware   = $attrs.hardware;
				let touchpoint = JSON.parse($attrs.touchpoint);
				let product    = CartService.createProduct(touchpoint, hardware);

				CartService.addToCart(product);

			});

		}

	};

}
