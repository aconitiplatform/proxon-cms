class HttpInputController{

	constructor(){

		'ngInject';

	}

}

export function HttpInputDirective($compile, $q, $translate){

	'ngInject';

	return {
		controller: HttpInputController,
		require: 'ngModel',
		scope: true,
		link: function(scope, element, attrs, controller){

			if(typeof attrs.prefix == 'undefined'){

				attrs.prefix = 'http://';

			}else{

				if(attrs.prefix != 'https://' && attrs.prefix != 'http://'){

					attrs.prefix = 'http://';

				}

			}

			var inputValue = '';

			scope.$watch(attrs.ngModel, function(value) {

				inputValue = value;

			});

			element.bind('blur', (event) => {

				if (inputValue.search(/^http[s]?\:\/\//) == -1) {

					var newValue = attrs.prefix + inputValue;

					controller.$setViewValue(newValue);

				}

				controller.$render();

			});

		}
	}

}
