class ClearInputController{

	constructor($rootScope, $compile){

		'ngInject';

		this.$rootScope = $rootScope;

	}

}

export function ClearInputDirective($compile){

	'ngInject';

	return {
		controller: ClearInputController,
		restrict: 'AC',
		require: 'ngModel',
		//templateUrl: './clear-input-template.html',
		//replace: true,
		scope: true,
		link: function(scope, element, attrs, controller){

			let content = angular.element('<md-button class="md-icon-button input-clear"><md-icon class="material-icons">close</md-icon></md-button>');
			content.insertAfter(element);
			$compile(content)(scope);

			content.bind('click', () => {
				// actual reset
				controller.$setViewValue('');
				controller.$render();
				element[0].focus();
				element[0].blur();
			});

		}

	}

}
