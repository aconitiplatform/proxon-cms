import {QrCodeDialogController} from '../../dialogs/qr-code/qr-code.dialog';

class QrDialogDirective {

	constructor($mdDialog, DialogService, UserService) {

		'ngInject';

		this.$mdDialog = $mdDialog;
		this.DialogService = DialogService;

	}

	openQrDialog($url) {
		console.log($url);
		this.DialogService.fromTemplate('', 'qr-code', {
			controller: QrCodeDialogController,
			controllerAs: 'vm',
			clickOutsideToClose: true,
			fullscreen: false,
			locals: {url: $url}
		});

	}

}

export function OpenQrDialogDirective() {

	// 'ngInject';

	return {
		restrict: 'A',
		replace: false,
		scope: {},
		controllerAs: 'vm',
		controller: QrDialogDirective,
		link: function($scope, $element, $attrs, $ctrl) {
			$element.on('click', () => {
				$ctrl.openQrDialog($attrs.openQrDialog);
			});
		}

	};

}
