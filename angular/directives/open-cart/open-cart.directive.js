import {CartDialogController} from '../../dialogs/cart/cart.dialog';

export function OpenCartDirective($rootScope, DialogService) {

	'ngInject';

	return {
		restrict: 'A',
		replace: false,
		scope: {},
		controllerAs: 'vm',
		link: function($scope, $element, $attrs, $ctrl) {

			let badge = document.createElement('span');

			badge.id = 'cart-badge';
			badge.innerText = '1';

			$element.attr('id', 'cart-btn');
			$element.prepend(badge);

			let cartSizeListener = $rootScope.$on('cartSize', (event, size) => {
				badge.innerText = size;
			});

			$scope.$on('$destroy', cartSizeListener);

			$element.on('click', () => {

				DialogService.fromTemplate('', 'cart', {
					controller: CartDialogController,
					controllerAs: 'vm',
					clickOutsideToClose: true,
					fullscreen: false
				});

			});

		}

	};

}
