class MarkdownSearchController{

	constructor($localStorage){

		'ngInject';

		this.$localStorage = $localStorage;

		this.search = this.type + 'Search'

	}

	$onInit(){
	}

	resetSearch(){

		this.$localStorage[this.search].content = '';

	}

}

export const MarkdownSearchComponent = {
	templateUrl: './views/app/components/help/markdown-search/markdown-search.component.html',
	controller: MarkdownSearchController,
	controllerAs: 'vm',
	bindings: {
		type: '<'
	}
}
