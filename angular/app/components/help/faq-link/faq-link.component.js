import {DisplayFaqController} from '../../../../dialogs/display-faq/display-faq.dialog';

class FaqLinkController{
	constructor(DialogService, MarkdownService){

		'ngInject';

		this.DialogService = DialogService;
		this.MarkdownService = MarkdownService;

		this.MarkdownService.loadFaqs();

	}

	displayFaq() {

		this.DialogService.fromTemplate('', 'display-faq', {
			controller: DisplayFaqController,
			controllerAs: 'vm',
			clickOutsideToClose: true,
			fullscreen: true,
			skipHide: true,
			locals: {
				faqId: this.faqId
			}
		});

	}
}

export const FaqLinkComponent = {
	templateUrl: './views/app/components/help/faq-link/faq-link.component.html',
	controller: FaqLinkController,
	controllerAs: 'vm',
	bindings: {
		faqId: '<'
	}
}
