class MarkdownListController{
	constructor($localStorage, MarkdownService, $http, $timeout) {

		'ngInject';
		this.$localStorage = $localStorage;
		this.MarkdownService = MarkdownService;
		this.$http = $http;
		this.$timeout = $timeout;

		this.search = this.type + 'Search';
		this.$localStorage[this.search] = {
			content: ''
		};

		this.itemArray = [];
		this.reverse = !!this.reverse;

	}

	$onInit() {

		this.MarkdownService.getMarkdown(this.type).then((response) => {
			this.items = response;

			for (let key in this.items) {

				let url = this.MarkdownService.getMarkdownUrl(this.type, key);

				this.$http.get(url).success((content) => {

					this.itemArray.push({
						id: key,
						content: content
					});

				}).error(() => {

					console.error('Failed to get: "' + url + '"');

				});

			}

		});

	}

	versionCompare(v1, v2) {

		 // If we don't get strings, just compare by index
		if (v1.type !== 'string' || v2.type !== 'string') {
			return (v1.index < v2.index) ? -1 : 1;
		}
		let versionRegex = /\D*(\d+)\.(\d+)\.(\d+).*/;

		let res1, res2;
		let data1 = {};
		let data2 = {};

		res1 = v1.value.match(versionRegex);
		if (res1 && res1.length == 4) {
			data1.major = parseInt(res1[1], 10);
			data1.minor = parseInt(res1[2], 10);
			data1.patch = parseInt(res1[3], 10);
		}

		res2 = v2.value.match(versionRegex);
		if (res2 && res2.length == 4) {
			data2.major = parseInt(res2[1], 10);
			data2.minor = parseInt(res2[2], 10);
			data2.patch = parseInt(res2[3], 10);
		}

		let ret = data1.major == data2.major ? 0 : data1.major > data2.major ? 1 : -1;
		if (ret == 0) {
			ret = data1.minor == data2.minor ? 0 : data1.minor > data2.minor ? 1 : -1;
		}
		if (ret == 0) {
			ret = data1.patch == data2.patch ? 0 : data1.patch > data2.patch ? 1 : -1;
		}

		return ret;

	}

}

export const MarkdownListComponent = {
	templateUrl: './views/app/components/help/markdown-list/markdown-list.component.html',
	controller: MarkdownListController,
	controllerAs: 'vm',
	bindings: {
		type: '<',
		reverse: '<',
		version: '<'
	}
}
