class KpiCardController{

	constructor(KpiCardService){

		'ngInject';

		this.kpiCardService = KpiCardService;
		this.kpis = {};
		this.kpiLoaded = false;

		this.yesterday = moment().add(-1, 'days').add(-1, 'hours').seconds(0).minutes(0);

	}

	$onInit(){

		this.kpiCardService.getKpis().then((kpis) => {
			this.kpis = kpis;
			this.kpiLoaded = true;
		}, () => {
			this.kpis = {};
		});

	}

}

export const KpiCardComponent = {
	templateUrl: './views/app/components/dashboard/demo/kpi-card/kpi-card.component.html',
	controller: KpiCardController,
	controllerAs: 'vm',
	bindings: {

	}
}
