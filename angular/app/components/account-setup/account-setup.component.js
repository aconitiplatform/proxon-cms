import { DesignComponent } from "../admin/design/design.component";


class AccountSetupController {

	constructor(ProductConfigService, $state, $localStorage, TenantService, DesignService) {

		'ngInject';

		this.ProductConfigService = ProductConfigService;
		this.$state = $state;
		this.$localStorage = $localStorage;
		this.TenantService = TenantService;
		this.DesignService = DesignService;

		this.headerColor = '#000000';
		this.headerBackgroundColor = '#bb1c3e';
		this.color = '#000000';
		this.backgroundColor = '#fffffff';
		this.footerColor = '#ffffff';
		this.footerBackgroundColor = '#021A24';
		this.imprint = '';
		this.lastPageNumber = 5;

		this.setPage(0);

	}

	$onInit() {

		this.backgroundImage = {
			'background-size': 'cover',
			'min-height': '100vh',
			'background-position': 'center'
		}

		this.TenantService.getTenant().then((tenant) => {
			// we need to ensure that things are loaded in $localStorage
		});

		this.DesignService.getDesign().then(() => {
			// we need to ensure that things are loaded in $localStorage
		});

		this.ProductConfigService.getConfig().then((config) => {

			this.productConfig = config;
			this.backgroundImage['background-image'] = 'url(' + config.login_screen_image + ')';

		});

	}

	exit() {
		this.$state.transitionTo('authorized.pois');
	}

	exitAndSave() {

		// update imprint
		let tenantChanges = this.$localStorage.tenantChanges;
		let controllername = 'AccountSetup';
		tenantChanges.imprint = this.imprint;

		this.TenantService.updateTenant(tenantChanges, controllername).then((response) => {
			console.log('updated tenant!', response);
		});

		// update design
		let designChanges = this.$localStorage.designChanges;

		designChanges.header.color = this.headerColor;
		designChanges.header.background = this.headerBackgroundColor;

		designChanges.body.color = this.color;
		designChanges.edward_app.background = this.backgroundColor;

		designChanges.footer.color = this.footerColor;
		designChanges.footer.background = this.footerBackgroundColor;

		this.loading = true;
		this.DesignService.updateDesign().then((response) => {
			this.$state.transitionTo('authorized.pois');
			this.loading = false;
		}, (error) => {
			loading = false;
		});

	}

	setPage(pageNumber) {

		this.currentPage = pageNumber;
		this.isFirstPage = pageNumber == 0;
		this.isLastPage = pageNumber == this.lastPageNumber;

	}

	previousPage() {
		this.setPage(this.currentPage - 1);
	}

	nextPage() {
		this.setPage(this.currentPage + 1);
	}

}

export const AccountSetupComponent = {
	templateUrl: './views/app/components/account-setup/account-setup.component.html',
	controller: AccountSetupController,
	controllerAs: 'vm',
	bindings: {}
}
