class MicroPaymentListController{

	constructor($scope, $state, API, $localStorage, CommonService) {

		'ngInject';

		this.$scope = $scope;
		this.$state = $state;
		this.API = API;
		this.$localStorage = $localStorage;
		this.CommonService = CommonService;

		this.propertyName = 'name';
		this.reverse = false;

	}

	$onInit() {

		if(!this.$localStorage.MicroPaymentSort){

			this.$localStorage.MicroPaymentSort = {
				propertyName: 'name',
				reverse: false
			}

		}

	}

	sortBy(propertyName) {

		this.$localStorage.MicroPaymentSort.reverse = (this.$localStorage.MicroPaymentSort.propertyName === propertyName) ? !this.$localStorage.MicroPaymentSort.reverse : true;
		this.$localStorage.MicroPaymentSort.propertyName = propertyName;

	}

	edit(microPayment){

		this.$state.transitionTo('authorized.micropayment', {id: microPayment.id});

	}

}

export const MicroPaymentListComponent = {
	templateUrl: './views/app/components/micro-payment/micro-payment-list/micro-payment-list.component.html',
	controller: MicroPaymentListController,
	controllerAs: 'vm',
	bindings: {}
}
