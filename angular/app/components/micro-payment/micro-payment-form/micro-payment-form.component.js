class MicroPaymentFormController{

	constructor(MicroPaymentService, TranslatedToastService, $state, DialogService, $translate, $q) {

		'ngInject';

		this.MicroPaymentService = MicroPaymentService;
		this.TranslatedToastService = TranslatedToastService;
		this.$state = $state;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.$q = $q;

		this.types = [
			{
				name: 'Single',
				id: 'single'
			}
		];

		this.paymentTypes = [
			'credit_card'
		]

		this.isNewPayment = false;

		if(!this.microPayment) {

			this.isNewPayment = !this.isNewPayment;

			this.microPayment = {
				name: '',
				description: '',
				amount: 50,
				type: this.types[0].id,
				currency: 'EUR',
				allowed_payment_types: [
					this.paymentTypes[0]
				],
				emails_payee: false,
				emails_payer: false,
				email: '',
				callback_url: '',
				heartbeat_url: null,
				fields: []
			}

		}

	}

	$onInit() {

	}

	saveMicroPayment() {

		if(this.isNewPayment) {

			this.MicroPaymentService.addMicroPayment(this.microPayment).then((microPayment) => {

				this.TranslatedToastService.show('TOASTS.MP_CREATED', { name: this.microPayment.name });
				this.$state.transitionTo('authorized.micropayment', { id: microPayment.id });

			}, (notMicroPayment) => {
				this.TranslatedToastService.error('TOASTS.MP_CREATE_ERROR', { name: this.microPayment.name });
			});

		} else {

			this.MicroPaymentService.updateMicroPayment(this.microPayment).then((microPayment) => {

				this.TranslatedToastService.show('TOASTS.MP_UPDATED', { name: this.microPayment.name });

				this.microPayment = microPayment;

			}, (notMicroPayment) => {
				this.TranslatedToastService.error('TOASTS.MP_UPDATE_ERROR', { name: this.microPayment.name });
			});

		}

	}

	deleteMicroPayment(event) {

		this.$q.all([
			this.$translate('MICRO_PAYMENTS.CONFIRM_DELETE_HEADING', { name: this.microPayment.name }),
			this.$translate('MICRO_PAYMENTS.CONFIRM_DELETE_BODY')
		]).then((i18n) => {

			let confirm = this.DialogService.confirm(i18n[0],i18n[1],null);

			confirm.then(() => {

				this.MicroPaymentService.deleteMicroPayment(this.microPayment).then((resolve) => {

					this.TranslatedToastService.show('TOASTS.MP_DELETED', { name: this.microPayment.name });
					this.$state.transitionTo('authorized.micro-payments');

				}, () => {

					this.TranslatedToastService.error('TOASTS.MP_DELETE_ERROR', { name: this.microPayment.name });

				});

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

}

export const MicroPaymentFormComponent = {
	templateUrl: './views/app/components/micro-payment/micro-payment-form/micro-payment-form.component.html',
	controller: MicroPaymentFormController,
	controllerAs: 'vm',
	bindings: {
		microPayment: '<'
	}
}
