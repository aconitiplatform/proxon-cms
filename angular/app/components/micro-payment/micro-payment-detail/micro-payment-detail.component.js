import {ShowTransactionsController} from '../../../../dialogs/show-transactions/show-transactions.dialog';

class MicroPaymentDetailController{

	constructor(MicroPaymentService, $state, $stateParams, DialogService) {

		'ngInject';

		this.MicroPaymentService = MicroPaymentService;
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.DialogService = DialogService;

		this.$state.current.title = 'Micro Payment';

		this.microPayment = {};

		this.microPaymentLoaded = false;

	}

	$onInit() {

		this.getMicroPayment();

	}

	getMicroPayment() {

		this.MicroPaymentService.getMicroPayment(this.$stateParams.id).then((microPayment) => {

			this.$state.current.title = microPayment.name;

			this.microPayment = microPayment;

			this.microPaymentLoaded = true;

		}, (notUser) => {

			this.$state.transitionTo('public.notfound');

		});

	}

	showTransactions(){

		this.DialogService.fromTemplate('', 'show-transactions', {
			controller: ShowTransactionsController,
			controllerAs: 'vm',
			clickOutsideToClose:false,
			fullscreen: true,
			locals: {
				microPaymentId: this.$stateParams.id
			}
		}).then((changes) => {


		});

	}

}

export const MicroPaymentDetailComponent = {
	templateUrl: './views/app/components/micro-payment/micro-payment-detail/micro-payment-detail.component.html',
	controller: MicroPaymentDetailController,
	controllerAs: 'vm',
	bindings: {}
}
