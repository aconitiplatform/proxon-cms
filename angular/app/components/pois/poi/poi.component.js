
import {ModuleSelectionController} from '../../../../dialogs/module-selection/module-selection.dialog';
import {GroupEditController} from '../../../../dialogs/group-edit/group-edit.dialog';

class PoiController {

	constructor($transitions, $rootScope, $state, PoiService, $localStorage, LanguageNamesService, $stateParams, $mdDialog,
				PoiValidatorService, TranslatedToastService, $scope, DialogService, $window, $location, $q, $translate, Analytics,
				DesignService, EventUnsubscribeService, PusherService) {

		'ngInject';

		this.$transitions = $transitions;
		this.$rootScope = $rootScope;
		this.$state = $state;
		this.PoiService = PoiService;
		this.$localStorage = $localStorage;
		this.LanguageNamesService = LanguageNamesService;
		this.$stateParams = $stateParams;
		this.$mdDialog = $mdDialog;
		this.PoiValidatorService = PoiValidatorService;
		this.TranslatedToastService = TranslatedToastService;
		this.$scope = $scope;
		this.DialogService = DialogService;
		this.$window = $window;
		this.$location = $location;
		this.$q = $q;
		this.$translate = $translate;
		this.Analytics = Analytics;
		this.DesignService = DesignService;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		// pusher
		this.poiId = this.$stateParams.id;
		this.userId = $localStorage.user.id;
		this.poiIsLockedByAnotherUser = PusherService.poiLockedByAnotherUser(this.poiId, this.userId);

		this.hasChanges = false;

		this.selectedLanguage = this.$localStorage.tenant.fallback_language;

		// create local storage for
		if(!this.$localStorage.poi){

			this.$localStorage.poi = {};

		}

		this.checkForErrors();

	}

	$onInit() {

		this.unsubscribe = [];

		this.getPoi();
		this.loadDesign();

		this.unsubscribe.push(this.$rootScope.$on('poiIsLockedByAnotherUser', (event, isLocked) => {
			console.log("updating poiIsLockedByAnotherUser in <poi>", isLocked);
			this.poiIsLockedByAnotherUser = isLocked;
		}));

		this.unsubscribe.push(this.$rootScope.$on('PoiHasChanged', (event) => {

			this.PoiService.checkForChanges(this.poiId).then((hasChanges) => {

				this.checkForErrors();

				if(hasChanges){

					this.hasChanges = true;

				}else{

					this.hasChanges = false;

				}

			});

		}));

		this.unsubscribe.push(this.$rootScope.$on('RevalidatePoi', (event) => {

			this.revalidatePoi();

		}));

		this.$window.onbeforeunload = () => {

			if(this.hasChanges){

				return 'Unsaved changes!';

			}

		}

		this.$scope.$on('$locationChangeStart', (event) => {

			if(this.hasChanges){

				var targetPath = this.$location.path();

				event.preventDefault();

				this.$q.resolve(
					this.$translate(['POIS.SAVE_CONFIRMATION.TITLE', 'POIS.SAVE_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
				).then((translations) => {

					this.DialogService.confirm(translations['POIS.SAVE_CONFIRMATION.TITLE'], translations['POIS.SAVE_CONFIRMATION.TEXT'], {
						skipHide: true
					}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

						this.hasChanges = false;

						this.$location.path(targetPath);

					});

				}, (reject) => {
					console.info("Strings missing.");
					console.info(reject);
				});

			}

		});

	}

	$onDestroy() {

	}

	checkForErrors() {

		let poi = this.$localStorage.poi[this.poiId];
		let hasErrors = false;
		let check = true;

		if (!poi) {
			check = false;
		}

		if (check && !poi.changes) {
			check = false;
		}

		if (check && !poi.changes.poi_contents) {
			check = false;
		}

		if (check) {

			for (let content of poi.changes.poi_contents) {

				check = true;

				if (check && !content.data) {
					check = false;
				}

				if (check && !content.data.groups) {
					check = false;
				}

				if (check) {
					for (let group of content.data.groups) {
						if (group.modules) {
							for (let module of group.modules) {
								if (module.data && module.data.errors) {
									hasErrors = true;
								}
							}
						}
					}
				}
			}
		}

		this.poiHasErrors = hasErrors;

	}

	revalidatePoi() {

		this.PoiValidatorService.validate(this.$localStorage.poi[this.poiId].changes).then(() => {

			this.$rootScope.$emit('PoiHasMessages', []);

		}, (errors) => {

			this.$rootScope.$emit('PoiHasMessages', errors, true);

		});

	}

	getPoi(){

		this.PoiService.getPoi(this.poiId).then((poi) => {

			console.info(poi);

			this.$state.current.title = poi.name;

			this.selectedLanguage = poi.fallback_language;

			var poiData = {
				loaded: angular.copy(poi),
				changes: angular.copy(poi)
			}

			this.$localStorage.poi[this.poiId] = poiData;

			this.$rootScope.$emit('poiLoaded');

		}, () => {

			this.$state.transitionTo('public.notfound');

		});

	}

	updatePoi(){

		console.info(this.$localStorage.poi[this.poiId].changes);

		this.spinnerIsVisible = true;

		this.PoiValidatorService.validate(this.$localStorage.poi[this.poiId].changes).then(() => {

			this.PoiService.updatePoi(this.poiId).then((poi) => {

				this.$localStorage.poi[this.poiId].loaded = angular.copy(poi);
				this.$localStorage.poi[this.poiId].changes = angular.copy(poi);

				this.$rootScope.$broadcast('PoiHasChanged');
				this.$rootScope.$broadcast('PoiGotSaved');
				this.$rootScope.$emit('PoiHasMessages', []);

				this.TranslatedToastService.show('TOASTS.CHANGES_SAVED');

				this.spinnerIsVisible = false;

			}, () => {

				this.spinnerIsVisible = false;

			});

		}, (errors) => {

			this.$rootScope.$emit('PoiHasMessages', errors);

			this.spinnerIsVisible = false;

		});

	}

	loadDesign(){

		this.DesignService.getDesign().then((design) => {

			this.$localStorage.design = angular.copy(design.data);

		});

	}

	openStatusContextMenu($mdOpenMenu, ev){

		$mdOpenMenu(ev);

	}

	publish(){

		this.$localStorage.poi[this.poiId].changes.active = true;

		this.updatePoi();

	}

	unpublish(){

		this.$localStorage.poi[this.poiId].changes.active = false;

		this.updatePoi();

	}

}

export const PoiComponent = {
	templateUrl: './views/app/components/pois/poi/poi.component.html',
	controller: PoiController,
	controllerAs: 'vm',
	bindings: {}
}
