class PoiAddButtonController{

	constructor(){

		'ngInject';

	}

	$onInit(){

	}

}

export const PoiAddButtonComponent = {
	templateUrl: './views/app/components/pois/poi-add-button/poi-add-button.component.html',
	controller: PoiAddButtonController,
	controllerAs: 'vm',
	bindings: {}
}
