class PoisController{

	constructor($rootScope, $transitions, $scope, $state, API, $localStorage, CommonService, PusherService, EventUnsubscribeService){

		'ngInject';

		this.$rootScope = $rootScope;
		this.$transitions = $transitions;
		this.$scope = $scope;
		this.$state = $state;
		this.API = API;
		this.$localStorage = $localStorage;
		this.CommonService = CommonService;
		this.PusherService = PusherService;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.propertyName = 'name';
		this.reverse = false;

		this.lockedPois = PusherService.getEventData('locked-pois').pois || {};
		this.tenantId = $localStorage.tenant.id;
		console.warn("pois-controller");

	}

	$onInit(){

		this.unsubscribe = [];

		if(!this.$localStorage.POISort){

			this.$localStorage.POISort = {
				propertyName: 'name',
				reverse: false
			}

		}

		this.$scope.$on('contentHasBeenSelected', (event, data) => {

		});

		this.initPusher();

	}

	$onDestroy() {

		this.termPusher();

	}

	initPusher() {

		this.PusherService.subscribe('pois-channel-' + this.tenantId);
		this.PusherService.bind('locked-pois', 'pois-channel-' + this.tenantId, eventData => {
			this.lockedPois = eventData.pois;
		});

	}

	termPusher() {

		this.PusherService.unsubscribe('pois-channel-' + this.tenantId);

	}

	sortBy(propertyName) {

		this.$localStorage.POISort.reverse = (this.$localStorage.POISort.propertyName === propertyName) ? !this.$localStorage.POISort.reverse : true;
		this.$localStorage.POISort.propertyName = propertyName;

	}

	edit(poi){

		this.$state.transitionTo('authorized.poi', {id: poi.id});

	}

	preview(){

		alert('preview much?');

	}

}

export const PoisComponent = {
	templateUrl: './views/app/components/pois/pois/pois.component.html',
	controller: PoisController,
	controllerAs: 'vm',
	bindings: {}
}
