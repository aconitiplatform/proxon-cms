class PoiTabsController{

	constructor($scope, $rootScope, $localStorage, $stateParams, EventUnsubscribeService, TranslatedToastService, PusherService){

		'ngInject';

		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.$stateParams = $stateParams;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.TranslatedToastService = TranslatedToastService;
		this.PusherService = PusherService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		// select content to be the default selected tab
		this.currentNavItem = 'content';

		// pusher
		this.poiId = this.$stateParams.id;
		this.userId = $localStorage.user.id;
		this.tenantId = $localStorage.tenant.id;
		this.poiIsLockedByAnotherUser = PusherService.poiLockedByAnotherUser(this.poiId, this.userId);

	}

	$onInit(){

		this.initPusher();

		this.unsubscribe = [];

		this.unsubscribe.push(this.$rootScope.$on('poiLoaded', (event) => {
			this.poiLoaded = true;
		}));

	}

	$onDestroy() {

		this.termPusher();

	}

	navChange(navItem){

		this.$rootScope.$emit('POINavChanged', navItem);

	}

	preview(){
		alert('test');
	}

	initPusher() {

		this.PusherService.subscribe('pois-channel-' + this.tenantId);
		this.PusherService.bind('locked-pois', 'pois-channel-' + this.tenantId, eventData => {

			this.poiIsLockedByAnotherUser = this.PusherService.poiLockedByAnotherUser(this.poiId, this.userId);

		});

		this.PusherService.subscribe('presence-poi-' + this.poiId);
		this.PusherService.bind('poi-saved', 'presence-poi-' + this.poiId, eventData => {

			if (eventData.poi.id == this.poiId && this.poiIsLockedByAnotherUser ) {
				this.TranslatedToastService.show('TOASTS.RELOAD_PAGE');
			}

		});

	}

	termPusher() {

		this.PusherService.unsubscribe('presence-poi-' + this.poiId);
		this.PusherService.unsubscribe('pois-channel-' + this.tenantId);

	}

}

export const PoiTabsComponent = {
	templateUrl: './views/app/components/pois/poi-tabs/poi-tabs.component.html',
	controller: PoiTabsController,
	controllerAs: 'vm',
	bindings: {}
}
