class ModuleErrorsController {

	constructor(){

		'ngInject';

		this.errorStrings = [];

	}

	$onInit(){

		if(this.errors){

			_.forIn(this.errors, (value, key) => {

				if (key != 'voucher_image_id') {

					_.each(value, (string) => {

						this.errorStrings.push(string);

					});

				}

			});

			console.info(this.errorStrings);

		}

	}

}

export const ModuleErrorsComponent = {
	templateUrl: './views/app/components/pois/module-errors/module-errors.component.html',
	controller: ModuleErrorsController,
	controllerAs: 'vm',
	bindings: {
		errors: '<'
	}
};
