class PoiActivityController {

	constructor($state, $scope, $rootScope, $stateParams, PoiActivityService, ListService) {

		'ngInject';

		this.$state = $state;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.$stateParams = $stateParams;
		this.PoiActivityService = PoiActivityService;
		this.ListService = ListService;

		this.poiId = $stateParams.id;
		this.note = '';
		this.logs = [];

		ListService.initList('pois', this.poiId, 'log');

	}

	$onInit() {

		this.loadLogs();

		this.listChangedListener = this.$rootScope.$on('pois' + 'log' + this.poiId + 'ListChanged', (event, poisLogList) => {
			this.logs = poisLogList.data;
			this.poisLogList = poisLogList;
		});

		this.poiGotSaved = this.$rootScope.$on('PoiGotSaved', () => {
			this.loadLogs();
		});

	}

	$onDestroy() {

		this.$scope.$on('$destroy', this.listChangedListener);
		this.$scope.$on('$destroy', this.poiGotSaved);

	}

	loadLogs() {
		this.ListService.loadList('pois', this.poiId, false, 'log');
	}

	loadMoreLogs() {
		this.ListService.loadMore('pois', this.poiId, false, 'log');
	}

	addNote() {

		this.PoiActivityService.createNote(this.poiId, this.note).then(success => {
			// show toast?
			this.loadLogs();
			this.note = '';
			this.$scope.noteForm.$setPristine();
			this.$scope.noteForm.$setUntouched();
		});

	}

}

export const PoiActivityComponent = {
	templateUrl: './views/app/components/pois/poi-activity/poi-activity.component.html',
	controller: PoiActivityController,
	controllerAs: 'vm',
	bindings: {}
};
