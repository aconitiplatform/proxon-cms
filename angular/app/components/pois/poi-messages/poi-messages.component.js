class PoiMessagesController{

	constructor($rootScope, $window, EventUnsubscribeService){

		'ngInject';

		this.$rootScope = $rootScope;
		this.$window = $window;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.hasMessages = false;

		this.messages = [];

	}

	$onInit(){

		this.unsubscribe = this.$rootScope.$on('PoiHasMessages', (event, messages, suppressScrollTop) => {

			if(messages.length > 0){

				this.hasMessages = true;

				this.messages = messages;

				if (!suppressScrollTop) {
					this.$window.document.getElementById('main_content').scrollTop = 0;
				}

			}else{

				this.hasMessages = false;

				this.messages = messages;

			}

		});

	}

}

export const PoiMessagesComponent = {
	templateUrl: './views/app/components/pois/poi-messages/poi-messages.component.html',
	controller: PoiMessagesController,
	controllerAs: 'vm',
	bindings: {}
}
