class PoiInteractionController{

	constructor($state, PoiService, $localStorage, $stateParams, FeedbackService, SweepstakeService) {

		'ngInject';

		this.$state = $state;
		this.PoiService = PoiService;
		this.$localStorage = $localStorage;
		this.$stateParams = $stateParams;
		this.FeedbackService = FeedbackService;
		this.SweepstakeService = SweepstakeService;

		this.poiId = this.$stateParams.id;

		this.feedbacks = [];
		this.sweepstakes = [];
		this.participants = 0;

	}

	$onInit(){

		this.FeedbackService.getFeedbacks(this.poiId).then((feedbacks) => {
			this.feedbacks = feedbacks;
		}, (noFeedbacks) => {

		});

		this.SweepstakeService.getSweepstakes(this.poiId).then((sweepstakes) => {
			this.sweepstakes = sweepstakes.data;
			this.participants = sweepstakes.meta.count;
		}, (noSweepstakes) => {

		});

	}

}

export const PoiInteractionComponent = {
	templateUrl: './views/app/components/pois/poi-interaction/poi-interaction.component.html',
	controller: PoiInteractionController,
	controllerAs: 'vm',
	bindings: {}
}
