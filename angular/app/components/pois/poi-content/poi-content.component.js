import { ModuleSelectionController } from '../../../../dialogs/module-selection/module-selection.dialog';
import { GroupEditController } from '../../../../dialogs/group-edit/group-edit.dialog';
import { PoiPreviewController } from '../../../../dialogs/poi-preview/poi-preview.dialog';
import { PoiMetaController } from '../../../../dialogs/poi-meta/poi-meta.dialog';

class PoiContentController {

	constructor($rootScope, API, $state, $localStorage, LanguageNamesService, $stateParams, $mdDialog, TranslatedToastService, $scope,
		DialogService, ModuleEditService, CommonService, $q, $translate, EventUnsubscribeService,
		ModuleValidationService, deviceDetector, PusherService, GenerateNotificationService, TenantService) {

		'ngInject';

		this.$rootScope = $rootScope;
		this.API = API;
		this.$state = $state;
		this.$localStorage = $localStorage;
		this.LanguageNamesService = LanguageNamesService;
		this.$stateParams = $stateParams;
		this.$mdDialog = $mdDialog;
		this.TranslatedToastService = TranslatedToastService;
		this.$scope = $scope;
		this.DialogService = DialogService;
		this.ModuleEditService = ModuleEditService;
		this.CommonService = CommonService;
		this.$q = $q;
		this.$translate = $translate;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.ModuleValidationService = ModuleValidationService;
		this.deviceDetector = deviceDetector;
		this.GenerateNotificationService = GenerateNotificationService;
		this.TenantService = TenantService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.TenantService.getAvailableModules().then((modules) => {
			this.modulesByName = {};
			for (let moduleDefinition of modules) {
				this.modulesByName[moduleDefinition.name] = moduleDefinition;
			}
		}, (error) => {
			console.log('Failed to load modules', error);
		});

		// pusher
		this.poiId = this.$stateParams.id;
		this.userId = $localStorage.user.id;
		this.poiIsLockedByAnotherUser = PusherService.poiLockedByAnotherUser(this.poiId, this.userId);

		this.selectedLanguage = this.$localStorage.poi[this.poiId].changes.fallback_language;

		if (!this.$localStorage.clipBoard) {

			this.$localStorage.clipBoard = {
				item_type: ''
			};

		}

		this.hintHttp = false;
		this.checkAllGroupTimeHints();

	}

	$onInit() {

		this.unsubscribe = [];

		this.unsubscribe.push(this.$rootScope.$on('PoiHasChanged', (event, data) => {

			if (data) {

				if (data.deletedLanguage == this.selectedLanguage) {

					this.selectedLanguage = this.$localStorage.poi[this.poiId].changes.fallback_language;

				}

			}

			this.checkAllGroupTimeHints();

		}));

		this.unsubscribe.push(this.$rootScope.$on('poiIsLockedByAnotherUser', (event, isLocked) => {
			this.poiIsLockedByAnotherUser = isLocked;
		}));

		this.checkClipboard();

	}

	checkClipboard() {

		var item_type = this.$localStorage.clipBoard.item_type;

		switch (item_type) {
			case 'group':
				this.noGroupInClipBoard = false;
				this.noModuleInClipBoard = true;
				break;

			case 'module':
				this.noGroupInClipBoard = true;
				this.noModuleInClipBoard = false;
				break;

			default:
				this.noGroupInClipBoard = true;
				this.noModuleInClipBoard = true;
				break;
		}

	}

	openPreview() {

		this.DialogService.fromTemplate('', 'poi-preview', {
			controller: PoiPreviewController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			locals: {
				language: this.selectedLanguage
			}
		}).then(() => {

			alert('closed');

		});

	}

	checkUrl(contentIndex) {

		let regEx = /^https:\/\//g;
		var text = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].url;

		if (text) {

			if (!regEx.exec(text)) {
				this.hintHttp = true;
			} else {
				this.hintHttp = false;
			}

		}

	}

	contentChanged() {

		this.$rootScope.$emit('PoiHasChanged');

	}

	editMeta(poiId, contentIndex) {

		this.DialogService.fromTemplate('', 'poi-meta', {
			controller: PoiMetaController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			locals: {
				poiId: poiId,
				contentIndex: contentIndex
			}
		}).then(() => {

			this.$rootScope.$emit('PoiHasChanged');

		});

	}

	urlPreview(url) {

		window.open(url, '_blank');

	}

	createGroupAndPasteModule(position, contentIndex, groupIndex, moduleIndex) {

		this.addGroup(contentIndex).then(() => {

			console.info(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups);

			this.pasteModule(position, contentIndex, groupIndex, moduleIndex);

		});

	}

	addGroup(contentIndex) {

		return this.$q((resolve) => {

			this.$q.resolve(
				this.$translate('POIS.NEW_GROUP')
			).then((translation) => {
				var now = moment().unix();

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups.push({
					'modules': [],
					'label': translation,
					'time_from': moment().unix(),
					'weekdays_active': false,
					'weekdays': {
						'mon': {
							'active': false,
							'times': []
						},
						'tue': {
							'active': false,
							'times': []
						},
						'wed': {
							'active': false,
							'times': []
						},
						'thu': {
							'active': false,
							'times': []
						},
						'fri': {
							'active': false,
							'times': []
						},
						'sat': {
							'active': false,
							'times': []
						},
						'sun': {
							'active': false,
							'times': []
						}
					},
					'css': {}
				});

				this.$rootScope.$emit('PoiHasChanged');

				resolve();

			});

		});

	}

	editGroup(contentIndex, groupIndex) {

		let group = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex]);

		this.DialogService.fromTemplate('', 'group-edit', {
			controller: GroupEditController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			locals: {
				group: group,
				poiIsLockedByAnotherUser: this.poiIsLockedByAnotherUser
			}
		}).then((changes) => {

			if (changes) {

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex] = angular.copy(changes);

				this.$rootScope.$emit('PoiHasChanged');

			} else {

				this.$q.resolve(
					this.$translate(['MODULES.DIALOG_DELETE_GROUP_HEADING', 'MODULES.DIALOG_DELETE_GROUP_BODY', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
				).then((translations) => {

					this.DialogService.confirm(translations['MODULES.DIALOG_DELETE_GROUP_HEADING'], translations['MODULES.DIALOG_DELETE_GROUP_BODY'], {
						skipHide: true
					}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

						this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups.splice(groupIndex, 1);

					});

				}, (reject) => {
					console.info('Strings missing.');
					console.info(reject);
				});

				this.$rootScope.$emit('PoiHasChanged');

			}

		});

	}

	copyGroup(contentIndex, groupIndex) {

		var item = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex]);

		for (var key in item.modules) {

			delete item.modules[key].data.uuid;

		}

		this.$localStorage.clipBoard = {
			item_type: 'group',
			item: item
		}

		this.checkClipboard();

		this.TranslatedToastService.show('TOASTS.GROUP_COPIED_TO_CLIPBOARD');

	}

	pasteGroup(position, contentIndex, groupIndex) {

		if (this.$localStorage.clipBoard.item_type == 'group') {

			var groups = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups);

			if (position == 'before') {

				var index = groupIndex;

			} else {

				var index = groupIndex + 1;

			}

			groups.splice(index, 0, this.$localStorage.clipBoard.item);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups = angular.copy(groups);

			groups = null;

			this.$rootScope.$emit('PoiHasChanged');

		}

	}

	showGroup(contentIndex, groupIndex) {

		delete this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].hidden;

		this.$rootScope.$emit('PoiHasChanged');

	}

	hideGroup(contentIndex, groupIndex) {

		this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].hidden = true;

		this.$rootScope.$emit('PoiHasChanged');

	}

	deleteGroup(contentIndex, groupIndex) {

		this.$q.all([
			this.$translate('MODULES.DIALOG_DELETE_GROUP_HEADING'),
			this.$translate('MODULES.DIALOG_DELETE_GROUP_BODY'),
			this.$translate('COMMON.DIALOG_CONFIRM'),
			this.$translate('COMMON.DIALOG_REJECT')
		]).then((i18n) => {

			this.DialogService.confirm(i18n[0], i18n[1], {
				skipHide: true
			}, i18n[2], i18n[3]).then(() => {

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups.splice(groupIndex, 1);

				this.$rootScope.$emit('PoiHasChanged');
				this.$rootScope.$emit('RevalidatePoi');

				this.DialogService.hide();

			});

		}, (reject) => {
			console.info('Strings missing.');
			console.info(reject);
		});

	}

	openGroupContextMenu($mdOpenMenu, ev) {

		this.checkClipboard();

		$mdOpenMenu(ev);

	}

	addModule(contentIndex, groupIndex) {

		this.DialogService.fromTemplate('', 'module-selection', {
			controller: ModuleSelectionController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true
		}).then((moduleType) => {

			let moduleDefinition = this.modulesByName[moduleType];

			let newModule = angular.copy(moduleDefinition);

			this.$localStorage.poi[this.poiId].changes['has_' + newModule.type] = true;

			let index = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.push(newModule) - 1;

			this.$rootScope.$emit('PoiHasChanged');
			this.$localStorage.invalideModule = false;

			this.ModuleValidationService.validate(newModule).then((validatedModule) => {

				if (this.$localStorage.invalideModule == false) {
					this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[index] = validatedModule;
					this.$rootScope.$emit('PoiHasChanged');
				}

			}, () => {
				console.error('Failed to validate module!');
			});

		});

	}

	editModule(contentIndex, groupIndex, moduleIndex) {

		this.$localStorage.invalideModule = true;

		this.ModuleEditService.openDialog(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex], this.poiIsLockedByAnotherUser).then((module) => {

			if (module) {

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex] = module;
				this.$rootScope.$emit('PoiHasChanged');
				this.$localStorage.invalideModule = false;

				this.ModuleValidationService.validate(module).then((validatedModule) => {

					if (this.$localStorage.invalideModule == false) {
						this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex] = validatedModule;
						this.$rootScope.$emit('PoiHasChanged');
					}

				}, () => {
					console.error('Failed to validate module!');
				});

			} else {

				this.$q.all([
					this.$translate('MODULES.DIALOG_DELETE_MODULE_HEADING'),
					this.$translate('MODULES.DIALOG_DELETE_MODULE_BODY'),
					this.$translate('COMMON.DIALOG_CONFIRM'),
					this.$translate('COMMON.DIALOG_REJECT')
				]).then((i18n) => {

					this.DialogService.confirm(i18n[0], i18n[1], {
						skipHide: true
					}, i18n[2], i18n[3]).then(() => {

						this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.splice(moduleIndex, 1);
						this.$rootScope.$emit('PoiHasChanged');

					});

				}, (reject) => {
					console.info('Strings missing.');
					console.info(reject);
				});

			}

		});

	}

	copyModule(contentIndex, groupIndex, moduleIndex) {

		var item = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex]);

		delete item.data.uuid;

		this.$localStorage.clipBoard = {
			item_type: 'module',
			item: item
		}

		this.checkClipboard();

		this.TranslatedToastService.show('MODULES.MODULE_COPIED');

	}

	pasteModule(position, contentIndex, groupIndex, moduleIndex) {

		if (this.$localStorage.clipBoard.item_type == 'module') {

			var modules = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules);

			if (position == 'before') {

				var index = moduleIndex;

			} else {

				var index = moduleIndex + 1;

			}

			modules.splice(index, 0, this.$localStorage.clipBoard.item);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules = angular.copy(modules);

			modules = null;

			this.$rootScope.$emit('PoiHasChanged');

		}

	}

	showModule(contentIndex, groupIndex, moduleIndex) {

		delete this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex].hidden;

		this.$rootScope.$emit('PoiHasChanged');

	}

	hideModule(contentIndex, groupIndex, moduleIndex) {

		this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex].hidden = true;

		this.$rootScope.$emit('PoiHasChanged');

	}

	deleteModule(contentIndex, groupIndex, moduleIndex) {

		this.$q.all([
			this.$translate('MODULES.DIALOG_DELETE_MODULE_HEADING'),
			this.$translate('MODULES.DIALOG_DELETE_MODULE_BODY'),
			this.$translate('COMMON.DIALOG_CONFIRM'),
			this.$translate('COMMON.DIALOG_REJECT')
		]).then((i18n) => {

			this.DialogService.confirm(i18n[0], i18n[1], {
				skipHide: true
			}, i18n[2], i18n[3]).then(() => {
				var moduleType = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex].type;
				var noOfModuleType = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.filter(module => module.type === moduleType).length;
				if (noOfModuleType <= 1)
					this.$localStorage.poi[this.poiId].changes['has_' + moduleType] = false;

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.splice(moduleIndex, 1);
				this.$rootScope.$emit('PoiHasChanged');
				this.DialogService.hide();

			});

		}, (reject) => {
			console.info('Strings missing.');
			console.info(reject);
		});

	}

	openModuleContextMenu($mdOpenMenu, ev) {

		this.checkClipboard();

		$mdOpenMenu(ev);

	}

	movedItem() {

		this.$rootScope.$emit('PoiHasChanged');

	}

	moveModuleUp(contentIndex, groupIndex, moduleIndex) {

		if (moduleIndex > 0) {

			let x = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex - 1]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex - 1] = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex] = angular.copy(x);

		} else {

			if (groupIndex > 0) {

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex - 1].modules.push(angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex]));

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.splice(moduleIndex, 1);

			}

		}

		this.$rootScope.$emit('PoiHasChanged');

	}

	moveModuleDown(contentIndex, groupIndex, moduleIndex) {

		let moduleCount = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.length;

		if (moduleIndex < moduleCount - 1) {

			let x = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex + 1]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex + 1] = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex] = angular.copy(x);

		} else {

			let groupCount = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups.length;

			if (groupIndex < groupCount - 1) {

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex + 1].modules.unshift(angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules[moduleIndex]));

				this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex].modules.splice(moduleIndex, 1);

			}

		}

	}

	moveGroupUp(contentIndex, groupIndex) {

		if (groupIndex > 0) {

			let x = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex - 1]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex - 1] = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex] = angular.copy(x);

		}

	}

	moveGroupDown(contentIndex, groupIndex) {

		let groupCount = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups.length;

		if (groupIndex < groupCount - 1) {

			let x = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex + 1]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex + 1] = angular.copy(this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex]);

			this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex] = angular.copy(x);

		}

	}

	checkAllGroupTimeHints() {

		for (let contentIndex in this.$localStorage.poi[this.poiId].changes.poi_contents) {
			for (let groupIndex in this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups) {
				this.checkGroupTimeHint(contentIndex, groupIndex);
			}
		}

	}

	checkGroupTimeHint(contentIndex, groupIndex) {

		let group = this.$localStorage.poi[this.poiId].changes.poi_contents[contentIndex].data.groups[groupIndex];

		let currentTime = new Date().getTime();

		let timeFrom = group.time_from * 1000;
		let startTimeDifference = timeFrom - currentTime;
		let started = startTimeDifference < 0;

		let ended = false;
		if (group.time_to) {
			let timeTo = group.time_to * 1000;
			let endTimeDifference = timeTo - currentTime;
			ended = endTimeDifference < 0;
		}


		let hasTimes = false;
		if (group.weekdays_active) {
			for (let weekday in group.weekdays) {
				let weekday = group.weekdays[weekday];
				hasTimes |= weekday.active;
			}
		}

		let showTimeHint = !started || ended || hasTimes;

		group.ignoreInPOIChange = {
			visibilityNotStarted: !started,
			visibilityExpired: ended,
			visibilityHasTimes: hasTimes,
			showTimeHint: showTimeHint
		}

	}

	generateNotification(poiContent) {

		this.notificationLoading = true;

		this.GenerateNotificationService.changeMeta(

			poiContent.url,
			poiContent.meta,

		).then((response) => {

			this.notificationLoading = false;

		}, (error) => {

			this.notificationLoading = false;

		});

	}

}

export const PoiContentComponent = {
	templateUrl: './views/app/components/pois/poi-content/poi-content.component.html',
	controller: PoiContentController,
	controllerAs: 'vm',
	bindings: {}
}
