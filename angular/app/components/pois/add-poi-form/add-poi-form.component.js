class AddPoiFormController{

	constructor(LanguageNamesService, API, $localStorage, $state, PoiService){

		'ngInject';

		this.LanguageNamesService = LanguageNamesService;
		this.API = API;
		this.$localStorage = $localStorage;
		this.$state = $state;
		this.PoiService = PoiService;

		this.languages = this.LanguageNamesService.getLanguages();

		this.poi = {
			name: '',
			fallback_language: this.$localStorage.tenant.fallback_language,
			description: '',
			has_url: false,
			has_channel: false,
			poi_contents: []
		}

		this.submitted = false;

	}

	$onInit(){

	}

	addPoi(){

		this.submitted = true;

		this.poi.poi_contents.push({
			"language": this.poi.fallback_language,
			"data": {
				"groups": []
			},
			"active": true,
			"has_url": false,
			"meta": {
				"title": this.poi.name
			}
		});

		this.PoiService.addPoi(this.poi).then((poi) => {

			console.info(poi);

			this.$state.transitionTo('authorized.poi', {id:poi.id});

		}, (fail) => {

			console.error('Creating the POI failed', fail);
			this.submitted = false;

		});

	}

}

export const AddPoiFormComponent = {
	templateUrl: './views/app/components/pois/add-poi-form/add-poi-form.component.html',
	controller: AddPoiFormController,
	controllerAs: 'vm',
	bindings: {}
}
