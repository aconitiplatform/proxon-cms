class PoiDesignDefaultValueController {

	constructor(){

		'ngInject';

	}

	$onInit(){

	}

}

export const PoiDesignDefaultValueComponent = {
	templateUrl: './views/app/components/pois/poi-design-default-value/poi-design-default-value.component.html',
	controller: PoiDesignDefaultValueController,
	controllerAs: 'vm',
	bindings: {
		defaultValue: '<',
		valueType: '<'
	}
};
