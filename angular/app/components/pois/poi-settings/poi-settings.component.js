class PoiSettingsController{

	constructor(API, PoiService, $stateParams, $state, $mdDialog, $rootScope, $localStorage, LanguageNamesService, DialogService, $translate, $q, TenantService){

		'ngInject';

		this.API = API;
		this.PoiService = PoiService;
		this.$stateParams = $stateParams;
		this.$state = $state;
		this.$mdDialog = $mdDialog;
		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.LanguageNamesService = LanguageNamesService;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.$q = $q;
		this.TenantService = TenantService;

		this.languages = this.LanguageNamesService.getLanguages();

		this.poiId = this.$stateParams.id;

		this.originatorEv;

		this.TenantService.getTenant().then((tenant) => {
			this.tenant = tenant;
		});

	}

	$onInit(){

	}

	loadAvailableLanguages() {

		var languages = angular.copy(this.languages);

		for(let content of this.$localStorage.poi[this.poiId].changes.poi_contents){

			delete languages[content.language];

		}

		this.availableLanguages = languages;

	}

	addLanguage(language){

		this.$localStorage.poi[this.poiId].changes.poi_contents.push({
			"language": language,
			"data": {
				"groups": []
			},
			"active": false,
			"has_url": false,
			"meta": {
				"title": this.$localStorage.poi[this.poiId].changes.name
			}
		});

		this.$rootScope.$broadcast('PoiHasChanged');

	}

	makeDefaultLanguage(newDefaultLanguage){

		this.$localStorage.poi[this.poiId].changes.fallback_language = newDefaultLanguage;

		this.$rootScope.$broadcast('PoiHasChanged');

	}

	removeLanguage(languageToRemove){

		this.$q.resolve(
			this.$translate(['POIS.DIALOG_LANGUAGE_HEADING', 'POIS.DIALOG_LANGUAGE_BODY', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
		).then((translations) => {

			this.DialogService.confirm(translations['POIS.DIALOG_LANGUAGE_HEADING'], translations['POIS.DIALOG_LANGUAGE_BODY'], null, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

				for(let i in this.$localStorage.poi[this.poiId].changes.poi_contents){

					if(this.$localStorage.poi[this.poiId].changes.poi_contents[i].language == languageToRemove){

						let poiId = this.$localStorage.poi[this.poiId].changes.id;
						let contentId = this.$localStorage.poi[this.poiId].changes.poi_contents[i].id;

						let wasUnsavedLanguage = !contentId;

						let data = {
							deletedLanguage: languageToRemove,
							wasUnsavedLanguage: wasUnsavedLanguage
						}

						if(wasUnsavedLanguage) {

							this.$localStorage.poi[this.poiId].changes.poi_contents.splice(i, 1);


						} else {

							this.$localStorage.poi[this.poiId].changes.poi_contents[i].action = 'delete';

						}

						this.$rootScope.$broadcast('PoiHasChanged', data);

						break;

					}

				}

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

	settingsChanged(){

		this.$rootScope.$broadcast('PoiHasChanged');

	}

	deletePoi(){

		this.$q.resolve(
			this.$translate(['POIS.DIALOG_DELETE_POI_HEADING', 'POIS.DIALOG_DELETE_POI_BODY', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
		).then((translations) => {

			var confirm = this.$mdDialog.confirm()
				.title(translations['POIS.DIALOG_DELETE_POI_HEADING'])
				.textContent(translations['POIS.DIALOG_DELETE_POI_BODY'])
				.ariaLabel(translations['POIS.DIALOG_DELETE_POI_BODY'])
				.ok(translations['COMMON.DIALOG_CONFIRM'])
				.cancel(translations['COMMON.DIALOG_REJECT']);

			this.$mdDialog.show(confirm).then(() => {

				this.PoiService.deletePoi(this.poiId).then(() => {

					this.$state.transitionTo('authorized.pois');

				});

			}, () => {

				// do nothing

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

	openLanguageSelection($mdOpenMenu, ev) {

		this.loadAvailableLanguages();
		this.originatorEv = ev;
		$mdOpenMenu(ev);

	}

	cancelLanguageSelection(){

	}

}

export const PoiSettingsComponent = {
	templateUrl: './views/app/components/pois/poi-settings/poi-settings.component.html',
	controller: PoiSettingsController,
	controllerAs: 'vm',
	bindings: {}
}
