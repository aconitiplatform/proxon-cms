import {MediaUploadController} from '../../../../dialogs/media-upload/media-upload.dialog';

class PoiDesignController {

	constructor($stateParams, $rootScope, $localStorage, $q, $translate, DesignService, $mdDialog, DialogService, CommonService){

		'ngInject';

		this.$stateParams = $stateParams;
		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.$q = $q;
		this.$translate = $translate;
		this.DesignService = DesignService;
		this.$mdDialog = $mdDialog;
		this.DialogService = DialogService;
		this.CommonService = CommonService;

		this.poiId = this.$stateParams.id;

	}

	$onInit(){

	}

	designChanged(){

		this.$rootScope.$emit('PoiHasChanged');

	}

	resetDesign(){

		this.$q.resolve(
			this.$translate(['POIS.RESET_CONFIRMATION.TITLE', 'POIS.RESET_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
		).then((translations) => {

			var confirm = this.$mdDialog.confirm()
				.title(translations['POIS.RESET_CONFIRMATION.TITLE'])
				.textContent(translations['POIS.RESET_CONFIRMATION.TEXT'])
				.ariaLabel(translations['POIS.RESET_CONFIRMATION.TEXT'])
				.ok(translations['COMMON.DIALOG_CONFIRM'])
				.cancel(translations['COMMON.DIALOG_REJECT']);

			this.$mdDialog.show(confirm).then(() => {

				this.DesignService.getPoiDesignBlueprint().then((design) => {

					this.$localStorage.poi[this.poiId].changes.poi_design = design;
					this.designChanged();

				});

			}, () => {

				// do nothing

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

	onSelect(image) {

		if (image == 'header_logo') {
			this.$localStorage.poi[this.poiId].changes.poi_design.header_logo_settings.type = 'id';
		} else if (image == 'footer_logo') {
			this.$localStorage.poi[this.poiId].changes.poi_design.footer_logo_settings.type = 'id';
		}

		this.designChanged();

	}

	clear(image) {

		if (image == 'header_logo') {

			delete this.$localStorage.poi[this.poiId].changes.poi_design.header_logo_settings.id;
			this.$localStorage.poi[this.poiId].changes.poi_design.header_logo_settings.type = 'default';

		} else if(image == 'footer_logo') {

			delete this.$localStorage.poi[this.poiId].changes.poi_design.footer_logo_settings.id;
			this.$localStorage.poi[this.poiId].changes.poi_design.footer_logo_settings.type = 'default';

		}

		this.designChanged();

	}

}

export const PoiDesignComponent = {
	templateUrl: './views/app/components/pois/poi-design/poi-design.component.html',
	controller: PoiDesignController,
	controllerAs: 'vm',
	bindings: {}
};
