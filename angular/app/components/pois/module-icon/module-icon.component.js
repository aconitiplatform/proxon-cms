class ModuleIconController{

	constructor(){

		'ngInject';

	}

	$onInit(){

	}

}

export const ModuleIconComponent = {
	templateUrl: './views/app/components/pois/module-icon/module-icon.component.html',
	controller: ModuleIconController,
	controllerAs: 'vm',
	bindings: {
		icon: '<'
	}
}
