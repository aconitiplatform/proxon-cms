class PoiTouchpointsController{

	constructor($rootScope, TouchpointService, $localStorage, $stateParams, EventUnsubscribeService, PaginationService, GlobalConstants){

		'ngInject';

		this.$rootScope = $rootScope;
		this.TouchpointService = TouchpointService;
		this.$localStorage = $localStorage
		this.$stateParams = $stateParams;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.PaginationService = PaginationService;
		this.GlobalConstants = GlobalConstants;

		this.poiId = this.$stateParams.id;

		this.EventUnsubscribeService.registerUnsubscribe(this);

	}

	$onInit(){

		this.PaginationService.checkLastModified('beacons');

		this.$localStorage.poiBeaconChanges = {};

		this.unsubscribe = this.$rootScope.$on('POINavChanged', (event, navItem) => {

			if(navItem == 'touchpoints' && !this.touchpoints){

				this.getTouchpoints();

			}

		});

	}

	getTouchpoints(){

		this.PaginationService.getList('beacons').then((touchpoints) => {

			for (let b1 of this.$localStorage.poi[this.poiId].changes.beacons) {

				for (let i = 0; i < touchpoints.length; i++) {

					let b2 = touchpoints[i];

					if (b1.id == b2.id) {
						touchpoints.splice(i, 1);
						break;
					}

				}

			}

			this.touchpoints = touchpoints;

		});

	}

	addTouchpoint(touchpoint){

		this.touchpoints.splice(this.touchpoints.indexOf(touchpoint),1);

		let add = true;
		for (let b of this.$localStorage.poi[this.poiId].changes.beacons) {
			if (b.id == touchpoint.id) {
				add = false;
				break;
			}
		}

		if (add) {
			this.$localStorage.poi[this.poiId].changes.beacons.push(touchpoint);
		}

		this.$localStorage.poiBeaconChanges[touchpoint.id] = 'add';

		this.$rootScope.$emit('PoiHasChanged');

	}

	removeTouchpoint(touchpoint){


		this.$localStorage.poi[this.poiId].changes.beacons.splice(this.$localStorage.poi[this.poiId].changes.beacons.indexOf(touchpoint),1);

		let add = true;
		for (let b of this.touchpoints) {
			if (b.id == touchpoint.id) {
				add = false;
				break;
			}
		}

		if (add) {
			this.touchpoints.push(touchpoint);
		}

		this.$localStorage.poiBeaconChanges[touchpoint.id] = 'remove';

		this.$rootScope.$emit('PoiHasChanged');

	}

	resetSearch(){

		this.filter = '';

	}

	getDisplayName(touchpoint) {
		let ret = touchpoint.name;

		if (touchpoint.alias) {
			ret = touchpoint.alias + ' (' + touchpoint.name + ')';
		}

		return ret;
	}

}

export const PoiTouchpointsComponent = {
	templateUrl: './views/app/components/pois/poi-touchpoints/poi-touchpoints.component.html',
	controller: PoiTouchpointsController,
	controllerAs: 'vm',
	bindings: {}
}
