class LineChartController{
	constructor(CommonService, ChartsService, $q, $translate, $rootScope, EventUnsubscribeService){
		'ngInject';

		this.CommonService = CommonService;
		this.ChartsService = ChartsService;
		this.$q = $q;
		this.$translate = $translate;
		this.$rootScope = $rootScope;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.chartDrawing = false;
		this.showChart = false;

		this.labels = [];

		if(!this.time) {
			let d = new Date();
			this.time = d.getTime();
			this.time = Math.round(this.time/1000);
		}

		if(!this.days) {
			this.days = 7;
		}

		this.translationValues = {};
		this.urlParams = {};

	}

	$onInit(){

		if(this.type == 'four-weeks-delivered-content' || this.type == 'seven-day-delivered-content' || this.type == 'four-weeks-scanned-beacons' || this.type == 'four-weeks-used-channels') {
			this.chartDrawing = true;
			this.drawChart();
		}

		this.unsubscribe = [];

		this.unsubscribe.push(this.$rootScope.$on('chart-url-access-shall-change', (event, data) => {

			if(this.type == 'four-weeks-url-access' || this.type == 'seven-day-url-access') {

				this.translationValues.name = data.name;
				this.urlParams.url = data.url;
				this.showChart = false;
				this.chartDrawing = true;
				this.drawChart();

			}

		}));

		this.unsubscribe.push(this.$rootScope.$on('chart-poi-shall-change', (event, data) => {

			if(this.type == 'four-weeks-poi-access' || this.type == 'four-weeks-poi-scans' || this.type == 'four-weeks-poi-channels') {

				this.translationValues.name = data.name;
				this.urlParams.poi = data.id;
				this.showChart = false;
				this.chartDrawing = true;
				this.drawChart();

			}

		}));

	}

	drawChart() {

		this.$q.all(this.ChartsService.getTranslationPromises(this.type,this.days,this.translationValues)).then((i18n) => {

			this.title = i18n[0];

			this.ChartsService.getChartDataByType(this.time,this.type,this.urlParams).then((resolve) => {

				this.CommonService.getWeekDayMapping().then((weekdays) => {

					let i = 0;

					for(let label in resolve.labels) {


						if(resolve.labels.length - 2 == i) {
							this.labels[label] = weekdays.dby;
						} else if (resolve.labels.length - 1 == i) {
							this.labels[label] = weekdays.y;
						} else {
							this.labels[label] = weekdays[resolve.labels[label]];
						}

						i++;

					}
					this.data = resolve.series;
					this.options = this.ChartsService.getChartConfiguration(this.title);
					this.datasetOverride = [
						{
							'borderColor':'#56abbf',
							'label':i18n[1],
							'borderWidth':3,
							'type':'line',
							'fill': false,
							'backgroundColor': '#FFFFFF',
							'pointBorderColor': '#56abbf',
							'pointBackgroundColor': '#FFFFFF',
							'pointBorderWidth': 3,
							'pointRadius': 10,
							'pointHoverRadius': 10,
							'pointHoverBorderWidth': 3,
							'pointHoverBorderColor': '#56abbf',
							'pointHoverBackgroundColor': '#FFFFFF',
							'pointHitRadius': 20,
							'spanGaps': true
						},
						{
							'borderColor':'#C79F8B',
							'label':i18n[2],
							'borderWidth':1,
							'type':'line',
							'fill': false,
							'backgroundColor': '#FFFFFF',
							'pointBorderColor': '#C79F8B',
							'pointBackgroundColor': '#FFFFFF',
							'pointBorderWidth': 1,
							'pointRadius': 8,
							'pointHoverRadius': 8,
							'pointHoverBorderWidth': 1,
							'pointHoverBorderColor': '#C79F8B',
							'pointHoverBackgroundColor': '#FFFFFF',
							'pointHitRadius': 20,
							'spanGaps': true
						}
					];
					this.chartDrawing = false;
					this.showChart = true;

				});

			},(reject)=> {
				console.log("Could not load chart.")
			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

}

export const LineChartComponent = {
	templateUrl: './views/app/components/charts/line-chart/line-chart.component.html',
	controller: LineChartController,
	controllerAs: 'vm',
	bindings: {
		time: '<',
		type: '<',
		days: '<'
	}
}
