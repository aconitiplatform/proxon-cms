class AppsDetailController{
	constructor(AppService, $state, $stateParams){

		'ngInject';

		this.AppService = AppService;
		this.$state = $state;
		this.$stateParams = $stateParams;

		this.$state.current.title = 'App';

		this.app = {};

		this.appLoaded = false;

	}

	$onInit(){

		this.getApp();

	}

	getApp() {

		this.AppService.getApp(this.$stateParams.id).then((app) => {

			this.$state.current.title = app.fullname;

			this.app = app;

			this.appLoaded = true;

		}, (notApp) => {

			this.$state.transitionTo('public.notfound');

		});

	}
}

export const AppsDetailComponent = {
	templateUrl: './views/app/components/admin/apps/apps-detail/apps-detail.component.html',
	controller: AppsDetailController,
	controllerAs: 'vm',
	bindings: {
		isProfile: '<'
	}
}
