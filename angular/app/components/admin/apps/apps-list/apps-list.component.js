class AppsListController {

	constructor($scope, $state, API, AppsService, $localStorage){

		'ngInject';

		this.$scope = $scope;
		this.$state = $state;
		this.API = API;
		this.AppsService = AppsService;
		this.$localStorage = $localStorage;

		this.propertyName = 'appname';
		this.reverse = false;

	}

	$onInit(){

		if(!this.$localStorage.AppsSort){

			this.$localStorage.AppsSort = {
				propertyName: 'appname',
				reverse: false
			}

		}

	}

	sortBy(propertyName) {

		this.$localStorage.AppsSort.reverse = (this.$localStorage.AppsSort.propertyName === propertyName) ? !this.$localStorage.AppsSort.reverse : true;
		this.$localStorage.AppsSort.propertyName = propertyName;

	}

}

export const AppsListComponent = {
	templateUrl: './views/app/components/admin/apps/apps-list/apps-list.component.html',
	controller: AppsListController,
	controllerAs: 'vm',
	bindings: {}
}
