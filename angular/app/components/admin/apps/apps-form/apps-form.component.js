class AppsFormController{

	constructor(LanguageNamesService, $localStorage, AppService, $state, DialogService, $translate, $q, TranslatedToastService){

		'ngInject';

		this.LanguageNamesService = LanguageNamesService;
		this.$localStorage = $localStorage;
		this.AppService = AppService;
		this.$state = $state;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.$q = $q;
		this.TranslatedToastService = TranslatedToastService;

		this.languages = this.LanguageNamesService.getLanguages();

		this.isNewApp = false;

		if(!this.app) {

			this.isNewApp = !this.isNewApp;

			this.app = {
				appname: '',
				language: this.$localStorage.tenant.fallback_language
			}

		}

	}

	$onInit(){

	}

	saveApp() {

		if(this.isNewApp) {

			this.AppService.addApp(this.app).then((app) => {

				this.TranslatedToastService.show('TOASTS.APP_CREATED');

				this.$state.transitionTo('authorized.appDetail', { id: app.id });

			}, (notApp) => {
				this.TranslatedToastService.show('TOASTS.APP_CREATE_ERROR');
			});

		} else {

			this.AppService.updateApp(this.app).then((app) => {

				this.TranslatedToastService.show('TOASTS.APP_UPDATED');

				this.app = app;

			}, (notApp) => {
				this.TranslatedToastService.show('TOASTS.APP_UPDATE_ERROR');
			});

		}

	}

	deleteApp(event) {

		this.$q.all([
			this.$translate('APPS.CONFIRM_DELETE_HEADING', { appname: this.app.appname }),
			this.$translate('APPS.CONFIRM_DELETE_BODY')
		]).then((i18n) => {

			let confirm = this.DialogService.confirm(i18n[0],i18n[1],null);

			confirm.then(() => {

				this.AppService.deleteApp(this.app).then((resolve) => {

					this.TranslatedToastService.show('TOASTS.APP_DELETED', { appname: this.app.appname });
					this.$state.transitionTo('authorized.apps');

				}, () => {

					this.TranslatedToastService.error('TOASTS.APP_DELETE_ERROR', { appname: this.app.appname });

				});

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

}

export const AppsFormComponent = {
	templateUrl: './views/app/components/admin/apps/apps-form/apps-form.component.html',
	controller: AppsFormController,
	controllerAs: 'vm',
	bindings: {
		app: '<'
	}
}
