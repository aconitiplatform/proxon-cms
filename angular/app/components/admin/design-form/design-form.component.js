
class DesignFormController{

	constructor(){

		'ngInject';

		if (!this.type) {
			this.type = 'padding';
		}

		if (!this.orientation) {
			this.orientation = 'top,right,bottom,left';
			if (this.type == 'margin') {
				this.orientation = 'top,bottom';
			}
		}

		if (!Array.isArray(this.orientation)) {
			this.orientations = this.orientation.split(/\W+/g);
		}

	}

	$onInit(){

	}

	shouldDisplay(orientation) {
		return this.orientations.indexOf(orientation) >= 0;
	}

}

export const DesignFormComponent = {
	templateUrl: './views/app/components/admin/design-form/design-form.component.html',
	controller: DesignFormController,
	controllerAs: 'vm',
	bindings: {
		type: '<',
		orientation: '@',
		module: '<'
	}
};
