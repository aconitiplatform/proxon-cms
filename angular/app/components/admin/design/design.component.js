import {FontSelectController} from '../../../../dialogs/font-select/font-select.dialog';

class DesignController{

	constructor($localStorage, $rootScope, DesignService, DialogService, CommonService, FontService, $q, $translate, $timeout){

		'ngInject';

		this.$localStorage = $localStorage;
		this.$rootScope = $rootScope;
		this.DesignService = DesignService;
		this.DialogService = DialogService;
		this.CommonService = CommonService;
		this.FontService = FontService;
		this.$q = $q;
		this.$translate = $translate;
		this.$timeout = $timeout;

		this.currentNavItem = 'general';
		this.FontService.loadFonts();

	}

	$onInit(){

		this.loadDesign();

	}

	ensureValidState() {

		if (!this.$localStorage.designChanges.fonts) {
			this.$localStorage.designChanges.fonts = [];
		}

		if (!Array.isArray(this.$localStorage.designChanges.fonts)) {
			this.$localStorage.designChanges.fonts = [];
		}

	}

	prepareFonts() {

		if (this.$localStorage.designChanges.fonts) {

			for (let i = 0; i < this.$localStorage.designChanges.fonts.length; i ++) {
				let font = this.$localStorage.designChanges.fonts[i];
				this.FontService.appendFontByName(font.family);
			}

		}

	}

	loadDesign(){

		this.DesignService.getDesign().then((design) => {


			this.designLoaded = true;

			if (this.$localStorage.designChanges.header_logo_settings.type == 'id') {

				this.headerLogoPreviewUrl = this.CommonService.getMediaURL(this.$localStorage.designChanges.header_logo_settings.id);

			}

			if (this.$localStorage.designChanges.footer_logo_settings.type == 'id') {

				this.footerLogoPreviewUrl = this.CommonService.getMediaURL(this.$localStorage.designChanges.footer_logo_settings.id);

			}

			this.ensureValidState();
			this.prepareFonts();

		});

	}

	designChanged(){

		this.$timeout().then(() => {
			this.$rootScope.$emit('DesignHasChanged');
		});

	}

	selectImage(image){

		if(image == 'header_logo'){
			this.$localStorage.designChanges.header_logo_settings.type = 'id';
		}

		if(image == 'footer_logo'){
			this.$localStorage.designChanges.footer_logo_settings.type = 'id';
		}

		this.designChanged();

	}

	clear(image) {

		if(image == 'header_logo'){

			this.$localStorage.designChanges.header_logo_settings.type = 'none';
			this.$localStorage.designChanges.header_logo_settings.id = undefined;

			this.headerLogoPreviewUrl = undefined;

		}

		if(image == 'footer_logo'){

			this.$localStorage.designChanges.footer_logo_settings.type = 'none';
			this.$localStorage.designChanges.footer_logo_settings.id = undefined;

			this.footerLogoPreviewUrl = undefined;

		}

		this.designChanged();

	}

	openFontSelection() {

		let forbidden = [];

		for (let i = 0; i < this.$localStorage.designChanges.fonts.length; i ++) {

			let font = this.$localStorage.designChanges.fonts[i];
			forbidden.push(font.family);

		}

		this.DialogService.fromTemplate('', 'font-select', {
			controller: FontSelectController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				selected: null,
				forbidden: forbidden
			}
		}).then((selected) => {

			this.ensureValidState();

			let uriName = encodeURIComponent(selected);

			this.$localStorage.designChanges.fonts.push({
				family: selected,
				import: 'https://fonts.googleapis.com/css?family=' + uriName,
				standard: false
			});

			this.designChanged();

		});

	}

	removeFont(font) {

		this.$q.resolve(
			this.$translate(['ADMIN.DESIGN.REMOVE_CONFIRMATION.TITLE', 'ADMIN.DESIGN.REMOVE_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
		).then((translations) => {

			this.DialogService.confirm(translations['ADMIN.DESIGN.REMOVE_CONFIRMATION.TITLE'], translations['ADMIN.DESIGN.REMOVE_CONFIRMATION.TEXT'], {
				skipHide: true
			}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

				for (let i = 0; i < this.$localStorage.designChanges.fonts.length; i ++) {
					let testFont = this.$localStorage.designChanges.fonts[i];
					if (testFont.family == font.family) {
						this.$localStorage.designChanges.fonts.splice(i, 1);
						this.designChanged();
						return;
					}

				}

			});

		}, (reject) => {
			console.info(reject);
		});

	}

	makeDefaultFont(font) {

		for (let i = 0; i < this.$localStorage.designChanges.fonts.length; i ++) {
			let testFont = this.$localStorage.designChanges.fonts[i];
			if (testFont.family == font.family) {
				testFont.standard = true;
			} else {
				testFont.standard = false;
			}
		}

		this.designChanged();

	}

	removeDefaultFont(font) {

		for (let i = 0; i < this.$localStorage.designChanges.fonts.length; i ++) {
			let testFont = this.$localStorage.designChanges.fonts[i];
			if (testFont.family == font.family) {
				testFont.standard = false;
			}
		}

		this.designChanged();

	}

}

export const DesignComponent = {
	templateUrl: './views/app/components/admin/design/design.component.html',
	controller: DesignController,
	controllerAs: 'vm',
	bindings: {}
};
