
class FontSelectController{

	constructor(FontService, $localStorage, DesignService, $timeout){

		'ngInject';

		this.FontService = FontService;
		this.$localStorage = $localStorage;
		this.DesignService = DesignService;
		this.$timeout = $timeout;

		this.options = [];

		if (!this.ngModel) {
			this.ngModel = undefined;
		}

		this.loadDesign();

	}

	$onInit(){

	}

	loadDesign() {

		if (!this.$localStorage.design) {

			this.DesignService.getDesign().then((design) => {

				this.prepareOptions();

			});

		} else {

			this.prepareOptions();

		}

	}

	prepareOptions() {

		if (this.$localStorage.design.fonts) {

			for (let i = 0; i < this.$localStorage.design.fonts.length; i ++) {
				let font = this.$localStorage.design.fonts[i];
				this.FontService.appendFontByName(font.family);
				this.options.push(font.family);
			}

		}

		let modelOkay = false;

		for (let option of this.options) {

			if (this.ngModel == option) {
				modelOkay = true;
				break;
			}

		}

		if (!modelOkay) {
			this.ngModel = undefined;
		}

	}

	changed(){

		if (this.ngChange) {
			this.$timeout().then(() => {
				this.ngChange();
			});
		}

	}

}

export const FontSelectComponent = {
	templateUrl: './views/app/components/admin/font-select/font-select.component.html',
	controller: FontSelectController,
	controllerAs: 'vm',
	bindings: {
		ngModel: '=',
		ngChange: '&'
	}
};
