
class ButtonDesignController{

	constructor($timeout){

		'ngInject';

		this.$timeout = $timeout;

		if (!this.ngModel[this.field]) {
			this.ngModel[this.field] = {};
		}

	}

	changed(){

		if (this.ngChange) {
			this.$timeout().then(() => {
				this.ngChange();
			});
		}

	}

	$onInit(){

	}

}

export const ButtonDesignComponent = {
	templateUrl: './views/app/components/admin/button-design/button-design.component.html',
	controller: ButtonDesignController,
	controllerAs: 'vm',
	bindings: {
		ngModel: '=',
		ngChange: '&',
		field: '<'
	}
};
