class UsersDetailController{
	constructor(UserService, $state, $stateParams){

		'ngInject';

		this.UserService = UserService;
		this.$state = $state;
		this.$stateParams = $stateParams;

		this.$state.current.title = 'User';

		this.user = {};

		this.userLoaded = false;

	}

	$onInit(){

		if(this.isProfile) {
			this.user = this.UserService.getLoggedInUser();
			this.$state.current.title = this.user.fullname;
			this.userLoaded = true;
		} else {
			this.getUser();
		}

	}

	getUser() {

		this.UserService.getUser(this.$stateParams.id).then((user) => {

			this.$state.current.title = user.fullname;

			this.user = user;

			this.userLoaded = true;

		}, (notUser) => {

			this.$state.transitionTo('public.notfound');

		});

	}
}

export const UsersDetailComponent = {
	templateUrl: './views/app/components/admin/users/users-detail/users-detail.component.html',
	controller: UsersDetailController,
	controllerAs: 'vm',
	bindings: {
		isProfile: '<'
	}
}
