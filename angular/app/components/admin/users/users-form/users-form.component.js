class UsersFormController{

	constructor(LanguageNamesService, $localStorage, UserService, TranslatedToastService, $state, DialogService, $translate, $q){

		'ngInject';

		this.LanguageNamesService = LanguageNamesService;
		this.$localStorage = $localStorage;
		this.UserService = UserService;
		this.TranslatedToastService = TranslatedToastService;
		this.$state = $state;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.$q = $q;

		this.languages = this.LanguageNamesService.getLanguages();
		this.locales = this.LanguageNamesService.getLocales();

		this.roles = [
			{
				value: "admin",
				translation: "admin"
			}, {
				value:"user",
				translation: "user"
			}, {
				value:"subuser",
				translation: "subuser"
			}
		];

		this.$q.resolve(
			this.$translate(['USERS.ROLE_NAME.ADMIN', 'USERS.ROLE_NAME.USER', 'USERS.ROLE_NAME.SUBUSER'])
		).then((translations) => {

			this.roles[0].translation = translations['USERS.ROLE_NAME.ADMIN'];
			this.roles[1].translation = translations['USERS.ROLE_NAME.USER'];
			this.roles[2].translation = translations['USERS.ROLE_NAME.SUBUSER'];

		}, (reject) => {
			console.info(reject);
		});

		this.isNewUser = false;

		if(!this.user) {

			this.isNewUser = !this.isNewUser;

			this.user = {
				firstname: '',
				lastname: '',
				email: '',
				role: 'user',
				language: this.$localStorage.tenant.fallback_language,
				touchpoints: [],
				active: true
			}

		}

	}

	isSelf() {

		return this.UserService.isSelf(this.user);

	}

	$onInit(){

	}

	saveUser() {

		if(this.isNewUser) {

			this.UserService.addUser(this.user).then((user) => {

				this.TranslatedToastService.show('TOASTS.USER_CREATED', { firstname: this.user.firstname });
				this.$state.transitionTo('authorized.userDetail', { id: user.id });

			}, (notUser) => {
				this.TranslatedToastService.error('TOASTS.USER_CREATE_ERROR', { firstname: this.user.firstname });
			});

		} else {

			this.UserService.updateUser(this.user).then((user) => {

				this.TranslatedToastService.show('TOASTS.USER_UPDATED', { firstname: this.user.firstname });

				this.user = user;

			}, (notUser) => {
				this.TranslatedToastService.error('TOASTS.USER_UPDATE_ERROR', { firstname: this.user.firstname });
			});

		}

	}

	resetPassword(event) {

		this.UserService.resetPassword(this.user);

	}

	deleteUser(event) {

		this.$q.all([
			this.$translate('USERS.CONFIRM_DELETE_HEADING', { firstname: this.user.firstname }),
			this.$translate('USERS.CONFIRM_DELETE_BODY')
		]).then((i18n) => {

			let confirm = this.DialogService.confirm(i18n[0],i18n[1],null);

			confirm.then(() => {

				this.UserService.deleteUser(this.user).then((resolve) => {

					this.TranslatedToastService.show('TOASTS.USER_DELETED', { firstname: this.user.firstname });
					this.$state.transitionTo('authorized.users');

				}, () => {

					this.TranslatedToastService.show('TOASTS.USER_DELETE_ERROR', { firstname: this.user.firstname });

				});

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

}

export const UsersFormComponent = {
	templateUrl: './views/app/components/admin/users/users-form/users-form.component.html',
	controller: UsersFormController,
	controllerAs: 'vm',
	bindings: {
		user: '<'
	}
}
