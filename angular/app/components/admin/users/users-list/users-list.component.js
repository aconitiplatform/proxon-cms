class UsersListController {

	constructor($scope, $state, API, $localStorage, UserService){

		'ngInject';

		this.$scope = $scope;

		this.$state = $state;

		this.API = API;

		this.propertyName = 'lastname';
		this.reverse = false;

		this.$localStorage = $localStorage;

		this.UserService = UserService;

	}

	$onInit(){

		if(!this.$localStorage.UsersSort){

			this.$localStorage.UsersSort = {
				propertyName: 'lastname',
				reverse: false
			}

		}

	}

	sortBy(propertyName) {

		this.$localStorage.UsersSort.reverse = (this.$localStorage.UsersSort.propertyName === propertyName) ? !this.$localStorage.UsersSort.reverse : true;
		this.$localStorage.UsersSort.propertyName = propertyName;

	}

	loggedInUserIsAdmin() {
		return this.UserService.getloggedInUserIsAdmin();
	}

}

export const UsersListComponent = {
	templateUrl: './views/app/components/admin/users/users-list/users-list.component.html',
	controller: UsersListController,
	controllerAs: 'vm',
	bindings: {}
}
