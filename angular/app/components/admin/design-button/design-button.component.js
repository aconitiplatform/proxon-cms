
class DesignButtonController {

	constructor($rootScope, $state, $localStorage, $stateParams, $mdDialog, TranslatedToastService, $scope, DialogService, $window, $location, PusherService, $q, $translate, DesignService, EventUnsubscribeService) {

		'ngInject';

		this.$rootScope = $rootScope;
		this.$state = $state;
		this.$localStorage = $localStorage;
		this.$stateParams = $stateParams;
		this.$mdDialog = $mdDialog;
		this.TranslatedToastService = TranslatedToastService;
		this.$scope = $scope;
		this.DialogService = DialogService;
		this.$window = $window;
		this.$location = $location;
		this.PusherService = PusherService;
		this.$q = $q;
		this.$translate = $translate;
		this.DesignService = DesignService;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.hasChanges = false;

	}

	$onInit() {

		this.unsubscribe = this.$rootScope.$on('DesignHasChanged', (event) => {

			if (angular.toJson(this.$localStorage.designChanges) != angular.toJson(this.$localStorage.design)) {

				this.hasChanges = true;

			} else {

				this.hasChanges = false;

			}

		});

		this.$window.onbeforeunload = () => {

			if(this.hasChanges){

				return 'Unsaved changes!';

			}

		}

		this.$scope.$on('$locationChangeStart', (event) => {

			if(this.hasChanges){

				var targetPath = this.$location.path();

				event.preventDefault();

				this.$q.resolve(
					this.$translate(['POIS.SAVE_CONFIRMATION.TITLE', 'POIS.SAVE_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
				).then((translations) => {

					this.DialogService.confirm(translations['POIS.SAVE_CONFIRMATION.TITLE'], translations['POIS.SAVE_CONFIRMATION.TEXT'], {
						skipHide: true
					}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

						this.hasChanges = false;

						this.$location.path(targetPath);

					});

				}, (reject) => {
					console.info(reject);
				});

			}

		});

	}

	updateDesign(){

		var formScope = angular.element(document.getElementById('admin_design_form')).scope();

	  	if (!formScope.adminDesignForm.$invalid) {

			this.spinnerIsVisible = true;

			this.DesignService.updateDesign().then((design) => {

				this.TranslatedToastService.show('TOASTS.CHANGES_SAVED');

				this.spinnerIsVisible = false;
				this.hasChanges = false;

			});

		} else {

			this.TranslatedToastService.error('ADMIN.DESIGN.INPUT_ERRORS');

		}

	}

}

export const DesignButtonComponent = {
	templateUrl: './views/app/components/admin/design-button/design-button.component.html',
	controller: DesignButtonController,
	controllerAs: 'vm',
	bindings: {}
}
