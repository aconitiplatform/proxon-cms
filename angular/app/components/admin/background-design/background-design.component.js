
class BackgroundDesignController{

	constructor($timeout, CommonService, GlobalConstants){

		'ngInject';

		this.$timeout = $timeout;
		this.CommonService = CommonService;
		this.GlobalConstants = GlobalConstants;

		this.checkIfIdValid = /(?:[0-9][a-f])+/gi;

		this.navIndex = 0;

	}

	$onInit(){

		this.parseIncoming();
		this.calculate();

		if (this.colorData.active) {
			this.navIndex = 0;
		} else if (this.imageData.active) {
			this.navIndex = 1;
		} else if (this.gradientData.active) {
			this.navIndex = 2;
		}

	}

	parseGradient(s) {

		let checkIfGradient = /gradient/g;
		let checkIfRepeating = /repeating\-/g;

		let parseLinearGradient = /linear\-gradient\s*\((\-?\d{1,3})deg,\s*(\w+\([^\)]+\)|(?:[^\,\s]+))\s*(\d{0,3})%?,\s*(\w+\([^\)]+\)|(?:[^\,\s]+))\s*(\d{0,3})%?\)/gi;
		let parseRadialGradient = /radial\-gradient\s*\(circle at ((?:top|bottom|center|left|right)\s*(?:top|bottom|center|left|right)?),\s*(\w+\([^\)]+\)|(?:[^\,\s]+))\s*(\d{0,3})%?,\s*(\w+\([^\)]+\)|(?:[^\,\s]+))\s*(\d{0,3})%?\)/gi;
		let parseDirection = /(top|bottom|center|left|right)\s*(top|bottom|center|left|right)?/gi;

		let isGradient = checkIfGradient.test(s);

		if (isGradient) {
			if (!this.gradientData) {
				this.gradientData = {stop1:{color:''},stop2:{color:''}};
			}
			this.gradientData.isRepeating = checkIfRepeating.test(s);

			let linearMatch = parseLinearGradient.exec(s);
			let radiantMatch = parseRadialGradient.exec(s);

			if (radiantMatch && radiantMatch.length == 6) {
				this.gradientData.type = 'radial';
				this.gradientData.position = radiantMatch[1];
				this.gradientData.stop1 = {
					color: radiantMatch[2],
					percentage: radiantMatch[3] ? Number.parseInt(radiantMatch[3]) : 0
				};
				this.gradientData.stop2 = {
					color: radiantMatch[4],
					percentage: radiantMatch[5] ? Number.parseInt(radiantMatch[5]) : 0
				};

				let positionMatch = parseDirection.exec(this.gradientData.position);
				if (positionMatch && positionMatch.length == 3) {
					this.gradientData.position1 = positionMatch[1];
					this.gradientData.position2 = positionMatch[2];
				}
			} else if (linearMatch && linearMatch.length == 6) {
				this.gradientData.type = 'linear';
				this.gradientData.degree = linearMatch[1] ? Number.parseInt(linearMatch[1]) : 0;
				this.gradientData.stop1 = {
					color: linearMatch[2],
					percentage: linearMatch[3] ? Number.parseInt(linearMatch[3]) : 0
				};
				this.gradientData.stop2 = {
					color: linearMatch[4],
					percentage: linearMatch[5] ? Number.parseInt(linearMatch[5]) : 0
				};
			} else {
				this.gradientData.invalid = true;
			}
		}

		return isGradient;

	}

	parseImage(s) {

		let checkIfImage = /url\([^\)]+\)/gi;
		let parseUrl = /url\([\"\']([^\)\"\']+)[\"\']\)/gi;
		let parseRepeat = /(repeat|space|round|no\-repeat)\s*((?:repeat|space|round|no\-repeat))?/gi;
		let parsePosition = /(top|bottom|center|left|right)\s*((?:top|bottom|center|left|right))?/gi;
		let parseSize = /(auto auto|contain|cover)/gi;
		let extractId = /\/api\/media\/([0-9a-f\-]{36})\/file/gi;

		let isImage = checkIfImage.test(s);

		if (isImage) {
			let urlMatch = parseUrl.exec(s);
			let repeatMatch = parseRepeat.exec(s);
			let positionMatch = parsePosition.exec(s);
			let sizeMatch = parseSize.exec(s);

			if (!this.imageData) {
				this.imageData = {};
			}

			if (urlMatch && urlMatch.length == 2) {
				this.imageData.url = urlMatch[1];

				let idMatch = extractId.exec(this.imageData.url);
				if (idMatch && idMatch.length == 2) {
					this.imageData.id = idMatch[1];
					this.imageData.invalid = !this.checkIfIdValid.test(this.imageData.id);
				} else {
					this.imageData.invalid = true;
				}
			} else {
				this.imageData.invalid = true;
			}
			if (repeatMatch && repeatMatch.length == 3) {
				this.imageData.repeat1 = repeatMatch[1];
				this.imageData.repeat2 = repeatMatch[2];
			}
			if (positionMatch && positionMatch.length == 3) {
				this.imageData.position1 = positionMatch[1];
				this.imageData.position2 = positionMatch[2];
			}
			if(sizeMatch && sizeMatch.length == 2) {
				this.imageData.size = sizeMatch[1];
			}
		}

		return isImage;

	}

	parseColor(s) {

		let parseColor = /^\s*(#[0-9a-f]{3}?|#[0-9a-f]{6}|rgba?\([^\)]+\)|hsla?\([^\)]+\))\s*$/gi
		let colorMatch = parseColor.exec(s);
		let isColor = colorMatch && colorMatch.length == 2
		if (isColor) {
			if (!this.colorData) {
				this.colorData = {}
			}
			this.colorData.color = colorMatch[1];
		}
		return isColor;

	}

	splitString(s) {

		let arr = s.split('');
		let ret = [];
		let parenthesesCount = 0;
		let lastPartStart = 0;
		let i;
		for (i = 0; i < arr.length; i++) {
			let c = arr[i];
			if (c == '(') {
				parenthesesCount ++;
			} else if (c == ')') {
				parenthesesCount --;
			} else if (c == ',' && parenthesesCount == 0) {
				ret.push(s.substring(lastPartStart, i));
				lastPartStart = i +1;
			}
		}
		if (lastPartStart < i -1) {
			ret.push(s.substring(lastPartStart, s.length));
		}

		return ret;

	}

	parseIncoming() {
		let background = this.ngModel.background;
		if (!background) {
			background = '';
		}

		// https://stackoverflow.com/questions/26195975/regex-to-match-only-commas-but-not-inside-multiple-parentheses
		// doesn't work always :(
		//let matchCommaNotInParenthesis = /,(?![^()]*(?:\([^()]*\))?\))/gm;
		//let parts = background.split(matchCommaNotInParenthesis);

		let parts = this.splitString(background);

		let haveGradient = false;
		let haveImage = false;
		let haveColor = false;
		for (let part of parts) {
			let isGradient = this.parseGradient(part);
			haveGradient = haveGradient || isGradient;
			let isImage = this.parseImage(part);
			haveImage = haveImage || isImage;
			// check color last to avoid parsing color within gradient or so
			if (!isImage && !isGradient) {
				let isColor = this.parseColor(part);
				haveColor = haveColor || isColor;
			}
		}

		if (haveGradient) {
			this.gradientData.active = true;
		} else {
			this.parseGradient('linear-gradient(0deg, white 0%, black 100%)');
			this.gradientData.active = false;
		}

		if (haveImage) {
			this.imageData.active = true;
		} else {
			this.parseImage('url(\'/api/media/------------------------------------/file\') no-repeat no-repeat center center/contain');
			this.imageData.active = false;
		}

		if (haveColor) {
			this.colorData.active = true;
		} else {
			this.parseColor('#ffffff');
			this.colorData.active = false;
		}
	}

	calculateGradient() {
		let ret = '';
		if (this.gradientData.isRepeating) {
			ret += 'repeating-';
		}
		ret += this.gradientData.type;
		ret += '-gradient(';
		if (this.gradientData.type == 'linear') {
			ret += this.gradientData.degree + 'deg, '
		} else if (this.gradientData.type == 'radial') {
			ret += 'circle at ' + this.gradientData.position1;
			if (this.gradientData.position2) {
				ret += ' ' + this.gradientData.position2;
			}
			ret += ', ';
		}
		ret += this.gradientData.stop1.color;
		if (this.gradientData.stop1.percentage) {
			ret += ' ' + this.gradientData.stop1.percentage + '%'
		}
		ret += ', ';
		ret += this.gradientData.stop2.color;
		if (this.gradientData.stop2.percentage) {
			ret += ' ' + this.gradientData.stop2.percentage + '%'
		}
		ret += ')';
		return ret;
	}

	calculateImage() {
		let ret = '';
		if (!this.imageData.id) {
			this.imageData.invalid = true;
		} else {
			this.imageData.invalid = !this.checkIfIdValid.test(this.imageData.id);
		}

		if (this.imageData.invalid) {
			return ret;
		}
		this.imageData.url = this.GlobalConstants.appUrl + this.CommonService.getMediaURL(this.imageData.id);
		let space = false;
		ret += 'url(\'' + this.imageData.url + '\')';
		if (this.imageData.repeat1) {
			ret += ' ' + this.imageData.repeat1;
		}
		if (this.imageData.repeat2) {
			ret += ' ' + this.imageData.repeat2;
		}
		if (this.imageData.position1) {
			ret += ' ' +this.imageData.position1;
		}
		if (this.imageData.position2) {
			ret += ' ' + this.imageData.position2;
		}
		if (this.imageData.position1 || this.imageData.position2) {
			// size is only valid with position
			if (this.imageData.size) {
				// this must be a slash
				ret += '/' + this.imageData.size;
			}
		}
		return ret;
	}

	calculateColor() {
		let ret = '';
		if (this.colorData.color) {
			ret += this.colorData.color;
		}
		return ret;
	}

	calculate() {
		this.gradientStyle = this.calculateGradient();
		this.imageStyle = this.calculateImage();
		this.colorStyle = this.calculateColor();

		let styles = [];
		if (this.gradientStyle && this.gradientData.active && !this.gradientData.invalid) {
			styles.push(this.gradientStyle);
		}
		if (this.imageStyle && this.imageData.active && !this.imageData.invalid) {
			styles.push(this.imageStyle);
			// prepare showing loading indicator
			this.imageData.loading = true;
			let image = new Image();
			image.onload = () => {
				this.$timeout().then(() => {
					this.imageData.loading = false;
				});
			};
			image.onload = image.onload.bind(this);
			image.src = this.imageData.url;
		}
		if (this.colorStyle && this.colorData.active && !this.colorData.invalid) {
			styles.push(this.colorStyle);
		}
		let ret = styles.join(',');

		return ret;
	}

	onChange() {
		let style = this.calculate();

		if (style) {
			this.ngModel.background = style;
		} else {
			delete this.ngModel.background;
		}

		if (this.ngChange) {
			this.$timeout().then(() => {
				this.ngChange();
			});
		}
	}

	onGradientTypeChange() {
		if (this.gradientData.type == 'linear') {
			this.parseGradient('linear-gradient(0deg, white 0%, black 100%)');
		} else {
			this.parseGradient('radial-gradient(circle at center center, white 0%, black 100%)');
		}
	}

}

export const BackgroundDesignComponent = {
	templateUrl: './views/app/components/admin/background-design/background-design.component.html',
	controller: BackgroundDesignController,
	controllerAs: 'vm',
	bindings: {
		ngModel: '=',
		ngChange: '&'
	}
};
