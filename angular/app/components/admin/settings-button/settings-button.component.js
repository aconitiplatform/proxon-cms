class SettingsButtonController {

	constructor($rootScope, $window, $scope, $localStorage, $location, $q, DialogService, $translate, TenantService, TranslatedToastService, EventUnsubscribeService){

		'ngInject';

		this.$rootScope = $rootScope;
		this.$window = $window;
		this.$scope = $scope;
		this.$localStorage = $localStorage;
		this.$location = $location;
		this.$q = $q;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.TenantService = TenantService;
		this.TranslatedToastService = TranslatedToastService;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);
		this.hasChanges = false;

	}

	$onInit(){

		this.unsubscribe = this.$rootScope.$on('SettingsHaveChanged', (event) => {

			if (angular.toJson(this.$localStorage.tenantChanges) != angular.toJson(this.$localStorage.tenant)) {

				this.hasChanges = true;

			} else {

				this.hasChanges = false;

			}

		});

		this.$window.onbeforeunload = () => {

			if(this.hasChanges){

				return 'Unsaved changes!';

			}

		}

		this.$scope.$on('$locationChangeStart', (event) => {

			if(this.hasChanges){

				var targetPath = this.$location.path();

				event.preventDefault();

				this.$q.resolve(
					this.$translate(['POIS.SAVE_CONFIRMATION.TITLE', 'POIS.SAVE_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
				).then((translations) => {

					this.DialogService.confirm(translations['POIS.SAVE_CONFIRMATION.TITLE'], translations['POIS.SAVE_CONFIRMATION.TEXT'], {
						skipHide: true
					}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

						this.hasChanges = false;

						this.$location.path(targetPath);

					});

				}, (reject) => {
					console.info('Strings missing.');
					console.info(reject);
				});

			}

		});

	}

	updateSettings(){

		this.spinnerIsVisible = true;

		this.TenantService.updateTenant(this.$localStorage.tenantChanges).then((response) => {

			this.$localStorage.tenant = angular.copy(response);
			this.$localStorage.tenantChanges = angular.copy(response);

			this.$rootScope.$emit('SettingsHaveChanged');

			this.TranslatedToastService.show('TOASTS.CHANGES_SAVED');

			this.spinnerIsVisible = false;

		}, () => {

			this.spinnerIsVisible = false;

		});

	}

}

export const SettingsButtonComponent = {
	templateUrl: './views/app/components/admin/settings-button/settings-button.component.html',
	controller: SettingsButtonController,
	controllerAs: 'vm',
	bindings: {}
};
