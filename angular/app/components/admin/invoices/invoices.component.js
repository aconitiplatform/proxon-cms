class InvoicesController{

	constructor(InvoicesService, FileSaver) {

		'ngInject';

		this.InvoicesService = InvoicesService;
		this.FileSaver = FileSaver;

	}

	$onInit() {

		this.InvoicesService.getInvoices().then((invoices) => {

			this.invoices = invoices;

			if(this.invoices.length == 0){
				this.noInvoicesYet = true;
			}

		});

	}

	downloadPdf(id, number) {

		this.InvoicesService.getInvoicePdf(id).then((pdf) => {

			var file = new Blob([pdf], {type: 'application/pdf'});

			this.FileSaver.saveAs(file, 'Rechnung_' + number + '.pdf');

		});

	}

}

export const InvoicesComponent = {
	templateUrl: './views/app/components/admin/invoices/invoices.component.html',
	controller: InvoicesController,
	controllerAs: 'vm',
	bindings: {}
};
