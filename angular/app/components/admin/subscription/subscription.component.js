import {UpdateInvoiceInformationController} from '../../../../dialogs/update-invoice-information/update-invoice-information.dialog';

class SubscriptionController{

	constructor(SubscriptionService, $localStorage, StripeService, TranslatedToastService, DialogService, $q, $translate, FileSaver){

		'ngInject';

		this.SubscriptionService = SubscriptionService;
		this.$localStorage = $localStorage;
		this.StripeService = StripeService;
		this.TranslatedToastService = TranslatedToastService;
		this.DialogService = DialogService;
		this.$q = $q;
		this.$translate = $translate;
		this.FileSaver = FileSaver;

		this.subscriptionArray = [];
		this.invoices = [];
		this.currentNavItem = 'plans';
		this.loading = true;
		this.updating = false;
		this.accountCanceled = !!this.$localStorage.tenant.canceled_at;
		this.downloadPending = false;

		this.checkShowBilling();

	}

	$onInit() {

		this.StripeService.setupStripe();

		this.loading = true;

		this.SubscriptionService.getSubscriptionPlans().then((subscriptions) => {

			this.subscriptions = subscriptions;

			this.calculatePlans();

			this.loading = false;

		}, (error) => {
			this.loading = false;
			this.TranslatedToastService.error('SUBSCRIPTION.ERROR_GETTING_PLANS');
		});

		this.creditCardInfoAvailable =  (this.$localStorage.tenant.card_last_four === null) ? false : true;

	}

	checkShowBilling() {

		let plan = this.$localStorage.tenant.stripe_plan;
		this.showBilling = plan && plan != 'startrampe';

	}

	calculatePlans() {

		this.subscriptionArray = [];

		for (let subscriptionName in this.subscriptions) {
			this.subscriptionArray.push(this.subscriptions[subscriptionName]);
		}

		console.log('this.subscriptionArray', this.subscriptionArray);

		this.subscriptionArray.sort((a, b) => {
			if (a.rank == b.rank) {
				return 0;
			} else if (a.rank < b.rank) {
				return -1;
			} else {
				return 1;
			}
		});

		this.currentPlan = this.$localStorage.tenant.stripe_plan;
		if (!this.currentPlan) {
			this.currentPlan = 'startrampe';
		}

		// set the flag ".taken" on plans for UI-list
		let taken = true;
		for (let subscription of this.subscriptionArray) {
			subscription.taken = taken;
			if (subscription.id == this.currentPlan) {
				taken = false;
			}
		}

	}

	loadInvoices() {

		console.log('loading invoices');
		if (!this.invoices || this.invoices.length == 0) {

			this.loading = true;

			this.SubscriptionService.getSubscriptionInvoices().then((invoices) => {
				this.invoices = invoices;
				console.log('invoices are: ', invoices);
				this.loading = false;
			}, (error) => {
				console.log('Failed to get invoices', error);
				this.loading = false;
				this.TranslatedToastService.error('SUBSCRIPTION.ERROR_LOADING_INVOICES');
			});

		}

	}

	subscribe(planId) {

		console.log('subcribe to plan', planId);
		console.log('this subscriptions', this.subscriptions);

		let price = this.subscriptions[planId].price_per_month * 100;
		this.loading = true;

		this.StripeService.openStripeCheckout(price).then((stripeToken) => {
			console.log('stripe checkout successfull', stripeToken);
			this.TranslatedToastService.show('TOASTS.ORDER_SUCCESS');

			this.SubscriptionService.subscribeToPlan(stripeToken, planId).then((response) => {
				console.log('successfully upgraded plan', response);
				this.calculatePlans();

				this.creditCardInfoAvailable = true;
				this.checkShowBilling();

				this.loading = false;
			}, (error) => {
				console.log('Failed to upgrade plan', error);
				this.TranslatedToastService.error('SUBSCRIPTION.ERROR_UPGRADING_PLAN');
				this.loading = false;
			});

		}, (error) => {
			console.log('Failed to stripe', error);
			this.TranslatedToastService.error('TOASTS.ORDER_ERROR');
			this.loading = false;
		});

	}

	cancelSubscription() {

		this.loading = true;
		this.$q.resolve(
			this.$translate(['SUBSCRIPTION.CANCEL_SUBSCRIPTION', 'SUBSCRIPTION.CANCEL_WARNING', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
		).then((translations) => {

			this.DialogService.confirm(translations['SUBSCRIPTION.CANCEL_SUBSCRIPTION'], translations['SUBSCRIPTION.CANCEL_WARNING'], {
				skipHide: true
			}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

				this.SubscriptionService.unsubscribe().then((response) => {
					console.log('successfully unsubscribed from substance', response);
					this.TranslatedToastService.show('SUBSCRIPTION.CANCELING_SUCCESSFULL');
					this.accountCanceled = !!this.$localStorage.tenant.canceled_at;
					this.loading = false;
				}, (error) => {
					console.log('Failed to unsubscribe from plan', error);
					this.TranslatedToastService.error('SUBSCRIPTION.ERROR_CANCELING_SUBSCRIPTION');
					this.loading = false;
				});

			});

		}, (reject) => {
			this.TranslatedToastService.error('SUBSCRIPTION.ERROR_CANCELING_SUBSCRIPTION');
			this.loading = false;
		});

	}

	updateCreditCard() {

		this.updating = true;

		this.StripeService.openStripeCheckout(null, {
			'email': this.$localStorage.user.email,
			'description': 'COMMON.UPDATE_CREDIT_CARD_INFO',
			'panel-label': 'COMMON.UPDATE.BUTTON'
		}).then((stripeToken) => {

			return this.SubscriptionService.updateCard(stripeToken).then((response) => {
				this.TranslatedToastService.show('TOASTS.UPDATE_CARD_SUCCESS');
			}, (error) => {
				this.TranslatedToastService.error('TOASTS.UPDATE_CARD_ERROR');
			});

		}, (error) => {

		}).then(() => {
			this.updating = false;
		});

	}

	downloadInvoicePdf(id) {

		this.downloadPending = id;

		this.SubscriptionService.getSubscriptionInvoicePdf(id).then((pdf) => {

			let file = new Blob([pdf], {type: 'application/pdf'});

			this.FileSaver.saveAs(file, 'Rechnung_' + id + '.pdf');

			this.downloadPending = false;

		}, (error) => {

			this.downloadPending = false;

		});

	}

	updateInvoiceInformation() {

		this.DialogService.fromTemplate('', 'update-invoice-information', {
			controller: UpdateInvoiceInformationController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {}
		}).then(() => {

			console.log('dialog confirmed');

		});

	}

}

export const SubscriptionComponent = {
	templateUrl: './views/app/components/admin/subscription/subscription.component.html',
	controller: SubscriptionController,
	controllerAs: 'vm',
	bindings: {}
}
