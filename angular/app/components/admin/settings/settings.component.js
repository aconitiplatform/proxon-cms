import {MediaUploadController} from '../../../../dialogs/media-upload/media-upload.dialog';

class SettingsController {

	constructor($rootScope, $localStorage, TenantService, DialogService, CommonService, $timeout) {

		'ngInject';

		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.TenantService = TenantService;
		this.DialogService = DialogService;
		this.CommonService = CommonService;
		this.$timeout = $timeout;

		this.tenantLoaded = false;

		this.currentNavItem = 'general';

	}

	$onInit() {

		this.loadSettings();

	}

	settingsChanged() {

		this.$timeout().then(() => {
			this.$rootScope.$emit('SettingsHaveChanged');
		});

	}

	loadSettings() {

		this.TenantService.getTenant().then((tenant) => {

			this.$localStorage.tenant = angular.copy(tenant);
			this.$localStorage.tenantChanges = angular.copy(tenant);

			this.tenantLoaded = true;

			if (this.$localStorage.tenantChanges.logo_settings.type == 'id') {

				this.previewUrl = this.CommonService.getMediaURL(this.$localStorage.tenantChanges.logo_settings.id);

			}

		});

	}

	clear() {

		this.$localStorage.tenantChanges.logo_settings.type = 'none';

		this.previewUrl = undefined;

		// explicitly reset the other fields
		this.$localStorage.tenantChanges.logo_settings.id = undefined;
		this.$localStorage.tenantChanges.logo_settings.url = undefined;

		this.settingsChanged();

	}

}

export const SettingsComponent = {
	templateUrl: './views/app/components/admin/settings/settings.component.html',
	controller: SettingsController,
	controllerAs: 'vm',
	bindings: {}
};