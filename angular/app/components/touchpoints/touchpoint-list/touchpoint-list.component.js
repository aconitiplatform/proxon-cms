class TouchpointListController{

	constructor($scope, $state, API, $localStorage, $q, CommonService, GlobalConstants) {

		'ngInject';

		this.$scope = $scope;
		this.$state = $state;
		this.API = API;
		this.$localStorage = $localStorage;
		this.$q = $q;
		this.CommonService = CommonService;
		this.GlobalConstants = GlobalConstants;

		this.propertyName = 'name';
		this.reverse = false;

	}

	$onInit() {

		if(!this.$localStorage.BeaconListSort){

			this.$localStorage.BeaconListSort = {
				propertyName: 'name',
				reverse: false
			}

		}

	}

	sortBy(propertyName) {

		this.$localStorage.BeaconListSort.reverse = (this.$localStorage.BeaconListSort.propertyName === propertyName) ? !this.$localStorage.BeaconListSort.reverse : true;
		this.$localStorage.BeaconListSort.propertyName = propertyName;

	}

	edit(touchpoint){

		this.$state.transitionTo('authorized.newsletterconfiguration', {id: touchpoint.id});

	}

	getDisplayName(touchpoint) {

		let ret = touchpoint.name;

		if (touchpoint.alias) {
			ret = touchpoint.alias + ' (' + touchpoint.name + ')';
		}

		return ret;

	}

	openModuleContextMenu($mdOpenMenu, ev){

		$mdOpenMenu(ev);

	}

}

export const TouchpointListComponent = {
	templateUrl: './views/app/components/touchpoints/touchpoint-list/touchpoint-list.component.html',
	controller: TouchpointListController,
	controllerAs: 'vm',
	bindings: {}
}
