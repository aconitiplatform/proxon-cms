
class TouchpointButtonController {

	constructor($rootScope, $localStorage, TranslatedToastService, $scope, DialogService, $window, $location, $q, $translate, TouchpointService, EventUnsubscribeService) {

		'ngInject';

		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.TranslatedToastService = TranslatedToastService;
		this.$scope = $scope;
		this.DialogService = DialogService;
		this.$window = $window;
		this.$location = $location;
		this.$q = $q;
		this.$translate = $translate;
		this.TouchpointService = TouchpointService;
		this.EventUnsubscribeService = EventUnsubscribeService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.hasChanges = false;

		if(!this.$localStorage.beacon){

			this.$localStorage.beacon = {};

		}

	}

	$onInit() {

		this.unsubscribe = this.$rootScope.$on('TouchpointHasChanged', (event) => {

			this.TouchpointService.checkForChanges().then((hasChanges) => {

				if(hasChanges){

					this.hasChanges = true;

				}else{

					this.hasChanges = false;

				}

			});

		});

		this.$window.onbeforeunload = () => {

			if(this.hasChanges){

				return 'Unsaved changes!';

			}

		}

		this.$scope.$on('$locationChangeStart', (event) => {

			if(this.hasChanges){

				var targetPath = this.$location.path();

				event.preventDefault();

				this.$q.resolve(
					this.$translate(['POIS.SAVE_CONFIRMATION.TITLE', 'POIS.SAVE_CONFIRMATION.TEXT', 'COMMON.DIALOG_CONFIRM', 'COMMON.DIALOG_REJECT'])
				).then((translations) => {

					this.DialogService.confirm(translations['POIS.SAVE_CONFIRMATION.TITLE'], translations['POIS.SAVE_CONFIRMATION.TEXT'], {
						skipHide: true
					}, translations['COMMON.DIALOG_CONFIRM'], translations['COMMON.DIALOG_REJECT']).then(() => {

						this.hasChanges = false;

						this.$location.path(targetPath);

					});

				}, (reject) => {
					console.info(reject);
				});

			}

		});

	}

	updateTouchpoint(){

		this.spinnerIsVisible = true;

		this.TouchpointService.updateTouchpoint().then((touchpoint) => {

			this.$localStorage.beacon = angular.copy(touchpoint);
			this.$localStorage.beaconChanges = angular.copy(touchpoint);

			this.TranslatedToastService.show('TOASTS.CHANGES_SAVED');

			this.spinnerIsVisible = false;
			this.hasChanges = false;

		});

	}

}

export const TouchpointButtonComponent = {
	templateUrl: './views/app/components/touchpoints/touchpoint-button/touchpoint-button.component.html',
	controller: TouchpointButtonController,
	controllerAs: 'vm',
	bindings: {}
}
