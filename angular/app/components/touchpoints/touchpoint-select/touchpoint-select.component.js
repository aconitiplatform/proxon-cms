class TouchpointSelectController{

	constructor($q, TouchpointService) {

		'ngInject';

		this.$q = $q;
		this.TouchpointService = TouchpointService;

		this.touchpointsUserHas = this.user.beacons || [];

		this.touchpointsUserCanHave = [];

		this.checkingForTouchpoints = true;

		this.resetSearch();

	}

	$onInit() {

		this.getTouchpoints().then((touchpoints) => {
			this.touchpointsUserCanHave = touchpoints;
			this.checkingForTouchpoints = false;
		}, (notTouchpoints) => {
			this.checkingForTouchpoints = false;
		});

	}

	getTouchpoints() {

		return this.$q((resolve,reject) => {

			this.TouchpointService.getTouchpoints().then((touchpoints) => {

				let touchpointsUserCanHave = angular.copy(touchpoints);
				let originalTouchpointsUserCanHaveLength = touchpoints.length;

				for(let touchpointUserHas of this.touchpointsUserHas) {

					for(let touchpoint of touchpointsUserCanHave) {

						if(touchpoint.id == touchpointUserHas.id) {
							touchpointsUserCanHave.splice(touchpointsUserCanHave.indexOf(touchpoint),1);
						}

					}

				}

				if(touchpointsUserCanHave.length + this.touchpointsUserHas.length == originalTouchpointsUserCanHaveLength) {
					resolve(touchpointsUserCanHave);
				}

			}, (notTouchpoints) => {

				reject([]);

			});

		});

	}

	addTouchpointToAccessibleTouchpoints(touchpoint) {
		this.TouchpointService.addTouchpointUser(touchpoint,this.user).then((resolve) => {
			this.touchpointsUserCanHave.splice(this.touchpointsUserCanHave.indexOf(touchpoint),1);
			this.touchpointsUserHas.unshift(touchpoint);
			this.resetSearch();
		});
	}

	removeTouchpointFromAccessibleTouchpoints(touchpoint) {
		this.TouchpointService.deleteTouchpointUser(touchpoint,this.user).then((resolve) => {
			this.touchpointsUserHas.splice(this.touchpointsUserHas.indexOf(touchpoint),1);
			this.touchpointsUserCanHave.unshift(touchpoint);
			this.resetSearch();
		});
	}

	resetSearch() {

		this.touchpointsUserHasSearch = '';
		this.touchpointsUserCanHaveSearch = '';

	}

	getDisplayName(touchpoint) {
		let ret = touchpoint.name;

		if (touchpoint.alias) {
			ret = touchpoint.alias + ' (' + touchpoint.name + ')';
		}

		return ret;
	}

	filterFunction(prop) {
		return (touchpoint) => {

			// we have to use the filter function because we want to filter for name or alias

			if (!prop || prop == '') {
				return true;
			}

			if (touchpoint.name.includes(prop)) {
				return true;
			}

			if (touchpoint.alias && touchpoint.alias.includes(prop)) {
				return true;
			}

			return false;
		}

	}

}

export const TouchpointSelectComponent = {
	templateUrl: './views/app/components/touchpoints/touchpoint-select/touchpoint-select.component.html',
	controller: TouchpointSelectController,
	controllerAs: 'vm',
	bindings: {
		user: '<'
	}
}
