class TouchpointController{

	constructor($state, $stateParams, $scope, $rootScope, $localStorage, TouchpointService, GlobalConstants) {

		'ngInject';

		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.TouchpointService = TouchpointService;
		this.GlobalConstants = GlobalConstants;

	}

	$onInit() {

		this.getTouchpoint();

		this.$scope.$on('markerPosChanged', (event, pos) => {

			this.updateTouchpointLocationData(pos.lat, pos.lng);

		});

	}

	getTouchpoint() {

		this.TouchpointService.getTouchpoint(this.$stateParams.id).then((touchpoint) => {

			this.$state.current.title = touchpoint.name;

			if (!touchpoint.beacon_location) {
				touchpoint.beacon_location = {};
			}

			this.$localStorage.beacon = angular.copy(touchpoint);
			this.$localStorage.beaconChanges = angular.copy(touchpoint);

			this.touchpointLoaded = true;
			//this.initTouchpointMap();

		}, (notUser) => {

			this.$state.transitionTo('public.notfound');

		});

	}

	getTouchpointLocation() {

		return {
			lat: this.$localStorage.beacon.beacon_location.latitude,
			lng: this.$localStorage.beacon.beacon_location.longitude
		}

	}

	updateTouchpointLocationData(lat, lng) {

		this.$localStorage.beaconChanges.beacon_location.latitude = lat;
		this.$localStorage.beaconChanges.beacon_location.longitude = lng;
		this.touchpointChanged();

	}

	touchpointChanged() {

		this.$rootScope.$emit('TouchpointHasChanged');

	}

}

export const TouchpointComponent = {
	templateUrl: './views/app/components/touchpoints/touchpoint/touchpoint.component.html',
	controller: TouchpointController,
	controllerAs: 'vm',
	bindings: {}
}
