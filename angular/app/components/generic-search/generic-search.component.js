class GenericSearchController{
	constructor($localStorage, PaginationService){

		'ngInject';

		this.$localStorage = $localStorage;
		this.PaginationService = PaginationService;

		this.listName = this.type + 'List';

		if (!this.$localStorage[this.listName]) {
			this.$localStorage[this.listName] = {};
		}

		if (!this.$localStorage[this.listName].search) {
			this.$localStorage[this.listName].search = '';
		}

		this.resetSearch();

	}

	$onInit() {

	}

	resetSearch() {

		this.$localStorage[this.listName].search = '';
		this.onChange();

	}

	onChange() {

		let searchString = this.$localStorage[this.listName].search;

		if (searchString != '') {
			this.PaginationService.search(this.type, searchString).then((data) => {
				this.$localStorage[this.listName].use = 'searchData';
			}, (reject) => {
				console.error('Failed to search', reject);
			});
		} else {

			this.$localStorage[this.listName].use = 'data';
			if (!this.$localStorage[this.listName].data || this.$localStorage[this.listName].data.length == 0) {

				this.PaginationService.getList(this.type).then((list) => {
				});

			}

		}

	}

}

export const GenericSearchComponent = {
	templateUrl: './views/app/components/generic-search/generic-search.component.html',
	controller: GenericSearchController,
	controllerAs: 'vm',
	bindings: {
		type: '<'
	}
}
