
class SaveButtonController {

	constructor($rootScope) {

		'ngInject';

		this.$rootScope = $rootScope;

		this.controllersArray = this.controllers.split(',');

		this.changedControllers = {};
		this.loadingControllers = {};

		for (let controller of this.controllersArray) {
			this.changedControllers[controller] = false;
			this.loadingControllers[controller] = false;
		}

		this.changed = false;
		this.loading = false;

	}

	$onInit() {


		let changedListener = (event, changedFlag) => {
			let controllerName = this.extractControllerName(event, 'Changed');
			this.changedControllers[controllerName] = changedFlag;
			let changed = false;
			for (let controller of this.controllersArray) {
				if (this.changedControllers[controller]) {
					changed = true;
					break;
				}
			}
			this.changed = changed;
		}

		let loadingListener = (event, loadingFlag) => {
			let controllerName = this.extractControllerName(event, 'Loading');
			this.loadingControllers[controllerName] = loadingFlag;
			let loading = false;
			for (let controller of this.controllersArray) {
				if (this.loadingControllers[controller]) {
					loading = true;
					break;
				}
			}
			this.loading = loading;
		}

		for (let controller of this.controllersArray) {
			this.unsubscribe = [];
			let changedEventName = controller + 'Changed';
			let loadingEventName = controller + 'Loading';
			this.unsubscribe.push(this.$rootScope.$on(changedEventName, changedListener.bind(this)));
			this.unsubscribe.push(this.$rootScope.$on(loadingEventName, loadingListener.bind(this)));
		}

	};

	$onDestroy() {

		// unregister all the listeners!
		for (let listener of this.unsubscribe) {
			listener();
		}

	}

	extractControllerName(event, eventType) {
		let name = event.name;
		let controllerName = name.substring(0, name.length - eventType.length);
		return controllerName;
	}

	save() {
		for (let controller of this.controllersArray) {
			let saveEventName = controller + 'Save';
			this.$rootScope.$emit(saveEventName);
		}
	}

}

export const SaveButtonComponent = {
	templateUrl: './views/app/components/save-button/save-button.component.html',
	controller: SaveButtonController,
	controllerAs: 'vm',
	bindings: {
		controllers: '@'
	}
}
