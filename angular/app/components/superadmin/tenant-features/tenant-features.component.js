class TenantFeaturesController {

	constructor($state, $stateParams, TenantService, $rootScope, EventUnsubscribeService, TranslatedToastService) {

		'ngInject';

		this.$state = $state;
		this.$stateParams = $stateParams;
		this.TenantService = TenantService;
		this.$rootScope = $rootScope;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.TranslatedToastService = TranslatedToastService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

	}

	$onInit() {

		let saveCallback = (event) => {

			this.save();

		}

		this.unsubscribe = this.$rootScope.$on('TenantFeaturesSave', saveCallback.bind(this));

		this.tenantId = this.$stateParams.id;
		this.loadFeaturesAndModules();
	}

	loadFeaturesAndModules() {
		this.loading = true;
		this.TenantService.getFeaturesAndModulesForTenant(this.tenantId).then((data) => {

			this.modules = data.modules_of_authenticated_tenant;
			this.features = data.features_of_authenticated_tenant;

			this.changes = {
				modules: {},
				features: {}
			};
			this.original = {};
			for (let module of data.modules_of_authenticated_tenant) {
				this.changes.modules[module.slug] = {
					active: false,
					data: module
				}
			}
			for (let feature of data.features_of_authenticated_tenant) {
				this.changes.features[feature.slug] = {
					active: false,
					data: feature
				}
			}

			this.setQueriedTenantsPermissions('modules', data.modules_of_queried_tenant);
			this.setQueriedTenantsPermissions('features', data.features_of_queried_tenant);

			this.loading = false;

		}, (error) => {
			console.log('Failed to load tenant data', error);
			loading = false;
		});
	}

	setQueriedTenantsPermissions(modulesOrFeatures, dataArray) {

		for (let index in this.changes[modulesOrFeatures]) {
			let moduleOrFeature = this.changes[modulesOrFeatures][index];
			moduleOrFeature.active = false;
		}

		for (let moduleOrFeature of dataArray) {
			this.changes[modulesOrFeatures][moduleOrFeature.slug].active = true;
		}
		this.original[modulesOrFeatures] = angular.copy(this.changes[modulesOrFeatures]);

	}

	changed() {
		let hasChanged = this.hasChanged();
		this.$rootScope.$emit('TenantFeaturesChanged', hasChanged);
	}

	haveModulesChanged() {
		let ret = angular.toJson(this.original.modules) != angular.toJson(this.changes.modules);
		return ret;
	}

	haveFeaturesChanged() {
		let ret = angular.toJson(this.original.features) != angular.toJson(this.changes.features);
		return ret;
	}

	hasChanged() {
		return this.haveModulesChanged() || this.haveFeaturesChanged();
	}

	save() {

		if (this.haveModulesChanged()) {

			let slugArray = this.buildSlugArray(this.changes.modules);

			this.TenantService.updateTenantModules(this.tenantId, slugArray, 'TenantFeatures').then((response) => {
				this.setQueriedTenantsPermissions('modules', response.data);
				this.TranslatedToastService.show('TOASTS.TENANT_MODULES_UPDATED');
			}, (error) => {
				console.log('Failed to update modules', error);
				this.TranslatedToastService.error('TOASTS.TENANT_MODULES_UPDATE_ERROR');
			});

		}

		if (this.haveFeaturesChanged()) {

			let slugArray = this.buildSlugArray(this.changes.features);

			this.TenantService.updateTenantFeatures(this.tenantId, slugArray, 'TenantFeatures').then((response) => {
				this.setQueriedTenantsPermissions('features', response.data);
				this.TranslatedToastService.show('TOASTS.TENANT_FEATURES_UPDATED');
			}, (error) => {
				console.log('Failed to update features', error);
				this.TranslatedToastService.error('TOASTS.TENANT_FEATURES_UPDATE_ERROR');
			});

		}

	}

	buildSlugArray(source) {
		let ret = [];

		for (let key in source) {
			let entry = source[key];
			if (entry.active) {
				ret.push(entry.data.slug);
			}
		}

		return ret;
	}

}

export const TenantFeaturesComponent = {
	templateUrl: './views/app/components/superadmin/tenant-features/tenant-features.component.html',
	controller: TenantFeaturesController,
	controllerAs: 'vm',
	bindings: {}
};
