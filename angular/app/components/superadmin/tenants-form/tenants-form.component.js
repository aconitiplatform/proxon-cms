import {KontaktIoSyncController} from '../../../../dialogs/kontakt-io-sync/kontakt-io-sync.dialog';

class TenantsFormController {

	constructor($mdDialog, $state, LanguageNamesService, TenantService, DialogService, UserService, TranslatedToastService, GlobalConstants){

		'ngInject';

		this.$mdDialog = $mdDialog;
		this.$state = $state;
		this.LanguageNamesService = LanguageNamesService;
		this.TenantService = TenantService;
		this.DialogService = DialogService;
		this.UserService = UserService;
		this.TranslatedToastService = TranslatedToastService;
		this.GlobalConstants = GlobalConstants;

	}

	$onInit() {

		this.currentUser = this.UserService.getLoggedInUser();

		this.tenant = {
			locale: this.currentUser.locale,
			fallback_language: this.currentUser.language
		};
		this.tenant.kontakt_io = false;
		this.user;
		this.locales   = this.LanguageNamesService.getLocales();
		this.languages = this.LanguageNamesService.getLanguages();
		this.ownSubstanceUrl = false;
		this.ownEdwardstoneUrl = false;

		switch(this.currentUser.role) {
			case 'superadmin':
				if (this.GlobalConstants.superadminCanSetUrlsForTenants != undefined) {
					this.canSetUrlsForTenants = this.GlobalConstants.superadminCanSetUrlsForTenants;
				} else {
					this.canSetUrlsForTenants = true;
				}
				break;
			case 'reseller':
				if (this.GlobalConstants.resellerCanSetUrlsForTenants != undefined) {
					this.canSetUrlsForTenants = this.GlobalConstants.resellerCanSetUrlsForTenants;
				} else {
					this.canSetUrlsForTenants = false;
				}
				break;
			default:
				this.canSetUrlsForTenants = true;
				break;
		}

	}

	createTenant() {

		if (!this.ownSubstanceUrl) {
			this.tenant.substance_url = null;
		}

		if (!this.ownEdwardstoneUrl) {
			this.tenant.edward_stone_url = null;
		}

		this.TenantService.createTenant({
			tenant: this.tenant,
			user: this.user
		}).then(tenant => {
			this.TranslatedToastService.show('TOASTS.TENANT_CREATED', {name: this.tenant.name});
			if (this.tenant.kontakt_io) {
				this.openSyncDialog(tenant);
			} else {
				this.$state.transitionTo('authorized.tenants');
			}
		}).catch(error => {
			this.TranslatedToastService.error('TOASTS.TENANT_CREATE_ERROR');
		});

	}

	openSyncDialog(tenant) {

		this.DialogService.fromTemplate('', 'kontakt-io-sync', {
			controller: KontaktIoSyncController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			locals: {
				tenant: tenant,
				syncBatches: false
			}
		}).then(() => {
			this.$state.transitionTo('authorized.tenants');
		}, () => {
			this.$state.transitionTo('authorized.tenants');
		});

	}

}

export const TenantsFormComponent = {
	templateUrl: './views/app/components/superadmin/tenants-form/tenants-form.component.html',
	controller: TenantsFormController,
	controllerAs: 'vm',
	bindings: {}
};
