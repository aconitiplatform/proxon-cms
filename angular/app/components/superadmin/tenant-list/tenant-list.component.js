import {KontaktIoSyncController} from '../../../../dialogs/kontakt-io-sync/kontakt-io-sync.dialog';
import {AddUrlsDialogController} from '../../../../dialogs/add-urls/add-urls.dialog';

class TenantListController {

	constructor($localStorage, TenantService, DialogService, UserService) {

		'ngInject';

		this.$localStorage = $localStorage;
		this.TenantService = TenantService;
		this.DialogService = DialogService;
		this.UserService = UserService;

	}

	$onInit(){

		if(!this.$localStorage.TenantsListSort){

			this.$localStorage.TenantsListSort = {
				propertyName: 'name',
				reverse: false
			}

		}

		this.ownTenantId = this.UserService.getLoggedInUser().tenant_id;

	}

	sortBy(propertyName) {

		this.$localStorage.TenantsListSort.reverse = (this.$localStorage.TenantsListSort.propertyName === propertyName) ? !this.$localStorage.TenantsListSort.reverse : true;
		this.$localStorage.TenantsListSort.propertyName = propertyName;

	}

	toggleActivity(tenantId, $event) {

		this.toggleActivityInProgress = true;

		this.TenantService.toggleTenantLock(tenantId).then(tenant => {
			this.toggleActivityInProgress = false;
		});

	}

	openSyncDialog(tenant, bool) {

		this.DialogService.fromTemplate('', 'kontakt-io-sync', {
			controller: KontaktIoSyncController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			locals: {
				tenant: tenant,
				syncBatches: bool
			}
		}).then(() => {

		});

	}

	openAddUrlsDialog(tenant) {
		console.log(tenant);
		this.DialogService.fromTemplate('', 'add-urls', {
			controller: AddUrlsDialogController,
			controllerAs: 'vm',
			clickOutsideToClose: true,
			fullscreen: false,
			locals: {
				tenant: tenant
			}
		}).then(synced => {
			if (!synced) {
				tenant.syncable_batches = true;
				this.openSyncDialog(tenant, true);
			}
		});

	}

	openGroupContextMenu($mdOpenMenu, ev){

		$mdOpenMenu(ev);

	}

}

export const TenantListComponent = {
	templateUrl: './views/app/components/superadmin/tenant-list/tenant-list.component.html',
	controller: TenantListController,
	controllerAs: 'vm',
	bindings: {}
};
