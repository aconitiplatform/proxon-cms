class SuperadminSearchController{

	constructor($localStorage){

		'ngInject';

		this.$localStorage = $localStorage;

	}

	$onInit(){}

}

export const SuperadminSearchComponent = {
	templateUrl: './views/app/components/superadmin/superadmin-search/superadmin-search.component.html',
	controller: SuperadminSearchController,
	controllerAs: 'vm',
	bindings: {}
};
