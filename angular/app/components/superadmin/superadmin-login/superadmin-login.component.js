class SuperadminLoginController {

	constructor(SuperadminService, LogoutService, $localStorage, $auth, bsLoadingOverlayService, $state) {

		'ngInject';

		this.SuperadminService = SuperadminService;
		this.LogoutService = LogoutService;
		this.$localStorage = $localStorage;
		this.$auth = $auth;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.$state = $state;

	}

	$onInit() {

		this.SuperadminService.getAllTenants().then((tenants) => {

			this.tenants = tenants;

		});

	}

	login(user_id) {

		this.SuperadminService.login(user_id);

	}

}

export const SuperadminLoginComponent = {
	templateUrl: './views/app/components/superadmin/superadmin-login/superadmin-login.component.html',
	controller: SuperadminLoginController,
	controllerAs: 'vm',
	bindings: {}
};