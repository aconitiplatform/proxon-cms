class InfiniteScrollingController{

	constructor(PaginationService, $localStorage){

		'ngInject';

		this.PaginationService = PaginationService;
		this.$localStorage = $localStorage;

		this.listName = this.type + 'List';

	}

	$onInit() {

		this.PaginationService.checkLastModified(this.type);

	}

	load() {

		let isData = this.$localStorage[this.listName].use == 'data';
		let isComplete = this.$localStorage[this.listName].complete;
		let isRefreshed = this.$localStorage[this.listName].refreshed;

		if (isData && (!isComplete || !isRefreshed)) {

			this.PaginationService.getList(this.type).then((list) => {
			});

		}

	}

}

export const InfiniteScrollingComponent = {
	templateUrl: './views/app/components/infinite-scrolling/infinite-scrolling.component.html',
	controller: InfiniteScrollingController,
	controllerAs: 'vm',
	bindings: {
		type: '<'
	}
}
