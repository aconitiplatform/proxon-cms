class TenantEditController {

	constructor($timeout, $rootScope, $stateParams,TenantService, EventUnsubscribeService, UserService, TranslatedToastService) {

		'ngInject';

		this.$timeout = $timeout;
		this.$rootScope = $rootScope;
		this.$stateParams = $stateParams;
		this.TenantService = TenantService;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.EventUnsubscribeService.registerUnsubscribe(this);
		this.UserService = UserService;
		this.TranslatedToastService = TranslatedToastService;

	}

	$onInit() {

		this.tenant = {};
		this.original = {};
		this.tenantEditFormValid;
		this.currentNavItem = 'permissions';
		this.isReseller = this.UserService.getLoggedInUser().role == 'reseller' ? true : false;
		this.loading;

		this.tenantTrial = {
			tenant_id: this.$stateParams.id,
			bill_rest_of_month: false
		};

		this.loadTenant();

		let saveCallback = (event) => {
			this.saveTenant();
		}

		this.unsubscribe = this.$rootScope.$on('TenantEditSave', saveCallback.bind(this));

	}

	loadTenant() {

		this.loading = true;

		this.TenantService.getTenantPerId(this.$stateParams.id).then(tenant => {
			this.tenant = tenant;
			this.original.tenant = _.cloneDeep(tenant);
			this.syncBtnDisabled = false;
			this.loading = false;
		}, error => {});

	}

	changed() {

		this.$timeout(() => {
			this.$rootScope.$emit('TenantEditChanged',  this.hasTenantChanged());
		});

	}

	hasTenantChanged() {

		let hasChanges = angular.toJson(this.original.tenant) != angular.toJson(this.tenant);
		return this.tenantEditFormValid ? hasChanges : false;

	}

	saveTenant() {

		if (this.hasTenantChanged()) {

			this.TenantService.updateTenant(this.tenant, 'TenantEdit').then(tenant => {
				this.tenant = tenant;
				this.original.tenant = _.cloneDeep(tenant);
				this.TranslatedToastService.show('TOASTS.TENANT_UPDATED');
			}, error => {
				this.TranslatedToastService.error('TOASTS.TENANT_UPDATE_ERROR');
			});

		}

	}

	endTrial() {

		this.TenantService.endTenantTrial(this.tenantTrial).then(billableObj => {
			this.tenant.trial = false;
			this.TranslatedToastService.show('TOASTS.END_TRIAL_SUCCESS');
		}, error => {
			this.TranslatedToastService.error('TOASTS.END_TRIAL_ERROR');
		});

	}

}

export const TenantEditComponent = {
	templateUrl: './views/app/components/tenant-edit/tenant-edit.component.html',
	controller: TenantEditController,
	controllerAs: 'vm',
	bindings: {}
};
