class MapController {

	constructor($q, $scope, GlobalConstants) {

		'ngInject';

		this.$q = $q;
		this.$scope = $scope;
		this.GlobalConstants = GlobalConstants;

	}

	$onInit() {

		this.mapEl = document.getElementById('map');
		this.mapSearchEl = document.getElementById('map-search');
		this.center;
		this.map;
		this.marker;

		this.loadGMapScript().then(() => {

			this.createMap();
			this.createMarker();
			this.initSearch();

		});

	}

	loadGMapScript() {

		return this.$q((resolve, reject) => {

			if (typeof google == 'object') {
				resolve();
			} else {
				let gmapScript = document.createElement('script');
				gmapScript.type = 'text/javascript';
				gmapScript.async = true;
				gmapScript.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.GlobalConstants.googleApiKey + '&libraries=places';
				gmapScript.onload = () => {
					console.info('gmap script loaded!');
					resolve();
				};
				document.getElementsByTagName('head')[0].appendChild(gmapScript);
			}

		});

	}

	createMap() {

		// touchpoint got coordinates?
		console.log("create Map", this.lat, this.lng);
		if (this.lat && this.lng) {

			this.center = new google.maps.LatLng(this.lat, this.lng);

		} else {

			// default, center rakete7!
			this.center = new google.maps.LatLng(49.90304136665174, 10.870596232925436)

		}

		// create map
		this.map = new google.maps.Map(this.mapEl, {
			zoom: 15,
			center: this.center,
			scrollwheel: false,
			fullscreenControl: false,
			streetViewControl: false
		});

		// this.map.setCenter(this.marker.position);

	}

	createMarker() {

		// create marker
		this.marker = new google.maps.Marker({
			draggable: true,
			position: this.center
		});

		// add marker to map
		this.marker.setMap(this.map);

		// listen to 'dragend' on this marker
		google.maps.event.addListener(this.marker, 'dragend', (e) => {

			this.emitMarkerPos(e.latLng.lat(), e.latLng.lng());

		});

	}

	initSearch() {

		const searchBox = new google.maps.places.SearchBox(this.mapSearchEl);

		searchBox.addListener('places_changed', () => {

			let place = searchBox.getPlaces()[0];

			this.marker.setPosition(place.geometry.location);
			this.map.setCenter(place.geometry.location);

			this.emitMarkerPos(place.geometry.location.lat(), place.geometry.location.lng());

		});

	}

	emitMarkerPos($lat, $lng) {

		console.log("emiting 'markerPosChanged' Event with data:", {
			lat: $lat,
			lng: $lng
		});

		this.$scope.$emit('markerPosChanged', {
			lat: $lat,
			lng: $lng
		});

	}

}

export const MapComponent = {
	templateUrl: './views/app/components/map/map.component.html',
	controller: MapController,
	controllerAs: 'vm',
	bindings: {
		lat: '<',
		lng: '<'
	}
}

// Get address from lat,lng:
// https://developers.google.com/maps/documentation/javascript/examples/geocoding-reverse
