class MediaUploadController {

	constructor($window, $rootScope, Upload, $scope, $auth, CommonService) {

		'ngInject';

		this.$window = $window;
		this.$rootScope = $rootScope;
		this.Upload = Upload;
		this.$scope = $scope;
		this.$auth = $auth;
		this.CommonService = CommonService;

		this.$scope.$watch('files', () => {

			if (this.$scope.files) {

				this.files = angular.copy(this.$scope.files);
				this.upload(this.$scope.files, 0);

			}

		});

	}

	$onInit() {

		switch (this.dialogSelectedType) {

			case 'image':
				this.allowedTypes = "'image/jpg,image/jpeg,image/png,image/gif'";
				break;

			case 'audio':
				this.allowedTypes = "'audio/mp3,audio/wav'";
				break;

			case 'video':
				this.allowedTypes = "'video/mp4,video/webm'";
				break;

			case 'custom':
				this.allowedTypes = this.detectCustomType();
				break;

			default:
				this.allowedTypes = "'image/jpg,image/jpeg,image/png,image/gif,audio/mp3,audio/wav,video/mp4,video/webm'";

		}

	}

	detectCustomType() {

		// the problem is, that browsers expect different behavior for multiple types vs a single type
		// multiple types must be given as 'bla/blu1,bla/blu2' while single types must not have quotation marks
		let ret = '';
		let regex = /[^\/]*\/[^\/]*\/.*/g; // matches 'more two or more slashes'

		if (this.customMimeType) {

			if (this.customMimeType.match(regex)) {

				ret = "'" + this.customMimeType + "'";

			} else {

				ret = this.customMimeType;

			}

		}

		return ret;

	}

	upload(files, i) {

		if (files && files.length) {

			var file = files[i];

			if (!file.$error) {

				this.Upload.upload({
					url: this.CommonService.getMediaUploadURL(),
					data: {
						name: file.name,
						file: file
					},
					headers: {
						Authorization: 'Bearer ' + this.$window.localStorage.satellizer_token,
						Accept: 'application/x.laravel.v1+json'
					}
				}).then((response) => {

					
					this.$rootScope.$emit('uploadCompleted', {
						toast: response.data.data.name,
						file: response.data.data
					});

					setTimeout(() => {

						if (files.length != i + 1) {

							this.upload(files, i + 1);

						} else {

							this.uploadLog = false;

						}

					}, 500);


				}, (response) => {

					this.$rootScope.$emit('uploadFailed', {
						toast: 'Upload failed.'
					});

				}, (evt) => {

					// console.info(evt);
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					this.log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name;

					this.uploadLog = {
						file_number: i + 1,
						total_count: files.length,
						percentage: parseInt(100.0 * evt.loaded / evt.total),
						file_name: evt.config.data.file.name
					}

				});

			}

		}

	}

}

export const MediaUploadComponent = {
	templateUrl: './views/app/components/media/media-upload/media-upload.component.html',
	controller: MediaUploadController,
	controllerAs: 'vm',
	bindings: {
		isDialog: '<',
		dialogSelectedType: '<',
		customMimeType: '<' // only for dialogSelectedType = 'custom'
	}
}
