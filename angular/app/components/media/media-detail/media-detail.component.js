class MediaDetailController{

	constructor(MediaService, $state, $stateParams, DialogService, TranslatedToastService, $sce, $q, $translate){

		'ngInject';

		this.MediaService = MediaService;
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.DialogService = DialogService;
		this.TranslatedToastService = TranslatedToastService;
		this.$sce = $sce;
		this.$q = $q;
		this.$translate = $translate;

		this.$state.current.title = '';

		this.videoPlayer = {};

	}

	$onInit(){

		this.getDetails();

	}

	getDetails(){

		this.MediaService.getMediaDetails(this.$stateParams.id).then((details) => {

			console.info(details);

			this.$state.current.title = details.name;

			this.configureVideoPlayer(details);

			this.details = details;

		});

	}

	delete(){

		this.$q.all([
			this.$translate('MEDIA.CONFIRM_DELETE_HEADING'),
			this.$translate('MEDIA.CONFIRM_DELETE_BODY'),
			this.$translate('MEDIA.DELETE_FAIL_HEADING'),
			this.$translate('MEDIA.DELETE_FAIL_BODY')
		]).then((i18n) => {

			if(this.details.pois === 'undefined' || !this.details.pois || this.details.pois.length == 0){

				this.DialogService.confirm(i18n[0], i18n[1]).then(() => {

					this.MediaService.deleteFile(this.details.id).then((removed) => {

						this.TranslatedToastService.show('TOASTS.MEDIA_DELETED', { medianame: this.details.appname });

						this.$state.transitionTo('authorized.media');

					}, (notRemoved) => {

						this.TranslatedToastService.error('TOASTS.MEDIA_DELETE_ERROR', { medianame: this.details.appname });

					});

				});

			}else{

				this.DialogService.confirm(i18n[2], i18n[3]);

			}

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

	configureVideoPlayer(file) {

		if(file.simple_type == 'video') {
			this.videoPlayer.config = {
				sources: [
					{src: this.$sce.trustAsResourceUrl(file.url), type: file.type}
				],
				tracks: []
			}
		}

	}

}

export const MediaDetailComponent = {
	templateUrl: './views/app/components/media/media-detail/media-detail.component.html',
	controller: MediaDetailController,
	controllerAs: 'vm',
	bindings: {}
}
