class MediaController{

	constructor(MediaService, $localStorage, $state, $rootScope, ToastService, CommonService, $translate, $q, EventUnsubscribeService, PaginationService){

		'ngInject';

		this.MediaService = MediaService;
		this.$localStorage = $localStorage;
		this.$state = $state;
		this.$rootScope = $rootScope;
		this.ToastService = ToastService;
		this.CommonService = CommonService;
		this.$translate = $translate;
		this.$q = $q;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.PaginationService = PaginationService;

		this.selectedFiles = [];


		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.previews = {};

	}

	$onInit(){

		if(this.dialogSelectedType){

			this.setFilter(this.dialogSelectedType);

		}

		this.unsubscribe = [];

		this.unsubscribe.push(this.$rootScope.$on('uploadCompleted', (event, data) => {
			if(this.isDialog !== false && data && data.file){

				this.selectFile(data.file.id);

			}
			console.log(this.$localStorage.mediaList);
			this.$localStorage.mediaList.data.push(
				data.file
			);
			// this.PaginationService.checkLastModified('media');

		}));;

		this.unsubscribe.push(this.$rootScope.$on('uploadFailed', (event, data) => {
			this.ToastService.error(data.toast);
		}));

	}

	selectFile(id){


		if(this.isDialog === false){

			this.$state.transitionTo('authorized.mediaDetail', {id: id});

		} else {

			if(_.indexOf(this.disabledFiles, id) > -1){

				return;

			}

			if(this.allowMultipleSelections){

				if(_.indexOf(this.selectedFiles, id) == -1){

					this.selectedFiles.push(id);

				}else{

					_.pull(this.selectedFiles, id);

				}

				this.$rootScope.$emit('fileSelected', {
					files: this.selectedFiles
				});

			}else{

				this.selectedFiles[0] = id;

				this.$rootScope.$emit('fileSelected', {
					id: id
				});

			}

		}

	}

	isSelected(id){

		if(_.indexOf(this.selectedFiles, id) > -1){

			return true;

		}else{

			return false;

		}

	}

	isDisabled(id){

		let ret = false;

		if(_.indexOf(this.disabledFiles, id) > -1){

			ret = true;

		}

		return ret;

	}

	setFilter(simpleType) {

		//audio, movie, image

		if(simpleType == 'all') {

			this.filter = {};

		} else if (simpleType == 'custom') {

			if (!this.customMimeType) {
				console.error('media.component simpleType "costum" requires custom mime type!');
			}

			this.filter = {
				type: this.customMimeType
			}

		} else {

			this.filter = {
				type: simpleType
			}

		}

	}

}

export const MediaComponent = {
	templateUrl: './views/app/components/media/media/media.component.html',
	controller: MediaController,
	controllerAs: 'vm',
	bindings: {
		isDialog: '<',
		dialogSelectedType: '<',
		customMimeType: '<',
		allowMultipleSelections: '<',
		disabledFiles: '<'
	}
}
