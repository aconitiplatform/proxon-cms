import {MediaUploadController} from '../../../../dialogs/media-upload/media-upload.dialog';

class ImageSelectionController{

	constructor(DialogService, $timeout){

		'ngInject';

		this.DialogService = DialogService;
		this.$timout = $timeout;

	}

	$onInit(){

	}

	openDialog() {

		this.DialogService.fromTemplate('', 'media-upload', {
			controller: MediaUploadController,
			controllerAs: 'vm',
			clickOutsideToClose: false,
			fullscreen: true,
			skipHide: true,
			locals: {
				type: 'image',
				allowMultipleSelections: false,
				disabledFiles: [],
				customMimeType: false,
				isDialog: true
			}
		}).then((id) => {

			this.ngModel = id;

			this.$timout().then(() => {

				if (this.onSelect) {
					this.onSelect();
				}

				this.change();

			});

		});

	}

	change() {

		if (this.ngChange) {
			this.ngChange();
		}

	}

	clear() {

		if (this.onClear) {
			this.onClear();
		}

		change();

	}

}

export const ImageSelectionComponent = {
	templateUrl: './views/app/components/media/image-selection/image-selection.component.html',
	controller: ImageSelectionController,
	controllerAs: 'vm',
	bindings: {
		ngModel: '=',
		ngChange: '&?',
		onSelect: '&?',
		onClear: '&?',
		withPreview: '<?',
		dontResize: '<?'
	}
}
