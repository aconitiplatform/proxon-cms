class ImagePreviewController{

	constructor(CommonService, $timeout){

		'ngInject';

		this.CommonService = CommonService;
		this.$timeout = $timeout;

		this.recalculate();

	}

	$onInit(){

	}

	$onChanges(ev) {

		this.recalculate();

	}

	recalculate() {

		if (!this.imageId) {
			return;
		}

		if (this.oldImageId == this.imageId) {
			return;
		}

		this.oldImageId = this.imageId;

		if (this.dontResize) {
			this.url = this.CommonService.getMediaURL(this.imageId);
		} else {
			this.url = this.CommonService.getCachedMediaPreviewURL(this.imageId);
		}

		this.alreadyResized = false;
		this.imageLoaded = false;

		let img = new Image();
		img.onload = this.loadListener.bind(this, img);
		img.src = this.url;
	}

	loadListener(img) {

		let height = img.height;
		let width = img.width;

		// this timeout is required to ensure that the ng-if really triggers (for whatever reason)
		this.$timeout(() => {
			this.alreadyResized = this.dontResize || (width <= 200 && height <= 200);
			this.imageLoaded = true;
		});

	}

}

export const ImagePreviewComponent = {
	templateUrl: './views/app/components/media/image-preview/image-preview.component.html',
	controller: ImagePreviewController,
	controllerAs: 'vm',
	bindings: {
		imageId: '<',
		dontResize: '<?'
	}
}
