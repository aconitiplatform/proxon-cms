class AppHeaderController{

	constructor($mdSidenav, bsLoadingOverlayService, $state, $auth, LogoutService, $localStorage, $q, $translate,
		DialogService, $window, UserService, $mdDateLocale, EventUnsubscribeService, $rootScope, SuperadminService,
		CartService, $timeout, RegisterService, TranslatedToastService){

		'ngInject';

		this.$mdSidenav = $mdSidenav;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.$state = $state;
		this.$auth = $auth;
		this.LogoutService = LogoutService;
		this.$localStorage = $localStorage;
		this.$q = $q;
		this.$translate = $translate;
		this.DialogService = DialogService;
		this.$window = $window;
		this.UserService = UserService;
		this.$mdDateLocale = $mdDateLocale;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.$rootScope = $rootScope;
		this.SuperadminService = SuperadminService;
		this.CartService = CartService;
		this.$timeout = $timeout;
		this.RegisterService = RegisterService;
		this.TranslatedToastService = TranslatedToastService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

		this.originatorEv;

	}

	$onInit(){

		this.doLocalizations();

		this.unsubscribe = [];

		this.unsubscribe.push(this.$rootScope.$on('SubstanceHasUpdates', () => {

			this.updateAvailable = true;

		}));

		let checkCartSize = (size) => {
			this.cartIsEmpty = (size > 0) ? false : true;
		}

		this.unsubscribe.push(this.$rootScope.$on('cartSize', (event, size) => {
			console.log('cart size: ', size);
			checkCartSize(size);
		}));

	}

	$postLink() {

		let checkCartSize = (size) => {
			this.cartIsEmpty = (size > 0) ? false : true;
		}

		// it doesn't seem to be possible to handle this better :(
		this.$timeout().then(() => {
			this.CartService.emitCartSize(); // update UI
			checkCartSize(this.CartService.getCartSize());
		});

	}

	refresh(){

		this.$window.location.reload();

	}

	toggleSideMenu(){

		this.$mdSidenav('left').toggle();

	}

	toggleAccountMenu($mdOpenMenu, ev) {

		this.originatorEv = ev;
		$mdOpenMenu(ev);

	}

	logout(){

		// logout confirmation
		this.$q.resolve(
			this.$translate(['COMMON.LOGOUT_CONFIRMATION.TITLE', 'COMMON.LOGOUT_CONFIRMATION.TEXT', 'COMMON.LOGOUT_CONFIRMATION.OK', 'COMMON.LOGOUT_CONFIRMATION.CANCEL'])
		).then((translations) => {

			this.DialogService.confirm(translations['COMMON.LOGOUT_CONFIRMATION.TITLE'], translations['COMMON.LOGOUT_CONFIRMATION.TEXT'], {
				skipHide: true
			}, translations['COMMON.LOGOUT_CONFIRMATION.OK'], translations['COMMON.LOGOUT_CONFIRMATION.CANCEL']).then(() => {

				// actual logout code
				this.bsLoadingOverlayService.start();

				this.$auth.logout();

				this.LogoutService.resetAllServices();

				this.$state.transitionTo('public.login');

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

	backToSuperadmin(){

		this.SuperadminService.backToSuperadmin();

	}

	isSuperadmin() {

		if(this.$localStorage.superAdminToken){

			if(this.$localStorage.user.role == 'superadmin'){

				return false;

			}else{

				return true;

			}

		}else{

			return false;

		}

	}

	doLocalizations() {

		let user = this.UserService.getLoggedInUser();

		// date picker localization
		this.$mdDateLocale.firstDayOfWeek = 1;

		if (!user.locale || user.locale.toLowerCase() != 'en_us') {

			this.$mdDateLocale.formatDate = function(date) {
				return date ? moment(date).format('DD.MM.YYYY') : '';
			};

			this.$mdDateLocale.parseDate = function(dateString) {
				var m = moment(dateString, 'DD.MM.YYYY', true);
				return m.isValid() ? m.toDate() : new Date(NaN);
			};

		}

		// moment localization for timepicker
		moment.locale(user.locale);

	}

	confirmEmail() {

		if (!this.$localStorage.user.email_confirmed) {
			this.RegisterService.resendConfirmationEmail(this.$localStorage.user.email).then((response) => {
				this.TranslatedToastService.show('COMMON.CONFIRM.EMAIL_SENT_TO', {email:this.$localStorage.user.email});
			}, (error) => {
				if (error && error.status == 400) {
					// email already synched
					this.TranslatedToastService.show('COMMON.CONFIRM.ALREADY_CONFIRMED');
					this.UserService.getUser(this.$localStorage.user.id).then((user) => {
						this.$localStorage.user = user;
						this.$localStorage.userChanges = angular.copy(user);
					}, (error) => {

					});
				} else {
					this.TranslatedToastService.show('COMMON.CONFIRM.SENDING_FAILED');
				}
			});
		}

	}

}

export const AppHeaderComponent = {
	templateUrl: './views/app/components/app/app-header/app-header.component.html',
	controller: AppHeaderController,
	controllerAs: 'vm',
	bindings: {}
}
