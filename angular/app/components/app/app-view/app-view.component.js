class AppViewController {

	constructor($mdToast, TranslatedToastService, $window, $mdSidenav, bsLoadingOverlayService, $state, $rootScope, $localStorage, $timeout, DialogService, EventUnsubscribeService, deviceDetector, UpdateService) {

		'ngInject';

		this.$mdToast = $mdToast;
		this.TranslatedToastService = TranslatedToastService;
		this.$window = $window;
		this.$mdSidenav = $mdSidenav;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.$state = $state;
		this.$rootScope = $rootScope;
		this.$localStorage = $localStorage;
		this.$timeout = $timeout;
		this.DialogService = DialogService;
		this.EventUnsubscribeService = EventUnsubscribeService;
		this.deviceDetector = deviceDetector;
		this.UpdateService = UpdateService;

		this.EventUnsubscribeService.registerUnsubscribe(this);

	}

	$onInit() {

		this.registerHideOverlayOnLocationChange();

		if(this.$localStorage.productConfig){

			this.hideOverlay();

		}else{

			this.showOverlay();

		}

		this.osClass = this.deviceDetector.os;

		this.checkForUpdate();

	}

	showOverlay(){

		this.bsLoadingOverlayService.start();

	}

	hideOverlay(){

		this.bsLoadingOverlayService.stop();

	}

	registerHideOverlayOnLocationChange(){

		this.unsubscribe = this.$rootScope.$on('$locationChangeSuccess', (event, currentLocation) => {

			this.$timeout(() => {

				this.hideOverlay();

			}, 1500);

		});

	}

	checkForUpdate(){

		this.UpdateService.checkForUpdate().then((updateAvailable) => {

			if(updateAvailable){

				if(this.$localStorage.user){

					this.$rootScope.$emit('SubstanceHasUpdates');

				}else{

					this.$window.location.reload();

				}

			}

		});

		this.$timeout(() => {
			this.checkForUpdate();
		}, 10000);

	}

}

export const AppViewComponent = {
	templateUrl: './views/app/components/app/app-view/app-view.component.html',
	controller: AppViewController,
	controllerAs: 'vm',
	bindings: {}
}
