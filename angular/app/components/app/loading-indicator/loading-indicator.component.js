class LoadingIndicatorController{
	constructor(){
		'ngInject';

		//
	}

	$onInit(){
	}
}

export const LoadingIndicatorComponent = {
	templateUrl: './views/app/components/app/loading-indicator/loading-indicator.component.html',
	controller: LoadingIndicatorController,
	controllerAs: 'vm',
	bindings: {}
}
