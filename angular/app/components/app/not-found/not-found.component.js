class NotFoundController{

	constructor($location, LocationChangeService){

		'ngInject';

		this.url = LocationChangeService.lastLocation;

	}

	$onInit(){
	}

}

export const NotFoundComponent = {
	templateUrl: './views/app/components/app/not-found/not-found.component.html',
	controller: NotFoundController,
	controllerAs: 'vm',
	bindings: {}
}
