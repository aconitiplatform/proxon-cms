class SideMenuController{

	constructor($mdSidenav, $state, $localStorage, UserService, GlobalConstants, TenantService){

		'ngInject';

		this.$mdSidenav = $mdSidenav;
		this.$state = $state;
		this.$localStorage = $localStorage;
		this.UserService = UserService;
		this.GlobalConstants = GlobalConstants;
		this.TenantService = TenantService;

		this.selectedItem = '';

		this.isAdmin = false;

		this.submenu = false;

		this.version = '0.0.0';
		this.loading = true;
		this.features = {};

		if(this.GlobalConstants.appVersion) {
			this.version = this.GlobalConstants.appVersion;
		}

	}

	$onInit(){

		this.isAdmin = this.UserService.getloggedInUserIsAdmin();
		this.checkBillingIsVisible();
		this.checkSubscriptionIsVisible();

		this.TenantService.getAvailableFeatures().then((features) => {
			for (let feature of features) {
				this.features[feature.slug] = true;
			}
			this.loading = false;
		}, (error) => {
			console.log('Failed to get available features for current tenant!', error);
			this.loading = false;
		});

	}

	checkForFeature(slug) {
		return !!this.features[slug];
	}

	toggleSideMenu(){

		this.$mdSidenav('left').toggle();

	}

	toggleAdmin(){

		this.submenu = true;

	}

	selectItem(item){

		this.selectedItem = item;

	}

	openChangelog() {
		this.$state.transitionTo('authorized.changelog');
	}

	isVisible(roles, slug){

		if(_.indexOf(roles, this.UserService.getLoggedInUser().role) < 0){

			return false;

		} else {

			if (slug) {
				return this.checkForFeature(slug);
			} else {
				return true;
			}

		}

	}

	checkBillingIsVisible() {

		this.billingIsVisible = false;

		let user = this.UserService.getLoggedInUser();

		if (user.role == 'reseller') {

			this.TenantService.getTenant().then((tenant) => {
				if (tenant.billable == 'tenant') {
					this.billingIsVisible = true;
				}
			});

		}

	}

	checkSubscriptionIsVisible() {

		this.subscriptionIsVisible = false;

		this.TenantService.getTenant().then((tenant) => {
			if (tenant.billable == 'stripe') {
				this.subscriptionIsVisible = true;
			}
		});

	}

}

export const SideMenuComponent = {
	templateUrl: './views/app/components/app/side-menu/side-menu.component.html',
	controller: SideMenuController,
	controllerAs: 'vm',
	bindings: {}
}
