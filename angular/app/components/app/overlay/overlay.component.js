class OverlayController{
	constructor(){
		'ngInject';

		//
	}

	$onInit(){
	}
}

export const OverlayComponent = {
	templateUrl: './views/app/components/app/overlay/overlay.component.html',
	controller: OverlayController,
	controllerAs: 'vm',
	bindings: {}
}
