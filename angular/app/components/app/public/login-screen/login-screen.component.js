class LoginScreenController {

    constructor($location, ProductConfigService, bsLoadingOverlayService){

		'ngInject';
		
		this.$location = $location;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;

    }

    $onInit(){

		this.getConfig();

		this.backgroundImage = {
			"background-size": "cover",
			"min-height": "100vh",
			"background-position": "center"
		}

	}

	getConfig(){

		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage["background-image"] = "url(" + config.login_screen_image + ")";

			this.productLogo = config.login_screen_product_logo;

			this.bsLoadingOverlayService.stop();

		});

	}
	
}

export const LoginScreenComponent = {
    templateUrl: './views/app/components/app/public/login-screen/login-screen.component.html',
    controller: LoginScreenController,
    controllerAs: 'vm',
    bindings: {}
};
