class RegisterFormController {

	constructor($state, $location, $translate, ProductConfigService, bsLoadingOverlayService,
				LanguageNamesService, GlobalConstants, RegisterService, ToastService, TranslatedToastService) {

		'ngInject';

		this.$state = $state;
		this.$location = $location;
		this.$translate = $translate;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.LanguageNamesService = LanguageNamesService;
		this.GlobalConstants = GlobalConstants;
		this.RegisterService = RegisterService;
		this.ToastService = ToastService;
		this.TranslatedToastService = TranslatedToastService;

	}

	$onInit() {

		this.languages = this.LanguageNamesService.getLanguages();
		this.locales = this.LanguageNamesService.getLocales();
		this.user = {};
		this.tenant = {};
		this.agb = false;
		this.backgroundImage = {
			'background-size': 'cover',
			'min-height': '100vh',
			'background-position': 'center'
		}

		this.getConfig();

	}

	getConfig() {

		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage['background-image'] = 'url(' + config.login_screen_image + ')';
			this.productLogo = config.login_screen_product_logo;
			this.bsLoadingOverlayService.stop();

		});

	}

	registerUser() {

		this.RegisterService.reCaptchaVerifyUserResponse(this.recaptchaResponse).then(() => {

			this.RegisterService.registerUser(this.user, this.tenant).then(responseData => {

				/*
					responseData.tenant
					responseData.user
				 */

				this.TranslatedToastService.show('TOASTS.REGISTRATION_SUCCESS');
				this.$state.transitionTo('public.login');

			}, error => {

				this.TranslatedToastService.error('TOASTS.REGISTRATION_ERROR');

			});

		});

	}

}

export const RegisterFormComponent = {
	templateUrl: './views/app/components/app/public/register-form/register-form.component.html',
	controller: RegisterFormController,
	controllerAs: 'vm',
	bindings: {}
}
