class ResetPasswordController {

	constructor(API, ToastService, $state, $translate, $q, LogoutService, ProductConfigService, bsLoadingOverlayService) {

		'ngInject';

		this.API = API;
		this.$state = $state;
		this.ToastService = ToastService;
		this.$translate = $translate;
		this.$q = $q;
		this.LogoutService = LogoutService;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;

	}

	$onInit(){

		this.backgroundImage = {
			"background-size": "cover",
			"min-height": "100vh",
			"background-position": "center"
		}

		this.getConfig();

		this.password = '';
		this.password_confirmation = '';
		this.isValidToken = false;

		this.verifyToken();

	}

	getConfig(){
		
		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage["background-image"] = "url(" + config.login_screen_image + ")";

			this.productLogo = config.login_screen_product_logo;

			this.bsLoadingOverlayService.stop();

		});

	}

	verifyToken() {
		let email = this.$state.params.email;
		let token = this.$state.params.token;

		this.API.all('auth/password').get('verify', {
			email, token
		}).then(() => {
			this.isValidToken = true;
		}, () => {
			this.$state.go('public.login');
		});
	}

	submit() {
		let data = {
			email: this.$state.params.email,
			token: this.$state.params.token,
			password: this.password,
			password_confirmation: this.password_confirmation
		};

		this.API.all('auth/password/reset').post(data).then(() => {

			this.$q.resolve(
				this.$translate('TOASTS.PASSWORD_CHANGED')
			).then((translation) => {
				this.ToastService.show(translation);
				this.LogoutService.resetAllServices();
				this.$state.go('public.login');
			});

		});
	}
}

export const ResetPasswordComponent = {
	templateUrl: './views/app/components/app/public/reset-password/reset-password.component.html',
	controller: ResetPasswordController,
	controllerAs: 'vm',
	bindings: {
		newUser: '<'
	}
}
