class ConfirmEmailController {

	constructor(API, ToastService, $state, ProductConfigService, bsLoadingOverlayService, RegisterService, $localStorage) {

		'ngInject';

		this.API = API;
		this.$state = $state;
		this.ToastService = ToastService;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.RegisterService = RegisterService;
		this.$localStorage = $localStorage;

	}

	$onInit(){

		this.backgroundImage = {
			'background-size': 'cover',
			'min-height': '100vh',
			'background-position': 'center'
		}

		this.getConfig();

		this.loading = true;

		this.failed = false;
		this.success = false;

		let token = this.$state.params.token;
		this.RegisterService.confirmEmailAddress(token).then((user) => {

			this.$localStorage.user = user;
			this.$localStorage.userChanges = angular.copy(user);

			this.loading = false;
			this.success = true;
		}, (error) => {
            this.loading = false;
			this.failed = true;
		});

	}

	getConfig(){

		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage['background-image'] = 'url(' + config.login_screen_image + ')';

			this.productLogo = config.login_screen_product_logo;

			this.bsLoadingOverlayService.stop();

		});

	}

}

export const ConfirmEmailComponent = {
	templateUrl: './views/app/components/app/public/confirm-email/confirm-email.component.html',
	controller: ConfirmEmailController,
	controllerAs: 'vm',
	bindings: {}
}
