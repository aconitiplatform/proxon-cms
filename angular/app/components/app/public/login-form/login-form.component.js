class LoginFormController {
	constructor($auth, ToastService, bsLoadingOverlayService, $state, $localStorage, deviceDetector, LoginService) {

		'ngInject';

		this.$auth = $auth;
		this.ToastService = ToastService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		this.$state = $state;
		this.$localStorage = $localStorage;
		this.deviceDetector = deviceDetector;
		this.LoginService = LoginService;

		this.isCompatible = true;
		this.wrongPassword = false;
		this.lockedAccount = false;
	}

	$onInit() {

		this.password = '';

		this.LoginService.handleIfAlreadyAuthenticated();

		if (this.deviceDetector.browser == 'ie') {

			this.isCompatible = false;

		}

	}

	login() {

		let user = {
			email: this.email,
			password: this.password
		};

		this.LoginService.login(user).then((response) => {
			this.bsLoadingOverlayService.stop();
			// everythign important is handled in LoginService now
		}, (error) => {
			this.failedLogin(error);
		});

	}

	failedLogin(response) {

		this.bsLoadingOverlayService.stop();

		if (response.status === 401 || response.status === 422) {

			this.wrongPassword = true;
			this.lockedAccount = false;

		} else if (response.status === 429) {

			this.lockedAccount = true;
			this.wrongPassword = false;

		} else {

			this.lockedAccount = false;
			this.wrongPassword = false;

		}

	}

}

export const LoginFormComponent = {
	templateUrl: './views/app/components/app/public/login-form/login-form.component.html',
	controller: LoginFormController,
	controllerAs: 'vm',
	bindings: {}
}
