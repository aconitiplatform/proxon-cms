class ResendUnlockAccountController {

    constructor(API, ToastService, $state, $translate, $q, ProductConfigService, bsLoadingOverlayService) {

        'ngInject';

        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
		this.$translate = $translate;
		this.$q = $q;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;

    }

    $onInit(){

		this.backgroundImage = {
			"background-size": "cover",
			"min-height": "100vh",
			"background-position": "center"
		}

		this.getConfig();

		this.email = this.$state.params.email;

		if (!this.email) {

			this.email = '';

		}

	}
	
	getConfig(){
		
		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage["background-image"] = "url(" + config.login_screen_image + ")";

			this.productLogo = config.login_screen_product_logo;

			this.bsLoadingOverlayService.stop();

		});

	}

    submit() {
			
        this.API.all('auth/resend-unlock-account').post({
            email: this.email
        }).then(() => {

			this.$q.resolve(
				this.$translate('TOASTS.CHECK_EMAIL_INSTRUCTIONS')
			).then((translation) => {
				this.ToastService.show(translation);
            	this.$state.go('public.login');
			});

        });
    }
}

export const ResendUnlockAccountComponent = {
    templateUrl: './views/app/components/app/public/resend-unlock-account/resend-unlock-account.component.html',
    controller: ResendUnlockAccountController,
    controllerAs: 'vm',
    bindings: {}
}
