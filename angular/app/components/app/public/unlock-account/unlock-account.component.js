class UnlockAccountController {

    constructor(API, ToastService, $state, ProductConfigService, bsLoadingOverlayService) {

        'ngInject';

        this.API = API;
        this.$state = $state;
		this.ToastService = ToastService;
		this.ProductConfigService = ProductConfigService;
		this.bsLoadingOverlayService = bsLoadingOverlayService;
		
    }

    $onInit(){

		this.backgroundImage = {
			"background-size": "cover",
			"min-height": "100vh",
			"background-position": "center"
		}

		this.getConfig();

        this.password = '';
        this.password_confirmation = '';
        this.isValidToken = false;
        this.isUnlocked = false;

		this.verifyToken();
		
	}
	
	getConfig(){

		this.ProductConfigService.getConfig().then((config) => {

			this.backgroundImage["background-image"] = "url(" + config.login_screen_image + ")";

			this.productLogo = config.login_screen_product_logo;

			this.bsLoadingOverlayService.stop();

		});

	}

    verifyToken() {
        let email = this.$state.params.email;
        let token = this.$state.params.token;

        this.API.all('auth/password').get('verify', {
            email, token
        }).then(() => {
            this.isValidToken = true;
            this.unlock();
        }, () => {
            this.$state.go('public.login');
        });
    }

    unlock() {
        let data = {
            email: this.$state.params.email,
            token: this.$state.params.token
        };

        this.API.all('auth/unlock-account').post(data).then(() => {
            this.isUnlocked = true;
        });
    }
}

export const UnlockAccountComponent = {
    templateUrl: './views/app/components/app/public/unlock-account/unlock-account.component.html',
    controller: UnlockAccountController,
    controllerAs: 'vm',
    bindings: {}
}
