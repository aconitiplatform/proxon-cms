class SpinnerController{
	constructor(){
		'ngInject';

		//
	}

	$onInit(){
	}
}

export const SpinnerComponent = {
	templateUrl: './views/app/components/app/spinner/spinner.component.html',
	controller: SpinnerController,
	controllerAs: 'vm',
	bindings: {}
}
