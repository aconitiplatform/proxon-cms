class AnalyticsOverviewController{
	constructor(UserService){

		'ngInject';

		this.UserService = UserService;

		let user = this.UserService.getLoggedInUser();
		this.displayKPI = true;

		if (user.role === 'subuser') {
			this.displayKPI = false;
		}

	}

	$onInit(){
	}
}

export const AnalyticsOverviewComponent = {
	templateUrl: './views/app/components/analytics/analytics-overview/analytics-overview.component.html',
	controller: AnalyticsOverviewController,
	controllerAs: 'vm',
	bindings: {}
}
