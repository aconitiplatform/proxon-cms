class UrlAccessController{
	constructor(TouchpointService, $rootScope){

		'ngInject';

		this.TouchpointService = TouchpointService;
		this.$rootScope = $rootScope;

		this.translationValues = {
			name: ''
		}

		this.urlParams = {
			url: ''
		}

		this.touchpoints = [];
		this.selectedTouchpoint = {};

	}

	$onInit(){

		this.TouchpointService.getTouchpoints().then((touchpoints) => {
			this.touchpoints = touchpoints;
		}, (notTouchpoints) => {
			this.touchpoints = [];
		});

	}

	updateChart(touchpoint) {

		this.$rootScope.$broadcast('chart-url-access-shall-change', {
			url: touchpoint.url,
			name: touchpoint.name
		});

	}
}

export const UrlAccessComponent = {
	templateUrl: './views/app/components/analytics/url-access/url-access.component.html',
	controller: UrlAccessController,
	controllerAs: 'vm',
	bindings: {}
}
