class DeliveredContentController{
	constructor(){
		'ngInject';

		//
	}

	$onInit(){
	}
}

export const DeliveredContentComponent = {
	templateUrl: './views/app/components/analytics/delivered-content/delivered-content.component.html',
	controller: DeliveredContentController,
	controllerAs: 'vm',
	bindings: {}
}
