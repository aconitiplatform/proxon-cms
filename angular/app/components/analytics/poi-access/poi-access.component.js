class PoiAccessController{
	constructor(PoiService, $rootScope){

		'ngInject';

		this.PoiService = PoiService;
		this.$rootScope = $rootScope;

		this.translationValues = {
			name: ''
		}

		this.urlParams = {
			url: ''
		}

		this.poisWithEvents = [];
		this.selectedPoiWithEvents = {};

	}

	$onInit(){

		this.PoiService.getPoisWithEvents().then((poisWithEvents) => {
			this.poisWithEvents = poisWithEvents;
		}, (notPoisWithEvents) => {
			this.poisWithEvents = [];
		});

	}

	updateChart(poiWithEvents) {

		this.$rootScope.$broadcast('chart-poi-shall-change', {
			id: poiWithEvents.id,
			name: poiWithEvents.name
		});

	}
}

export const PoiAccessComponent = {
	templateUrl: './views/app/components/analytics/poi-access/poi-access.component.html',
	controller: PoiAccessController,
	controllerAs: 'vm',
	bindings: {}
}
