class NewsletterConfigurationListController{

	constructor($scope, $state, API, $localStorage, CommonService) {

		'ngInject';

		this.$scope = $scope;
		this.$state = $state;
		this.API = API;
		this.$localStorage = $localStorage;
		this.CommonService = CommonService;

		this.propertyName = 'name';
		this.reverse = false;

	}

	$onInit() {

		if(!this.$localStorage.NewsletterConfigurationSort){

			this.$localStorage.NewsletterConfigurationSort = {
				propertyName: 'name',
				reverse: false
			}

		}

	}

	sortBy(propertyName) {

		this.$localStorage.NewsletterConfigurationSort.reverse = (this.$localStorage.NewsletterConfigurationSort.propertyName === propertyName) ? !this.$localStorage.NewsletterConfigurationSort.reverse : true;
		this.$localStorage.NewsletterConfigurationSort.propertyName = propertyName;

	}

	edit(newsletterConfiguration){

		this.$state.transitionTo('authorized.newsletterconfiguration', {id: newsletterConfiguration.id});

	}

}

export const NewsletterConfigurationListComponent = {
	templateUrl: './views/app/components/newsletter-configuration/newsletter-configuration-list/newsletter-configuration-list.component.html',
	controller: NewsletterConfigurationListController,
	controllerAs: 'vm',
	bindings: {}
}
