class NewsletterConfigurationFormController{

	constructor(NewsletterConfigurationService, TranslatedToastService, $state, DialogService, $translate, $q) {

		'ngInject';

		this.NewsletterConfigurationService = NewsletterConfigurationService;
		this.TranslatedToastService = TranslatedToastService;
		this.$state = $state;
		this.DialogService = DialogService;
		this.$translate = $translate;
		this.$q = $q;

		this.types = [
			{
				name: 'MailChimp',
				id: 'mailchimp'
			}
		];

		this.isNewConfig = false;

		if(!this.newsletterConfiguration) {

			this.isNewConfig = !this.isNewConfig;

			this.newsletterConfiguration = {
				name: '',
				type: this.types[0],
				configuration: {
					apikey: '',
					list: ''
				}
			}

		}

	}

	$onInit() {

	}

	saveNewsletterConfiguration() {

		if(this.isNewConfig) {

			this.NewsletterConfigurationService.addNewsletterConfiguration(this.newsletterConfiguration).then((newsletterConfiguration) => {

				this.TranslatedToastService.show('TOASTS.NLC_CREATED', { name: this.newsletterConfiguration.name });
				this.$state.transitionTo('authorized.newsletterconfiguration', { id: newsletterConfiguration.id });

			}, (notNewsletterConfiguration) => {
				this.TranslatedToastService.error('TOASTS.NLC_CREATE_ERROR', { name: this.newsletterConfiguration.name });
			});

		} else {

			this.NewsletterConfigurationService.updateNewsletterConfiguration(this.newsletterConfiguration).then((newsletterConfiguration) => {

				this.TranslatedToastService.show('TOASTS.NLC_UPDATED', { name: this.newsletterConfiguration.name });

				this.newsletterConfiguration = newsletterConfiguration;

			}, (notNewsletterConfiguration) => {
				this.TranslatedToastService.error('TOASTS.NLC_UPDATE_ERROR', { name: this.newsletterConfiguration.name });
			});

		}

	}

	deleteNewsletterConfiguration(event) {

		this.$q.all([
			this.$translate('NEWSLETTER_CONFIGURATIONS.CONFIRM_DELETE_HEADING', { name: this.newsletterConfiguration.name }),
			this.$translate('NEWSLETTER_CONFIGURATIONS.CONFIRM_DELETE_BODY')
		]).then((i18n) => {

			let confirm = this.DialogService.confirm(i18n[0],i18n[1],null);

			confirm.then(() => {

				this.NewsletterConfigurationService.deleteNewsletterConfiguration(this.newsletterConfiguration).then((resolve) => {

					this.TranslatedToastService.show('TOASTS.NLC_DELETED', { name: this.newsletterConfiguration.name });
					this.$state.transitionTo('authorized.newsletter-configurations');

				}, () => {

					this.TranslatedToastService.error('TOASTS.NLC_DELETE_ERROR', { name: this.newsletterConfiguration.name });

				});

			});

		}, (reject) => {
			console.info("Strings missing.");
			console.info(reject);
		});

	}

}

export const NewsletterConfigurationFormComponent = {
	templateUrl: './views/app/components/newsletter-configuration/newsletter-configuration-form/newsletter-configuration-form.component.html',
	controller: NewsletterConfigurationFormController,
	controllerAs: 'vm',
	bindings: {
		newsletterConfiguration: '<'
	}
}
