class NewsletterConfigurationDetailController{

	constructor(NewsletterConfigurationService, $state, $stateParams) {

		'ngInject';

		this.NewsletterConfigurationService = NewsletterConfigurationService;
		this.$state = $state;
		this.$stateParams = $stateParams;

		this.$state.current.title = 'Newsletter Configuration';

		this.newsletterConfiguration = {};

		this.newsletterConfigurationLoaded = false;

	}

	$onInit() {

		this.getNewsletterConfiguration();

	}

	getNewsletterConfiguration() {

		this.NewsletterConfigurationService.getNewsletterConfiguration(this.$stateParams.id).then((newsletterConfiguration) => {

			this.$state.current.title = newsletterConfiguration.name;

			this.newsletterConfiguration = newsletterConfiguration;

			this.newsletterConfigurationLoaded = true;

		}, (notUser) => {

			this.$state.transitionTo('public.notfound');

		});

	}

}

export const NewsletterConfigurationDetailComponent = {
	templateUrl: './views/app/components/newsletter-configuration/newsletter-configuration-detail/newsletter-configuration-detail.component.html',
	controller: NewsletterConfigurationDetailController,
	controllerAs: 'vm',
	bindings: {}
}
