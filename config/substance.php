<?php

return [

    /**
     *
     */
    'owner' => env('OWNER','default'),

    /**
     * The URL on which CMS is available.
     */
    'url' => env('APP_URL'),

    /**
     * The URL on which Edward Stone is available.
     */
    'edward_stone_url' => env('APP_EDWARD_STONE_URL'),

    'edward_stone_qr_url' => env('APP_EDWARD_STONE_QR_URL'),
    'edward_stone_nfc_url' => env('APP_EDWARD_STONE_NFC_URL'),
    'edward_stone_nearby_url' => env('APP_EDWARD_STONE_NEARBY_URL'),

    /**
     * Activate the feature check middleware.
     */
    'feature_check_active' => env('FEATURE_CHECK_ACTIVE',true),

    /**
     * The length of a beacon URL.
     */
    'url_length' => env('APP_URL_LENGTH',8),

    /**
     * The length of the beacon label (name)
     */
    'beacon_name_length' => 4,

    /**
     * The email address CMS uses to send emails from.
     */
    'mail_address' => env('MAIL_ADDRESS'),

    /**
     * The email address CMS uses to contact its administrators.
     */
    'admin_email' => env('ADMIN_EMAIL'),

    /**
     * The email address CMS uses for accounting emails.
     */
    'accounting_email' => env('ACCOUNTING_EMAIL'),

    /**
     * The default time CMS caches charts or KPIs.
     */
    'cache_analytics_defaul_time' => env('CACHE_ANALYTICS_DEFAULT_TIME',60),

    /**
     * The prefix that should be used within the cache. Must be identical to the one configured in Edward Stone.
     */
    'cache_prefix' => env('CACHE_PREFIX'),

    /**
     * The default Stripe account CMS uses for mobile payments.
     */
    'stripe_secret' => env('STRIPE_SECRET',null),

    /**
     * The prefix that should be used within the queue. Must be identical to the one configured in Edward Stone.
     */
    'queue_prefix' => env('QUEUE_PREFIX'),

    /**
     * The URL CMS uses to configure beacons. Usually the Edward Stone URL without the protocol.
     */
    'beacon_url' => env('BEACON_URL'),

    /**
     * The URL prefix CMS uses to configure beacons. 03 means https://
     * For further prefixes refer to the Eddystone specification.
     */
    'beacon_url_prefix' => env('BEACON_URL_PREFIX','03'),

    /**
     * The email and password to login the CMS super admin.
     */
    'super_admin' => [
        'email' => env('SUPER_ADMIN_EMAIL'),
        'password' => env('SUPER_ADMIN_PASSWORD'),
        'can_set_urls_for_tenants' => env('SUPERADMIN_CAN_SET_URLS_FOR_TENANTS',false),
    ],

    'recaptcha' => [
        'public' => env('RECAPTCHA_PUBLIC'),
        'secret' => env('RECAPTCHA_SECRET'),
    ],

    /**
     * External services
     */

    'firebase' => [
        'type' => env('FIREBASE_TYPE'),
        'project_id' => env('FIREBASE_PROJECT_ID'),
        'private_key_id' => env('FIREBASE_PRIVATE_KEY_ID'),
        'private_key' => env('FIREBASE_PRIVATE_KEY'),
        'client_email' => env('FIREBASE_CLIENT_EMAIL'),
        'client_id' => env('FIREBASE_CLIENT_ID'),
        'auth_uri' => env('FIREBASE_AUTH_URI'),
        'token_uri' => env('FIREBASE_TOKEN_URI'),
        'auth_provider_x509_cert_url' => env('FIREBASE_AUTH_PROVIDER_X509_CERT_URL'),
        'client_x509_cert_url' => env('FIREBASE_CLIENT_X509_CERT_URL'),
    ],

    'kontakt_io' => [
        'api_key' => env('KONTAKT_IO_API_KEY',null),
        'api_version' => env('KONTAKT_IO_API_VERSION',9),
    ],

    'easybill' => [
        'api_key' => env('EASYBILL_API_KEY',null),
        'api_limit' => env('EASYBILL_API_LIMIT',10),
    ],

    'billing' => [
        'from' => "",
        'tax' => "",
        'hrb' => "",
    ],

    'email' => [
        'confirmation_period' => 7 //days
    ],

    /**
     * These should be fixed values. Only change if necessary.
     */

    'name' => 'PROXON PLATFORM',
    'sidebar_product_logo' => env('APP_URL').'/img/substance_side_bar_logo.png',
    'login_screen_product_logo' => env('APP_URL').'/img/substance_logo.png',
    'login_screen_company_logo' => env('APP_URL').'/img/substance_logo.png',
    'login_screen_image' => env('APP_URL').'/img/substance-login.jpg',
    'company_name' => 'PROXON',
    'public' => [
        'name' => 'PROXON PLATFORM',
        'sidebar_product_logo' => env('APP_URL').'/img/substance_side_bar_logo.png',
        'login_screen_product_logo' => env('APP_URL').'/img/substance_logo.png',
        'login_screen_company_logo' => env('APP_URL').'/img/substance_logo.png',
        'login_screen_image' => env('APP_URL').'/img/substance-login.jpg',
        'company_name' => 'Proxon',
        'url' => env('APP_URL'),
        'edward_stone_url' => env('APP_EDWARD_STONE_URL'),
    ],
    'storage' => 's3',
    'auth_password_reset' => '/#/reset-password/',
    'auth_password_set' => '/#/set-password/',
    'unlock_account' => '/#/unlock-account/',
    'confirm_email' => '/#/confirm-email/',
    'image_preview' => '200-60_',
    'image_resized' => '1000-80_',
    'endpoint_media' => 'media',
    'page_size' => 50,
    'resize_image_wait' => 7,
    'barcode_api' => 'https://api-bwipjs.metafloor.com/?bcid=%%bc_type%%&text=%%bc_content%%&includetext&scaleX=4&scaleY=4',
    'barcode_api_type' => '%%bc_type%%',
    'barcode_api_text' => '%%bc_content%%',
    'slack_dev_logging_webhook' => '',
    'slack_prod_logging_webhook' => '',
    'slack_dev_logging_channel' => '',
    'slack_prod_logging_channel' => '',
    'tenant_default_request_limit' => env('TENANT_DEFAULT_REQUEST_LIMIT',100000),

    /**
     * Only change these endpoints, if you change them in the routing.
     */

    'payment' => [
        'standard_callback' => '/api/payments/callback',
        'standard_heartbeat' => '/payments/heartbeat'
    ],

    'request' => [
        'forces_objects' => false,
    ],

    'reseller' => [
        'by' => false,
        'can_set_urls_for_tenants' => env('RESELLER_CAN_SET_URLS_FOR_TENANTS',false),
    ],

    'whitelabel' => [
        'active' => env('WHITELABEL_ACTIVE',false),
        'price_per_beacon' => env('WHITELABEL_PRICE_PER_BEACON',900),
        'discount' => env('WHITELABEL_DISCOUNT',0),
        'allow_trials' => env('WHITELABEL_ALLOW_TRIALS',true),
        'bill_rest_of_month' => env('WHITELABEL_BILL_REST_OF_MONTH',true),
        'bill_manually' => env('WHITELABEL_BILL_MANUALLY',false),
    ],

    'prices' => [
        'shipping_cost' => env('SHIPPING_COST'),
        'nfc_tag' => env('NFC_TAG_COST'),
        'beacon' => env('BEACON_COST'),
        'beacon_pro' => env('BEACON_PRO_COST'),
    ],

    /**
     * available Features in CMS
     */

     'features' => [
         [
             'name' => 'POI',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Beacon',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Media',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Newsletter',
             'related_modules' => [
                 // 'newsletter',
             ],
             'default'=>true,
         ],
         [
             'name' => 'Payment',
             'related_modules' => [
                 // 'micro-payment',
             ],
             'default'=>false,
         ],
         [
             'name' => 'Analytics',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Profile',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Admin',
             'related_modules' => [

             ],
             'default'=>true,
         ],
         [
             'name' => 'Survey',
             'related_modules' => [

             ],
             'default'=>false,
         ],
         [
             'name' => 'Help',
             'related_modules' => [

             ],
             'default'=>true,
         ]
     ],

];
