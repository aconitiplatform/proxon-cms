<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "null", "sync", "database", "beanstalkd",
    |            "sqs", "redis"
    |
    */

    'default' => env('QUEUE_DRIVER', 'sync'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table'  => 'jobs',
            'queue'  => 'default',
            'expire' => 60,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host'   => 'localhost',
            'queue'  => 'default',
            'ttr'    => 60,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key'    => 'your-public-key',
            'secret' => 'your-secret-key',
            'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
            'queue'  => 'your-queue-name',
            'region' => 'us-east-1',
        ],

        'substance' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'substance',
            'expire'     => 60,
        ],

        'firebase' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'firebase',
            'expire'     => 60,
        ],

        'subscriptions' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'subscriptions',
            'expire'     => 60,
        ],

        'payments' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'payments',
            'expire'     => 60,
        ],

        'relations' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'relations',
            'expire'     => 60,
        ],

        'contacts' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'contacts',
            'expire'     => 60,
        ],

        'sweepstakes' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'sweepstakes',
            'expire'     => 60,
        ],

        'pois' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'pois',
            'expire'     => 60,
        ],

        'analytics' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'analytics',
            'expire'     => 60,
        ],

        'emails' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'emails',
            'expire'     => 60,
        ],

        'eventlogs' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'eventlogs',
            'expire'     => 60,
        ],

        'invoices' => [
            'driver'     => 'redis',
            'connection' => 'queue',
            'queue'      => env('QUEUE_PREFIX').'invoices',
            'expire'     => 60,
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table'    => 'failed_jobs',
    ],

];
