global['elixir']     = require('laravel-elixir');

require('./tasks/clean.task.js');
require('./tasks/concatScripts.task.js');
require('./tasks/swPrecache.task.js');
require('./tasks/ngHtml2Js.task.js');
require('./tasks/angular.task.js');
require('./tasks/bower.task.js');
require('./tasks/customVersion.task.js');
require('./tasks/removeLogging.task.js');
require('./tasks/constants.task.js');
require('./tasks/markdown.task.js');
require('laravel-elixir-karma');
require('./tasks/awesomeSass.task.js');
require('./tasks/mergeLanguageFiles.task.js');
var git = require('git-rev-promises');
var minimist = require('minimist');
var sleep = require('sleep');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */



elixir((mix) => {

	var knownOptions = {
		string: 'commit',
		default: ''
	};

	var options = minimist(process.argv.slice(2), knownOptions);

	global['commit'] = '';

	if(options.commit && options.commit != '') {
		console.log('commit: ' + options.commit);
		global['commit'] = options.commit;
		global['commit_long'] = options.commit;
	} else {

		git.short().then((str) => {
			console.log('commit: ' + str);
			global['commit'] = str;
		});
		git.long().then((long) => {
			console.log('commit_long: ' + long);
			global['commit_long'] = long;
		});
		sleep.sleep(3)

	}

	var assets = [
			'public/js/final.js',
			'public/css/final.css'
		],
		scripts = [
			'./public/js/vendor.js', './public/js/partials.js', './public/js/app.js'
		],
		styles = [
			'./public/css/vendor.css', './public/css/app.css'
		],
		i18n = [
			'./resources/i18n/**/*.json'
		],
		karmaJsDir = [
			'public/js/vendor.js',
			'node_modules/angular-mocks/angular-mocks.js',
			'node_modules/ng-describe/dist/ng-describe.js',
			'public/js/partials.js',
			'public/js/app.js',
			'tests/angular/**/*.spec.js'
		];

	mix.clean();
	mix.mergeLanguageFiles(i18n);
	mix.bower();
	mix.constants();
	mix.angular('./angular/');
	mix.ngHtml2Js('./angular/**/*.html');

	if(elixir.config.production){
		mix.removeLogging();
	}

	mix.concatScripts(scripts, 'final.js');
	mix.awesomeSass(['./angular/**/*.scss', '!./angular/scss/_*.scss', '!./angular/scss/critical.scss'], 'app.scss');
	mix.sass('./angular/scss/critical.scss', 'public/css/critical.css');
	mix.styles(styles, './public/css/final.css');

	if(elixir.config.production){
		mix.customVersion(assets);
		// mix.swPrecache();
	} else {
		mix.version(assets);
		// mix.swPrecache();
	}

	mix.markdown();

});
