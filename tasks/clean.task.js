var gulp = require('gulp');
var del = require('del');

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('clean', function () {

	new Task('clean', function() {

		return del(['./public/build/']);

	});

});
