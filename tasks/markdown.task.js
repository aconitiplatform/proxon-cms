let gulp = require('gulp');
let del = require('del');
let fs = require('fs');
let clean = require('gulp-clean');

let Elixir = require('laravel-elixir');

let Task = Elixir.Task;

Elixir.extend('markdown', function() {

	new Task('markdown', function() {

		let log = [];

		let getDirs = () => {
			let ret = [];
			let files = fs.readdirSync('./markdown');
			for (let file of files) {
				let path = 'markdown/' + file
				if (fs.lstatSync(path).isDirectory()) {
					ret.push({
						path: path,
						name: file
					});
				}
			}
			return ret;
		}
		let dirs = getDirs();

		del.sync('public/markdown',{ force: true });

		fs.mkdirSync('public/markdown');
		for (let dir of dirs) {
			fs.mkdirSync('public/' + dir.path);
		}


		let ids = {};

		let langs = ['de','en'];

		let addFiles = (markdownFiles, langId, type) => {

			if (!ids[type]) {
				ids[type] = {};
			}
			let markdownIds = ids[type];

			for(let key in markdownFiles){
				let filename = markdownFiles[key].split('.md');
				let id = filename[0];

				if (markdownIds[id]) {
					let markdownId = markdownIds[id];
					markdownId.langs.push(langId);
				} else {
					let markdownId = {
						id: id,
						langs: []
					};
					markdownId.langs.push(langId);
					markdownIds[id] = markdownId;
				}

			}

		}

		for (let langId of langs) {

			for (let dir of dirs) {

				let files = fs.readdirSync('markdown/' + dir.name + '/' + langId);
				addFiles(files, langId, dir.name);

			}

		}

		for (let dir of dirs) {

			let path = 'public/' + dir.path + '/' + dir.name + '.json';
			fs.writeFileSync(path, JSON.stringify(ids[dir.name]));
		}

		global.log = log;

		return gulp.src(['markdown/**/*']).pipe(gulp.dest('public/markdown/'));

	}).watch(['markdown']);

});
