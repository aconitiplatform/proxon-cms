var gulp = require('gulp');
var merge = require('gulp-merge-json');

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('mergeLanguageFiles', function(i18n) {

	new Task('mergeLanguageFiles', function() {

		gulp.src('resources/i18n/en/*.json')
			.pipe(merge({fileName: 'locale-en.json'}))
			.pipe(gulp.dest('public/i18n'));

		gulp.src('resources/i18n/es/*.json')
			.pipe(merge({fileName: 'locale-es.json'}))
			.pipe(gulp.dest('public/i18n'));

		gulp.src('resources/i18n/de/*.json')
			.pipe(merge({fileName: 'locale-de.json'}))
			.pipe(gulp.dest('public/i18n'));

	}).watch(i18n);

});
