var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var debug = require('gulp-debug');
var header = require('gulp-header');
var inject = require('gulp-inject');

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('awesomeSass', function(src, dest, options) {

	new Task('awesomeSass', function() {

		var imports = gulp.src(['./angular/scss/_*.scss']);

		return gulp.src(src)
			.pipe(debug({title: 'SCSS file found:'}))
			.pipe(sourcemaps.init())
			.pipe(concat(dest))
			.pipe(header('/* inject:imports */ \n /* endinject */ \n'))
			.pipe(
				inject(
					gulp.src(
						['./angular/scss/_mixins.scss'], 
						{read: true}
					), 
					{
						starttag: '/* inject:imports */',
						endtag: '/* endinject */',
						transform: function (filepath) {
							console.log("file path",'@import ".' + filepath + '";');
							return '@import ".' + filepath + '";';
						}
					}
				)
			)
			.pipe(sass().on('error', sass.logError))
			.pipe(sourcemaps.write('./'))
			.pipe(gulp.dest('public/css/'));

	}).watch(src);

});