

var Elixir = require('laravel-elixir');

var Task = Elixir.Task;

Elixir.extend('removeLogging', function () {

	new Task('removeLogging', function() {

		var gulp = require('gulp');
		var stripDebug = require('gulp-strip-debug');

		return gulp.src('public/js/*.js')
			.pipe(stripDebug())
			.pipe(gulp.dest('public/js'));

	});

});