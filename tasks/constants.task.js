let gulp = require('gulp');

let Elixir = require('laravel-elixir');

let Task = Elixir.Task;

Elixir.extend('constants', function () {

	new Task('constants', function() {

		let dotenv = require('dotenv');
		let dotenvExpand = require('dotenv-expand');
		let jsonfile = require('jsonfile');
		let replace = require('gulp-string-replace');
		let rename = require('gulp-rename');

		let env = dotenv.config();

		replaceConstantFiles = (sourceFile, newName, destinationDirectoy) => {
			gulp.src([sourceFile])
			.pipe(replace('APP_ENV', env.APP_ENV))
			.pipe(replace('APP_URL', env.APP_URL))
			.pipe(replace('APP_EDWARD_STONE_URL', env.APP_EDWARD_STONE_URL))
			.pipe(replace('APP_VERSION', env.APP_VERSION))
			.pipe(replace('PUSHER_KEY', env.PUSHER_KEY))
			.pipe(replace('SUBSTANCE_GA_TRACKING_ID', env.SUBSTANCE_GA_TRACKING_ID))
			.pipe(replace('GOOGLE_API_KEY', env.GOOGLE_API_KEY))
			.pipe(replace('COMMIT_ID', global['commit']))
			.pipe(replace('COMMIT_LONG', global['commit_long']))
			.pipe(replace('RESELLER_CAN_SET_URLS_FOR_TENANTS', env.RESELLER_CAN_SET_URLS_FOR_TENANTS))
			.pipe(replace('SUPERADMIN_CAN_SET_URLS_FOR_TENANTS', env.SUPERADMIN_CAN_SET_URLS_FOR_TENANTS))
			.pipe(replace('THEME_PRIMARY', env.THEME_PRIMARY))
			.pipe(replace('THEME_ACCENT', env.THEME_ACCENT))
			.pipe(replace('THEME_WARN', env.THEME_WARN))
			.pipe(replace('THEME_BACKGROUND', env.THEME_BACKGROUND))
			.pipe(replace('STRIPE_PUBLIC', env.STRIPE_PUBLIC))
			.pipe(replace('RECAPTCHA_PUBLIC', env.RECAPTCHA_PUBLIC))
			.pipe(replace('SHIPPING_COST', env.SHIPPING_COST))
			.pipe(replace('NFC_TAG_COST', env.NFC_TAG_COST))
			.pipe(replace('BEACON_COST', env.BEACON_COST))
			.pipe(replace('BEACON_PRO_COST', env.BEACON_PRO_COST))
			.pipe(replace('APP_EDWARD_STONE_QR_URL', env.APP_EDWARD_STONE_QR_URL))
			.pipe(replace('APP_EDWARD_STONE_NFC_URL', env.APP_EDWARD_STONE_NFC_URL))
			.pipe(replace('APP_EDWARD_STONE_NEARBY_URL', env.APP_EDWARD_STONE_NEARBY_URL))
			.pipe(rename(newName))
			.pipe(gulp.dest(destinationDirectoy));
		}

		replaceConstantFiles('./angular/index.constants.TEMPLATE.js', 'index.constants.js', './angular/');
		replaceConstantFiles('./angular/scss/_themes.scss.template', '_themes.scss', './angular/scss/');

	});

});
